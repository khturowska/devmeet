Hello {email},

Your Devmeet account has been created, please click on the URL below to activate it:
{link}

Regards,
Devmeet Team.