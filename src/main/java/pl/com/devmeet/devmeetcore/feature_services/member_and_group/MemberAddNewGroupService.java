package pl.com.devmeet.devmeetcore.feature_services.member_and_group;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.com.devmeet.devmeetcore.feature_services.member_and_group.crud.RelationsMemberGroupCrudService;
import pl.com.devmeet.devmeetcore.feature_services.member_and_group.exceptions.MemberAddNewGroupServiceException;
import pl.com.devmeet.devmeetcore.feature_services.member_and_group.exceptions.RelationsMemberGroupCrudServiceException;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class MemberAddNewGroupService {

    @Getter
    private RelationsMemberGroupCrudService relationsMemberGroupCrudService;

    @Autowired
    public MemberAddNewGroupService(RelationsMemberGroupCrudService relationsMemberGroupCrudService) {
        this.relationsMemberGroupCrudService = relationsMemberGroupCrudService;
    }

    public GroupEntity memberCreateNewGroup(Long memberId, GroupEntity newGroup) throws MemberNotFoundException, GroupAlreadyExistsException, RelationsMemberGroupCrudServiceException, MemberAddNewGroupServiceException {
        MemberEntity founder = relationsMemberGroupCrudService.findMember(memberId);

        checkIsGroupHasNotEmptyName(newGroup);
        checkMembersLimit(founder, newGroup);

        List<MemberEntity> membersOfGroup = new ArrayList<>();
        membersOfGroup.add(founder);

        GroupEntity createdGroup = relationsMemberGroupCrudService
                .createNewGroup(setDefaultLimitsIfNull(
                        addGroupsFounder(newGroup, founder),
                        membersOfGroup));

        log.info("The Member (id=" + memberId + ") has create new Group (id=" + newGroup.getId() + ") and has been added to it");
        return createdGroup;
    }

    private GroupEntity addGroupsFounder(GroupEntity groupEntity, MemberEntity founder) {
        groupEntity.setFounder(founder);
        return groupEntity;
    }

    private void checkMembersLimit(MemberEntity memberEntity, GroupEntity groupEntity) throws RelationsMemberGroupCrudServiceException {
        Integer membersLimit = groupEntity.getMembersLimit();
        if (membersLimit != null && membersLimit == 0)
            throw new RelationsMemberGroupCrudServiceException("The Member (id=" + memberEntity.getId() + ") tries to add a Group (id=" + groupEntity.getId() + ") but members limit must be greater than 0");
    }

    private GroupEntity setDefaultLimitsIfNull(GroupEntity groupEntity, List<MemberEntity> members) {
        groupEntity.setMembersLimit((groupEntity.getMembersLimit() != null && groupEntity.getMembersLimit() > 0) ? groupEntity.getMembersLimit() : 10);
        groupEntity.setMembers(members);
        return groupEntity;
    }

    //// TODO: 04.11.2020 member nie powinien miec mozliwosci utworzenia grupy bez nazwy. Nazwa grupy nie musi byc unikalna, jezeli member nie zalozyl dwoch takich samych Testy!
    private void checkIsGroupHasNotEmptyName(GroupEntity group) throws MemberAddNewGroupServiceException {
        String groupName = group.getGroupName();
        if (groupName.isEmpty() || groupName == null)
            throw new MemberAddNewGroupServiceException("Group name cannot be empty");
    }
}
