package pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.exceptions;

public class MemberAddNewMeetingInGroupServiceException extends Exception {
    public MemberAddNewMeetingInGroupServiceException(String message) {
        super(message);
    }
}
