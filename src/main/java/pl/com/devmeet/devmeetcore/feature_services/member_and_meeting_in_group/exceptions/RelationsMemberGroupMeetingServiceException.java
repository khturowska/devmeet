package pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.exceptions;

public class RelationsMemberGroupMeetingServiceException extends Exception {

    public RelationsMemberGroupMeetingServiceException(String message) {
        super(message);
    }
}
