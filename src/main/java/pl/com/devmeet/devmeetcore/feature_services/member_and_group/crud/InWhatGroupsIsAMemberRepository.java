package pl.com.devmeet.devmeetcore.feature_services.member_and_group.crud;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;

import java.util.List;

@Repository
interface InWhatGroupsIsAMemberRepository extends JpaRepository<GroupEntity, Long> {

    List<GroupEntity> findAllByMembers(MemberEntity member);
    List<GroupEntity> findAllByFounder(MemberEntity founder);
}
