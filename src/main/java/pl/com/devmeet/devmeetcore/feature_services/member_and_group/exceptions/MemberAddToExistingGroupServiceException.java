package pl.com.devmeet.devmeetcore.feature_services.member_and_group.exceptions;

public class MemberAddToExistingGroupServiceException extends Exception {
    public MemberAddToExistingGroupServiceException(String message) {
        super(message);
    }
}
