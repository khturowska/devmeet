package pl.com.devmeet.devmeetcore.feature_services.user_and_member.login.exceptions;

public class WrongUserPasswordLoginException extends Exception{
    public WrongUserPasswordLoginException(String message) {
        super(message);
    }
}
