package pl.com.devmeet.devmeetcore.feature_services.member_and_group.exceptions;

public class MemberAddNewGroupServiceException extends Exception {
    public MemberAddNewGroupServiceException(String message) {
        super(message);
    }
}
