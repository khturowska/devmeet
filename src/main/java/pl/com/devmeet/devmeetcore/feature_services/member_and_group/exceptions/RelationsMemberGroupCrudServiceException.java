package pl.com.devmeet.devmeetcore.feature_services.member_and_group.exceptions;

public class RelationsMemberGroupCrudServiceException extends Exception{
    public RelationsMemberGroupCrudServiceException(String message) {
        super(message);
    }
}
