package pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.com.devmeet.devmeetcore.feature_services.member_and_group.crud.RelationsMemberGroupCrudService;
import pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.crud.MeetingInfoEmailSenderService;
import pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.crud.RelationsMemberGroupMeetingCrudService;
import pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.exceptions.MemberAddToExistingMeetingInGroupServiceException;
import pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.exceptions.RelationsMemberGroupMeetingServiceException;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;

import java.util.List;

@Slf4j
@Component
public class MemberAddToExistingMeetingInGroupService {

    private RelationsMemberGroupMeetingCrudService relationsMemberGroupMeetingCrudService;
    private RelationsMemberGroupCrudService relationsMemberGroupCrudService;
    private MeetingInfoEmailSenderService meetingInfoEmailSenderService;

    public MemberAddToExistingMeetingInGroupService(RelationsMemberGroupMeetingCrudService relationsMemberGroupMeetingCrudService,
                                                    RelationsMemberGroupCrudService relationsMemberGroupCrudService,
                                                    MeetingInfoEmailSenderService meetingInfoEmailSenderService) {

        this.relationsMemberGroupMeetingCrudService = relationsMemberGroupMeetingCrudService;
        this.relationsMemberGroupCrudService = relationsMemberGroupCrudService;
        this.meetingInfoEmailSenderService = meetingInfoEmailSenderService;
    }

    public MeetingEntity addMemberToExistingMeetingInGroup(Long meetingId, Long memberId) throws MemberNotFoundException, RelationsMemberGroupMeetingServiceException, MemberAddToExistingMeetingInGroupServiceException, MeetingNotFoundException {
        String notification;
        MemberEntity member = getMemberEntity(memberId);
        MeetingEntity meeting;
        List<MeetingEntity> memberMeetings = relationsMemberGroupMeetingCrudService.findAllMeetingWhereIsMember(member);

        if (checkIsMemberIsAlreadyMeetingMember(memberMeetings, meetingId)) {
            notification = String.format("Member (memberId=%d) can't be add to Meeting (meetingId=%d), because is already a member of the meeting", memberId, meetingId);
            log.warn(notification);
            throw new MemberAddToExistingMeetingInGroupServiceException(notification);

        } else {
            meeting = getMeetingEntity(meetingId);
            checkIsMemberIsMemberOfGroup(meeting, member);
            meeting = addMemberToMeetingMembers(memberMeetings, member, meeting);

            log.info(String.format("Member (memberId=%d) has been added to Meeting (meetingId=%d) in to Group (groupId=%d)",
                    memberId,
                    meeting.getId(),
                    meeting.getGroup().getId()));

            sendEmailToMemberAsAnotherMeetingMember(member, meeting);
            return meeting;
        }
    }

    private void sendEmailToMemberAsAnotherMeetingMember(MemberEntity member, MeetingEntity meeting){
        meetingInfoEmailSenderService.sendMailToMeetingMember(member, meeting);
    }

    private MemberEntity getMemberEntity(Long memberId) throws MemberNotFoundException {
        return relationsMemberGroupCrudService.findMember(memberId);
    }

    private MeetingEntity getMeetingEntity(Long meetingId) throws MeetingNotFoundException {
        return relationsMemberGroupMeetingCrudService
                .getMeetingCrudService()
                .findById(meetingId);
    }

    private boolean checkIsMemberIsAlreadyMeetingMember(List<MeetingEntity> meetings, Long meetingId) {
        return meetings.stream()
                .map(MeetingEntity::getId)
                .anyMatch(id -> id.equals(meetingId));
    }

    private void checkIsMemberIsMemberOfGroup(MeetingEntity meeting, MemberEntity member) throws MemberAddToExistingMeetingInGroupServiceException {
        String notification;
        Long groupId = meeting.getGroup().getId();
        Long memberId = member.getId();
        List<GroupEntity> memberGroups = relationsMemberGroupCrudService.findInWhatGroupsIsAMember(member);
        List<MemberEntity> groupMembers = relationsMemberGroupCrudService.findMembersInGroup(meeting.getGroup());

        boolean hasGroup = memberGroups.stream()
                .map(GroupEntity::getId)
                .anyMatch(id -> id.equals(groupId));

        boolean isGroupMember = groupMembers.stream()
                .map(MemberEntity::getId)
                .anyMatch(id -> id.equals(memberId));

        if (!hasGroup || !isGroupMember) {
            notification = String.format("Member (memberId=%d) can't be add to Meeting (meetingId=%d), because is not a Group (groupId=%d) member", member.getId(), meeting.getId(), groupId);
            log.error(notification);
            throw new MemberAddToExistingMeetingInGroupServiceException(notification);
        }
    }

    private MeetingEntity addMemberToMeetingMembers(List<MeetingEntity> memberMeetings, MemberEntity member, MeetingEntity meeting) throws RelationsMemberGroupMeetingServiceException {
        memberMeetings.add(meeting);
        member.setMeetings(memberMeetings);
        relationsMemberGroupMeetingCrudService.updateMemberMeetings(member);

        List<MemberEntity> meetingMembers = relationsMemberGroupMeetingCrudService.findAllMeetingMembers(meeting);
        meetingMembers.add(member);
        meeting.setMembers(meetingMembers);
        meeting = relationsMemberGroupMeetingCrudService.updateMembersInMeeting(meeting);
        log.info(String.format("Member (memberId=%d) has been successfully added to the Meeting (meetingId=%d in Group (groupId=%d)", member.getId(), meeting.getId(), meeting.getGroup().getId()));
        return meeting;
    }
}
