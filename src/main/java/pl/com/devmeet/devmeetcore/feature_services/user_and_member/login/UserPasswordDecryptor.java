package pl.com.devmeet.devmeetcore.feature_services.user_and_member.login;

import org.springframework.security.crypto.bcrypt.BCrypt;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;

class UserPasswordDecryptor {

    public boolean decryptUserPasswordAndMathWithInput(UserEntity user, String plainTextPassword) {
        try {
            return BCrypt.checkpw(plainTextPassword, getHashedPasswordFromUser(user));
        } catch (IllegalArgumentException e){
            return false;
        }
    }

    private String getHashedPasswordFromUser(UserEntity user) {
        return user.getPassword();
    }
}
