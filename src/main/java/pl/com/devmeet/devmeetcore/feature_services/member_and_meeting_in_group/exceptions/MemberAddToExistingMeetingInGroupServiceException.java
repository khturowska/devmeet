package pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.exceptions;

public class MemberAddToExistingMeetingInGroupServiceException extends Exception {
    public MemberAddToExistingMeetingInGroupServiceException(String message) {
        super(message);
    }
}
