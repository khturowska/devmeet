package pl.com.devmeet.devmeetcore.feature_services.user_and_member.user_registration;

import org.springframework.security.crypto.bcrypt.BCrypt;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;

public class UserPasswordEncryptor {

    private int saltSize = 11;

    public UserEntity encryptUserPassword(UserEntity user) {
        user.setPassword(encryptPassword(user.getPassword()));
        return user;
    }

    public String encryptPassword(String password) {
        String salt = generateRandomSalt();
        return generateHashedPassword(password, salt);
    }

    private String generateRandomSalt() {
        return BCrypt.gensalt(saltSize);
    }

    private String generateHashedPassword(String userPassword, String salt){
        return BCrypt.hashpw(userPassword, salt);
    }
}
