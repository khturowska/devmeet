package pl.com.devmeet.devmeetcore.feature_services.user_and_member.user_activation_after_registration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.com.devmeet.devmeetcore.feature_services.user_and_member.user_activation_after_registration.exceptions.InvalidUUIDStringException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberUserNotActiveException;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserCrudService;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyActiveException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.util.UUID;

@Component
public class UserActivationService {

    private UserCrudService userService;
    private MemberCrudService memberService;

    @Autowired
    public UserActivationService(UserCrudService userService,
                                 MemberCrudService memberService) {
        this.userService = userService;
        this.memberService = memberService;
    }

    public void activateUser(String userEmail, String receivedUserKey) throws UserNotFoundException, UserAlreadyActiveException, MemberAlreadyExistsException, MemberUserNotActiveException {
        UserEntity user = userService.findByEmail(userEmail);
        if (!user.isActive()) {
            if (checkReceivedUuidKeyWithKeyInUser(receivedUserKey, user)) {
                activateUserInDb(user);
                createMember(user);
            } else
                throw new InvalidUUIDStringException("Wrong key!");
        } else
            throw new UserAlreadyActiveException(userEmail + " has already been activated.");
    }

    private boolean checkReceivedUuidKeyWithKeyInUser(String receivedKey, UserEntity user) {
        try {
            UUID uuid = UUID.fromString(receivedKey);
            return user.getActivationKey().equals(uuid);
        } catch (IllegalArgumentException ex) {
            throw new InvalidUUIDStringException(ex.getMessage());
        } catch (NullPointerException ex) {
            throw new InvalidUUIDStringException("Key data for user: " +
                    user.getEmail() + " is not available. Please contact database administrator!");
        }
    }

    private void activateUserInDb(UserEntity user) throws UserNotFoundException, UserAlreadyActiveException {
        userService.activate(user);
    }

    private void createMember(UserEntity userEntity) throws MemberAlreadyExistsException, UserNotFoundException, MemberUserNotActiveException {
        new UserActivationMemberCreator(memberService)
                .createMemberWithDefaultUserNick(userEntity);
    }
}