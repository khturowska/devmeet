package pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.crud;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;

import java.util.List;

@Repository
interface MeetingMembersRepository extends JpaRepository<MemberEntity, Long> {

    List<MemberEntity> findAllByMeetings(MeetingEntity meeting);
}
