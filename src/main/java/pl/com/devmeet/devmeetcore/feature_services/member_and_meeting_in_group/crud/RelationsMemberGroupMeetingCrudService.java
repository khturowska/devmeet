package pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.crud;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.com.devmeet.devmeetcore.feature_services.member_and_group.crud.RelationsMemberGroupCrudService;
import pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.exceptions.RelationsMemberGroupMeetingServiceException;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.crud.MeetingCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceNotFoundException;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class RelationsMemberGroupMeetingCrudService {

    private RelationsMemberGroupCrudService relationsMemberGroupCrudService;
    private MeetingCrudService meetingCrudService;

    private MeetingMembersRepository meetingMembersRepository;
    private GroupMeetingsRepository groupMeetingsRepository;
    private MemberMeetingsRepository memberMeetingsRepository;

    @Autowired
    public RelationsMemberGroupMeetingCrudService(RelationsMemberGroupCrudService relationsMemberGroupCrudService,
                                                  MeetingCrudService meetingCrudService,
                                                  MeetingMembersRepository meetingMembersRepository,
                                                  GroupMeetingsRepository groupMeetingsRepository,
                                                  MemberMeetingsRepository memberMeetingsRepository) {
        this.relationsMemberGroupCrudService = relationsMemberGroupCrudService;
        this.meetingCrudService = meetingCrudService;
        this.meetingMembersRepository = meetingMembersRepository;
        this.groupMeetingsRepository = groupMeetingsRepository;
        this.memberMeetingsRepository = memberMeetingsRepository;
    }

    public MeetingCrudService getMeetingCrudService(){
        return meetingCrudService;
    }

    public MeetingEntity createNewMeeting(MeetingEntity meetingEntity) throws MeetingAlreadyExistsException, GroupNotFoundException, PlaceNotFoundException {
        return meetingCrudService.add(meetingEntity);
    }

    public MeetingEntity updateMembersInMeeting(MeetingEntity meetingEntity) {
        meetingEntity.setModificationTime(LocalDateTime.now());
        return memberMeetingsRepository.save(meetingEntity);
    }

    public MemberEntity updateMemberMeetings(MemberEntity memberEntity){
        memberEntity.setModificationTime(LocalDateTime.now());
        return meetingMembersRepository.save(memberEntity);
    }

    public List<MeetingEntity> findAllMeetingsWhereMemberIsAPromoter(MemberEntity memberEntity) throws RelationsMemberGroupMeetingServiceException {
        if (memberEntity.getId() != null)
            return memberMeetingsRepository.findAllByPromoter(memberEntity);
        else
            throw new RelationsMemberGroupMeetingServiceException(("Member id=" + memberEntity.getId().toString()));
    }

    public List<MeetingEntity> findAllMeetingWhereIsMember(MemberEntity memberEntity) throws RelationsMemberGroupMeetingServiceException {
        if (memberEntity.getId() != null)
            return memberMeetingsRepository.findAllByMembers(memberEntity);
        else
            throw new RelationsMemberGroupMeetingServiceException(("Member id=" + memberEntity.getId().toString()));
    }

    public List<MeetingEntity> findAllMeetingsInGroup(GroupEntity groupEntity) throws RelationsMemberGroupMeetingServiceException {
        if (groupEntity.getId() != null)
            return groupMeetingsRepository.findAllByGroup(groupEntity);
        else
            throw new RelationsMemberGroupMeetingServiceException(("Group id=" + groupEntity.getId().toString()));
    }

    public List<MemberEntity> findAllMeetingMembers(MeetingEntity meetingEntity) throws RelationsMemberGroupMeetingServiceException {
        if (meetingEntity.getId() != null)
            return meetingMembersRepository.findAllByMeetings(meetingEntity);
        else
            throw new RelationsMemberGroupMeetingServiceException(("Meeting id=" + meetingEntity.getId().toString()));
    }

    public MemberEntity findMember(Long memberId) throws MemberNotFoundException {
        return relationsMemberGroupCrudService
                .findMember(memberId);
    }
}
