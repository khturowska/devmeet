package pl.com.devmeet.devmeetcore.feature_services.user_and_member.user_activation_after_registration.exceptions;

public class InvalidUUIDStringException extends IllegalArgumentException {
    public InvalidUUIDStringException(String message) {
        super(message);
    }
}
