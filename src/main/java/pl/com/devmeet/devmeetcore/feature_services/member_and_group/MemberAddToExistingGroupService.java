package pl.com.devmeet.devmeetcore.feature_services.member_and_group;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.com.devmeet.devmeetcore.feature_services.member_and_group.crud.RelationsMemberGroupCrudService;
import pl.com.devmeet.devmeetcore.feature_services.member_and_group.exceptions.MemberAddNewGroupServiceException;
import pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.crud.MeetingInfoEmailSenderService;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Slf4j
@Component
public class MemberAddToExistingGroupService {

    @Getter
    private RelationsMemberGroupCrudService relationsMemberGroupCrudService;
    private MeetingInfoEmailSenderService meetingInfoEmailSenderService;

    public MemberAddToExistingGroupService(RelationsMemberGroupCrudService relationsMemberGroupCrudService,
                                           MeetingInfoEmailSenderService meetingInfoEmailSenderService) {

        this.relationsMemberGroupCrudService = relationsMemberGroupCrudService;
        this.meetingInfoEmailSenderService = meetingInfoEmailSenderService;
    }

    public GroupEntity addMemberToExistingGroup(final Long groupId, final Long memberId) throws GroupNotFoundException, MemberNotFoundException, MemberAddNewGroupServiceException {
        GroupEntity foundGroup = relationsMemberGroupCrudService.findGroup(groupId);
        MemberEntity memberToAdd = relationsMemberGroupCrudService.findMember(memberId);

        Set<MemberEntity> members = new LinkedHashSet<>(relationsMemberGroupCrudService.findMembersInGroup(foundGroup));
        boolean isAlreadyGroupMember = checkIfTheMemberIsAlreadyRegisteredInTheGroup(members, memberToAdd);

        if (isAlreadyGroupMember) {
            log.warn(String.format("Member (id=%d) is already Group (id=%d) member", memberId, groupId));
            return foundGroup;
        } else {
            GroupEntity updatedGroup = updateMembersListInGroup(
                    addMemberIfTheNumberOfMembersHasNotBeenExceeded(foundGroup, new ArrayList<>(members), memberToAdd));
            log.info(String.format("Member (id=%d) has been added to Group (id=%d)", memberId, groupId));
            return updatedGroup;
        }
    }

    private boolean checkIfTheMemberIsAlreadyRegisteredInTheGroup(Set<MemberEntity> groupMembers, MemberEntity memberEntity) {
        Long memberId = memberEntity.getId();
        return groupMembers.stream()
                .anyMatch(member -> member.getId().equals(memberId));
    }

    private GroupEntity addMemberIfTheNumberOfMembersHasNotBeenExceeded(GroupEntity group, List<MemberEntity> members, MemberEntity memberToAdd) throws MemberAddNewGroupServiceException {
        Integer membersLimit;

        membersLimit = group.getMembersLimit();

        if (membersLimit == null || membersLimit == 0) {
            membersLimit = members.size() + 1;
            group.setMembersLimit(membersLimit);
        }

        if (members.size() < membersLimit) {
            members.add(memberToAdd);

            group.setMembers(new ArrayList<>(members));
        } else {
            throw new MemberAddNewGroupServiceException(("The Member (id=" + memberToAdd.getId() + ") can't be added to the Group (id=" + group.getId() + "), because members limit (max=" + membersLimit.toString() + ") has been reached"));
        }
        return group;
    }

    private GroupEntity updateMembersListInGroup(final GroupEntity groupEntity) {
        return relationsMemberGroupCrudService.updateMemberListInGroup(groupEntity);
    }


}
