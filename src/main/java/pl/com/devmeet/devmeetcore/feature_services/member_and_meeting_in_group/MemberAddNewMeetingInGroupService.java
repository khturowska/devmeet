package pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.place.crud.PlaceCrudService;
import pl.com.devmeet.devmeetcore.domain.place.models.PlaceEntity;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceNotFoundException;
import pl.com.devmeet.devmeetcore.feature_services.member_and_group.crud.RelationsMemberGroupCrudService;
import pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.crud.MeetingInfoEmailSenderService;
import pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.crud.RelationsMemberGroupMeetingCrudService;
import pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.exceptions.MemberAddNewMeetingInGroupServiceException;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class MemberAddNewMeetingInGroupService {

    private RelationsMemberGroupMeetingCrudService relationsMemberGroupMeetingCrudService;
    private RelationsMemberGroupCrudService relationsMemberGroupCrudService;
    private MeetingInfoEmailSenderService meetingInfoEmailSenderService;
    private PlaceCrudService placeCrudService;

    @Autowired
    public MemberAddNewMeetingInGroupService(RelationsMemberGroupMeetingCrudService relationsMemberGroupMeetingCrudService,
                                             RelationsMemberGroupCrudService relationsMemberGroupCrudService,
                                             MeetingInfoEmailSenderService meetingInfoEmailSenderService,
                                             PlaceCrudService placeCrudService) {

        this.relationsMemberGroupMeetingCrudService = relationsMemberGroupMeetingCrudService;
        this.relationsMemberGroupCrudService = relationsMemberGroupCrudService;
        this.meetingInfoEmailSenderService = meetingInfoEmailSenderService;
        this.placeCrudService = placeCrudService;
    }

    public MeetingEntity addNewMeetingInGroup(MeetingEntity meetingEntity) throws MemberNotFoundException, GroupNotFoundException, PlaceNotFoundException, MeetingAlreadyExistsException, MemberAddNewMeetingInGroupServiceException {
        MeetingEntity newMeeting = relationsMemberGroupMeetingCrudService
                .createNewMeeting(addPlaceToMeeting(
                        addPromoterAsFirstMemberInMeetingMembers(
                                addPromoter(
                                        checkIsGroupExists(meetingEntity)))));

        log.info(String.format("Member (memberId=%d) has been added to Meeting (meetingId=%d) as Promoter and Member in to Group (groupId=%d)",
                meetingEntity.getPromoter().getId(),
                meetingEntity.getId(),
                meetingEntity.getGroup().getId()));

        sendEmailToPromoterAsFirstMeetingMember(newMeeting);
        return newMeeting;
    }

    private void sendEmailToPromoterAsFirstMeetingMember(MeetingEntity meetingEntity) {
        meetingInfoEmailSenderService.sendMailToMeetingMember(meetingEntity.getPromoter(), meetingEntity);
    }

    private MeetingEntity checkIsGroupExists(MeetingEntity meeting) throws MemberAddNewMeetingInGroupServiceException {
        try {
            relationsMemberGroupCrudService.findGroup(meeting.getGroup().getId());
        } catch (GroupNotFoundException e) {
            throw new MemberAddNewMeetingInGroupServiceException(e.getMessage());
        }
        return meeting;
    }

    private MeetingEntity addPromoter(MeetingEntity meetingEntity) throws MemberNotFoundException, MemberAddNewMeetingInGroupServiceException {
        MemberEntity promoter = meetingEntity.getPromoter();
        if (promoter != null) {
            promoter = relationsMemberGroupMeetingCrudService
                    .findMember(meetingEntity
                            .getPromoter()
                            .getId());
        } else
            throw new MemberAddNewMeetingInGroupServiceException("Promoter is not set");

        checkIsPromoterIsAMemberOfGroup(meetingEntity.getGroup(), promoter);

        meetingEntity.setPromoter(promoter);
        return meetingEntity;
    }

    private void checkIsPromoterIsAMemberOfGroup(GroupEntity groupEntity, MemberEntity promoter) throws MemberAddNewMeetingInGroupServiceException {
        Long promoterId = promoter.getId();
        String err;
        List<MemberEntity> groupMembers = relationsMemberGroupCrudService
                .findMembersInGroup(groupEntity);

        boolean isGroupMember = groupMembers
                .stream()
                .anyMatch(memberEntity -> memberEntity.getId().equals(promoterId));

        if (!isGroupMember) {
            err = String.format("Promoter (memberId=%d) is not member of the Group (groupId=%d)",
                    promoterId,
                    groupEntity.getId());
            log.warn(err);
            throw new MemberAddNewMeetingInGroupServiceException(err);
        }
    }

    private MeetingEntity addPromoterAsFirstMemberInMeetingMembers(MeetingEntity meetingEntity) {
        List<MemberEntity> meetingMembers = new ArrayList<>();
        meetingMembers.add(meetingEntity.getPromoter());
        meetingEntity.setMembers(meetingMembers);
        return meetingEntity;
    }

    private MeetingEntity addPlaceToMeeting(MeetingEntity meetingEntity) throws MemberAddNewMeetingInGroupServiceException, PlaceNotFoundException {
        PlaceEntity place = meetingEntity.getPlace();
        if (place != null){
            place = placeCrudService.findPlaceById(place.getId());
        } else
            throw new MemberAddNewMeetingInGroupServiceException("Place is not set");

        meetingEntity.setPlace(place);
        return meetingEntity;
    }
}
