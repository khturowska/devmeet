package pl.com.devmeet.devmeetcore.feature_services.user_and_member.user_registration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.com.devmeet.devmeetcore.api.UserRegistrationActivationAndLoginApi;
import pl.com.devmeet.devmeetcore.config.AddressAndPortConfigurationInfoService;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserCrudService;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyExistsEmailDuplicationException;
import pl.com.devmeet.devmeetcore.email.EmailSenderService;

import java.util.UUID;

@Slf4j
@Component
public class UserRegistrationService {

    private UserCrudService userService;
    private EmailSenderService emailSenderService;
    private UserRegistrationEmailSender emailSender;
    private AddressAndPortConfigurationInfoService environment;

    @Autowired
    UserRegistrationService(UserCrudService userService,
                            EmailSenderService emailSenderService,
                            AddressAndPortConfigurationInfoService environment) {
        this.userService = userService;
        this.emailSenderService = emailSenderService;
        this.environment = environment;
        this.emailSender = initRegistrationEmailSenderWithDefaultConfig();
    }

    private UserRegistrationEmailSender initRegistrationEmailSenderWithDefaultConfig() {
        return UserRegistrationEmailSender.builder()
                .emailSenderService(emailSenderService)
                .build();
    }

    public UserRegistrationEmailSender getEmailSender() {
        return emailSender;
    }

    public UserEntity registerNotActiveUserUsingApiWithInitialPasswordAndSendEmailToUser(UserEntity user) throws UserAlreadyExistsEmailDuplicationException {
        emailSender.setPathToRegistrationTemplate("/email_templates/registrationEmailTemplateWithInitialPassword.txt");
        emailSender.setUriEndpoint(UserRegistrationActivationAndLoginApi.apiUrl);

        UserEntity newUser = saveUserAndSendMail(user);

        log.info(String.format("New User (id=%d) with initial password is want to became a DevMeet Member. Waiting for activation", newUser.getId()));
        return newUser;
    }

    public UserEntity registerNotActiveUserUsingVaadinAndSendEmailToUser(UserEntity user) throws UserAlreadyExistsEmailDuplicationException {
        emailSender.setPathToRegistrationTemplate("/email_templates/registrationEmailTemplate.txt");
        emailSender.setUriEndpoint(UserRegistrationActivationAndLoginApi.apiUrl + UserRegistrationActivationAndLoginApi.vaadinEndpointPrefix);

        UserEntity newUser = saveUserAndSendMail(user);

        log.info(String.format("New User (id=%d) is want to became a DevMeet Member. Waiting for activation", newUser.getId()));
        return newUser;
    }

    private UserEntity saveUserAndSendMail(UserEntity user) throws UserAlreadyExistsEmailDuplicationException {
        user.setActivationKey(UUID.randomUUID());
        try {
            UserEntity newUser = userService.add(new UserPasswordEncryptor()
                    .encryptUserPassword(user));
            sentRegistrationMessageToUser(newUser);
            return newUser;
        } catch (UserAlreadyExistsEmailDuplicationException e) {
            throw new UserAlreadyExistsEmailDuplicationException(e.getMessage());
        }
    }

    private void sentRegistrationMessageToUser(UserEntity user) {
        emailSender.sendRegistrationMessageToUser(user);
    }
}
