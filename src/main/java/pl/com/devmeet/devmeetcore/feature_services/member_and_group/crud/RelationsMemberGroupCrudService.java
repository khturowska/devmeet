package pl.com.devmeet.devmeetcore.feature_services.member_and_group.crud;

import org.springframework.stereotype.Service;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class RelationsMemberGroupCrudService {

    private GroupCrudService groupCrudService;
    private MemberCrudService memberCrudService;

    private WhatMembersAreInGroupRepository whatMembersAreInGroupRepository;
    private InWhatGroupsIsAMemberRepository inWhatGroupsIsAMemberRepository;

    public RelationsMemberGroupCrudService(GroupCrudService groupCrudService,
                                           MemberCrudService memberCrudService,
                                           WhatMembersAreInGroupRepository whatMembersAreInGroupRepository,
                                           InWhatGroupsIsAMemberRepository inWhatGroupsIsAMemberRepository) {
        this.groupCrudService = groupCrudService;
        this.memberCrudService = memberCrudService;
        this.whatMembersAreInGroupRepository = whatMembersAreInGroupRepository;
        this.inWhatGroupsIsAMemberRepository = inWhatGroupsIsAMemberRepository;
    }

    public GroupEntity createNewGroup(GroupEntity groupEntity) throws GroupAlreadyExistsException {
        return groupCrudService.add(groupEntity);
    }

    public GroupEntity updateMemberListInGroup(GroupEntity groupEntity) {
        groupEntity.setModificationTime(LocalDateTime.now());
        return groupCrudService
                .getGroupCrudRepository()
                .save(groupEntity);
    }

    public List<MemberEntity> findMembersInGroup(GroupEntity groupEntity) {
        return whatMembersAreInGroupRepository
                .findAllByGroups(groupEntity);
    }

    public List<GroupEntity> findInWhatGroupsIsAMember(MemberEntity memberEntity) {
        return inWhatGroupsIsAMemberRepository.findAllByMembers(memberEntity);
    }

    public List<GroupEntity> findInWhatGroupsMemberIsAFounder(MemberEntity memberEntity){
        return inWhatGroupsIsAMemberRepository.findAllByFounder(memberEntity);
    }

    public GroupEntity findGroup(Long id) throws GroupNotFoundException {
        return groupCrudService.findById(id);
    }

    public MemberEntity findMember(Long id) throws MemberNotFoundException {
        return memberCrudService.findById(id);
    }
}
