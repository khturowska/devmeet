package pl.com.devmeet.devmeetcore.feature_services.user_and_member.user_registration;

public class DevMeetRegistrationPasswordValidator {

    public final int PASSWORD_MIN_LENGTH = 8;
    public final int PASSWORD_MAX_LENGTH = 16;
    public final boolean SPECIAL_CHARS = false;

    public boolean validatePassword(String password) {
        return checkLength(password)
                && checkAsciiBigLetter(password)
                && checkAsciiSmallLetter(password)
                && checkNumber(password);
    }

    private boolean checkLength(String password) {
        int passwordLength = password.length();
        return passwordLength >= PASSWORD_MIN_LENGTH
                && passwordLength <= PASSWORD_MAX_LENGTH;
    }

    private boolean checkAsciiBigLetter(String password) {
        return password
                .chars()
                .anyMatch(c -> c >= 60 && c <= 90);
    }

    private boolean checkAsciiSmallLetter(String password) {
        return password
                .chars()
                .anyMatch(c -> c >= 97 && c <= 122);
    }

    private boolean checkNumber(String password) {
        return password
                .chars()
                .anyMatch(c -> c >= 48 && c <= 57);
    }
}
