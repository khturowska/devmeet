package pl.com.devmeet.devmeetcore.feature_services.user_and_member.user_activation_after_registration;

import lombok.AllArgsConstructor;
import pl.com.devmeet.devmeetcore.email.email_template_reader.UserEmailPostfixTrimmer;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberUserNotActiveException;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;


@AllArgsConstructor
class UserActivationMemberCreator {

    private MemberCrudService memberCrudService;

    public Boolean createMemberWithDefaultUserNick(UserEntity userEntity) throws MemberUserNotActiveException, UserNotFoundException, MemberAlreadyExistsException {
        MemberEntity member = MemberEntity.builder()
                .nick(trimEmailPostfix(userEntity.getEmail()))
                .user(userEntity)
                .build();

        return memberCrudService.add(member).getId() != null;
    }

    private String trimEmailPostfix(String email) {
        return new UserEmailPostfixTrimmer().trim(email);
    }
}
