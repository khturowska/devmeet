package pl.com.devmeet.devmeetcore.feature_services.user_and_member.user_registration;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.email.EmailSenderService;
import pl.com.devmeet.devmeetcore.email.Mail;
import pl.com.devmeet.devmeetcore.email.email_template_reader.EmailContentGenerator;
import pl.com.devmeet.devmeetcore.email.email_template_reader.exceptions.EmailContentGeneratorException;
import pl.com.devmeet.devmeetcore.email.email_template_reader.exceptions.ResourcesTemplateReaderErrorException;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
class UserRegistrationEmailSender {
    @NonNull
    private EmailSenderService emailSenderService;

    @Builder.Default
    private String emailFromAddress = "no-reply@devmeet.com";
    @Builder.Default
    private String emailSubject = "Devmeet user registration";
    private String uriEndpoint;
    private String pathToRegistrationTemplate;

    //todo Jeżeli aplikacja bedzie pracowala na serwerze w trybie produkcyjnym to wygenerowany email bedzie prowadzil do nikad: wykorzystac pliki "properties" i DevmeetServerConfiguration
    public Mail sendRegistrationMessageToUser(UserEntity userEntity) {
        String userActivationLink = generateURL(userEntity);
        Map<String, String> definedParametersInTemplate = generateMapWithParamsDefinedInTemplate(userEntity, userActivationLink);
        String emailContentString = null;
        Mail generatedMail = null;
        try {
            emailContentString = generateContentFromTemplate(definedParametersInTemplate);
            generatedMail = Mail.builder()
                    .from(emailFromAddress)
                    .to(userEntity.getEmail())
                    .subject(emailSubject)
                    .content(emailContentString)
                    .build();

            emailSenderService.sendSimpleMessage(generatedMail);
            log.info(String.format("Email to User (id=%d) has been send successful", userEntity.getId()));

        } catch (ResourcesTemplateReaderErrorException | EmailContentGeneratorException e) {
            log.error(String.format("Can't send registration email to User (id=%d) because: %s", userEntity.getId(), e.getMessage()));
        }
        return generatedMail;
    }

    private String generateURL(UserEntity userEntity) {
        String endpointPath = uriEndpoint + "/" + userEntity.getEmail() + "/" + userEntity.getActivationKey();
        return emailSenderService.generateServerAddressAndPortEndpointPathString(endpointPath);
    }

    private Map<String, String> generateMapWithParamsDefinedInTemplate(UserEntity userEntity, String userActivationLink) {
        return new HashMap() {{
            put("email", userEntity.getEmail());
            put("link", userActivationLink);
            put("password", userEntity.getPassword());
        }};
    }

    private String generateContentFromTemplate(Map<String, String> params) throws EmailContentGeneratorException, ResourcesTemplateReaderErrorException {
        return new EmailContentGenerator(pathToRegistrationTemplate)
                .generateEmailContent(params);
    }
}
