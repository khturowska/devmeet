package pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.crud;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingEntity;

import java.util.List;

@Repository
interface GroupMeetingsRepository extends JpaRepository<MeetingEntity, Long> {

    List<MeetingEntity> findAllByGroup(GroupEntity group);

}
