package pl.com.devmeet.devmeetcore.feature_services.user_and_member.login;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.com.devmeet.devmeetcore.feature_services.user_and_member.login.exceptions.WrongUserPasswordLoginException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserCrudService;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

@Slf4j
@Component
public class UserLoginService {

    private UserCrudService userCrudService;
    private MemberCrudService memberCrudService;

    @Autowired
    public UserLoginService(UserCrudService userCrudService, MemberCrudService memberCrudService) {
        this.userCrudService = userCrudService;
        this.memberCrudService = memberCrudService;
    }

    public MemberEntity loginUserAndGetMember(UserEntity userForm) throws UserNotFoundException, WrongUserPasswordLoginException, UserFoundButNotActiveException {
        UserEntity userEntity = checkIsUserActiveAndGetUser(userForm);

        if (isPasswordMatch(userEntity, userForm.getPassword())) {
            return getUsersMemberIfExistOrCreateMemberIfFirstLogin(userEntity);
        } else {
            String passwordNotMathMessage = String.format("User (id=%d) tried to log in, but password not math.",
                    userEntity.getId());
            throw new WrongUserPasswordLoginException(passwordNotMathMessage);
        }
    }

    private UserEntity checkIsUserActiveAndGetUser(UserEntity user) throws UserNotFoundException, UserFoundButNotActiveException {
        UserEntity userEntity = userCrudService.findByEmail(user.getEmail());

        if (userEntity.isActive())
            return userEntity;
        else {
            String errMessage = String.format("User %s can't log in because User (id=%d) is not active", userEntity.getEmail(), userEntity.getId());
            log.warn(errMessage);
            throw new UserFoundButNotActiveException(String.format("User (id=%d) found but not active yet", userEntity.getId()));
        }
    }

    private boolean isPasswordMatch(UserEntity userEntity, String password) {
        return new UserPasswordDecryptor()
                .decryptUserPasswordAndMathWithInput(userEntity, password);
    }

    private MemberEntity getUsersMemberIfExistOrCreateMemberIfFirstLogin(UserEntity userEntity) throws UserNotFoundException {
        try {
            return memberCrudService.findByUserId(userEntity.getId());
        } catch (MemberNotFoundException e) {
            return createMemberForFirstLoginUser(userEntity);
        }
    }

    private MemberEntity createMemberForFirstLoginUser(UserEntity userEntity){
        return null;
    }
}
