package pl.com.devmeet.devmeetcore.feature_services.user_and_member.user_registration;

import java.util.Random;

class UserRegistrationRandomPasswordGenerator {

    public String generateRandomPassword() {
        int min = 100000;
        int max = 999999;
        return String.valueOf(new Random()
                .ints(min, (max + 1))
                .limit(1)
                .findFirst()
                .orElse(123456));
    }
}
