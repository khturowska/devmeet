package pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.crud;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.com.devmeet.devmeetcore.email.EmailSenderService;
import pl.com.devmeet.devmeetcore.email.Mail;
import pl.com.devmeet.devmeetcore.email.email_template_reader.EmailContentGenerator;
import pl.com.devmeet.devmeetcore.email.email_template_reader.exceptions.EmailContentGeneratorException;
import pl.com.devmeet.devmeetcore.email.email_template_reader.exceptions.ResourcesTemplateReaderErrorException;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.place.models.PlaceEntity;

import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Map;

@Slf4j
@Component
public class MeetingInfoEmailSenderService {

    private EmailSenderService emailSenderService;

    private String emailFromAddress = "no-reply@devmeet.com.pl";
    private String emailSubjectPostfix = "Devmeet meeting";
    private String pathToRegistrationTemplate = "/email_templates/meetingInfoEmailTemplate.txt";

    @Autowired
    public MeetingInfoEmailSenderService(EmailSenderService emailSenderService) {
        this.emailSenderService = emailSenderService;
    }

    public Mail sendMailToMeetingMember(MemberEntity memberEntity, MeetingEntity meetingEntity) {
        Map<String, String> definedParametersInTemplate = generateMapWithParamsDefinedInTemplate(memberEntity, meetingEntity.getGroup(), meetingEntity, meetingEntity.getPlace());
        String emailContentString = null;
        Mail meetingInfoMail = null;

        try {
            emailContentString = generateContentFromTemplate(definedParametersInTemplate);
            meetingInfoMail = Mail.builder()
                    .from(emailFromAddress)
                    .to(memberEntity.getUser().getEmail())
                    .subject(String.format("%s: %s in %s group",
                            emailSubjectPostfix,
                            meetingEntity.getMeetingName(),
                            meetingEntity.getGroup().getGroupName()))
                    .content(emailContentString)
                    .build();

            emailSenderService.sendSimpleMessage(meetingInfoMail);
            log.info(String.format("Email with info about Meeting has been send to Member (memberId=%d)",
                    memberEntity.getId()));
        } catch (EmailContentGeneratorException | ResourcesTemplateReaderErrorException e) {
            log.error(String.format("Can't send email with info about Meeting (id=%d) in Group (id=%d) to Member (id=%d) because: %s",
                    meetingEntity.getId(),
                    meetingEntity.getGroup().getId(),
                    meetingEntity.getPromoter().getId(),
                    e.getMessage()));
        }
        return meetingInfoMail;
    }

    private Map<String, String> generateMapWithParamsDefinedInTemplate(MemberEntity member, GroupEntity group,
                                                                       MeetingEntity meeting, PlaceEntity place) {
        String timeFormat = "yyyy-MM-dd hh:mm";
        Map<String, String> params = new LinkedHashMap<>();
        params.put("memberNick", member.getNick());
        params.put("groupName", group.getGroupName());
        params.put("meetingName", meeting.getMeetingName());
        params.put("begin", meeting.getBegin().format(DateTimeFormatter.ofPattern(timeFormat)));
        params.put("end", meeting.getEnd().format(DateTimeFormatter.ofPattern(timeFormat)));
        params.put("placeName", place.getPlaceName());
        params.put("placeDescription", place.getDescription());
        params.put("placeWebsite", place.getWebsite());
        params.put("placeLocation", place.getLocation());
        return params;
    }

    private String generateContentFromTemplate(Map<String, String> params) throws EmailContentGeneratorException, ResourcesTemplateReaderErrorException {
        return new EmailContentGenerator(pathToRegistrationTemplate)
                .generateEmailContent(params);
    }
}
