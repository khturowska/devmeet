package pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions;

public enum MeetingCrudStatusEnum {

    ALREADY_EXIST("Meeting already exists"),
    NOT_FOUND("Meeting not found"),
    NULL_ID("Meeting Id was not specified"),
    FOUND_BUT_NOT_ACTIVE("Meeting found, but not active"),
    NOT_FOUND_BY_GROUP("Meetings not found by group");

    String status;

    MeetingCrudStatusEnum(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return status;
    }
}
