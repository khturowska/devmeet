package pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain.status_and_exceptions;

/**
 * Created by IntelliJ IDEA.
 * User: Kamil Ptasinski
 * Date: 15.11.2019
 * Time: 23:53
 */
public class AvailabilityVoteFoundButNotActiveException extends Exception {
    public AvailabilityVoteFoundButNotActiveException(String message) {
        super(message);
    }
}
