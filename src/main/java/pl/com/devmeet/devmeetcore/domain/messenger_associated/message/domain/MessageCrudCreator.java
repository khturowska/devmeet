package pl.com.devmeet.devmeetcore.domain.messenger_associated.message.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.message.status_and_exceptions.MessageArgumentNotSpecifiedException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.message.status_and_exceptions.MessageCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerEntity;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class MessageCrudCreator {

    private MessageCrudSaver messageCrudSaver;
    private MessageCrudFinder messageCrudFinder;
    private MessengerFinder messengerFinder;

    public MessageEntity createEntity(MessageEntity dto) throws UserNotFoundException, MessengerNotFoundException, GroupNotFoundException, MemberNotFoundException, MessageArgumentNotSpecifiedException {
        MessengerEntity sender = messengerFinder.findMessenger(dto.getSender());
        MessengerEntity receiver = messengerFinder.findMessenger(dto.getReceiver());

        checkIsMessageIsNotEmpty(dto.getMessage());

        return messageCrudSaver.saveEntity(
                setDefaultValuesWhenMessageNotExists(
                        connectMessengers(dto, sender, receiver)));

    }

    private MessageEntity setDefaultValuesWhenMessageNotExists(MessageEntity messageEntity) {
        messageEntity.setCreationTime(DateTime.now());
        messageEntity.setActive(true);

        return messageEntity;
    }

    private void checkIsMessageIsNotEmpty(String message) throws MessageArgumentNotSpecifiedException {
        if (message.equals(""))
            throw new MessageArgumentNotSpecifiedException(MessageCrudStatusEnum.MESSAGE_IS_EMPTY.toString());
    }

    public MessageEntity connectMessengers(MessageEntity messageEntity, MessengerEntity senderEntity, MessengerEntity receiverEntity) {
        messageEntity.setSender(senderEntity);
        messageEntity.setReceiver(receiverEntity);

        return messageEntity;
    }
}