package pl.com.devmeet.devmeetcore.domain.group_associated.meeting.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingNotFoundException;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class MeetingCrudDeactivator {

    private MeetingCrudFinder meetingCrudFinder;
    private MeetingCrudSaver meetingCrudSaver;

    public MeetingEntity deactivateEntity(MeetingEntity dto) throws MeetingNotFoundException, GroupNotFoundException, MeetingFoundButNotActiveException {
        MeetingEntity meetingEntity = meetingCrudFinder.findByIdOrFeatures(dto);
        return save(setDefaultValues(meetingEntity));
    }

    public MeetingEntity deactivateEntity(Long meetingId) throws MeetingNotFoundException, MeetingFoundButNotActiveException {
        MeetingEntity meetingEntity = meetingCrudFinder.findById(meetingId);
        return setDefaultValues(meetingEntity);
    }

    private MeetingEntity setDefaultValues(MeetingEntity meetingEntity) throws MeetingFoundButNotActiveException {
        if (meetingEntity.isActive()) {
            meetingEntity.setActive(false);
            meetingEntity.setModificationTime(LocalDateTime.now());
            return meetingEntity;
        }
        throw new MeetingFoundButNotActiveException();
    }

    private MeetingEntity save (MeetingEntity meetingEntity){
        return meetingCrudSaver.saveEntity(meetingEntity);
    }
}