package pl.com.devmeet.devmeetcore.domain.member_associated.availability.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.models.AvailabilityEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions.AvailabilityCrudInfoStatusEnum;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions.AvailabilityFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions.AvailabilityNotFoundException;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class AvailabilityCrudDeleter {

    private AvailabilityCrudSaver availabilityCrudSaver;
    private AvailabilityCrudFinder availabilityCrudFinder;

    public AvailabilityEntity deactivateEntity(AvailabilityEntity dto) throws AvailabilityNotFoundException, AvailabilityFoundButNotActiveException {
        AvailabilityEntity availability = availabilityCrudFinder.findByEntity(dto);
        return deactivate(availability);
    }

    public AvailabilityEntity deactivateEntityById(Long id) throws AvailabilityNotFoundException, AvailabilityFoundButNotActiveException {
        AvailabilityEntity availability = availabilityCrudFinder.findById(id);
        return deactivate(availability);
    }

    private AvailabilityEntity deactivate(AvailabilityEntity availability) throws AvailabilityFoundButNotActiveException {
        boolean availabilityActivity = availability.isActive();

        if (availabilityActivity) {
            availability.setActive(false);
            availability.setModificationTime(LocalDateTime.now());

            return availabilityCrudSaver.saveEntity(availability);
        }
        throw new AvailabilityFoundButNotActiveException(AvailabilityCrudInfoStatusEnum.AVAILABILITY_FOUND_BUT_NOT_ACTIVE.toString());
    }
}
