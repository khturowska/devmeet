package pl.com.devmeet.devmeetcore.domain.messenger_associated.message.domain;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.message.status_and_exceptions.MessageArgumentNotSpecifiedException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.message.status_and_exceptions.MessageCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.message.status_and_exceptions.MessageNotFoundException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class MessageCrudUpdater {

    private MessageCrudFinder messageCrudFinder;
    private MessageCrudSaver messageCrudSaver;

    public MessageEntity updateEntity(MessageEntity oldDto, MessageEntity newDto) throws MessageNotFoundException, GroupNotFoundException, MessengerNotFoundException, UserNotFoundException, MessageArgumentNotSpecifiedException, MemberNotFoundException {
        MessageEntity foundMessage = messageCrudFinder.findEntity(oldDto);

        return messageCrudSaver.saveEntity(
                updateAllowedParameters(foundMessage, messageChecker(newDto)));
    }

    private MessageEntity updateAllowedParameters(MessageEntity oldEntity, MessageEntity newDto) {
        oldEntity.setMessage(newDto.getMessage());
        oldEntity.setModificationTime(DateTime.now());

        return oldEntity;
    }

    private MessageEntity messageChecker(MessageEntity messageEntity) throws MessageArgumentNotSpecifiedException {
        String message;

        try {
            message = messageEntity.getMessage();
            checkIsMessageIsNotEmpty(message);

        } catch (NullPointerException e) {
            throw new MessageArgumentNotSpecifiedException(MessageCrudStatusEnum.MESSAGE_IS_EMPTY.toString());
        }

        return messageEntity;
    }

    private void checkIsMessageIsNotEmpty(String message) throws MessageArgumentNotSpecifiedException {
        if (message.equals(""))
            throw new MessageArgumentNotSpecifiedException(MessageCrudStatusEnum.MESSAGE_IS_EMPTY.toString());
    }

}