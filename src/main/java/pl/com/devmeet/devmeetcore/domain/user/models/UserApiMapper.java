package pl.com.devmeet.devmeetcore.domain.user.models;

public class UserApiMapper {

    public UserApiDto mapToFrontend(UserEntity userEntity) {
        return userEntity != null ? UserApiDto.builder()
                .id(userEntity.getId())
                .email(userEntity.getEmail())
//                .password(userDto.getPassword()) //todo Czy user powinien zwracać hasło na front?
                .build() : null;
    }

    public UserEntity mapToBackend(UserApiDto userApiDto) {
        return userApiDto != null ?
                UserEntity.builder()
                        .id(userApiDto.getId())
                        .email(userApiDto.getEmail())
                        .password(userApiDto.getPassword())
                        .build() : null;
    }
}
