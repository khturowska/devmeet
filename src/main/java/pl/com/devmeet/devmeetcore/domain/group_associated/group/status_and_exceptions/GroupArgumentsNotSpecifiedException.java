package pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions;

public class GroupArgumentsNotSpecifiedException extends RuntimeException {

    public GroupArgumentsNotSpecifiedException() {
        super(GroupCrudStatusEnum.ARGUMENTS_NOT_SPECIFIED.toString());
    }

    public GroupArgumentsNotSpecifiedException(String message) {
        super(message);
    }

    public GroupArgumentsNotSpecifiedException(String message, Throwable cause) {
        super(message, cause);
    }
}
