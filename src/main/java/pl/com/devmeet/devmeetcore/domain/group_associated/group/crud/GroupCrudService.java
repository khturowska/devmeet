package pl.com.devmeet.devmeetcore.domain.group_associated.group.crud;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberRepository;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerCrudService;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerRepository;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserRepository;

import java.util.List;

@Service
public class GroupCrudService {

    @Getter
    private GroupCrudRepository groupCrudRepository;
    private MemberRepository memberRepository;
    private UserRepository userRepository;
    private MessengerRepository messengerRepository;

    @Autowired
    public GroupCrudService(GroupCrudRepository groupCrudRepository, MemberRepository memberRepository, UserRepository userRepository, MessengerRepository messengerRepository) {
        this.groupCrudRepository = groupCrudRepository;
        this.memberRepository = memberRepository;
        this.userRepository = userRepository;
        this.messengerRepository = messengerRepository;
    }

    private GroupMemberFinder initMemberFinder() {
        return new GroupMemberFinder(new MemberCrudService(memberRepository, userRepository, messengerRepository, groupCrudRepository));
    }

    private GroupCrudSaver initSaver() {
        return new GroupCrudSaver(groupCrudRepository);
    }

    private GroupCrudCreator initCreator() {
        return GroupCrudCreator.builder()
                .groupCrudFinder(initFinder())
                .groupCrudSaver(initSaver())
                .groupMessengerCreator(initMessengerCreator())
                .build();
    }

    private GroupCrudFinder initFinder() {
        return GroupCrudFinder.builder()
                .groupCrudRepository(groupCrudRepository)
                .memberFinder(initMemberFinder())
                .build();
    }

    private GroupCrudUpdater initUpdater() {
        return GroupCrudUpdater.builder()
                .groupCrudFinder(initFinder())
                .groupCrudSaver(initSaver())
                .build();
    }

    private GroupCrudDeactivator initDeleter() {
        return GroupCrudDeactivator.builder()
                .groupCrudFinder(initFinder())
                .groupCrudSaver(initSaver())
                .groupMessengerDeactivator(initMessengerDeactivator())
                .build();
    }

    private MessengerCrudService initMessengerFacade() {
        return new MessengerCrudService(messengerRepository, userRepository, memberRepository, groupCrudRepository);
    }

    private GroupMessengerCreator initMessengerCreator() {
        return new GroupMessengerCreator(initMessengerFacade());
    }

    private GroupMessengerDeactivator initMessengerDeactivator() {
        return new GroupMessengerDeactivator(initMessengerFacade());
    }

    public GroupCrudRepository getGroupCrudRepository(){
        return groupCrudRepository;
    }

    public GroupEntity add(GroupEntity dto) throws GroupAlreadyExistsException {
        return initCreator().createEntity(dto);
    }

    public GroupEntity findById(Long id) throws GroupNotFoundException {
        return initFinder().findById(id);
    }

    public GroupEntity findByGroup(GroupEntity groupEntity) throws GroupNotFoundException {
        return initFinder().findEntityByGroup(groupEntity);
    }

    public List<GroupEntity> findAll() {
        return initFinder().findAllEntities();
    }

    public GroupEntity update(GroupEntity toUpdateBasedOnId) throws GroupNotFoundException, GroupFoundButNotActiveException {
        return initUpdater().updateEntity(toUpdateBasedOnId);
    }

    public GroupEntity delete(GroupEntity dto) throws GroupNotFoundException, GroupFoundButNotActiveException, MemberNotFoundException {
        return initDeleter().deactivateEntity(dto);
    }

    public List<GroupEntity> findBySearchText(String searchText) {
        if (searchText != null)
            return groupCrudRepository.findAllBySearchText(searchText);
        else return findAll();
    }

    public List<GroupEntity> findByActive(Boolean isActive) {
        return groupCrudRepository.findAllByActive(isActive);
    }
}
