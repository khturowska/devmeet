package pl.com.devmeet.devmeetcore.domain.group_associated.meeting.crud;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingEntity;

import java.util.List;
import java.util.Optional;

@Repository
public interface MeetingCrudRepository extends JpaRepository<MeetingEntity, Long> {

    Optional<MeetingEntity> findByMeetingName(String meetingName);

    Optional<MeetingEntity> findByMeetingNameAndGroup(String meetingName, GroupEntity group);

    List<MeetingEntity> findAllByGroup(GroupEntity group);

    List<MeetingEntity> findAllByGroup_Id(Long groupId);

    List<MeetingEntity> findAllByPlace_Id(Long placeId);

    @Query("select m from MeetingEntity m where m.isActive = :isActive")
    List<MeetingEntity> findByIsActive(boolean isActive);
}
