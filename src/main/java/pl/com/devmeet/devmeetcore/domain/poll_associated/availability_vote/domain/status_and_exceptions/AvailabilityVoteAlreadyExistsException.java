package pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain.status_and_exceptions;

/**
 * Created by IntelliJ IDEA.
 * User: Kamil Ptasinski
 * Date: 16.11.2019
 * Time: 11:18
 */
public class AvailabilityVoteAlreadyExistsException extends Exception {
    public AvailabilityVoteAlreadyExistsException(String message) {
        super(message);
    }
}
