package pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerArgumentNotSpecified;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerInfoStatusEnum;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

@RequiredArgsConstructor
class MessengerMemberOrGroupConnector {

    @NonNull
    private MessengerCrudFinder messengerCrudFinder;

    public MessengerEntity connectWithMessenger(MessengerEntity messengerEntity, MessengerEntity messengerDto) throws MessengerArgumentNotSpecified, MemberNotFoundException, UserNotFoundException, GroupNotFoundException {
        MemberEntity memberEntity = checkMemberIsNotNull(messengerDto);
        GroupEntity groupEntity = checkGroupIsNotNull(messengerDto);

        if (memberEntity != null && groupEntity == null) {
            messengerEntity.setMember(
                    findMember(memberEntity));

        } else if (groupEntity != null && memberEntity == null) {
            messengerEntity.setGroup(
                    findGroup(groupEntity));

        } else
            throw new MessengerArgumentNotSpecified(MessengerInfoStatusEnum.NOT_SPECIFIED_MEMBER_OR_GROUP.toString());

        return messengerEntity;
    }

    private MemberEntity checkMemberIsNotNull(MessengerEntity messengerEntity) {
        return messengerCrudFinder.checkMemberIsNotNullAndGet(messengerEntity);
    }

    private GroupEntity checkGroupIsNotNull(MessengerEntity messengerEntity) {
        return messengerCrudFinder.checkGroupIsNotNullAndGet(messengerEntity);
    }

    private MemberEntity findMember(MemberEntity member) throws MemberNotFoundException, UserNotFoundException {
        return messengerCrudFinder.findMember(member);
    }

    private GroupEntity findGroup(GroupEntity group) throws GroupNotFoundException {
        return messengerCrudFinder.findGroup(group);
    }
}
