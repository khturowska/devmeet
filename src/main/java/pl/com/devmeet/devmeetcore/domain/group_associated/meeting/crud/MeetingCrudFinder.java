package pl.com.devmeet.devmeetcore.domain.group_associated.meeting.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingNotFoundException;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class MeetingCrudFinder {

    private MeetingCrudRepository meetingRepository;

    private MeetingGroupFinder meetingGroupFinder;

    public MeetingEntity findByIdOrFeatures(MeetingEntity meetingEntity) throws MeetingNotFoundException, GroupNotFoundException {
        Long meetingId = meetingEntity.getId();
        if (meetingId != null)
            return findById(meetingId);
        else
            return findByMeetingNameAndGroup(meetingEntity);
    }

    public MeetingEntity findById(Long id) throws MeetingNotFoundException {
        if (id != null)
            return meetingRepository.findById(id)
                    .orElseThrow(() -> new MeetingNotFoundException(id));
        else
            throw new MeetingNotFoundException(MeetingCrudStatusEnum.NULL_ID.toString());
    }

    public MeetingEntity findByMeetingNameAndGroup(MeetingEntity meetingEntity) throws GroupNotFoundException, MeetingNotFoundException {
        return meetingRepository.findByMeetingNameAndGroup(
                meetingEntity.getMeetingName(),
                meetingGroupFinder.findGroup(meetingEntity.getGroup()))
                .orElseThrow(() -> new MeetingNotFoundException(MeetingCrudStatusEnum.NOT_FOUND.toString()
                        + " : meetingName=" + meetingEntity.getMeetingName()
                        + "; groupId=" + meetingEntity.getGroup().getId()));
    }

    public MeetingEntity findByName(String meetingName) throws MeetingNotFoundException {
        return meetingRepository.findByMeetingName(meetingName)
                .orElseThrow(MeetingNotFoundException::new);
    }

    public List<MeetingEntity> findByIsActive(boolean isActive) {
        return meetingRepository.findByIsActive(isActive);
    }

    public List<MeetingEntity> findAll() {
        return meetingRepository.findAll();
    }

    public List<MeetingEntity> findMeetingEntitiesByGroup(GroupEntity groupEntity) throws GroupNotFoundException, MeetingNotFoundException {
        GroupEntity foundGroup = meetingGroupFinder.findGroup(groupEntity);
        return meetingRepository.findAllByGroup(foundGroup);
    }

    public List<MeetingEntity> findMeetingEntitiesByGroupId(Long groupId) throws MeetingNotFoundException {
        return meetingRepository.findAllByGroup_Id(groupId);
    }

    public List<MeetingEntity> findMeetingEntitiesByPlaceId(Long placeId) throws MeetingNotFoundException {
        return meetingRepository.findAllByPlace_Id(placeId);
    }


}