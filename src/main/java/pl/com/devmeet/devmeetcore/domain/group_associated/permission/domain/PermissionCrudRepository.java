package pl.com.devmeet.devmeetcore.domain.group_associated.permission.domain;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.UUID;

public interface PermissionCrudRepository extends PagingAndSortingRepository<PermissionEntity, UUID> {

//    Optional<PermissionEntity> findByMemberAndGroup(MemberEntity member, GroupEntity group);
}
