package pl.com.devmeet.devmeetcore.domain.messenger_associated.message.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudRepository;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberRepository;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.message.status_and_exceptions.MessageArgumentNotSpecifiedException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.message.status_and_exceptions.MessageFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.message.status_and_exceptions.MessageNotFoundException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerRepository;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserRepository;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.util.List;
import java.util.Optional;

@Service
public class MessageCrudService {

    private MessageRepository messageRepository;

    private MessengerRepository messengerRepository;
    private GroupCrudRepository groupCrudRepository;
    private MemberRepository memberRepository;
    private UserRepository userRepository;

    @Autowired
    public MessageCrudService(MessageRepository messageRepository, MessengerRepository messengerRepository, GroupCrudRepository groupCrudRepository, MemberRepository memberRepository, UserRepository userRepository) {
        this.messageRepository = messageRepository;
        this.messengerRepository = messengerRepository;
        this.groupCrudRepository = groupCrudRepository;
        this.memberRepository = memberRepository;
        this.userRepository = userRepository;
    }

    private MessengerFinder initMessengerFinder() {
        return MessengerFinder.builder()
                .messengerRepository(messengerRepository)
                .userRepository(userRepository)
                .memberRepository(memberRepository)
                .groupCrudRepository(groupCrudRepository)
                .build();
    }

    private MessageCrudFinder initFinder() {
        return MessageCrudFinder.builder()
                .repository(messageRepository)
                .messengerFinder(initMessengerFinder())
                .build();
    }

    private MessageCrudCreator initCreator() {
        return MessageCrudCreator.builder()
                .messengerFinder(initMessengerFinder())
                .messageCrudSaver(initSaver())
                .messageCrudFinder(initFinder())
                .build();
    }

    private MessageCrudSaver initSaver() {
        return MessageCrudSaver.builder()
                .messageRepository(messageRepository)
                .build();
    }

    private MessageCrudUpdater initUpdater() {
        return MessageCrudUpdater.builder()
                .messageCrudSaver(initSaver())
                .messageCrudFinder(initFinder())
                .build();
    }

    private MessageCrudDeactivator initDeleter() {
        return MessageCrudDeactivator.builder()
                .messageCrudSaver(initSaver())
                .messageCrudFinder(initFinder())
                .build();
    }

    public MessageEntity add(MessageEntity dto) throws UserNotFoundException, GroupNotFoundException, MessengerNotFoundException, MemberNotFoundException, MessageArgumentNotSpecifiedException {
        return initCreator().createEntity(dto);
    }

    //todo zmienic optional na wyjatek
    public Optional<MessageEntity> findById(Long id) {
        return initFinder().findById(id);
    }

    public List<MessageEntity> readAllGroupMessages(MessageEntity dto) throws UserNotFoundException, MessengerNotFoundException, MemberNotFoundException, GroupNotFoundException, MessageNotFoundException, MessageArgumentNotSpecifiedException {
        return initFinder().findEntitiesByGroupForGroupChat(dto);
    }

    public MessageEntity update(MessageEntity oldDto, MessageEntity newDto) throws MessageNotFoundException, GroupNotFoundException, MessengerNotFoundException, UserNotFoundException, MessageArgumentNotSpecifiedException, MemberNotFoundException {
        return initUpdater().updateEntity(oldDto, newDto);
    }

    public MessageEntity delete(MessageEntity dto) throws MessageNotFoundException, GroupNotFoundException, MessengerNotFoundException, UserNotFoundException, MessageArgumentNotSpecifiedException, MemberNotFoundException, MessageFoundButNotActiveException {
        return initDeleter().deactivateEntity(dto);
    }

    public MessageEntity find(MessageEntity dto) throws MessageNotFoundException, GroupNotFoundException, MessengerNotFoundException, UserNotFoundException, MessageArgumentNotSpecifiedException, MemberNotFoundException {
        return initFinder().findEntity(dto);
    }

    public List<MessageEntity> findAll(MessageEntity dto) throws MessageNotFoundException, GroupNotFoundException, MessengerNotFoundException, UserNotFoundException, MessageArgumentNotSpecifiedException, MemberNotFoundException {
        return initFinder().findEntities(dto);
    }
}