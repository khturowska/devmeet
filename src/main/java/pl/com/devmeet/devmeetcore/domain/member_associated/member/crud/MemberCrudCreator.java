package pl.com.devmeet.devmeetcore.domain.member_associated.member.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberUserNotActiveException;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.time.LocalDateTime;

@Builder
@NoArgsConstructor
@AllArgsConstructor
class MemberCrudCreator {

    private MemberCrudFinder memberFinder;
    private MemberCrudSaver saver;
    private MemberUserFinder memberUserFinder;
    private MemberMessengerCreator memberMessengerCreator;

    public MemberEntity createEntity(MemberEntity dto) throws MemberAlreadyExistsException, UserNotFoundException, MemberUserNotActiveException {
        MemberEntity memberEntity;
        UserEntity foundUser = findUser(dto);

        if (foundUser.isActive()) {
            try {
                memberEntity = memberFinder.findEntityByIdOrFeatures(dto);

                if (!memberEntity.isActive())
                    return saver.saveEntity(
                            setDefaultValuesIfMemberExistButNotActive(dto));

            } catch (MemberNotFoundException e) {
                memberEntity = saver.saveEntity(
                        setDefaultValuesIfMemberNotExist(
                                connectMemberWithUser(dto, foundUser)));
                return memberEntity;
            }
            throw new MemberAlreadyExistsException(MemberCrudStatusEnum.MEMBER_ALREADY_EXIST.toString());
        }
        throw new MemberUserNotActiveException(MemberCrudStatusEnum.MEMBER_USER_NOT_ACTIVE.toString());
    }

    private MemberEntity setDefaultValuesIfMemberNotExist(MemberEntity entity) {
        entity.setActive(true);
        entity.setCreationTime(LocalDateTime.now());

        return entity;
    }

    private MemberEntity setDefaultValuesIfMemberExistButNotActive(MemberEntity entity) {
        entity.setActive(true);
        entity.setModificationTime(LocalDateTime.now());

        return entity;
    }

    private MemberEntity connectMemberWithUser(MemberEntity memberEntity, UserEntity userEntity) throws UserNotFoundException {
        memberEntity.setUser(userEntity);
        return memberEntity;
    }

    private UserEntity findUser(MemberEntity memberEntity) throws UserNotFoundException {
        return memberUserFinder.findUserEntity(memberEntity.getUser());
    }
}