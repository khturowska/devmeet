package pl.com.devmeet.devmeetcore.domain.place.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.place.models.PlaceEntity;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceNotFoundException;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class PlaceCrudCreator {

    private PlaceCrudSaver placeCrudSaver;
    private PlaceCrudFinder placeCrudFinder;
    private PlaceMemberFinder placeMemberFinder;

    public PlaceEntity createEntity(PlaceEntity dto) throws PlaceAlreadyExistsException {
        PlaceEntity place;
        try {
            place = placeCrudFinder.findPlaceFeatures(dto);

            if (!place.isActive() && place.getModificationTime() != null)
                return placeCrudSaver.saveEntity(setDefaultValuesWhenPlaceExists(place));

        } catch (PlaceNotFoundException e) {
            place = setDefaultValuesWhenPlaceNotExists(dto);
            return placeCrudSaver.saveEntity(place);
        }

        throw new PlaceAlreadyExistsException(PlaceCrudStatusEnum.PLACE_ALREADY_EXISTS.toString());
    }

    private PlaceEntity setDefaultValuesWhenPlaceNotExists(PlaceEntity place) {
        place.setCreationTime(LocalDateTime.now());
        place.setActive(true);
        return place;
    }

    private PlaceEntity setDefaultValuesWhenPlaceExists(PlaceEntity place) {
        place.setModificationTime(LocalDateTime.now());
        place.setActive(true);
        return place;

    }
}
