package pl.com.devmeet.devmeetcore.domain.group_associated.permission.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.group_associated.permission.domain.status_and_exceptions.PermissionMethodNotImplemented;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class PermissionCrudFinder {

    private PermissionCrudRepository permissionRepository;
    private PermissionGroupFinder groupFinder;
    private PermissionMemberFinder memberFinder;

//    @Override
//    public PermissionEntity findEntity(PermissionDto dto) throws PermissionNotFoundException, MemberNotFoundException, UserNotFoundException, GroupNotFoundException {
//        Optional<PermissionEntity> permission = findPermission(dto);
//
//        if (permission.isPresent())
//            return permission.get();
//        else
//            throw new PermissionNotFoundException(PermissionCrudStatusEnum.PERMISSION_NOT_FOUND.toString());
//    }

    private MemberEntity findMemberEntity(MemberEntity member) throws MemberNotFoundException, UserNotFoundException {
        return memberFinder.findMember(member);
    }

    private GroupEntity findGroupEntity(GroupEntity group) throws GroupNotFoundException {
        return groupFinder.findGroup(group);
    }

//    private Optional<PermissionEntity> findPermission(PermissionDto dto) throws MemberNotFoundException, UserNotFoundException, GroupNotFoundException {
//        MemberEntity member = findMemberEntity(dto.getMember());
//        GroupEntity group = findGroupEntity(dto.getGroup());
//
//        return permissionRepository.findByMemberAndGroup(member, group);
//    }

    public List<PermissionEntity> findEntities(PermissionDto dto) throws PermissionMethodNotImplemented {
        throw new PermissionMethodNotImplemented("Method not implemented");
    }

//    @Override
//    public boolean isExist(PermissionDto dto) {
//        try {
//            return findPermission(dto).isPresent();
//        } catch (MemberNotFoundException | UserNotFoundException | GroupNotFoundException e) {
//            return false;
//        }
//    }
}
