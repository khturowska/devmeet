package pl.com.devmeet.devmeetcore.domain.group_associated.meeting.crud;

import lombok.AllArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;

@AllArgsConstructor
class MeetingGroupFinder {

    private GroupCrudService groupService;

    public GroupEntity findGroup(GroupEntity groupEntity) throws GroupNotFoundException {
        return groupService.findByGroup(groupEntity);
    }

    public GroupEntity findGroup(Long groupId) throws GroupNotFoundException {
        return groupService.findById(groupId);
    }
}
