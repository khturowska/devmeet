package pl.com.devmeet.devmeetcore.domain.place.crud;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudRepository;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberRepository;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerRepository;
import pl.com.devmeet.devmeetcore.domain.place.models.PlaceEntity;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserRepository;

import java.util.List;

@Service
public class PlaceCrudService {

    private PlaceCrudRepository placeRepository;
    private MemberRepository memberRepository;
    private UserRepository userRepository;
    private MessengerRepository messengerRepository;
    private GroupCrudRepository groupCrudRepository;

    @Autowired
    public PlaceCrudService(PlaceCrudRepository placeRepository, MemberRepository memberRepository, UserRepository userRepository, MessengerRepository messengerRepository, GroupCrudRepository groupCrudRepository) {
        this.placeRepository = placeRepository;
        this.memberRepository = memberRepository;
        this.userRepository = userRepository;
        this.messengerRepository = messengerRepository;
        this.groupCrudRepository = groupCrudRepository;
    }


    private PlaceMemberFinder initMemberFinder() {
        return PlaceMemberFinder.builder()
                .memberCrudService(new MemberCrudService(memberRepository, userRepository, messengerRepository, groupCrudRepository))
                .build();
    }


    private PlaceCrudCreator initCreator() {
        return PlaceCrudCreator.builder()
                .placeCrudFinder(initFinder())
                .placeCrudSaver(initSaver())
                .placeMemberFinder(initMemberFinder())
                .build();
    }

    private PlaceCrudSaver initSaver() {
        return PlaceCrudSaver.builder()
                .placeCrudRepository(placeRepository)
                .memberFinder(initMemberFinder())
                .build();
    }

    private PlaceCrudFinder initFinder() {
        return PlaceCrudFinder.builder()
                .placeRepository(placeRepository)
                .memberFinder(initMemberFinder())
                .build();
    }

    private PlaceCrudUpdater initUpdater() {
        return PlaceCrudUpdater.builder()
                .placeCrudFinder(initFinder())
                .placeCrudSaver(initSaver())
                .build();
    }

    private PlaceCrudDeleter initDeleter() {

        return PlaceCrudDeleter.builder()
                .placeCrudFinder(initFinder())
                .placeCrudSaver(initSaver())
                .build();
    }

    public PlaceEntity findPlaceById(Long id) throws PlaceNotFoundException {
        return initFinder().findPlaceById(id);
    }

    public List<PlaceEntity> findAllBySearchingText(String text) {
        return initFinder().findAllBySearchingText(text);
    }

    public List<PlaceEntity> findAll() {
        return initFinder().findAllEntities();
    }

    public PlaceEntity findPlaceByIdOrFeatures(PlaceEntity placeEntity) throws PlaceNotFoundException {
        return initFinder().findPlaceByIdOrPlaceFutures(placeEntity);
    }

    public boolean isExist(PlaceEntity dto) {
        return initFinder().isExist(dto);
    }

    public PlaceEntity add(PlaceEntity dto) throws PlaceAlreadyExistsException {
        return initCreator().createEntity(dto);
    }

    public PlaceEntity update(PlaceEntity toUpdate) throws PlaceNotFoundException {
        return initUpdater().updateEntity(toUpdate);
    }

    public PlaceEntity delete(PlaceEntity dto) throws PlaceNotFoundException, PlaceFoundButNotActiveException {
        return initDeleter().deactivateEntity(dto);
    }
}
