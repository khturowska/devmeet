package pl.com.devmeet.devmeetcore.domain.user.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class UserCrudDeactivator {

    private UserCrudSaver userSaver;
    private UserCrudFinder userFinder;

    public UserEntity deactivateById(Long userId) throws UserNotFoundException, UserFoundButNotActiveException {
        UserEntity found = userFinder.findById(userId);
        return defaultDeactivation(found);
    }

    public UserEntity deactivate(UserEntity dto) throws UserNotFoundException, UserFoundButNotActiveException {
        UserEntity found = userFinder.findEntityByIdOrUserEmail(dto);
        return defaultDeactivation(found);
    }

    private UserEntity defaultDeactivation(UserEntity found) throws UserFoundButNotActiveException {
        if (found.isActive()) {
            found.setActive(false);
            found.setModificationTime(LocalDateTime.now());
            return saveUserEntity(found);
        }
        throw new UserFoundButNotActiveException();
    }

    private UserEntity saveUserEntity(UserEntity entity) {
        return userSaver.saveEntity(entity);
    }
}
