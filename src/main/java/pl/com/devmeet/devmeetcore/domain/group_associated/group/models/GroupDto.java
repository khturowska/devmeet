package pl.com.devmeet.devmeetcore.domain.group_associated.group.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberApiDto;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GroupDto {

    private Long id;
    private String groupName;
    private String website;
    private String description;
    private MemberApiDto founder;
    private Integer membersLimit;
    private String creationTime; // date and time format: DateTimeFormatter.ISO_LOCAL_DATE_TIME

}
