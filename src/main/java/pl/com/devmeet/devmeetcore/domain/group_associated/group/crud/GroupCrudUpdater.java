package pl.com.devmeet.devmeetcore.domain.group_associated.group.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupException;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class GroupCrudUpdater {

    private GroupCrudSaver groupCrudSaver;
    private GroupCrudFinder groupCrudFinder;

    public GroupEntity updateEntity(GroupEntity update) throws GroupNotFoundException, GroupFoundButNotActiveException {
        GroupEntity founded = checkIsOldGroupActive(
                groupCrudFinder.findById(update.getId())
        );
        return groupCrudSaver.saveEntity(
                updateAllowedParameters(founded, update)
        );
    }

    private GroupEntity checkIsOldGroupActive(GroupEntity oldGroup) throws GroupFoundButNotActiveException {
        if (oldGroup.isActive())
            return oldGroup;
        else
            throw new GroupFoundButNotActiveException(GroupCrudStatusEnum.GROUP_FOUND_BUT_NOT_ACTIVE.toString());
    }

    private GroupEntity checkIsNewGroupHasAName(GroupEntity newGroup, GroupEntity oldGroup) throws GroupException {
        if (newGroup.getGroupName().equals(oldGroup.getGroupName()))
            return newGroup;

        throw new GroupException(GroupCrudStatusEnum.GROUP_INCORRECT_VALUES.toString());
    }

    private GroupEntity updateAllowedParameters(GroupEntity oldEntity, GroupEntity newEntity) {
        oldEntity.setGroupName(newEntity.getGroupName());
        oldEntity.setWebsite(newEntity.getWebsite() != null ? newEntity.getWebsite() : oldEntity.getWebsite());
        oldEntity.setDescription(newEntity.getDescription() != null ? newEntity.getDescription() : oldEntity.getDescription());
        oldEntity.setMembersLimit(newEntity.getMembersLimit() != null ? newEntity.getMembersLimit() : oldEntity.getMembersLimit());

        oldEntity.setModificationTime(LocalDateTime.now());
        return oldEntity;
    }
}
