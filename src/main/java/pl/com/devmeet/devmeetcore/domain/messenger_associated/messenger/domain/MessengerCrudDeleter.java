package pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerInfoStatusEnum;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

@NoArgsConstructor
@AllArgsConstructor
@Builder
class MessengerCrudDeleter {

    private MessengerCrudFinder messengerCrudFinder;
    private MessengerCrudSaver messengerCrudSaver;

    public MessengerEntity delete(MessengerEntity messengerDto) throws MessengerNotFoundException, MessengerAlreadyExistsException, MemberNotFoundException, UserNotFoundException, GroupNotFoundException {
        MessengerEntity messengerEntity = messengerCrudFinder.findEntity(messengerDto);

        if (messengerEntity.isActive()) {
            messengerEntity.setActive(false);

            return saveMessengerEntity(messengerEntity);
        }

        throw new MessengerAlreadyExistsException(MessengerInfoStatusEnum.MESSENGER_FOUND_BUT_NOT_ACTIVE.toString());
    }

    private MessengerEntity saveMessengerEntity(MessengerEntity entity) {
        return messengerCrudSaver.saveEntity(entity);
    }
}
