package pl.com.devmeet.devmeetcore.domain.group_associated.meeting.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingNotFoundException;
import pl.com.devmeet.devmeetcore.domain.place.models.PlaceEntity;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceNotFoundException;

import java.time.LocalDateTime;

//@Component
@AllArgsConstructor
@NoArgsConstructor
@Builder
class MeetingCrudCreator {

    private MeetingCrudFinder meetingCrudFinder;
    private MeetingCrudSaver meetingCrudSaver;

    private MeetingGroupFinder groupFinder;
    private MeetingPlaceFinder placeFinder;

    public MeetingEntity createEntity(MeetingEntity dto) throws MeetingAlreadyExistsException, PlaceNotFoundException, GroupNotFoundException {
        GroupEntity foundGroup = findGroupIfExist(dto.getGroup());
        PlaceEntity foundPlace = findPlaceIfExist(dto.getPlace());

        try {
            MeetingEntity meeting = meetingCrudFinder.findByIdOrFeatures(dto);

            if (!meeting.isActive())
                return meetingCrudSaver.saveEntity(
                        setDefaultValuesIfExist(meeting));

        } catch (MeetingNotFoundException e) {
            dto.setGroup(foundGroup);
            dto.setPlace(foundPlace);
            return meetingCrudSaver.saveEntity(
                    setDefaultValuesIfNotExist(dto));
        }
        throw new MeetingAlreadyExistsException(MeetingCrudStatusEnum.ALREADY_EXIST.toString());
    }

    private GroupEntity findGroupIfExist(GroupEntity groupEntity) throws GroupNotFoundException {
        return groupFinder.findGroup(groupEntity);
    }

    private PlaceEntity findPlaceIfExist(PlaceEntity placeEntity) throws PlaceNotFoundException {
        return placeFinder.findPlace(placeEntity);
    }

    private MeetingEntity setDefaultValuesIfNotExist(MeetingEntity meetingEntity) {
        meetingEntity.setCreationTime(LocalDateTime.now());
        meetingEntity.setActive(true);
        return meetingEntity;
    }

    private MeetingEntity setDefaultValuesIfExist(MeetingEntity meetingEntity) {
        meetingEntity.setActive(true);
        return meetingEntity;
    }

}
