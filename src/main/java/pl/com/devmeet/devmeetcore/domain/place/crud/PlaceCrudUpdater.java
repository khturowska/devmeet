package pl.com.devmeet.devmeetcore.domain.place.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.place.models.PlaceEntity;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceNotFoundException;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class PlaceCrudUpdater {

    private PlaceCrudSaver placeCrudSaver;
    private PlaceCrudFinder placeCrudFinder;

    public PlaceEntity updateEntity(PlaceEntity update) throws PlaceNotFoundException {
        PlaceEntity oldPlace = findPlaceEntity(update);

        return placeCrudSaver.saveEntity(
                updateAllowedParameters(oldPlace, update));
    }

    private PlaceEntity findPlaceEntity(PlaceEntity oldDto) throws PlaceNotFoundException {
        return placeCrudFinder.findPlaceById(oldDto.getId());
    }

    private PlaceEntity updateAllowedParameters(PlaceEntity oldEntity, PlaceEntity newEntity) {
        oldEntity.setPlaceName(newEntity.getPlaceName() != null ? newEntity.getPlaceName() : oldEntity.getPlaceName());
        oldEntity.setDescription(newEntity.getDescription() != null ? newEntity.getDescription() : oldEntity.getDescription());
        oldEntity.setWebsite(newEntity.getWebsite() != null ? newEntity.getWebsite() : oldEntity.getWebsite());
        oldEntity.setLocation(newEntity.getLocation() != null ? newEntity.getLocation() : oldEntity.getLocation());
        oldEntity.setModificationTime(LocalDateTime.now());
        return oldEntity;
    }
}
