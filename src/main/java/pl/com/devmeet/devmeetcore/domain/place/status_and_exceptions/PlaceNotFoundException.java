package pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions;

public class PlaceNotFoundException extends Exception {

    public PlaceNotFoundException() {
        super(PlaceCrudStatusEnum.PLACE_NOT_FOUND.toString());
    }

    public PlaceNotFoundException(String message) {
        super(message);
    }

    public PlaceNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
