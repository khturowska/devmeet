package pl.com.devmeet.devmeetcore.domain.group_associated.permission.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.group_associated.permission.domain.status_and_exceptions.PermissionAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.group_associated.permission.domain.status_and_exceptions.PermissionCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.group_associated.permission.domain.status_and_exceptions.PermissionNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class PermissionCrudDeleter {

    private PermissionCrudFinder permissionCrudFinder;
    private PermissionCrudSaver permissionCrudSaver;

    public PermissionEntity deactivateEntity(PermissionDto dto) throws PermissionAlreadyExistsException, UserNotFoundException, GroupNotFoundException, MemberNotFoundException, PermissionNotFoundException {
//        PermissionEntity permissionEntity = permissionCrudFinder.findEntity(dto);
//        boolean permissionActivity = permissionEntity.isActive();
//
//        if (permissionActivity) {
//            permissionEntity.setActive(false);
//            permissionEntity.setModificationTime(DateTime.now());
//
//            return permissionCrudSaver.saveEntity(permissionEntity);
//        }

        throw new PermissionAlreadyExistsException(PermissionCrudStatusEnum.PERMISSION_FOUND_BUT_NOT_ACTIVE.toString());
    }
}
