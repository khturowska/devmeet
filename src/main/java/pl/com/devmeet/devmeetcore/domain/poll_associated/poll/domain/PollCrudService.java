package pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudRepository;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberRepository;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerRepository;
import pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.status_and_exceptions.PollAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.status_and_exceptions.PollNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserRepository;

import java.util.Optional;

@Service
public class PollCrudService {

    private PollCrudRepository pollCrudRepository;
    private GroupCrudRepository groupCrudRepository;
    private MemberRepository memberRepository;
    private UserRepository userRepository;
    private MessengerRepository messengerRepository;

    @Autowired
    public PollCrudService(PollCrudRepository pollCrudRepository, GroupCrudRepository groupCrudRepository, MemberRepository memberRepository, UserRepository userRepository, MessengerRepository messengerRepository) {
        this.pollCrudRepository = pollCrudRepository;
        this.groupCrudRepository = groupCrudRepository;
        this.memberRepository = memberRepository;
        this.userRepository = userRepository;
        this.messengerRepository = messengerRepository;
    }

    private PollGroupFinder initGroupFinder() {
        return PollGroupFinder.builder()
                .groupCrudService(new GroupCrudService(groupCrudRepository, memberRepository, userRepository, messengerRepository))
                .build();
    }

    private PollCrudSaver initSaver() {
        return PollCrudSaver.builder()
                .pollCrudRepository(pollCrudRepository)
                .build();
    }

    private PollCrudFinder initFinder() {
        return PollCrudFinder.builder()
                .pollCrudRepository(pollCrudRepository)
                .groupFinder(initGroupFinder())
                .build();
    }

    private PollCrudCreator initCreator() {
        return PollCrudCreator.builder()
                .pollCrudSaver(initSaver())
                .pollCrudFinder(initFinder())
                .pollGroupFinder(initGroupFinder())
                .build();
    }

    private PollCrudDeleter initDeleter() {
        return PollCrudDeleter.builder()
                .pollCrudSaver(initSaver())
                .pollCrudFinder(initFinder())
                .build();
    }

    public PollEntity add(PollEntity dto) throws PollAlreadyExistsException, GroupNotFoundException {
        return initCreator().createEntity(dto);
    }

    //todo przerobic optional na wyjatek
    public Optional<PollEntity> findById(Long id) {
        return initFinder().findById(id);
    }


    public PollEntity delete(PollEntity dto) throws GroupNotFoundException, PollNotFoundException, PollAlreadyExistsException {
        return initDeleter().deactivateEntity(dto);
    }

    public PollEntity find(PollEntity dto) throws GroupNotFoundException, PollNotFoundException {
        return initFinder().findEntity(dto);
    }
}
