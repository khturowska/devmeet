package pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions;

public class GroupNotFoundException extends Exception {

    public GroupNotFoundException() {
        super(GroupCrudStatusEnum.GROUP_NOT_FOUND.toString());
    }

    public GroupNotFoundException(String message) {
        super(message);
    }

    public GroupNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
