package pl.com.devmeet.devmeetcore.domain.group_associated.permission.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

@Builder
@AllArgsConstructor
@NoArgsConstructor
class PermissionCrudSaver {

    private PermissionCrudRepository permissionCrudRepository;
    private PermissionGroupFinder groupFinder;
    private PermissionMemberFinder memberFinder;

    public PermissionEntity saveEntity(PermissionEntity entity) throws GroupNotFoundException, MemberNotFoundException, UserNotFoundException {
        return permissionCrudRepository
                .save(entity);
    }

//    private PermissionEntity connectPermissionWithGroup(PermissionEntity permissionEntity) throws GroupNotFoundException {
//        GroupEntity groupEntity = permissionEntity.getGroup();
//
//        if (groupEntity.getId() == null)
//            groupEntity = groupFinder.findGroup(GroupCrudService.map(permissionEntity.getGroup()));
//
//        permissionEntity.setGroup(groupEntity);
//        return permissionEntity;
//    }
//
//    private PermissionEntity connectPermissionWithMember(PermissionEntity permissionEntity) throws MemberNotFoundException, UserNotFoundException {
//        MemberEntity memberEntity = permissionEntity.getMember();
//
//        if (memberEntity.getId() == null)
//            memberEntity = memberFinder.findMember(MemberCrudService.map(permissionEntity.getMember()));
//
//        permissionEntity.setMember(memberEntity);
//        return permissionEntity;
//    }
}
