package pl.com.devmeet.devmeetcore.domain.member_associated.member.crud;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;

@RequiredArgsConstructor
class MemberCrudSaver {

    @NonNull
    private MemberRepository memberRepository;

    public MemberEntity saveEntity(MemberEntity entity) {
        return memberRepository.save(entity);
    }
}