package pl.com.devmeet.devmeetcore.domain.group_associated.group.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerArgumentNotSpecified;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class GroupCrudCreator {

    private GroupCrudSaver groupCrudSaver;
    private GroupCrudFinder groupCrudFinder;
    private GroupMessengerCreator groupMessengerCreator;

    public GroupEntity createEntity(GroupEntity dto) throws GroupAlreadyExistsException {
        GroupEntity group;

        try {
            group = groupCrudFinder.findEntityByGroup(dto);

            if (!group.isActive() && group.getModificationTime() != null)
                return groupCrudSaver.saveEntity(setDefaultValuesWhenGroupExists(group));

        } catch (GroupNotFoundException e) {
            group = groupCrudSaver.saveEntity(
                    setDefaultValuesWhenGroupNotExists(setDefaultLimits(dto)));
//            createMessengerForGroup(dto);

            return group;
        }

        throw new GroupAlreadyExistsException(GroupCrudStatusEnum.GROUP_ALREADY_EXISTS.toString());
    }

    private GroupEntity setDefaultValuesWhenGroupNotExists(GroupEntity group) {
        group.setCreationTime(LocalDateTime.now());
        group.setActive(true);
        return group;
    }

    private GroupEntity setDefaultValuesWhenGroupExists(GroupEntity entity) {
        entity.setModificationTime(LocalDateTime.now());
        entity.setActive(true);
        return entity;
    }

    private GroupEntity setDefaultLimits(GroupEntity group) {
        group.setMembersLimit(group.getMembersLimit() != null ? group.getMembersLimit() : 0);
        return group;
    }

    private void createMessengerForGroup(GroupEntity groupEntity) throws UserNotFoundException, MemberNotFoundException, MessengerAlreadyExistsException, GroupNotFoundException, MessengerArgumentNotSpecified {
        groupMessengerCreator.createMessenger(groupEntity);
    }
}
