package pl.com.devmeet.devmeetcore.domain.member_associated.member.models;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MemberApiDto {

    private Long id;
    private Long user;
    private String nick;

}
