package pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerInfoStatusEnum;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.util.Optional;

@NoArgsConstructor
@AllArgsConstructor
@Builder
class MessengerCrudFinder {

    private MessengerRepository messengerRepository;
    private MessengerGroupFinder groupFinder;
    private MessengerMemberFinder memberFinder;

    public MessengerEntity findEntity(MessengerEntity dto) throws MessengerNotFoundException, MemberNotFoundException, UserNotFoundException, GroupNotFoundException {
        MemberEntity foundMember;
        GroupEntity foundGroup;
        MemberEntity memberEntity;
        GroupEntity groupEntity;

        if (dto.getId() != null)
            return findById(dto.getId())
                    .orElseThrow(() -> new MessengerNotFoundException(MessengerInfoStatusEnum.MESSENGER_NOT_FOUND_BY_ID.toString()));

        else {
            memberEntity = checkMemberIsNotNullAndGet(dto);
            groupEntity = checkGroupIsNotNullAndGet(dto);

            if (memberEntity != null && groupEntity == null) {
                foundMember = findMember(memberEntity);
                return findMemberMessenger(foundMember);

            } else if (groupEntity != null && memberEntity == null) {
                foundGroup = findGroup(groupEntity);
                return findGroupMessenger(foundGroup);
            }

            throw new MessengerNotFoundException(MessengerInfoStatusEnum.NOT_SPECIFIED_MEMBER_OR_GROUP.toString());
        }
    }

    public MemberEntity checkMemberIsNotNullAndGet(MessengerEntity messengerEntity) {
        try {
            return messengerEntity.getMember();
        } catch (NullPointerException e) {
            return null;
        }
    }

    public GroupEntity checkGroupIsNotNullAndGet(MessengerEntity messengerEntity) {
        try {
            return messengerEntity.getGroup();
        } catch (NullPointerException e) {
            return null;
        }
    }

    public MemberEntity findMember(MemberEntity dto) throws MemberNotFoundException, UserNotFoundException {
        return memberFinder.findMember(dto);
    }

    public GroupEntity findGroup(GroupEntity dto) throws GroupNotFoundException {
        return groupFinder.findGroup(dto);
    }

    private MessengerEntity findMemberMessenger(MemberEntity memberEntity) throws MessengerNotFoundException {
        Optional<MessengerEntity> foundMessenger = messengerRepository.findByMember(memberEntity);

        if (foundMessenger.isPresent())
            return foundMessenger.get();

        throw new MessengerNotFoundException(MessengerInfoStatusEnum.MESSENGER_NOT_FOUND_BY_MEMBER.toString());
    }

    private MessengerEntity findGroupMessenger(GroupEntity groupEntity) throws MessengerNotFoundException {
        Optional<MessengerEntity> foundMessenger = messengerRepository.findByGroup(groupEntity);

        if (foundMessenger.isPresent())
            return foundMessenger.get();

        throw new MessengerNotFoundException(MessengerInfoStatusEnum.MESSENGER_NOT_FOUND_BY_GROUP.toString());
    }

    public boolean isExist(MessengerEntity dto) {
        try {
            return findEntity(dto) != null;
        } catch (MessengerNotFoundException | MemberNotFoundException | UserNotFoundException | GroupNotFoundException e) {
            return false;
        }
    }

    public Optional<MessengerEntity> findById(Long id) {
        return messengerRepository.findById(id);
    }
}