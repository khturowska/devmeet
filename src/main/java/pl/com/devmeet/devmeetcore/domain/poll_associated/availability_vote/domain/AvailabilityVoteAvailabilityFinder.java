package pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.crud.AvailabilityCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.models.AvailabilityEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions.AvailabilityNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

@NoArgsConstructor
@AllArgsConstructor
@Builder
class AvailabilityVoteAvailabilityFinder {

    private AvailabilityCrudService availabilityCrudService;

    public AvailabilityEntity findAvailability(AvailabilityEntity dto) throws MemberNotFoundException, AvailabilityNotFoundException, UserNotFoundException {
        return availabilityCrudService.findByEntity(dto);
    }
}
