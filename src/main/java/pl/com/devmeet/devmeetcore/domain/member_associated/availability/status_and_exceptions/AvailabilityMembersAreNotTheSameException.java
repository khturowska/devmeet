package pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions;

public class AvailabilityMembersAreNotTheSameException extends Exception {
    public AvailabilityMembersAreNotTheSameException(String message) {
        super(message);
    }
}
