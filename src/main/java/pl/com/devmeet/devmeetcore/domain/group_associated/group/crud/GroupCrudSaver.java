package pl.com.devmeet.devmeetcore.domain.group_associated.group.crud;

import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;

class GroupCrudSaver {

    private GroupCrudRepository repository;

    public GroupCrudSaver(GroupCrudRepository repository) {
        this.repository = repository;
    }

    public GroupEntity saveEntity(GroupEntity entity) {
        return repository.save(entity);
    }
}
