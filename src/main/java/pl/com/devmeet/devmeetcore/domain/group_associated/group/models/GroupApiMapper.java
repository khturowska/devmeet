package pl.com.devmeet.devmeetcore.domain.group_associated.group.models;

import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberApiMapper;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public class GroupApiMapper {

    public GroupDto mapToFrontend(GroupEntity groupEntity) {
        return groupEntity != null ? GroupDto.builder()
                .id(groupEntity.getId())
                .groupName(groupEntity.getGroupName())
                .website(groupEntity.getWebsite())
                .description(groupEntity.getDescription())
                .membersLimit(groupEntity.getMembersLimit())
                .founder(new MemberApiMapper().mapToFrontend(groupEntity.getFounder()))
                .creationTime(groupEntity.getCreationTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME))
                .build() : null;
    }

    public List<GroupDto> mapToFrontend(List<GroupEntity> groupEntities) {
        return groupEntities != null ? groupEntities.stream()
                .map(this::mapToFrontend)
                .collect(Collectors.toList()) : null;
    }

    public GroupEntity mapToBackend(GroupDto apiDto) {
        return apiDto != null ? GroupEntity.builder()
                .id(apiDto.getId())
                .groupName(apiDto.getGroupName())
                .website(apiDto.getWebsite())
                .description(apiDto.getDescription())
                .membersLimit(apiDto.getMembersLimit())
                .founder(new MemberApiMapper().mapToBackend(apiDto.getFounder()))
                .creationTime(LocalDateTime.parse(apiDto.getCreationTime(), DateTimeFormatter.ISO_LOCAL_DATE_TIME))
                .build() : null;
    }

    public List<GroupEntity> mapToBackend(List<GroupDto> groupDtos) {
        return groupDtos != null ? groupDtos.stream()
                .map(this::mapToBackend)
                .collect(Collectors.toList()) : null;
    }

}
