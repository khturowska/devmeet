package pl.com.devmeet.devmeetcore.domain.member_associated.member.crud;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerCrudService;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerEntity;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

@RequiredArgsConstructor
class MemberMessengerDeactivator {

    @NonNull
    private MessengerCrudService messengerCrudService;

    public MessengerEntity deactivateMessenger(MemberEntity memberEntity) throws UserNotFoundException, GroupNotFoundException, MessengerAlreadyExistsException, MessengerNotFoundException, MemberNotFoundException {
        return messengerCrudService.deactivateMembersMessenger(memberEntity);
    }
}
