package pl.com.devmeet.devmeetcore.domain.user.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "users")
@Entity
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    private String password;

    @Column(unique = true)
    @Email
    @NotNull
    @NotEmpty
    private String email;

    private LocalDateTime modificationTime;
    private LocalDateTime creationTime;

    private UUID activationKey;
    @Column(columnDefinition = "boolean default false")
    private boolean isActive;
}
