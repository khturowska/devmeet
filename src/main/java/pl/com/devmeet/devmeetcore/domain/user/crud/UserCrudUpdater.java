package pl.com.devmeet.devmeetcore.domain.user.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class UserCrudUpdater {

    private UserCrudSaver userSaver;
    private UserCrudFinder userFinder;

    public UserEntity update(UserEntity userEntity) throws UserNotFoundException, UserFoundButNotActiveException {
        UserEntity found = userFinder.findById(userEntity.getId());
        if (found.isActive())
            return saveUserEntity(updateAllowedParameters(found, userEntity));
        throw new UserFoundButNotActiveException("User: '" + found.getEmail() + "' cannot be updated. Please activate it first!");
    }

    private UserEntity updateAllowedParameters(UserEntity existingUser, UserEntity updatedUser) {
        String email = updatedUser.getEmail();
        String password = updatedUser.getPassword();
        boolean modification = false;

        if (isNotEmpty(email)) {
            existingUser.setEmail(updatedUser.getEmail());
            modification = true;
        }
        if (isNotEmpty(password)) {
            existingUser.setPassword(updatedUser.getPassword());
            modification = true;
        }
        if (existingUser.getActivationKey() == null) {
            existingUser.setActivationKey(UUID.randomUUID());
            modification = true;
        }
        if (modification)
            existingUser.setModificationTime(LocalDateTime.now());

        return existingUser;
    }

    private UserEntity saveUserEntity(UserEntity entity) {
        return userSaver.saveEntity(entity);
    }
}
