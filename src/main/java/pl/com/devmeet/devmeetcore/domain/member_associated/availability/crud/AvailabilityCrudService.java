package pl.com.devmeet.devmeetcore.domain.member_associated.availability.crud;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudRepository;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.models.AvailabilityEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions.AvailabilityAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions.AvailabilityFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions.AvailabilityMembersAreNotTheSameException;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions.AvailabilityNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberRepository;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerRepository;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserRepository;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.util.List;

@Slf4j
@Service
public class AvailabilityCrudService {

    private AvailabilityCrudRepository availabilityRepository;
    private MemberRepository memberRepository;
    private UserRepository userRepository;
    private MessengerRepository messengerRepository;
    private GroupCrudRepository groupCrudRepository;

    @Autowired
    public AvailabilityCrudService(AvailabilityCrudRepository availabilityRepository, MemberRepository memberRepository, UserRepository userRepository, MessengerRepository messengerRepository, GroupCrudRepository groupCrudRepository) {
        this.availabilityRepository = availabilityRepository;
        this.memberRepository = memberRepository;
        this.userRepository = userRepository;
        this.messengerRepository = messengerRepository;
        this.groupCrudRepository = groupCrudRepository;
    }

    private AvailabilityMemberFinder initMemberFinder() {
        return AvailabilityMemberFinder.builder()
                .memberCrudService(new MemberCrudService(memberRepository, userRepository, messengerRepository, groupCrudRepository))
                .build();
    }

    private AvailabilityCrudCreator initCreator() {
        return AvailabilityCrudCreator.builder()
                .availabilityCrudFinder(initFinder())
                .availabilityCrudSaver(initSaver())
                .memberFinder(initMemberFinder())
                .build();
    }

    private AvailabilityCrudSaver initSaver() {
        return AvailabilityCrudSaver.builder()
                .availabilityCrudRepository(availabilityRepository)
                .build();
    }

    private AvailabilityCrudFinder initFinder() {
        return AvailabilityCrudFinder.builder()
                .availabilityRepository(availabilityRepository)
                .memberFinder(initMemberFinder())
                .build();
    }

    private AvailabilityCrudUpdater initUpdater() {
        return AvailabilityCrudUpdater.builder()
                .availabilityCrudFinder(initFinder())
                .availabilityCrudSaver(initSaver())
                .build();
    }

    private AvailabilityCrudDeleter initDeleter() {

        return AvailabilityCrudDeleter.builder()
                .availabilityCrudFinder(initFinder())
                .availabilityCrudSaver(initSaver())
                .build();
    }

    public AvailabilityCrudFinder getFinder() {
        return initFinder();
    }

    public AvailabilityEntity add(AvailabilityEntity dto) throws MemberNotFoundException, AvailabilityAlreadyExistsException, UserNotFoundException {
        AvailabilityEntity created = initCreator().createEntity(dto);
        log.info("The Member (id=" + created.getMember().getId() + ") has create new Availability (id=" + created.getId() + ")");
        return created;
    }

    public AvailabilityEntity findByEntity(AvailabilityEntity dto) throws AvailabilityNotFoundException {
        return initFinder().findByEntity(dto);
    }

    public AvailabilityEntity findById(Long id) throws AvailabilityNotFoundException {
        return initFinder().findById(id);
    }

    public List<AvailabilityEntity> findAllMemberAvailabilities(AvailabilityEntity dto) throws MemberNotFoundException, AvailabilityNotFoundException, UserNotFoundException {
        return initFinder().findEntitiesByMember(dto);
    }

    public List<AvailabilityEntity> findAllMemberAvailabilitiesByMemberId(Long memberId) throws MemberNotFoundException, AvailabilityNotFoundException, UserNotFoundException {
        AvailabilityEntity availabilityEntity = AvailabilityEntity.builder()
                .member(MemberEntity.builder().id(memberId).build())
                .build();
        return findAllMemberAvailabilities(availabilityEntity);
    }

    public List<AvailabilityEntity> findAll() {
        return initFinder().findAll();
    }

    public AvailabilityEntity update(AvailabilityEntity newDto) throws UserNotFoundException, MemberNotFoundException, AvailabilityMembersAreNotTheSameException, AvailabilityNotFoundException {
        AvailabilityEntity updated = initUpdater().updateEntity(newDto);
        log.info("The Member (id=" + updated.getMember().getId() + ") has update Availability (id=" + updated.getId() + ")");
        return updated;
    }

    public AvailabilityEntity delete(AvailabilityEntity dto) throws AvailabilityNotFoundException, AvailabilityFoundButNotActiveException {
        return initDeleter().deactivateEntity(dto);
    }

    public AvailabilityEntity deleteById(Long availabilityId) throws AvailabilityFoundButNotActiveException, AvailabilityNotFoundException {
        return initDeleter().deactivateEntityById(availabilityId);
    }

    public List<AvailabilityEntity> findEntities(AvailabilityEntity dto) throws MemberNotFoundException, AvailabilityNotFoundException, UserNotFoundException {
        return initFinder().findEntitiesByMember(dto);
    }
}
