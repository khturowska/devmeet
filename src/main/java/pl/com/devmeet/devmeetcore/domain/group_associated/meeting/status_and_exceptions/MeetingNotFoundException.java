package pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions;

public class MeetingNotFoundException extends Exception {

    public MeetingNotFoundException() {
        super(MeetingCrudStatusEnum.NOT_FOUND.toString());
    }

    public MeetingNotFoundException(Long id) {
        super(MeetingCrudStatusEnum.NOT_FOUND.toString() + " id:" + id);
    }

    public MeetingNotFoundException(String message) {
        super(message);
    }

    public MeetingNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
