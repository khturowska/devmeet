package pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions;

public class UserAlreadyExistsEmailDuplicationException extends Exception {
    public UserAlreadyExistsEmailDuplicationException(String message) {
        super(message);
    }

    public UserAlreadyExistsEmailDuplicationException() {
    }

    public UserAlreadyExistsEmailDuplicationException(String message, Throwable cause) {
        super(message, cause);
    }
}
