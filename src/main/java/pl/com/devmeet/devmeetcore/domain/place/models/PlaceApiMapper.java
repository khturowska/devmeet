package pl.com.devmeet.devmeetcore.domain.place.models;

import java.util.List;
import java.util.stream.Collectors;

public class PlaceApiMapper {

    public PlaceEntity mapToBackend(PlaceDto dto) {
        return dto != null ? PlaceEntity
                .builder()
                .id(dto.getId())
                .placeName(dto.getPlaceName())
                .description(dto.getDescription())
                .website(dto.getWebsite())
                .location(dto.getLocation())
                .build() : null;
    }

    public List<PlaceEntity> mapToBackend(List<PlaceDto> dtos) {
        return dtos != null ? dtos.stream()
                .map(this::mapToBackend)
                .collect(Collectors.toList()) : null;
    }

    public PlaceDto mapToFrontend(PlaceEntity entity) {
        return entity != null ? PlaceDto
                .builder()
                .id(entity.getId())
                .placeName(entity.getPlaceName())
                .description(entity.getDescription())
                .website(entity.getWebsite())
                .location(entity.getLocation())
                .build() : null;
    }

    public List<PlaceDto> mapToFrontend(List<PlaceEntity> entities) {
        return entities != null ? entities.stream()
                .map(this::mapToFrontend)
                .collect(Collectors.toList()) : null;
    }
}
