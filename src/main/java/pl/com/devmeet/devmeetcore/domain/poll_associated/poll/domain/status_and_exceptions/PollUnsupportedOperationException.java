package pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.status_and_exceptions;

/**
 * Created by IntelliJ IDEA.
 * User: Kamil Ptasinski
 * Date: 16.11.2019
 * Time: 10:52
 */
public class PollUnsupportedOperationException extends Exception {
    public PollUnsupportedOperationException(String message) {
        super(message);
    }
}
