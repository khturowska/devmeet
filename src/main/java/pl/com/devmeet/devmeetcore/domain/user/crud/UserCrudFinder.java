package pl.com.devmeet.devmeetcore.domain.user.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.util.List;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class UserCrudFinder {

    @Getter
    private UserRepository repository;

    public UserEntity findEntityByIdOrUserEmail(UserEntity userEntity) throws UserNotFoundException {
        Long userId = userEntity.getId();
        if (userId != null)
            return findById(userId);
        else
            return findByEmail(userEntity.getEmail());
    }

    public UserEntity findById(Long userId) throws UserNotFoundException {
        if (userId != null)
            return repository.findById(userId)
                    .orElseThrow(() -> new UserNotFoundException("User id=" + userId + " not found!"));
        else
            throw new UserNotFoundException("Id can't be null");
    }

    public UserEntity findByEmail(String email) throws UserNotFoundException {
        if (isNotEmpty(email))
            return repository.findByEmailIgnoreCase(email)
                    .orElseThrow(() -> new UserNotFoundException("User email: " + email + " not found!"));
        else throw new UserNotFoundException("Email cannot be empty!");
    }

    public List<UserEntity> findByEmailLike(String email) {
        return repository.findEmailLike(email);
    }

    public List<UserEntity> findAllByIsActive(boolean state) {
        return repository.findAllByIsActive(state);
    }

    public List<UserEntity> findAllEntities() {
        return repository.findAll();
    }

    @Deprecated
    public boolean isExist(UserEntity dto) {
        try {
            return findEntityByIdOrUserEmail(dto) != null;
        } catch (UserNotFoundException e) {
            return false;
        }
    }
}
