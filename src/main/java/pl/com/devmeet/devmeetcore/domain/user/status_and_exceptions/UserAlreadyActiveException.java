package pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions;

public class UserAlreadyActiveException extends Exception {

    public UserAlreadyActiveException() {
        super(UserCrudStatusEnum.USER_ALREADY_ACTIVE.toString());
    }

    public UserAlreadyActiveException(String message) {
        super(message);
    }

    public UserAlreadyActiveException(String message, Throwable cause) {
        super(message, cause);
    }
}
