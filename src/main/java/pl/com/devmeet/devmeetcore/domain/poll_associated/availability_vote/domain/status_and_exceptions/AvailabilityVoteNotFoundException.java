package pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain.status_and_exceptions;

/**
 * Created by IntelliJ IDEA.
 * User: Kamil Ptasinski
 * Date: 15.11.2019
 * Time: 23:52
 */
public class AvailabilityVoteNotFoundException extends Exception {
    public AvailabilityVoteNotFoundException(String message) {
        super(message);
    }
}
