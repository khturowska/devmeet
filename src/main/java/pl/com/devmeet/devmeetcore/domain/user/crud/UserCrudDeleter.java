package pl.com.devmeet.devmeetcore.domain.user.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class UserCrudDeleter {

    private UserCrudFinder userFinder;
    private UserRepository userRepository;

    public boolean deleteById(Long userId) {
        UserEntity found;
        try {
            found = userFinder.findById(userId);
            deleteUserFromRepository(found);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean delete(UserEntity userEntity) {
        UserEntity found;
        try {
            found = userFinder.findEntityByIdOrUserEmail(userEntity);
            deleteUserFromRepository(found);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void deleteUserFromRepository(UserEntity userEntity) {
        userRepository.delete(userEntity);
    }
}
