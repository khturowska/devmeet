package pl.com.devmeet.devmeetcore.domain.member_associated.member.crud;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudRepository;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberUserNotActiveException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerCrudService;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerRepository;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserCrudService;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserRepository;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.util.List;

@Service
public class MemberCrudService {

    @Getter
    private MemberRepository memberRepository;
    private UserRepository userRepository;
    private MessengerRepository messengerRepository;
    private GroupCrudRepository groupCrudRepository;

    @Autowired
    public MemberCrudService(MemberRepository memberRepository, UserRepository userRepository, MessengerRepository messengerRepository, GroupCrudRepository groupCrudRepository) {
        this.memberRepository = memberRepository;
        this.userRepository = userRepository;
        this.messengerRepository = messengerRepository;
        this.groupCrudRepository = groupCrudRepository;
    }

    private MessengerCrudService initMessengerFacade() {
        return new MessengerCrudService(messengerRepository, userRepository, memberRepository, groupCrudRepository);
    }

    private MemberMessengerCreator initMessengerCreator() {
        return new MemberMessengerCreator(initMessengerFacade());
    }

    private MemberMessengerDeactivator initMessengerDeactivator() {
        return new MemberMessengerDeactivator(initMessengerFacade());
    }

    private MemberUserFinder initUserFinder() {
        return new MemberUserFinder(new UserCrudService(userRepository));
    }

    private MemberCrudSaver initSaver() {
        return new MemberCrudSaver(memberRepository);
    }

    private MemberCrudFinder initFinder() {
        return new MemberCrudFinder(memberRepository, initUserFinder());
    }

    private MemberCrudCreator initCreator() {
        return MemberCrudCreator.builder()
                .memberFinder(initFinder())
                .saver(initSaver())
                .memberUserFinder(initUserFinder())
                .memberMessengerCreator(initMessengerCreator())
                .build();
    }

    private MemberCrudDeactivator initDeleter() {
        return MemberCrudDeactivator.builder()
                .memberCrudFinder(initFinder())
                .memberCrudSaver(initSaver())
                .memberMessengerDeactivator(initMessengerDeactivator())
                .build();
    }

    private MemberCrudUpdater initUpdater() {
        return MemberCrudUpdater.builder()
                .memberFinder(initFinder())
                .memberSaver(initSaver())
                .build();
    }

    public MemberEntity add(MemberEntity dto) throws MemberAlreadyExistsException, UserNotFoundException, MemberUserNotActiveException {
        return initCreator().createEntity(dto);
    }

    public MemberEntity find(MemberEntity dto) throws MemberNotFoundException, UserNotFoundException {
        return initFinder().findEntityByIdOrFeatures(dto);
    }

    public MemberEntity findById(Long memberId) throws MemberNotFoundException {
        return initFinder().findById(memberId);
    }

    public MemberEntity findByUser(UserEntity userEntity) throws MemberNotFoundException, UserNotFoundException {
        return initFinder().findMemberEntityByUser(userEntity);
    }

    public MemberEntity findByUserId(Long id) throws MemberNotFoundException, UserNotFoundException {
        return initFinder().findMemberEntityByUser(UserEntity.builder().id(id).build());
    }

    public List<MemberEntity> findAll() {
        return initFinder().findAll();
    }

    public MemberEntity update(MemberEntity oldDto) throws MemberNotFoundException, MemberFoundButNotActiveException {
        return initUpdater().update(oldDto);
    }

    public MemberEntity deactivate(MemberEntity dto) throws MemberNotFoundException, UserNotFoundException, MemberFoundButNotActiveException, MessengerAlreadyExistsException, MessengerNotFoundException, GroupNotFoundException {
        return initDeleter().deactivateByMember(dto);
    }

    public MemberEntity deactivateById(Long memberId) throws UserNotFoundException, MessengerNotFoundException, GroupNotFoundException, MemberNotFoundException, MessengerAlreadyExistsException, MemberFoundButNotActiveException {
        return initDeleter().deactivateById(memberId);
    }

    public boolean isExist(MemberEntity memberEntity) {
        return initFinder().isExist(memberEntity);
    }
}