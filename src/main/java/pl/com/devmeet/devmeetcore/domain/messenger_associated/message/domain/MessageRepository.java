package pl.com.devmeet.devmeetcore.domain.messenger_associated.message.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerEntity;

import java.util.List;
import java.util.Optional;

@Repository
public interface MessageRepository extends JpaRepository<MessageEntity, Long> {

    Optional<MessageEntity> findBySenderAndMessage(MessengerEntity sender, String message);

    Optional<List<MessageEntity>> findAllBySender(MessengerEntity senderMessenger);

    Optional<List<MessageEntity>> findAllByReceiver(MessengerEntity receiverMemberOrGroupMessenger);
}