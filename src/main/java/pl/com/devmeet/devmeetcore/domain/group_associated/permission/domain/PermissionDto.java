package pl.com.devmeet.devmeetcore.domain.group_associated.permission.domain;

import lombok.*;
import org.joda.time.DateTime;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class PermissionDto {

//    private MemberDto member;
//    private GroupDto group;
//    private PermissionTypeEnum type;

    private boolean possibleToVote;
    private boolean possibleToMessaging;
    private boolean possibleToChangeGroupName;
    private boolean possibleToBanMember;

    private boolean memberBaned;

    private DateTime creationTime;
    private DateTime modificationTime;
    private boolean isActive;
}
