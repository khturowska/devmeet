package pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions;

public class GroupFoundButNotActiveException extends Exception {

    public GroupFoundButNotActiveException() {
        super(GroupCrudStatusEnum.GROUP_FOUND_BUT_NOT_ACTIVE.toString());
    }

    public GroupFoundButNotActiveException(String message) {
        super(message);
    }

    public GroupFoundButNotActiveException(String message, Throwable cause) {
        super(message, cause);
    }
}
