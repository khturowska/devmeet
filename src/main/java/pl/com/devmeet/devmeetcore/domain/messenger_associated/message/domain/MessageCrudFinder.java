package pl.com.devmeet.devmeetcore.domain.messenger_associated.message.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.message.status_and_exceptions.MessageArgumentNotSpecifiedException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.message.status_and_exceptions.MessageCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.message.status_and_exceptions.MessageNotFoundException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerEntity;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class MessageCrudFinder {

    private MessageRepository repository;
    private MessengerFinder messengerFinder;

    public MessageEntity findEntity(MessageEntity dto) throws UserNotFoundException, GroupNotFoundException, MessengerNotFoundException, MemberNotFoundException, MessageNotFoundException, MessageArgumentNotSpecifiedException {
        if (dto.getId() != null)
            return findById(dto.getId())
                    .orElseThrow(() -> new MessageNotFoundException(MessageCrudStatusEnum.MESSAGE_NOT_FOUND_BY_SENDER_AND_MESSAGE.toString()));
        else
            return repository.findBySenderAndMessage(findSender(dto), dto.getMessage())
                    .orElseThrow(() -> new MessageNotFoundException(MessageCrudStatusEnum.MESSAGE_NOT_FOUND_BY_SENDER_AND_MESSAGE.toString()));
    }

    public List<MessageEntity> findEntities(MessageEntity dto) throws UserNotFoundException, GroupNotFoundException, MessengerNotFoundException, MemberNotFoundException, MessageNotFoundException, MessageArgumentNotSpecifiedException {
        MessengerEntity sender = findSender(dto);

        Optional<List<MessageEntity>> allMessagesBySender = repository.findAllBySender(sender);

        if (allMessagesBySender.isPresent())
            return allMessagesBySender.get();

        throw new MessageNotFoundException(MessageCrudStatusEnum.MESSAGES_NOT_FOUND_BY_MEMBER.toString());
    }

    public Optional<MessageEntity> findById(Long id) {
        return repository.findById(id);
    }

    public List<MessageEntity> findEntitiesByGroupForGroupChat(MessageEntity dto) throws MessageNotFoundException, UserNotFoundException, GroupNotFoundException, MessengerNotFoundException, MemberNotFoundException, MessageArgumentNotSpecifiedException {
        MessengerEntity receiver = findReceiver(dto);

        Optional<List<MessageEntity>> allGroupChatMessages = repository.findAllByReceiver(receiver);

        if (allGroupChatMessages.isPresent())
            return allGroupChatMessages.get();

        throw new MessageNotFoundException(MessageCrudStatusEnum.MESSAGES_NOT_FOUND_BY_GROUP.toString());
    }

    private MessengerEntity findSender(MessageEntity dto) throws UserNotFoundException, MessengerNotFoundException, GroupNotFoundException, MemberNotFoundException, MessageArgumentNotSpecifiedException {
        MessengerEntity sender = null;
        try {
            sender = dto.getSender();
        } catch (NullPointerException e) {
        }
        if (sender != null)
            return messengerFinder.findMessenger(sender);

        throw new MessageArgumentNotSpecifiedException(MessageCrudStatusEnum.NOT_SPECIFIED_SENDER.toString());
    }

    private MessengerEntity findReceiver(MessageEntity dto) throws UserNotFoundException, MessengerNotFoundException, GroupNotFoundException, MemberNotFoundException, MessageArgumentNotSpecifiedException {
        MessengerEntity receiver = dto.getReceiver();

        if (receiver != null)
            return messengerFinder.findMessenger(receiver);

        throw new MessageArgumentNotSpecifiedException(MessageCrudStatusEnum.NOT_SPECIFIED_RECEIVER.toString());
    }

    public boolean isExist(MessageEntity dto) {
        try {
            return findEntity(dto) != null;
        } catch (UserNotFoundException | GroupNotFoundException | MessengerNotFoundException | MemberNotFoundException | MessageNotFoundException | MessageArgumentNotSpecifiedException e) {
            return false;
        }
    }
}