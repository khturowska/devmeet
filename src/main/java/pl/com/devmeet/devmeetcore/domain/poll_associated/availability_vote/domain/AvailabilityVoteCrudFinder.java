package pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain.status_and_exceptions.AvailabilityVoteCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain.status_and_exceptions.AvailabilityVoteNotFoundException;
import pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.PollEntity;
import pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.status_and_exceptions.PollNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class AvailabilityVoteCrudFinder {

    private AvailabilityVoteCrudRepository voteRepository;
    private AvailabilityVotePollFinder pollFinder;
    private AvailabilityVoteMemberFinder memberFinder;

    public AvailabilityVoteEntity findEntity(AvailabilityVoteEntity dto) throws MemberNotFoundException, UserNotFoundException, AvailabilityVoteNotFoundException, GroupNotFoundException, PollNotFoundException {
        if (dto.getId() != null)
            return findById(dto.getId())
                    .orElseThrow(() -> new AvailabilityVoteNotFoundException(AvailabilityVoteCrudStatusEnum.AVAILABILITY_VOTE_NOT_FOUND.toString()));
        else
            return findVoteByMemberAndPoll(dto);
    }

    private AvailabilityVoteEntity findVoteByMemberAndPoll(AvailabilityVoteEntity dto) throws MemberNotFoundException, UserNotFoundException, AvailabilityVoteNotFoundException, GroupNotFoundException, PollNotFoundException {
        MemberEntity memberEntity = findMember(dto);
        PollEntity pollEntity = findPoll(dto);

        Optional<AvailabilityVoteEntity> voteEntity = voteRepository.findByMemberAndPoll(memberEntity, pollEntity);

        if (voteEntity.isPresent())
            return voteEntity.get();

        throw new AvailabilityVoteNotFoundException(AvailabilityVoteCrudStatusEnum.AVAILABILITY_VOTE_NOT_FOUND.toString());
    }

    private MemberEntity findMember(AvailabilityVoteEntity dto) throws MemberNotFoundException, UserNotFoundException {
        return memberFinder.findMember(dto.getMember());
    }

    public List<AvailabilityVoteEntity> findEntities(AvailabilityVoteEntity dto) throws GroupNotFoundException, AvailabilityVoteNotFoundException, PollNotFoundException {
        return findAllActiveAndNotActiveVotes(dto);
    }

    private List<AvailabilityVoteEntity> findAllActiveAndNotActiveVotes(AvailabilityVoteEntity dto) throws AvailabilityVoteNotFoundException, GroupNotFoundException, PollNotFoundException {
        PollEntity pollEntity = findPoll(dto);

        Optional<List<AvailabilityVoteEntity>> voteEntities = voteRepository.findAllByPoll(pollEntity);

        if (voteEntities.isPresent())
            return voteEntities.get();

        throw new AvailabilityVoteNotFoundException(AvailabilityVoteCrudStatusEnum.AVAILABILITY_VOTES_NOT_FOUND.toString());
    }

    private PollEntity findPoll(AvailabilityVoteEntity dto) throws GroupNotFoundException, PollNotFoundException {
        return pollFinder.findPoll(dto.getPoll());
    }

    public boolean isExist(AvailabilityVoteEntity dto) {
        try {
            findEntity(dto);
            return true;
        } catch (MemberNotFoundException | UserNotFoundException | AvailabilityVoteNotFoundException | GroupNotFoundException | PollNotFoundException e) {
            return false;
        }
    }

    public Optional<AvailabilityVoteEntity> findById(Long id) {
        return voteRepository.findById(id);
    }
}
