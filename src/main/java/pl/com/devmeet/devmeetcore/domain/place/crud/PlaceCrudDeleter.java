package pl.com.devmeet.devmeetcore.domain.place.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.place.models.PlaceEntity;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceNotFoundException;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class PlaceCrudDeleter {

    private PlaceCrudSaver placeCrudSaver;
    private PlaceCrudFinder placeCrudFinder;

    public PlaceEntity deactivateEntity(PlaceEntity dto) throws PlaceFoundButNotActiveException, PlaceNotFoundException {
        PlaceEntity place = placeCrudFinder.findPlaceById(dto.getId());
        boolean placeActivity = place.isActive();

        if (placeActivity) {
            place.setActive(false);
            place.setModificationTime(LocalDateTime.now());

            return placeCrudSaver.saveEntity(place);
        }
        throw new PlaceFoundButNotActiveException(String.format("Place (id=%s) found but not active", place.getId()));
    }
}
