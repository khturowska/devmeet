package pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberRepository;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class MessengerCrudSaver {

    private MessengerRepository messengerRepository;
    private MemberRepository memberRepository;

    public MessengerEntity saveEntity(MessengerEntity entity) {
        return messengerRepository.save(entity);
    }
}