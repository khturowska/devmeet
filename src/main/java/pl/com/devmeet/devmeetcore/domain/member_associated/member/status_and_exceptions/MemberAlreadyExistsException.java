package pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions;

public class MemberAlreadyExistsException extends Exception {

    public MemberAlreadyExistsException() {
        super(MemberCrudStatusEnum.MEMBER_ALREADY_EXIST.toString());
    }

    public MemberAlreadyExistsException(String message) {
        super(message);
    }

    public MemberAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }
}
