package pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions;

public class MemberUserNotActiveException extends Exception {
    public MemberUserNotActiveException() {
    }

    public MemberUserNotActiveException(String message) {
        super(message);
    }

    public MemberUserNotActiveException(String message, Throwable cause) {
        super(message, cause);
    }
}
