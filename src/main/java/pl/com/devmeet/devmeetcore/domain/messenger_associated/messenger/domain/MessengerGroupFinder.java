package pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;

@RequiredArgsConstructor
class MessengerGroupFinder {

    @NonNull
    private GroupCrudService groupCrudService;

    public GroupEntity findGroup(GroupEntity groupEntity) throws GroupNotFoundException {
        return groupCrudService.findByGroup(groupEntity);
    }
}
