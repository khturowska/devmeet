package pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;

import java.util.List;
import java.util.Optional;

public interface PollCrudRepository extends JpaRepository<PollEntity, Long> {

    Optional<PollEntity> findByGroup(GroupEntity groupEntity);

    Optional<List<PollEntity>> findAllByGroup(GroupEntity groupEntity);
}
