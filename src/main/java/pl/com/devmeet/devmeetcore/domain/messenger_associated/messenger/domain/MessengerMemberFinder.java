package pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

@RequiredArgsConstructor
class MessengerMemberFinder {

    @NonNull
    private MemberCrudService memberCrudService;

    public MemberEntity findMember(MemberEntity memberEntity) throws MemberNotFoundException, UserNotFoundException {
        return memberCrudService.find(memberEntity);
    }
}
