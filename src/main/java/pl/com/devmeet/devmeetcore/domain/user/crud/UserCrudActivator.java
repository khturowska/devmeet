package pl.com.devmeet.devmeetcore.domain.user.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyActiveException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class UserCrudActivator {

    private UserCrudSaver userSaver;
    private UserCrudFinder userFinder;

    public UserEntity activate(UserEntity dto) throws UserNotFoundException, UserAlreadyActiveException {
        UserEntity found = userFinder.findEntityByIdOrUserEmail(dto);

        if (!found.isActive()) {
            found.setActive(true);
            found.setModificationTime(LocalDateTime.now());

            return saveUserEntity(found);

        } else
            throw new UserAlreadyActiveException(UserCrudStatusEnum.USER_ALREADY_ACTIVE.toString());
    }

    public boolean isUserActive(UserEntity dto) throws UserNotFoundException {
        return isUserActive(dto.getEmail());
    }

    public boolean isUserActive(String email) throws UserNotFoundException {
        return userFinder.findByEmail(email).isActive();
    }

    private UserEntity saveUserEntity(UserEntity entity) {
        return userSaver.saveEntity(entity);
    }
}
