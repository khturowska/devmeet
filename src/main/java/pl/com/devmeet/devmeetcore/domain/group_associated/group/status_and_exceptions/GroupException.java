package pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by IntelliJ IDEA.
 * User: Kamil Ptasinski
 * Date: 16.11.2019
 * Time: 10:35
 */
@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class GroupException extends Exception {
    public GroupException(String message) {
        super(message);
    }
}
