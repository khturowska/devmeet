package pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions;

public class PlaceAlreadyExistsException extends Exception {

    public PlaceAlreadyExistsException() {
        super(PlaceCrudStatusEnum.PLACE_ALREADY_EXISTS.toString());
    }

    public PlaceAlreadyExistsException(String message) {
        super(message);
    }

    public PlaceAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }
}
