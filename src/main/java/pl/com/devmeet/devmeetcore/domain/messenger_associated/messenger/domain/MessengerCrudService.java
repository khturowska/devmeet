package pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudRepository;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberRepository;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerArgumentNotSpecified;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerInfoStatusEnum;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserRepository;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.util.Optional;

@Service
public class MessengerCrudService {

    private MessengerRepository messengerRepository;
    private UserRepository userRepository;
    private MemberRepository memberRepository;
    private GroupCrudRepository groupRepository;

    @Autowired
    public MessengerCrudService(MessengerRepository messengerRepository,
                                UserRepository userRepository,
                                MemberRepository memberRepository,
                                GroupCrudRepository groupRepository) {

        this.messengerRepository = messengerRepository;
        this.userRepository = userRepository;
        this.memberRepository = memberRepository;
        this.groupRepository = groupRepository;
    }

    private MessengerMemberFinder initMemberFinder() {
        return new MessengerMemberFinder(new MemberCrudService(memberRepository, userRepository, messengerRepository, groupRepository));
    }

    private MessengerGroupFinder initGroupFinder() {
        return new MessengerGroupFinder(new GroupCrudService(groupRepository, memberRepository, userRepository, messengerRepository));
    }

    private MessengerCrudSaver initSaver() {
        return MessengerCrudSaver.builder()
                .memberRepository(memberRepository)
                .messengerRepository(messengerRepository)
                .build();
    }

    private MessengerCrudFinder initFinder() {
        return MessengerCrudFinder.builder()
                .messengerRepository(messengerRepository)
                .groupFinder(initGroupFinder())
                .memberFinder(initMemberFinder())
                .build();
    }

    private MessengerCrudCreator initCreator() {
        return MessengerCrudCreator.builder()
                .messengerCrudFinder(initFinder())
                .messengerCrudSaver(initSaver())
                .build();
    }

    private MessengerCrudDeleter deleterInit() {
        return MessengerCrudDeleter.builder()
                .messengerCrudFinder(initFinder())
                .messengerCrudSaver(initSaver())
                .build();
    }

    public MessengerEntity addToMember(MemberEntity memberEntity) throws UserNotFoundException, MessengerArgumentNotSpecified, GroupNotFoundException, MemberNotFoundException, MessengerAlreadyExistsException {
        return add(MessengerEntity.builder()
                .member(memberEntity)
                .build());
    }

    public MessengerEntity addToGroup(GroupEntity groupEntity) throws UserNotFoundException, MessengerArgumentNotSpecified, GroupNotFoundException, MemberNotFoundException, MessengerAlreadyExistsException {
        return add(MessengerEntity.builder()
                .group(groupEntity)
                .build());
    }

    public MessengerEntity add(MessengerEntity messengerEntity) throws MessengerAlreadyExistsException, MessengerArgumentNotSpecified, MemberNotFoundException, UserNotFoundException, GroupNotFoundException {
        return initCreator().createEntity(messengerEntity);
    }

    //todo zmienic optional na wyrzucanie wyjatku
    public Optional<MessengerEntity> findById(Long id) {
        return initFinder().findById(id);
    }

    public MessengerEntity update(MessengerEntity oldDto, MessengerEntity newDto) throws Exception {
        throw new Exception(MessengerInfoStatusEnum.METHOD_NOT_IMPLEMENTED.toString());
    }

    public MessengerEntity findByMember(MemberEntity memberEntity) throws UserNotFoundException, MemberNotFoundException, MessengerNotFoundException, GroupNotFoundException {
        MessengerEntity messengerEntity = MessengerEntity.builder()
                .member(memberEntity)
                .build();
        return findByMessenger(messengerEntity);
    }

    public MessengerEntity findByGroup(GroupEntity groupEntity) throws UserNotFoundException, MemberNotFoundException, MessengerNotFoundException, GroupNotFoundException {
        MessengerEntity messengerEntity = MessengerEntity.builder()
                .group(groupEntity)
                .build();
        return findByMessenger(messengerEntity);
    }

    public MessengerEntity findByMessenger(MessengerEntity messengerEntity) throws MessengerNotFoundException, MemberNotFoundException, UserNotFoundException, GroupNotFoundException {
        return initFinder().findEntity(messengerEntity);
    }

    public MessengerEntity deactivateMembersMessenger(MemberEntity memberEntity) throws UserNotFoundException, MessengerNotFoundException, GroupNotFoundException, MemberNotFoundException, MessengerAlreadyExistsException {
        return delete(MessengerEntity.builder()
                .member(memberEntity)
                .build());
    }

    public MessengerEntity deactivateGroupsMessenger(GroupEntity groupEntity) throws UserNotFoundException, MessengerNotFoundException, GroupNotFoundException, MemberNotFoundException, MessengerAlreadyExistsException {
        return delete(MessengerEntity.builder()
                .group(groupEntity)
                .build());
    }

    public MessengerEntity delete(MessengerEntity messengerEntity) throws UserNotFoundException, MemberNotFoundException, GroupNotFoundException, MessengerNotFoundException, MessengerAlreadyExistsException {
        return deleterInit().delete(messengerEntity);
    }

    @Deprecated
    public boolean isExist(MessengerEntity messengerEntity) {
        return initFinder().isExist(messengerEntity);
    }
}
