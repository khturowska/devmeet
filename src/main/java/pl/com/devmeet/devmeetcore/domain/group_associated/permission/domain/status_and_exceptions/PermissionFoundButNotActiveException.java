package pl.com.devmeet.devmeetcore.domain.group_associated.permission.domain.status_and_exceptions;

/**
 * Created by IntelliJ IDEA.
 * User: Kamil Ptasinski
 * Date: 16.11.2019
 * Time: 11:32
 */
public class PermissionFoundButNotActiveException extends Exception {
    public PermissionFoundButNotActiveException(String message) {
        super(message);
    }
}
