package pl.com.devmeet.devmeetcore.domain.member_associated.member.models;

import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class MemberApiMapper {

    public MemberEntity mapToBackend(MemberApiDto apiDto) {
        return apiDto != null ? MemberEntity.builder()
                .id(apiDto.getId())
                .user(UserEntity.builder().id(apiDto.getId()).build())
                .nick(apiDto.getNick())
                .build() : null;
    }

    public List<MemberEntity> mapToBackend(List<MemberApiDto> apiDtos) {
        return apiDtos != null ? apiDtos.stream()
                .map(this::mapToBackend)
                .collect(Collectors.toList()) : null;
    }

    public MemberApiDto mapToFrontend(MemberEntity dto) {
        return dto != null ? MemberApiDto.builder()
                .id(dto.getId())
                .user(dto.getUser().getId())
                .nick(dto.getNick())
                .build() : null;
    }

    public List<MemberApiDto> mapToFrontend(List<MemberEntity> entities) {
        return entities != null ? entities.stream()
                .map(this::mapToFrontend)
                .collect(Collectors.toList()) : Collections.emptyList();
    }
}
