package pl.com.devmeet.devmeetcore.domain.user.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class UserApiDto {

    private Long id;
    private String email;
    private String password;
}
