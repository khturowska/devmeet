package pl.com.devmeet.devmeetcore.domain.member_associated.availability.models;


import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@NoArgsConstructor
public class AvailabilityApiMapper {

    private String datePattern = "yyyy-MM-dd";
    private String timePattern = "HH:mm";

    public AvailabilityEntity mapToBackend(AvailabilityDto apiDto) {
        return apiDto != null ? AvailabilityEntity.builder()
                .id(apiDto.getId())
                .member(MemberEntity.builder().id(apiDto.getMemberId()).build())
                .beginDate(selectDateIfNotNullOrEmpty(apiDto, apiDto.getBeginDate()))
                .beginTime(selectTimeIfNotNullOrEmpty(apiDto, apiDto.getBeginTime()))
                .endDate(selectDateIfNotNullOrEmpty(apiDto, apiDto.getEndDate()))
                .endTime(selectTimeIfNotNullOrEmpty(apiDto, apiDto.getEndTime()))
                .freeTime(apiDto.getFreeTime())
                .remoteWork(apiDto.getRemoteWork())
                .build() : null;
    }

    private LocalDate selectDateIfNotNullOrEmpty(AvailabilityDto dto, String input) {
        if (!input.isEmpty()) {
            try {
                input = input.replace("/", "-");
                return LocalDate.parse(input, DateTimeFormatter.ofPattern(datePattern));
            } catch (Exception e) {
                log.warn(String.format("Illegal date format for Availability id=%d (owner is Member id=%d).", dto.getId(), dto.getMemberId()));
            }
        }
        return null;
    }

    private LocalTime selectTimeIfNotNullOrEmpty(AvailabilityDto dto, String input) {
        if (!input.isEmpty()) {
            try {
                input = input.replace(".", ":");
                return LocalTime.parse(input, DateTimeFormatter.ofPattern(timePattern));
            } catch (Exception e) {
                log.warn(String.format("Illegal time format for Availability id=%d (owner is Member id=%d).", dto.getId(), dto.getMemberId()));
            }
        }
        return null;
    }

    public List<AvailabilityEntity> mapListToBackend(List<AvailabilityDto> apiDtos) {
        return apiDtos.parallelStream()
                .map(this::mapToBackend)
                .collect(Collectors.toList());
    }

    public AvailabilityDto mapToFrontend(AvailabilityEntity dto) {
        return dto != null ? AvailabilityDto.builder()
                .id(dto.getId())
                .memberId(dto.getMember().getId())
                .beginDate(dto.getBeginDate().format(DateTimeFormatter.ofPattern(datePattern)))
                .beginTime(dto.getBeginTime().format(DateTimeFormatter.ofPattern(timePattern)))
                .endDate(dto.getEndDate().format(DateTimeFormatter.ofPattern(datePattern)))
                .endTime(dto.getEndTime().format(DateTimeFormatter.ofPattern(timePattern)))
                .freeTime(dto.getFreeTime())
                .remoteWork(dto.getRemoteWork())
                .build() : null;
    }

    public List<AvailabilityDto> mapListToFrontend(List<AvailabilityEntity> dtos) {
        return dtos.parallelStream()
                .map(this::mapToFrontend)
                .collect(Collectors.toList());
    }
}
