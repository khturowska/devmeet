package pl.com.devmeet.devmeetcore.domain.member_associated.member.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class MemberCrudUpdater {

    private MemberCrudFinder memberFinder;
    private MemberCrudSaver memberSaver;

    public MemberEntity update(MemberEntity member) throws MemberNotFoundException, MemberFoundButNotActiveException {
        MemberEntity foundMember = findMemberById(member.getId());

        if (foundMember.isActive())
            return memberSaver.saveEntity(updateAllowedValues(foundMember, member));

        throw new MemberFoundButNotActiveException(MemberCrudStatusEnum.MEMBER_FOUND_BUT_NOT_ACTIVE.toString());
    }

    private MemberEntity findMemberById(Long id) throws MemberNotFoundException {
        if (id != null)
            return memberFinder.findById(id);
        else
            throw new MemberNotFoundException(MemberCrudStatusEnum.ID_NOT_SPECIFIED.toString());
    }

    private MemberEntity updateAllowedValues(MemberEntity oldMember, MemberEntity updatedMember) {
        String nick = updatedMember.getNick();

        if (nick != null) {
            oldMember.setNick(nick);
            oldMember.setModificationTime(LocalDateTime.now());
        }

        return oldMember;
    }
}
