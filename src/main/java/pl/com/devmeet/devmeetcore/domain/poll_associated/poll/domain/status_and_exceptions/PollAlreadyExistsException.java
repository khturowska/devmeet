package pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.status_and_exceptions;

/**
 * Created by IntelliJ IDEA.
 * User: Kamil Ptasinski
 * Date: 16.11.2019
 * Time: 09:12
 */
public class PollAlreadyExistsException extends Exception {
    public PollAlreadyExistsException(String message) {
        super(message);
    }
}
