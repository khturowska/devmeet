package pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerArgumentNotSpecified;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerInfoStatusEnum;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

@NoArgsConstructor
@AllArgsConstructor
@Builder
class MessengerCrudCreator {

    private MessengerCrudFinder messengerCrudFinder;
    private MessengerCrudSaver messengerCrudSaver;

    public MessengerEntity createEntity(MessengerEntity dto) throws MessengerAlreadyExistsException, MessengerArgumentNotSpecified, MemberNotFoundException, UserNotFoundException, GroupNotFoundException {
        MessengerMemberOrGroupConnector messengerConnector = new MessengerMemberOrGroupConnector(messengerCrudFinder);
        MessengerEntity messenger;

        try {
            messenger = messengerCrudFinder.findEntity(dto);

            if (!messenger.isActive()) {
                return saveMessengerEntity(
                        setDefaultValuesWhenMessengerExists(messenger));
            }
        } catch (MessengerNotFoundException e) {

            return saveMessengerEntity(
                    setDefaultValuesWhenMessengerNotExists(
                            messengerConnector
                                    .connectWithMessenger(dto, dto)
                    ));
        }

        throw new MessengerAlreadyExistsException(MessengerInfoStatusEnum.MESSENGER_ALREADY_EXISTS.toString());
    }

    private MessengerEntity setDefaultValuesWhenMessengerNotExists(MessengerEntity entity) {
        entity.setActive(true);
        entity.setCreationTime(DateTime.now());
        return entity;
    }

    private MessengerEntity setDefaultValuesWhenMessengerExists(MessengerEntity entity) {
        return setDefaultValuesWhenMessengerNotExists(entity);
    }

    private MessengerEntity saveMessengerEntity(MessengerEntity messenger) {
        return messengerCrudSaver.saveEntity(messenger);
    }
}