package pl.com.devmeet.devmeetcore.domain.place.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PlaceDto {

    private Long id;

    private String placeName;
    private String description;
    private String website;
    private String location;
}
