package pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class PollGroupFinder {

    private GroupCrudService groupCrudService;

    public GroupEntity findGroup(GroupEntity dto) throws GroupNotFoundException {
        return groupCrudService.findByGroup(dto);
    }
}
