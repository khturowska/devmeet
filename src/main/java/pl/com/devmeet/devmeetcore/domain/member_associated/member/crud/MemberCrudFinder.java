package pl.com.devmeet.devmeetcore.domain.member_associated.member.crud;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.util.List;

@Getter
@RequiredArgsConstructor
class MemberCrudFinder {

    @NonNull
    private MemberRepository memberRepository;
    @NonNull
    private MemberUserFinder userFinder;

    public List<MemberEntity> findAll() {
        return memberRepository.findAll();
    }

    public MemberEntity findEntityByIdOrFeatures(MemberEntity dto) throws MemberNotFoundException, UserNotFoundException {
        if (dto.getId() != null)
            return findById(dto.getId());
        else
            return findMemberEntityByUser(dto.getUser());

    }

    public MemberEntity findMemberEntityByUser(UserEntity userDto) throws MemberNotFoundException, UserNotFoundException {
        UserEntity userEntity = findUser(userDto);
        return memberRepository
                .findByUser(userEntity)
                .orElseThrow(() -> new MemberNotFoundException(MemberCrudStatusEnum.MEMBER_NOT_FOUND.toString()));
    }

    private UserEntity findUser(UserEntity dto) throws UserNotFoundException {
        return userFinder.findUserEntity(dto);
    }

    public boolean isExist(MemberEntity dto) {
        try {
            findMemberEntityByUser(dto.getUser());
            return true;
        } catch (UserNotFoundException | MemberNotFoundException | NullPointerException e) {
            return false;
        }
    }

    public MemberEntity findById(Long id) throws MemberNotFoundException {
        if (id != null)
            return memberRepository.findById(id)
                    .orElseThrow(() -> new MemberNotFoundException(MemberCrudStatusEnum.MEMBER_NOT_FOUND.toString()));
        throw new MemberNotFoundException(MemberCrudStatusEnum.MEMBER_NOT_FOUND.toString());
    }
}
