package pl.com.devmeet.devmeetcore.domain.user.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class UserCrudSaver {

    @Getter
    private UserRepository repository;

    public UserEntity saveEntity(UserEntity entity) {
        return repository.save(entity);
    }
}
