package pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions;

/**
 * Created by IntelliJ IDEA.
 * User: Kamil Ptasinski
 * Date: 15.11.2019
 * Time: 23:46
 */
public class AvailabilityNotFoundException extends Exception {
    public AvailabilityNotFoundException(String message) {
        super(message);
    }
}
