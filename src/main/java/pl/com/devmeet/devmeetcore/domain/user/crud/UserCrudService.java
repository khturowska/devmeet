package pl.com.devmeet.devmeetcore.domain.user.crud;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyActiveException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyExistsEmailDuplicationException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.util.List;

@Service
public class UserCrudService {

    @Getter
    private UserRepository userRepository;

    @Autowired
    public UserCrudService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    private UserCrudSaver initSaver() {
        return UserCrudSaver.builder()
                .repository(userRepository)
                .build();
    }

    private UserCrudFinder initFinder() {
        return UserCrudFinder.builder()
                .repository(userRepository)
                .build();
    }

    private UserCrudCreator initCreator() {
        return UserCrudCreator.builder()
                .userFinder(initFinder())
                .userSaver(initSaver())
                .build();
    }

    private UserCrudUpdater initUpdater() {
        return UserCrudUpdater.builder()
                .userFinder(initFinder())
                .userSaver(initSaver())
                .build();
    }

    private UserCrudDeleter initDeleter() {
        return UserCrudDeleter.builder()
                .userRepository(userRepository)
                .userFinder(initFinder())
                .build();
    }

    private UserCrudActivator initActivator() {
        return UserCrudActivator.builder()
                .userFinder(initFinder())
                .userSaver(initSaver())
                .build();
    }

    private UserCrudDeactivator initDeactivator() {
        return UserCrudDeactivator.builder()
                .userFinder(initFinder())
                .userSaver(initSaver())
                .build();
    }

    public UserEntity add(UserEntity dto) throws UserAlreadyExistsEmailDuplicationException {
        return initCreator().create(dto);
    }

    public UserCrudFinder getUserFinder() {
        return initFinder();
    }

    public List<UserEntity> findByEmailLike(String email) {
        return initFinder().findByEmailLike(email);
    }

    public UserEntity findByIdOrUserEmail(UserEntity dto) throws UserNotFoundException {
        return initFinder().findEntityByIdOrUserEmail(dto);
    }

    public UserEntity findById(Long userId) throws UserNotFoundException {
        return initFinder().findById(userId);
    }

    public UserEntity findByEmail(String email) throws UserNotFoundException {
        return initFinder().findByEmail(email);
    }

    public List<UserEntity> findAll() {
        return initFinder().findAllEntities();
    }

    public List<UserEntity> findAllByIsActive(boolean state) {
        return initFinder().findAllByIsActive(state);
    }

    @Deprecated
    public boolean isExist(UserEntity dto) {
        return initFinder().isExist(dto);
    }


    public UserEntity activate(UserEntity dto) throws UserAlreadyActiveException, UserNotFoundException {
        return initActivator().activate(dto);
    }

    public boolean isUserActive(UserEntity dto) throws UserNotFoundException {
        return initActivator().isUserActive(dto);
    }

    public boolean isUserActive(String email) throws UserNotFoundException {
        return initActivator().isUserActive(email);
    }

    public UserEntity update(UserEntity userEntity) throws UserFoundButNotActiveException, UserNotFoundException {
        return initUpdater().update(userEntity);
    }

    public UserEntity deactivateById(Long id) throws UserNotFoundException, UserFoundButNotActiveException {
        return initDeactivator().deactivateById(id);
    }

    public UserEntity deactivate(UserEntity dto) throws UserFoundButNotActiveException, UserNotFoundException {
        return initDeactivator().deactivate(dto);
    }

    public boolean delete(UserEntity userEntity) {
        return initDeleter().delete(userEntity);
    }

    public boolean deleteById(Long userId) {
        return initDeleter().deleteById(userId);
    }
}
