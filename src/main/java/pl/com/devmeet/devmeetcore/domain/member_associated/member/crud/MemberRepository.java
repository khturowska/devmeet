package pl.com.devmeet.devmeetcore.domain.member_associated.member.crud;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;

import java.util.List;
import java.util.Optional;

@Repository
public interface MemberRepository extends JpaRepository<MemberEntity, Long> {

    MemberEntity findByNick(String nick);

    Optional<MemberEntity> findByUser(UserEntity user);

    List<MemberEntity> findAllByGroups(GroupEntity group);

    //todo add possibility to find Member by nick for admin and normal user when he/she wants to find other Member
    @Query("SELECT m from MemberEntity m " +
            "WHERE lower(m.nick) like lower(concat('%',:searchNick,'%'))")
    List<MemberEntity> findAllBySearchText(String searchNick);

    //@Query("Select m from Entity m where m.groups. = ?1") // do sprawd.
    //Optional<List<MemberEntity>> findByPlace(String nick);

    //@Query("Select g from GroupEntity g where g.members.nick = ?1") //do sprawdz.
    //Optional<List<MemberEntity>>findByGroup(String nick);
}
