package pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.models.AvailabilityEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions.AvailabilityNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain.status_and_exceptions.AvailabilityVoteException;
import pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain.status_and_exceptions.AvailabilityVoteNotFoundException;
import pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.status_and_exceptions.PollNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class AvailabilityVoteCrudUpdater {

    private AvailabilityVoteCrudFinder voteCrudFinder;
    private AvailabilityVoteCrudSaver voteCrudSaver;
    private AvailabilityVoteAvailabilityFinder availabilityFinder;

    public AvailabilityVoteEntity updateEntity(AvailabilityVoteEntity oldDto, AvailabilityVoteEntity newDto) throws MemberNotFoundException, UserNotFoundException, AvailabilityVoteNotFoundException, AvailabilityVoteException, GroupNotFoundException, AvailabilityNotFoundException, PollNotFoundException {
        AvailabilityVoteEntity oldVote = voteCrudFinder.findEntity(oldDto);
        AvailabilityEntity newVote = availabilityFinder.findAvailability(newDto.getAvailability());

        oldVote.setAvailability(newVote);

        return voteCrudSaver.saveEntity(oldVote);
    }
}
