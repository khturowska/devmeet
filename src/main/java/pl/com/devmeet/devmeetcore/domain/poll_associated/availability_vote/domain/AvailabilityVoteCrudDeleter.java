package pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions.AvailabilityNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain.status_and_exceptions.AvailabilityVoteCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain.status_and_exceptions.AvailabilityVoteFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain.status_and_exceptions.AvailabilityVoteNotFoundException;
import pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.status_and_exceptions.PollNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class AvailabilityVoteCrudDeleter {

    private AvailabilityVoteCrudFinder voteCrudFinder;
    private AvailabilityVoteCrudSaver voteCrudSaver;

    public AvailabilityVoteEntity deactivateEntity(AvailabilityVoteEntity dto) throws AvailabilityVoteFoundButNotActiveException, AvailabilityNotFoundException, MemberNotFoundException, GroupNotFoundException, UserNotFoundException, PollNotFoundException, AvailabilityVoteNotFoundException {
        AvailabilityVoteEntity found = voteCrudFinder.findEntity(dto);
        boolean voteActivity = found.isActive();

        if (voteActivity) {
            found.setActive(false);

            return voteCrudSaver.saveEntity(found);
        }
        throw new AvailabilityVoteFoundButNotActiveException(AvailabilityVoteCrudStatusEnum.AVAILABILITY_VOTE_FOUND_BUT_NOT_ACTIVE.toString());
    }
}
