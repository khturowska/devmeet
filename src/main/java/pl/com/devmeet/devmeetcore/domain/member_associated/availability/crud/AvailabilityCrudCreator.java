package pl.com.devmeet.devmeetcore.domain.member_associated.availability.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.models.AvailabilityEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions.AvailabilityAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions.AvailabilityCrudInfoStatusEnum;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions.AvailabilityNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.time.LocalDateTime;


@AllArgsConstructor
@NoArgsConstructor
@Builder
class AvailabilityCrudCreator {

    private AvailabilityCrudSaver availabilityCrudSaver;
    private AvailabilityCrudFinder availabilityCrudFinder;
    private MemberCrudService memberCrudService;
    private AvailabilityMemberFinder memberFinder;

    public AvailabilityEntity createEntity(AvailabilityEntity dto) throws AvailabilityAlreadyExistsException, MemberNotFoundException, UserNotFoundException {
        AvailabilityEntity availability;

        try {
            availability = availabilityCrudFinder.findByEntity(dto);
            if (!availability.isActive() && availability.getModificationTime() != null)
                return availabilityCrudSaver.saveEntity(connectAvailabilityWithMember(setDefaultValuesWhenAvailabilityExists(availability)));
        } catch (AvailabilityNotFoundException e) {
            availability = setDefaultValuesWhenAvailabilityNotExists(dto);
            return availabilityCrudSaver.saveEntity(connectAvailabilityWithMember(availability));
        }
        throw new AvailabilityAlreadyExistsException(AvailabilityCrudInfoStatusEnum.AVAILABILITY_ALREADY_EXISTS.toString());
    }

    private AvailabilityEntity connectAvailabilityWithMember(AvailabilityEntity availabilityEntity) throws MemberNotFoundException, UserNotFoundException {
        MemberEntity memberEntity = memberFinder.findMember(availabilityEntity.getMember());
        availabilityEntity.setMember(memberEntity);
        return availabilityEntity;
    }

    private AvailabilityEntity setDefaultValuesWhenAvailabilityNotExists(AvailabilityEntity availability) {
        availability.setCreationTime(LocalDateTime.now());
        availability.setActive(true);
        return availability;
    }

    private AvailabilityEntity setDefaultValuesWhenAvailabilityExists(AvailabilityEntity availability) {
        availability.setModificationTime(LocalDateTime.now());
        availability.setActive(true);
        return availability;
    }
}

