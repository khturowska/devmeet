package pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudRepository;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.crud.AvailabilityCrudRepository;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.crud.AvailabilityCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions.AvailabilityNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberRepository;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerRepository;
import pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain.status_and_exceptions.AvailabilityVoteAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain.status_and_exceptions.AvailabilityVoteException;
import pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain.status_and_exceptions.AvailabilityVoteFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain.status_and_exceptions.AvailabilityVoteNotFoundException;
import pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.PollCrudRepository;
import pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.PollCrudService;
import pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.status_and_exceptions.PollNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserRepository;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.util.List;
import java.util.Optional;

@Service
public class AvailabilityVoteCrudService {

    private AvailabilityVoteCrudRepository availabilityVoteRepository;
    private PollCrudRepository pollCrudRepository;
    private GroupCrudRepository groupRepository;
    private AvailabilityCrudRepository availabilityRepository;
    private MemberRepository memberRepository;
    private UserRepository userRepository;
    private MessengerRepository messengerRepository;

    @Autowired
    public AvailabilityVoteCrudService(AvailabilityVoteCrudRepository availabilityVoteRepository, PollCrudRepository pollCrudRepository, GroupCrudRepository groupRepository, AvailabilityCrudRepository availabilityRepository, MemberRepository memberRepository, UserRepository userRepository, MessengerRepository messengerRepository) {
        this.availabilityVoteRepository = availabilityVoteRepository;
        this.pollCrudRepository = pollCrudRepository;
        this.groupRepository = groupRepository;
        this.availabilityRepository = availabilityRepository;
        this.memberRepository = memberRepository;
        this.userRepository = userRepository;
        this.messengerRepository = messengerRepository;
    }

    private AvailabilityVotePollFinder initPollFinder() {
        return new AvailabilityVotePollFinder(new PollCrudService(pollCrudRepository, groupRepository, memberRepository, userRepository, messengerRepository));
    }

    private AvailabilityVoteMemberFinder initMemberFinder() {
        return new AvailabilityVoteMemberFinder(new MemberCrudService(memberRepository, userRepository, messengerRepository, groupRepository));
    }

    private AvailabilityVoteAvailabilityFinder initAvailabilityFinder() {
        return new AvailabilityVoteAvailabilityFinder(new AvailabilityCrudService(availabilityRepository, memberRepository, userRepository, messengerRepository, groupRepository));
    }

    private AvailabilityVoteCrudSaver initVoteSaver() {
        return AvailabilityVoteCrudSaver.builder()
                .availabilityVoteCrudRepository(availabilityVoteRepository)
                .build();
    }

    private AvailabilityVoteCrudFinder initVoteFinder() {
        return AvailabilityVoteCrudFinder.builder()
                .voteRepository(availabilityVoteRepository)
                .pollFinder(initPollFinder())
                .memberFinder(initMemberFinder())
                .build();
    }

    private AvailabilityVoteCrudCreator initVoteCreator() {
        return AvailabilityVoteCrudCreator.builder()
                .voteCrudFinder(initVoteFinder())
                .voteCrudSaver(initVoteSaver())
                .pollFinder(initPollFinder())
                .memberFinder(initMemberFinder())
                .availabilityFinder(initAvailabilityFinder())
                .build();
    }

    private AvailabilityVoteCrudUpdater initVoteUpdater() {
        return AvailabilityVoteCrudUpdater.builder()
                .voteCrudFinder(initVoteFinder())
                .voteCrudSaver(initVoteSaver())
                .availabilityFinder(initAvailabilityFinder())
                .build();
    }

    private AvailabilityVoteCrudDeleter initVoteDeleter() {
        return AvailabilityVoteCrudDeleter.builder()
                .voteCrudFinder(initVoteFinder())
                .voteCrudSaver(initVoteSaver())
                .build();
    }

    public AvailabilityVoteEntity add(AvailabilityVoteEntity dto) throws AvailabilityNotFoundException, GroupNotFoundException, AvailabilityVoteAlreadyExistsException, PollNotFoundException, MemberNotFoundException, UserNotFoundException {
        return initVoteCreator().createEntity(dto);
    }

    //todo Zmienic optional na wyjatek
    public Optional<AvailabilityVoteEntity> findById(Long id) {
        return initVoteFinder().findById(id);
    }

    public AvailabilityVoteEntity update(AvailabilityVoteEntity oldDto, AvailabilityVoteEntity newDto) throws AvailabilityVoteNotFoundException, GroupNotFoundException, UserNotFoundException, AvailabilityVoteException, MemberNotFoundException, AvailabilityNotFoundException, PollNotFoundException {
        return initVoteUpdater().updateEntity(oldDto, newDto);
    }

    public AvailabilityVoteEntity delete(AvailabilityVoteEntity dto) throws AvailabilityNotFoundException, GroupNotFoundException, UserNotFoundException, AvailabilityVoteNotFoundException, MemberNotFoundException, AvailabilityVoteFoundButNotActiveException, PollNotFoundException {
        return initVoteDeleter().deactivateEntity(dto);
    }

    public AvailabilityVoteEntity find(AvailabilityVoteEntity dto) throws MemberNotFoundException, UserNotFoundException, AvailabilityVoteNotFoundException, GroupNotFoundException, PollNotFoundException {
        return initVoteFinder().findEntity(dto);
    }

    public List<AvailabilityVoteEntity> findAll(AvailabilityVoteEntity dto) throws GroupNotFoundException, AvailabilityVoteNotFoundException, PollNotFoundException {
        return initVoteFinder().findEntities(dto);
    }
}
