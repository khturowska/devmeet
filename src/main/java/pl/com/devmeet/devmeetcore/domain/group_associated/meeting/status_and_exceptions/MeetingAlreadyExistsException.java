package pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions;

public class MeetingAlreadyExistsException extends Exception {

    public MeetingAlreadyExistsException() {
        super(MeetingCrudStatusEnum.ALREADY_EXIST.toString());
    }

    public MeetingAlreadyExistsException(String message) {
        super(message);
    }

    public MeetingAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }
}
