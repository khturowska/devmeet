package pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models;

import lombok.extern.slf4j.Slf4j;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.place.models.PlaceEntity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class MeetingApiMapper {

    private String datePattern = "yyyy-MM-dd";
    private String timePattern = "HH:mm";
    private String timeFormat = "yyyy-MM-dd HH:mm";

    public MeetingEntity mapToBackend(MeetingDto meetingDto) {
        return meetingDto != null ? MeetingEntity.builder()
                .id(meetingDto.getId())
                .meetingName(meetingDto.getMeetingName())
                .begin(LocalDateTime.parse(meetingDto.getBegin(), DateTimeFormatter.ofPattern(timeFormat)))
                .end(LocalDateTime.parse(meetingDto.getEnd(), DateTimeFormatter.ofPattern(timeFormat)))
                .group(GroupEntity.builder().id(meetingDto.getGroupId()).build())
                .promoter(MemberEntity.builder().id(meetingDto.getId()).build())
                .place(PlaceEntity.builder().id(meetingDto.getPlaceId()).build())
//                .members(new MemberApiMapper()
//                        .mapToBackend(
//                                meetingDto.getMembersIds()
//                                        .stream()
//                                        .map(id -> MemberApiDto.builder()
//                                                .id(id)
//                                                .build())
//                                        .collect(Collectors.toList())))
                .build() : null;
    }

    private LocalDate selectDateIfNotNullOrEmpty(MeetingDto dto, String input) {
        if (!input.isEmpty()) {
            try {
                input = input.replace("/", "-");
                return LocalDate.parse(input, DateTimeFormatter.ofPattern(datePattern));
            } catch (Exception e) {
                log.warn(String.format("Illegal date format for Meeting id=%d.", dto.getId()));
            }
        }
        return null;
    }

    private LocalTime selectTimeIfNotNullOrEmpty(MeetingDto dto, String input) {
        if (!input.isEmpty()) {
            try {
                input = input.replace(".", ":");
                return LocalTime.parse(input, DateTimeFormatter.ofPattern(timePattern));
            } catch (Exception e) {
                log.warn(String.format("Illegal time format for Meeting id=%d.", dto.getId()));
            }
        }
        return null;
    }

    public List<MeetingEntity> mapListToBackend(List<MeetingDto> meetingDtoList) {
        return meetingDtoList.stream()
                .map(this::mapToBackend)
                .collect(Collectors.toList());
    }

    public MeetingDto mapToFrontend(MeetingEntity meetingEntity) {
        return meetingEntity != null ? MeetingDto.builder()
                .id(meetingEntity.getId())
                .meetingName(meetingEntity.getMeetingName())
                .begin(meetingEntity.getBegin().format(DateTimeFormatter.ofPattern(timeFormat)))
                .end(meetingEntity.getEnd().format(DateTimeFormatter.ofPattern(timeFormat)))
                .groupId(meetingEntity.getGroup().getId())
                .promoterId(meetingEntity.getPromoter().getId())
                .placeId(meetingEntity.getPlace().getId())
//                .membersIds(meetingEntity.getMembers()
//                        .stream()
//                        .map(m -> m.getId())
//                        .collect(Collectors.toList()))
                .build() : null;
    }

    public List<MeetingDto> mapToFrontend(List<MeetingEntity> meetingEntityList) {
        return meetingEntityList.stream()
                .map(this::mapToFrontend)
                .collect(Collectors.toList());
    }
}
