package pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupDto;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberApiDto;
import pl.com.devmeet.devmeetcore.domain.place.models.PlaceDto;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MeetingDto {

    private Long id;
    private String meetingName;

    private String begin;
    private String end;

    private Long groupId;
    private Long promoterId;
    private Long placeId;

    private GroupDto group;
    private MemberApiDto promoter;
    private PlaceDto place;

    private List<Long> membersIds;
}
