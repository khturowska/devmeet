package pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.status_and_exceptions.PollCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.status_and_exceptions.PollNotFoundException;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class PollCrudFinder {

    private PollCrudRepository pollCrudRepository;
    private PollGroupFinder groupFinder;

    public PollEntity findEntity(PollEntity dto) throws PollNotFoundException, GroupNotFoundException {
        if (dto.getId() != null)
            return findById(dto.getId())
                    .orElseThrow(() -> new PollNotFoundException(PollCrudStatusEnum.POLL_NOT_FOUND.toString()));
        else
            return findPollByGroup(dto)
                    .orElseThrow(() -> new PollNotFoundException(PollCrudStatusEnum.POLL_NOT_FOUND.toString()));
    }

    private GroupEntity findGroupEntity(GroupEntity group) throws GroupNotFoundException {
        return groupFinder.findGroup(group);
    }

    private Optional<PollEntity> findPollByGroup(PollEntity dto) throws GroupNotFoundException {
        GroupEntity group;
        group = findGroupEntity(dto.getGroup());

        return pollCrudRepository.findByGroup(group);
    }

    private Optional<List<PollEntity>> findPolls(PollEntity dto) throws GroupNotFoundException {
        GroupEntity group;
        group = findGroupEntity(dto.getGroup());

        return pollCrudRepository.findAllByGroup(group);
    }

    public List<PollEntity> findEntities(PollEntity dto) throws PollNotFoundException, GroupNotFoundException {
        Optional<List<PollEntity>> polls = findPolls(dto);

        if (polls.isPresent())
            return polls.get();

        throw new PollNotFoundException(PollCrudStatusEnum.POLLS_NOT_FOUND.toString());
    }

    public boolean isExist(PollEntity dto) {
        return false;
    }

    public Optional<PollEntity> findById(Long id) {
        return pollCrudRepository.findById(id);
    }
}
