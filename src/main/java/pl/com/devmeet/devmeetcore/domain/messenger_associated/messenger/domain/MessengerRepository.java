package pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;

import java.util.Optional;

@Repository
public interface MessengerRepository extends JpaRepository<MessengerEntity, Long> {

    Optional<MessengerEntity> findByGroup(GroupEntity groupEntity);

    Optional<MessengerEntity> findByMember(MemberEntity memberEntity);
}

