package pl.com.devmeet.devmeetcore.domain.group_associated.permission.domain.status_and_exceptions;

/**
 * Created by IntelliJ IDEA.
 * User: Kamil Ptasinski
 * Date: 16.11.2019
 * Time: 11:31
 */
public class PermissionAlreadyExistsException extends Exception {
    public PermissionAlreadyExistsException(String message) {
        super(message);
    }
}
