package pl.com.devmeet.devmeetcore.domain.member_associated.member.models;

import lombok.*;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingEntity;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "members")
@Entity
public class MemberEntity {

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private UserEntity user;

    //@Column(unique = true)
    private String nick;

//    @OneToOne(mappedBy = "member")
//    private MessengerEntity messenger;

    @OneToMany
    private List<GroupEntity> groupFounder;

    @ManyToMany(mappedBy = "members") // fetch = FetchType.EAGER)
    private List<GroupEntity> groups;

    @ManyToMany(mappedBy = "members")
    private List<MeetingEntity> meetings;

//    @OneToMany(mappedBy = "promoter")
//    private List<MeetingEntity> meetingPromoters;

//    @OneToMany(mappedBy = "member", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, orphanRemoval = true)
//    private List<AvailabilityEntity> availabilities;
//
//    @ManyToMany(mappedBy = "members", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
//    private List<PlaceEntity> places;
//
//    @OneToMany(mappedBy = "member", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, orphanRemoval = true)
//    private List<PermissionEntity> permissions;
//
//    @OneToMany(mappedBy = "member", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, orphanRemoval = true)
//    private List<AvailabilityVoteEntity> availabilityVotes;
//
//    @OneToMany(mappedBy = "member", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, orphanRemoval = true)
//    private List<PlaceVoteEntity> placeVotes;

    private LocalDateTime creationTime;

    private LocalDateTime modificationTime;

    private boolean isActive;
}
