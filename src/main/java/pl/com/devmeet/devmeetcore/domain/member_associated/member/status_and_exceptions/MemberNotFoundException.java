package pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions;

public class MemberNotFoundException extends Exception {

    public MemberNotFoundException() {
        super(MemberCrudStatusEnum.MEMBER_NOT_FOUND.toString());
    }

    public MemberNotFoundException(String message) {
        super(message);
    }

    public MemberNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
