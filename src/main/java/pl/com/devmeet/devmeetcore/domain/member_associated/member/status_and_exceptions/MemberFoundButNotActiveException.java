package pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions;

public class MemberFoundButNotActiveException extends Exception {

    public MemberFoundButNotActiveException() {
        super(MemberCrudStatusEnum.MEMBER_FOUND_BUT_NOT_ACTIVE.toString());
    }

    public MemberFoundButNotActiveException(String message) {
        super(message);
    }

    public MemberFoundButNotActiveException(String message, Throwable cause) {
        super(message, cause);
    }
}
