package pl.com.devmeet.devmeetcore.domain.group_associated.meeting.crud;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudRepository;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberRepository;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerRepository;
import pl.com.devmeet.devmeetcore.domain.place.crud.PlaceCrudRepository;
import pl.com.devmeet.devmeetcore.domain.place.crud.PlaceCrudService;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class MeetingCrudService {

    private MeetingCrudRepository meetingRepository;
    private PlaceCrudRepository placeRepository;
    private GroupCrudRepository groupRepository;
    private UserRepository userRepository;
    private MemberRepository memberRepository;
    private MessengerRepository messengerRepository;

    @Autowired
    public MeetingCrudService(MeetingCrudRepository meetingRepository, PlaceCrudRepository placeRepository, GroupCrudRepository groupRepository, UserRepository userRepository, MemberRepository memberRepository, MessengerRepository messengerRepository) {
        this.meetingRepository = meetingRepository;
        this.placeRepository = placeRepository;
        this.groupRepository = groupRepository;
        this.userRepository = userRepository;
        this.memberRepository = memberRepository;
        this.messengerRepository = messengerRepository;
    }

    private MeetingCrudSaver initSaver() {
        return MeetingCrudSaver.builder()
                .meetingCrudRepository(meetingRepository)
                .build();
    }

    private MeetingCrudFinder initFinder() {
        return MeetingCrudFinder.builder()
                .meetingRepository(meetingRepository)
                .meetingGroupFinder(initGroupFinder())
                .build();
    }

    private MeetingCrudCreator initCreator() {
        return MeetingCrudCreator.builder()
                .meetingCrudFinder(initFinder())
                .meetingCrudSaver(initSaver())
                .groupFinder(initGroupFinder())
                .placeFinder(initPlaceFinder())
                .build();
    }

    private MeetingCrudUpdater initUpdater() {
        return MeetingCrudUpdater.builder()
                .meetingCrudFinder(initFinder())
                .meetingCrudSaver(initSaver())
                .build();
    }

    private MeetingCrudDeactivator initDeleter() {
        return MeetingCrudDeactivator.builder()
                .meetingCrudFinder(initFinder())
                .meetingCrudSaver(initSaver())
                .build();
    }

    private MeetingGroupFinder initGroupFinder() {
        return new MeetingGroupFinder(new GroupCrudService(groupRepository, memberRepository, userRepository, messengerRepository));
    }

    private MeetingPlaceFinder initPlaceFinder() {
        return new MeetingPlaceFinder(new PlaceCrudService(placeRepository, memberRepository, userRepository, messengerRepository, groupRepository));
    }

    public MeetingEntity add(MeetingEntity dto) throws MeetingAlreadyExistsException, GroupNotFoundException, PlaceNotFoundException {
        return initCreator().createEntity(dto);
    }

    public MeetingEntity findByIdOrFeatures(MeetingEntity dto) throws MeetingNotFoundException, GroupNotFoundException {
        return initFinder().findByIdOrFeatures(dto);
    }

    public MeetingEntity findById(Long id) throws MeetingNotFoundException {
        return initFinder().findById(id);
    }

    public List<MeetingEntity> findMeetingsByGroup(GroupEntity groupEntity) throws GroupNotFoundException, MeetingNotFoundException {
        return initFinder().findMeetingEntitiesByGroup(groupEntity);
    }

    public List<MeetingEntity> findMeetingsByGroup(Long groupId) throws MeetingNotFoundException {
        return initFinder().findMeetingEntitiesByGroupId(groupId);
    }

    public List<MeetingEntity> findMeetingsByPlace(Long placeId) throws MeetingNotFoundException {
        return initFinder().findMeetingEntitiesByPlaceId(placeId);
    }

    public MeetingEntity update(MeetingEntity meetingEntity) throws MeetingNotFoundException, MeetingFoundButNotActiveException {
        return initUpdater().updateEntity(meetingEntity);
    }

    public MeetingEntity deactivate(MeetingEntity dto) throws GroupNotFoundException, MeetingFoundButNotActiveException, MeetingNotFoundException {
        return initDeleter().deactivateEntity(dto);
    }

    public MeetingEntity deactivate(Long meetingId) throws MeetingFoundButNotActiveException, MeetingNotFoundException {
        return initDeleter().deactivateEntity(meetingId);
    }

    public List<MeetingEntity> findAll() {
        List<MeetingEntity> meetingEntityList = initFinder().findAll();
        List<MeetingEntity> meetingDtoList = new ArrayList<>();
        for (MeetingEntity entity : meetingEntityList) {
            meetingDtoList.add(entity);
        }
        return meetingDtoList;
    }

    public List<MeetingEntity> findAllByIsActive(boolean isActive) {
        return initFinder().findByIsActive(isActive);
    }
}