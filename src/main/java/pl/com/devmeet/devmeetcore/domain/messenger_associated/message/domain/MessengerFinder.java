package pl.com.devmeet.devmeetcore.domain.messenger_associated.message.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudRepository;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberRepository;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerCrudService;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerEntity;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerRepository;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserRepository;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class MessengerFinder {

    private MessengerRepository messengerRepository;
    private UserRepository userRepository;
    private MemberRepository memberRepository;
    private GroupCrudRepository groupCrudRepository;

    public MessengerEntity findMessenger(MessengerEntity messengerEntity) throws UserNotFoundException, MemberNotFoundException, MessengerNotFoundException, GroupNotFoundException {
        return initFacade().findByMessenger(messengerEntity);
    }

    private MessengerCrudService initFacade() {
        return new MessengerCrudService(messengerRepository, userRepository, memberRepository, groupCrudRepository);
    }
}
