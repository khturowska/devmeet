package pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;

@Builder
@AllArgsConstructor
@NoArgsConstructor
class AvailabilityVoteCrudSaver {

    private AvailabilityVoteCrudRepository availabilityVoteCrudRepository;

    public AvailabilityVoteEntity saveEntity(AvailabilityVoteEntity entity) throws GroupNotFoundException, MemberNotFoundException {
        return availabilityVoteCrudRepository.save(entity);
    }
}
