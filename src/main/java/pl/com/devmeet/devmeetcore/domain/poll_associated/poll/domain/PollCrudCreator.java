package pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.status_and_exceptions.PollAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.status_and_exceptions.PollCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.status_and_exceptions.PollNotFoundException;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class PollCrudCreator {

    private PollCrudSaver pollCrudSaver;
    private PollCrudFinder pollCrudFinder;
    private PollGroupFinder pollGroupFinder;

    public PollEntity createEntity(PollEntity dto) throws PollAlreadyExistsException, GroupNotFoundException {
        PollEntity poll;

        try {
            poll = pollCrudFinder.findEntity(dto);

            if (!poll.isActive()) {
                return pollCrudSaver.saveEntity(setDefaultValuesWhenPollExist(dto));
            }

        } catch (PollNotFoundException e) {
            poll = setDefaultValuesWhenPollNotExist(dto);
            return pollCrudSaver.saveEntity(poll);
        }
        throw new PollAlreadyExistsException(PollCrudStatusEnum.POLL_ALREADY_EXISTS.toString());
    }

    private PollEntity setDefaultValuesWhenPollNotExist(PollEntity entity) {
        entity.setActive(true);
        entity.setCreationTime(DateTime.now());
        return entity;
    }

    private PollEntity setDefaultValuesWhenPollExist(PollEntity entity) {
        return setDefaultValuesWhenPollNotExist(entity);
    }

    private PollEntity connectPollWithGroup(PollEntity pollEntity) throws GroupNotFoundException {
        GroupEntity groupEntity = pollEntity.getGroup();

        if (groupEntity.getId() == null)
            groupEntity = pollGroupFinder.findGroup(groupEntity);

        pollEntity.setGroup(groupEntity);
        return pollEntity;
    }
}
