package pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.models.AvailabilityEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions.AvailabilityNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain.status_and_exceptions.AvailabilityVoteAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain.status_and_exceptions.AvailabilityVoteCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain.status_and_exceptions.AvailabilityVoteNotFoundException;
import pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.PollEntity;
import pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.status_and_exceptions.PollNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

@Builder
@AllArgsConstructor
@NoArgsConstructor
class AvailabilityVoteCrudCreator {

    private AvailabilityVoteCrudFinder voteCrudFinder;
    private AvailabilityVotePollFinder pollFinder;
    private AvailabilityVoteMemberFinder memberFinder;
    private AvailabilityVoteAvailabilityFinder availabilityFinder;
    private AvailabilityVoteCrudSaver voteCrudSaver;

    public AvailabilityVoteEntity createEntity(AvailabilityVoteEntity dto) throws AvailabilityVoteAlreadyExistsException, AvailabilityNotFoundException, MemberNotFoundException, GroupNotFoundException, UserNotFoundException, PollNotFoundException {
        try {
            voteCrudFinder.findEntity(dto);
        } catch (AvailabilityVoteNotFoundException e) {
            return voteCrudSaver
                    .saveEntity(setDefaultValuesWhenVoteNotExist(
                            connectVoteWithAvailability(
                                    connectVoteWithMember(
                                            connectVoteWithPoll(dto)))));
        }
        throw new AvailabilityVoteAlreadyExistsException(AvailabilityVoteCrudStatusEnum.AVAILABILITY_VOTE_ALREADY_EXISTS.toString());
    }

    private AvailabilityVoteEntity setDefaultValuesWhenVoteNotExist(AvailabilityVoteEntity entity) {
        entity.setCreationTime(DateTime.now());
        entity.setActive(true);

        return entity;
    }

    private AvailabilityVoteEntity connectVoteWithMember(AvailabilityVoteEntity voteEntity) throws MemberNotFoundException, UserNotFoundException {
        MemberEntity memberEntity = voteEntity.getMember();

        if (memberEntity.getId() == null)
            memberEntity = memberFinder.findMember(memberEntity);

        voteEntity.setMember(memberEntity);
        return voteEntity;
    }

    private AvailabilityVoteEntity connectVoteWithAvailability(AvailabilityVoteEntity voteEntity) throws MemberNotFoundException, AvailabilityNotFoundException, UserNotFoundException {
        AvailabilityEntity availabilityEntity = voteEntity.getAvailability();

        if (availabilityEntity.getId() == null)
            availabilityEntity = availabilityFinder.findAvailability(availabilityEntity);

        voteEntity.setAvailability(availabilityEntity);
        return voteEntity;
    }

    private AvailabilityVoteEntity connectVoteWithPoll(AvailabilityVoteEntity voteEntity) throws GroupNotFoundException, PollNotFoundException {
        PollEntity pollEntity = voteEntity.getPoll();

        if (pollEntity.getId() == null)
            pollEntity = pollFinder.findPoll(pollEntity);

        voteEntity.setPoll(pollEntity);
        return voteEntity;
    }
}
