package pl.com.devmeet.devmeetcore.domain.member_associated.member.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerEntity;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.time.LocalDateTime;

@Builder
@AllArgsConstructor
@NoArgsConstructor
class MemberCrudDeactivator {

    private MemberCrudFinder memberCrudFinder;
    private MemberCrudSaver memberCrudSaver;
    private MemberMessengerDeactivator memberMessengerDeactivator;

    public MemberEntity deactivateByMember(MemberEntity memberDto) throws MemberFoundButNotActiveException, MemberNotFoundException, UserNotFoundException, MessengerAlreadyExistsException, MessengerNotFoundException, GroupNotFoundException {
        MemberEntity memberEntity = findMember(memberDto);
        return deactivate(memberEntity);
    }

    public MemberEntity deactivateById(Long memberId) throws MemberNotFoundException, UserNotFoundException, GroupNotFoundException, MessengerNotFoundException, MessengerAlreadyExistsException, MemberFoundButNotActiveException {
        MemberEntity memberEntity = memberCrudFinder.findById(memberId);
        return deactivate(memberEntity);
    }

    private MemberEntity deactivate(MemberEntity memberEntity) throws UserNotFoundException, MessengerNotFoundException, GroupNotFoundException, MemberNotFoundException, MessengerAlreadyExistsException, MemberFoundButNotActiveException {
        if (memberEntity.isActive())
            return saveMember(
                    setDefaultValuesWhenMemberIsDeactivate(memberEntity));

        throw new MemberFoundButNotActiveException(String.format("Member (id=%d) found but not active", memberEntity.getId()));
    }

    private MemberEntity setDefaultValuesWhenMemberIsDeactivate(MemberEntity memberEntity) throws UserNotFoundException, MemberNotFoundException, MessengerAlreadyExistsException, MessengerNotFoundException, GroupNotFoundException {
        memberEntity.setActive(false);
        memberEntity.setModificationTime(LocalDateTime.now());

        return memberEntity;
    }

    private MemberEntity findMember(MemberEntity dto) throws MemberNotFoundException, UserNotFoundException {
        return memberCrudFinder.findEntityByIdOrFeatures(dto);
    }

    private MessengerEntity deactivateMessenger(MemberEntity memberEntity) throws UserNotFoundException, MessengerNotFoundException, MemberNotFoundException, GroupNotFoundException, MessengerAlreadyExistsException {
        return memberMessengerDeactivator.deactivateMessenger(memberEntity);
    }

    private MemberEntity saveMember(MemberEntity entity) {
        return memberCrudSaver.saveEntity(entity);
    }
}