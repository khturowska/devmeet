package pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain.status_and_exceptions;

/**
 * Created by IntelliJ IDEA.
 * User: Kamil Ptasinski
 * Date: 16.11.2019
 * Time: 00:20
 */
public class AvailabilityVoteException extends Exception {
    public AvailabilityVoteException(String message) {
        super(message);
    }
}
