package pl.com.devmeet.devmeetcore.domain.member_associated.availability.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class AvailabilityDto {

    private Long id;
    private Long memberId;

    private String beginDate;
    private String beginTime;

    private String endDate;
    private String endTime;

    private Boolean freeTime;
    private Boolean remoteWork;
}
