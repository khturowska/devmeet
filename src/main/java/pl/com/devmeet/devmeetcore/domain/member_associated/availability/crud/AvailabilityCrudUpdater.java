package pl.com.devmeet.devmeetcore.domain.member_associated.availability.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.models.AvailabilityEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions.AvailabilityMembersAreNotTheSameException;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions.AvailabilityNotFoundException;

import java.time.LocalDateTime;

@Slf4j
@AllArgsConstructor
@NoArgsConstructor
@Builder
class AvailabilityCrudUpdater {

    private AvailabilityCrudSaver availabilityCrudSaver;
    private AvailabilityCrudFinder availabilityCrudFinder;

    public AvailabilityEntity updateEntity(AvailabilityEntity update) throws AvailabilityNotFoundException, AvailabilityMembersAreNotTheSameException {
        AvailabilityEntity found = findAvailabilityEntity(update);
        checkMember(found, update);
        return availabilityCrudSaver
                .saveEntity(
                        updateAllowedParameters(found, update));
    }

    private AvailabilityEntity findAvailabilityEntity(AvailabilityEntity oldDto) throws AvailabilityNotFoundException {
        return availabilityCrudFinder.findByEntity(oldDto);
    }

    private AvailabilityEntity updateAllowedParameters(AvailabilityEntity oldEntity, AvailabilityEntity newEntity) {
        return AvailabilityEntity.builder()
                .id(oldEntity.getId())
                .member(oldEntity.getMember())
                .creationTime(oldEntity.getCreationTime())
                .beginDate(newEntity.getBeginDate() != null ? newEntity.getBeginDate() : oldEntity.getBeginDate())
                .beginTime(newEntity.getBeginTime() != null ? newEntity.getBeginTime() : oldEntity.getBeginTime())
                .endDate(newEntity.getEndDate() != null ? newEntity.getEndDate() : oldEntity.getEndDate())
                .endTime(newEntity.getEndTime() != null ? newEntity.getEndTime() : oldEntity.getEndTime())
                .freeTime(newEntity.getFreeTime() != null ? newEntity.getFreeTime() : oldEntity.getFreeTime())
                .remoteWork(newEntity.getRemoteWork() != null ? newEntity.getRemoteWork() : oldEntity.getRemoteWork())
                .modificationTime(LocalDateTime.now())
                .build();
    }

    private AvailabilityEntity checkMember(AvailabilityEntity oldDto, AvailabilityEntity newDto) throws AvailabilityMembersAreNotTheSameException {
        if (oldDto.getMember().getNick().equals(newDto.getMember().getNick()))
            return newDto;
        String err = String.format(("The Availability update has another memberId (memberId=%d) " +
                "like Availability (id=%d) found in the database (found memberId=%d)"), newDto.getMember().getId(), oldDto.getId(), oldDto.getMember().getId());
        log.warn(err);
        throw new AvailabilityMembersAreNotTheSameException(err);
    }
}
