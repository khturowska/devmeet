package pl.com.devmeet.devmeetcore.domain.group_associated.permission.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.group_associated.permission.domain.status_and_exceptions.PermissionAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.group_associated.permission.domain.status_and_exceptions.PermissionCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class PermissionCrudCreator {

    private PermissionCrudSaver permissionCrudSaver;
    private PermissionCrudFinder permissionCrudFinder;

    public PermissionEntity createEntity(PermissionDto dto) throws PermissionAlreadyExistsException, GroupNotFoundException, MemberNotFoundException, UserNotFoundException {
        PermissionEntity permission;
//
//        try {
//            permission = permissionCrudFinder.findEntity(dto);
//
//            if (!permission.isActive() && permission.getModificationTime() != null) {
//                return permissionCrudSaver.saveEntity(setDefaultValuesWhenPermissionExist(permission));
//            }
//
//        } catch (PermissionNotFoundException e) {
//            permission = setDefaultValuesWhenPermissionNotExist(PermissionCrudService.map(dto));
//            return permissionCrudSaver.saveEntity(permission);
//        }

        throw new PermissionAlreadyExistsException(PermissionCrudStatusEnum.PERMISSION_ALREADY_EXISTS.toString());
    }

    private PermissionEntity setDefaultValuesWhenPermissionNotExist(PermissionEntity permission) {
        permission.setActive(true);
        permission.setCreationTime(DateTime.now());
        return permission;
    }

    private PermissionEntity setDefaultValuesWhenPermissionExist(PermissionEntity permission) {
        permission.setActive(true);
        permission.setModificationTime(DateTime.now());
        return permission;
    }
}
