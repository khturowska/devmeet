package pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.place.models.PlaceEntity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "meetings")
@Entity
public class MeetingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String meetingName;

    private LocalDateTime begin;
    private LocalDateTime end;

    @ManyToOne
    private GroupEntity group;

    @ManyToOne
    private MemberEntity promoter;

    @ManyToOne
    private PlaceEntity place;

    @ManyToMany
    private List<MemberEntity> members;

    private LocalDateTime modificationTime;

    private LocalDateTime creationTime;
    private boolean isActive;
}
