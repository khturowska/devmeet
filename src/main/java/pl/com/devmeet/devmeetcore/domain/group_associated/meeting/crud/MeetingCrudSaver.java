package pl.com.devmeet.devmeetcore.domain.group_associated.meeting.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingEntity;

//@Component
@Builder
@NoArgsConstructor
@AllArgsConstructor
class MeetingCrudSaver {

    MeetingCrudRepository meetingCrudRepository;

    public MeetingEntity saveEntity(MeetingEntity entity) {
        return meetingCrudRepository.save(entity);
    }
}
