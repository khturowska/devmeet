package pl.com.devmeet.devmeetcore.domain.member_associated.availability.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.models.AvailabilityEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions.AvailabilityCrudInfoStatusEnum;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions.AvailabilityNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.util.Collections;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Builder
class AvailabilityCrudFinder {

    private AvailabilityCrudRepository availabilityRepository;
    private AvailabilityMemberFinder memberFinder;

    public AvailabilityEntity findByEntity(AvailabilityEntity dto) throws AvailabilityNotFoundException {
        return findById(dto.getId());
    }

    public AvailabilityEntity findById(Long id) throws AvailabilityNotFoundException {
        if (id != null)
            return availabilityRepository.findById(id)
                    .orElseThrow(() -> new AvailabilityNotFoundException(AvailabilityCrudInfoStatusEnum.AVAILABILITY_NOT_FOUND.toString()));
        throw new AvailabilityNotFoundException(AvailabilityCrudInfoStatusEnum.AVAILABILITY_NOT_FOUND.toString());
    }

    public List<AvailabilityEntity> findEntitiesByMember(AvailabilityEntity dto) throws AvailabilityNotFoundException, MemberNotFoundException, UserNotFoundException {
        MemberEntity member = findMemberEntity(dto.getMember());
        return availabilityRepository.findAllByMember(member)
                .orElse(Collections.emptyList());
    }

    public List<AvailabilityEntity> findAll() {
        return availabilityRepository.findAll();
    }

    private MemberEntity findMemberEntity(MemberEntity member) throws MemberNotFoundException, UserNotFoundException {
        return memberFinder.findMember(member);
    }

    public boolean isExist(AvailabilityEntity dto) {
        try {
            return findById(dto.getId()) != null;
        } catch (AvailabilityNotFoundException e) {
            return false;
        }
    }
}
