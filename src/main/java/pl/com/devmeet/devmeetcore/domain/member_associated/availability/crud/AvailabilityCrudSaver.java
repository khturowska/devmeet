package pl.com.devmeet.devmeetcore.domain.member_associated.availability.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.models.AvailabilityEntity;


@Builder
@AllArgsConstructor
@NoArgsConstructor
class AvailabilityCrudSaver {

    private AvailabilityCrudRepository availabilityCrudRepository;

    public AvailabilityEntity saveEntity(AvailabilityEntity entity) {
        return availabilityCrudRepository
                .save(entity);
    }
}

