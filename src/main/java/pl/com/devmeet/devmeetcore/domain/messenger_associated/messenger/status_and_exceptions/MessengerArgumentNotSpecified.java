package pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions;

/**
 * Created by IntelliJ IDEA.
 * User: Kamil Ptasinski
 * Date: 01.12.2019
 * Time: 14:28
 */

public class MessengerArgumentNotSpecified extends Exception {
    public MessengerArgumentNotSpecified(String message) {
        super(message);
    }
}
