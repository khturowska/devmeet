package pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.PollEntity;

import java.util.List;
import java.util.Optional;

@Repository
public interface AvailabilityVoteCrudRepository extends JpaRepository<AvailabilityVoteEntity, Long> {

    Optional<List<AvailabilityVoteEntity>> findAllByPoll(PollEntity pollEntity);

    Optional<AvailabilityVoteEntity> findByMemberAndPoll(MemberEntity memberEntity, PollEntity pollEntity);
}
