package pl.com.devmeet.devmeetcore.domain.group_associated.group.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "tgroup")
public class GroupEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank @NotNull @NotEmpty
    private String groupName;
    private String website;
    private String description;

    @ManyToOne
    private MemberEntity founder;
//    @OneToOne(mappedBy = "group", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, orphanRemoval = true)
//    private MessengerEntity messenger;

    private Integer membersLimit;

    @ManyToMany//(fetch = FetchType.EAGER)
    private List<MemberEntity> members;
//
//    @OneToMany(mappedBy = "group", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, orphanRemoval = true)
//    private List<PermissionEntity> permissions;
//
//    @OneToMany(mappedBy = "group", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, orphanRemoval = true)
//    private List<PollEntity> polls;

    @OneToMany(mappedBy = "group")
    private List<MeetingEntity> meetings;

    private LocalDateTime creationTime; // date and time format: DateTimeFormatter.ISO_LOCAL_DATE_TIME

    private LocalDateTime modificationTime;
    private boolean isActive;
}
