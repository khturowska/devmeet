package pl.com.devmeet.devmeetcore.domain.group_associated.meeting.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingNotFoundException;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class MeetingCrudUpdater {

    private MeetingCrudFinder meetingCrudFinder;
    private MeetingCrudSaver meetingCrudSaver;

    public MeetingEntity updateEntity(MeetingEntity meetingEntity) throws MeetingNotFoundException, MeetingFoundButNotActiveException {
        MeetingEntity foundEntity = meetingCrudFinder.findById(meetingEntity.getId());

        if (foundEntity.isActive())
            return meetingCrudSaver.saveEntity(updateAllowedValues(foundEntity, meetingEntity));
        else
            throw new MeetingFoundButNotActiveException(MeetingCrudStatusEnum.FOUND_BUT_NOT_ACTIVE.toString() + ": id=" + foundEntity.getId());
    }

    private MeetingEntity updateAllowedValues(MeetingEntity oldMeeting, MeetingEntity newMeeting) {
        return MeetingEntity.builder()
                .id(oldMeeting.getId())
                .group(oldMeeting.getGroup())
                .creationTime(oldMeeting.getCreationTime())
                .isActive(oldMeeting.isActive())
                .meetingName((!newMeeting.getMeetingName().isEmpty() && newMeeting.getMeetingName() != null) ? newMeeting.getMeetingName() : oldMeeting.getMeetingName())
                .begin(newMeeting.getBegin() != null ? newMeeting.getBegin() : oldMeeting.getBegin())
                .end(newMeeting.getEnd() != null ? newMeeting.getEnd() : oldMeeting.getEnd())
//                .beginDate(newMeeting.getBeginDate() != null ? newMeeting.getBeginDate() : oldMeeting.getBeginDate())
//                .beginTime(newMeeting.getBeginTime() != null ? newMeeting.getBeginTime() : oldMeeting.getBeginTime())
//                .endDate(newMeeting.getEndDate() != null ? newMeeting.getEndDate() : oldMeeting.getEndDate())
//                .endTime(newMeeting.getEndTime() != null ? newMeeting.getEndTime() : oldMeeting.getEndTime())
                .place(newMeeting.getPlace() != null ? newMeeting.getPlace() : oldMeeting.getPlace())
                .members((newMeeting.getMembers() != null && newMeeting.getMembers().size() != oldMeeting.getMembers().size()) ?
                        newMeeting.getMembers() : oldMeeting.getMembers())
                .modificationTime(LocalDateTime.now())
                .build();
    }
}