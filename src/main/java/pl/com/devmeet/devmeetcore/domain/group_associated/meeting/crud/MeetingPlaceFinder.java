package pl.com.devmeet.devmeetcore.domain.group_associated.meeting.crud;

import lombok.AllArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.place.crud.PlaceCrudService;
import pl.com.devmeet.devmeetcore.domain.place.models.PlaceEntity;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceNotFoundException;

@AllArgsConstructor
class MeetingPlaceFinder {

    private PlaceCrudService placeService;

    public PlaceEntity findPlace(PlaceEntity placeEntity) throws PlaceNotFoundException {
        return placeService.findPlaceByIdOrFeatures(placeEntity);
    }
}
