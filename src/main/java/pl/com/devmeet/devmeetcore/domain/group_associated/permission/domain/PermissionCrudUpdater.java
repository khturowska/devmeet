package pl.com.devmeet.devmeetcore.domain.group_associated.permission.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class PermissionCrudUpdater {

    private PermissionCrudFinder permissionCrudFinder;
    private PermissionCrudSaver permissionCrudSaver;

//    public PermissionEntity updateEntity(PermissionDto oldDto, PermissionDto newDto) throws GroupNotFoundException, MemberNotFoundException, UserNotFoundException, PermissionException, PermissionNotFoundException {
//        PermissionEntity oldPermission = findPermissionEntity(oldDto);
//        PermissionEntity newPermission = mapDtoToEntity(checkMemberAndGroup(oldDto, newDto));
//
//        return permissionCrudSaver.saveEntity(updateAllowedParameters(oldPermission, newPermission));
//    }
//
//    public PermissionEntity changeMemberBanInGroupStatus(PermissionDto oldDto, boolean memberBanInGroupStatus) throws GroupNotFoundException, MemberNotFoundException, UserNotFoundException, PermissionNotFoundException {
//        PermissionEntity foundPermission = findPermissionEntity(oldDto);
//
//        if (foundPermission.isMemberBaned() != memberBanInGroupStatus) {
//            foundPermission.setMemberBaned(memberBanInGroupStatus);
//            foundPermission.setModificationTime(DateTime.now());
//
//            return permissionCrudSaver.saveEntity(foundPermission);
//        }
//        return foundPermission;
//    }

//    private PermissionEntity findPermissionEntity(PermissionDto oldDto) throws UserNotFoundException, GroupNotFoundException, MemberNotFoundException, PermissionNotFoundException {
//        return permissionCrudFinder.findEntity(oldDto);
//    }

//    private PermissionDto checkMemberAndGroup(PermissionDto oldDto, PermissionDto newDto) throws PermissionException {
//        if (oldDto.getMember().getNick().equals(newDto.getMember().getNick()))
//            if(oldDto.getGroup().getGroupName().equals(newDto.getGroup().getGroupName()))
//                return newDto;
//        throw new PermissionException(PermissionCrudStatusEnum.INCORRECT_MEMBER_OR_GROUP.toString());
//    }

    private PermissionEntity mapDtoToEntity(PermissionDto dto) {
        return PermissionCrudService.map(dto);
    }

    private PermissionEntity updateAllowedParameters(PermissionEntity oldEntity, PermissionEntity newEntity) {
        oldEntity.setPossibleToVote(newEntity.isPossibleToVote());
        oldEntity.setPossibleToMessaging(newEntity.isPossibleToMessaging());
        oldEntity.setPossibleToChangeGroupName(newEntity.isPossibleToChangeGroupName());
        oldEntity.setPossibleToBanMember(newEntity.isPossibleToBanMember());

        oldEntity.setMemberBaned(newEntity.isMemberBaned());

        oldEntity.setModificationTime(DateTime.now());
        return oldEntity;
    }
}
