package pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.status_and_exceptions;

/**
 * Created by IntelliJ IDEA.
 * User: Kamil Ptasinski
 * Date: 16.11.2019
 * Time: 09:11
 */
public class PollNotFoundException extends Exception {
    public PollNotFoundException(String message) {
        super(message);
    }
}
