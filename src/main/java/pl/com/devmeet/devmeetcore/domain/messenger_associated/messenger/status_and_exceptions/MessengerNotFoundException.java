package pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions;

/**
 * Created by IntelliJ IDEA.
 * User: kamil
 * Date: 29.11.2019
 * Time: 18:37
 */

public class MessengerNotFoundException extends Exception {
    public MessengerNotFoundException(String message) {
        super(message);
    }
}
