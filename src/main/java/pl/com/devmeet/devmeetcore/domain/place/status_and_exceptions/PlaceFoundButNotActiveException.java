package pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions;

public class PlaceFoundButNotActiveException extends Exception {

    public PlaceFoundButNotActiveException(String message) {
        super(message);
    }
}
