package pl.com.devmeet.devmeetcore.domain.place.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.place.models.PlaceEntity;

@Builder
@AllArgsConstructor
@NoArgsConstructor
class PlaceCrudSaver {

    private PlaceCrudRepository placeCrudRepository;
    private PlaceMemberFinder memberFinder;

    public PlaceEntity saveEntity(PlaceEntity entity) {
        return placeCrudRepository
                .save(entity);
    }
}