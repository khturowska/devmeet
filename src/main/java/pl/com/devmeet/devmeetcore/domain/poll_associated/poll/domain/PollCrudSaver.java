package pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
class PollCrudSaver {

    private PollCrudRepository pollCrudRepository;

    public PollEntity saveEntity(PollEntity entity) throws GroupNotFoundException {
        return pollCrudRepository.save(entity);
    }
}
