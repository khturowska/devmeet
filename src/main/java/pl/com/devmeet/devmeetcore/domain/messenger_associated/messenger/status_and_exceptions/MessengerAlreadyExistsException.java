package pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions;

/**
 * Created by IntelliJ IDEA.
 * User: kamil
 * Date: 29.11.2019
 * Time: 18:52
 */

public class MessengerAlreadyExistsException extends Exception {
    public MessengerAlreadyExistsException(String message) {
        super(message);
    }
}
