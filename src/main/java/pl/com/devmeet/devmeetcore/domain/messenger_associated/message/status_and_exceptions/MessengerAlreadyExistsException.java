package pl.com.devmeet.devmeetcore.domain.messenger_associated.message.status_and_exceptions;

/**
 * Created by IntelliJ IDEA.
 * User: Kamil Ptasinski
 * Date: 02.12.2019
 * Time: 17:32
 */

public class MessengerAlreadyExistsException extends Exception {
    public MessengerAlreadyExistsException(String message) {
        super(message);
    }
}
