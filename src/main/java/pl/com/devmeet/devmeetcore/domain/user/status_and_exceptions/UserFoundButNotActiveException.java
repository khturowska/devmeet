package pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions;

public class UserFoundButNotActiveException extends Exception {

    public UserFoundButNotActiveException() {
        super(UserCrudStatusEnum.USER_FOUND_BUT_NOT_ACTIVE.toString());
    }

    public UserFoundButNotActiveException(String message) {
        super(message);
    }

    public UserFoundButNotActiveException(String message, Throwable cause) {
        super(message, cause);
    }
}
