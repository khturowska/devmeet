package pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions;

public class MeetingFoundButNotActiveException extends Exception {

    public MeetingFoundButNotActiveException() {
        super(MeetingCrudStatusEnum.FOUND_BUT_NOT_ACTIVE.toString());
    }

    public MeetingFoundButNotActiveException(String message) {
        super(message);
    }

    public MeetingFoundButNotActiveException(String message, Throwable cause) {
        super(message, cause);
    }
}
