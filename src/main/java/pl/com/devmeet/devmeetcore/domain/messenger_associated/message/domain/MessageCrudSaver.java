package pl.com.devmeet.devmeetcore.domain.messenger_associated.message.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class MessageCrudSaver {

    private MessageRepository messageRepository;

    public MessageEntity saveEntity(MessageEntity messageEntity) {
        return messageRepository.save(messageEntity);
    }
}