package pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions;

//todo Usunąć enumy. Zamiast nich w wyjątkach wysyłać konkretny powód jego wystąpienia
//todo można zostawić te enumy i przekazywac je w konstruktorze bezparametrowym dla wyjątków tzn jeśli nie chcemy określać powodu to poleci domyslny z enuma, drug konstruktor z message moze przejąć ww. rolę

public enum UserCrudStatusEnum {
    USER_NOT_FOUND("User not found"),
    USER_FOUND_BUT_NOT_ACTIVE("User found but not active"),
    USER_ALREADY_ACTIVE("User already active"),
    USER_ALREADY_EXISTS("User already exists");

    private String status;

    UserCrudStatusEnum(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return status;
    }
}
