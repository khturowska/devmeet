package pl.com.devmeet.devmeetcore.domain.messenger_associated.message.status_and_exceptions;

/**
 * Created by IntelliJ IDEA.
 * User: Kamil Ptasinski
 * Date: 05.12.2019
 * Time: 01:25
 */
public class MessageArgumentNotSpecifiedException extends Exception {
    public MessageArgumentNotSpecifiedException(String message) {
        super(message);
    }
}
