package pl.com.devmeet.devmeetcore.domain.group_associated.group.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerEntity;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class GroupCrudDeactivator {

    private GroupCrudSaver groupCrudSaver;
    private GroupCrudFinder groupCrudFinder;
    private GroupMessengerDeactivator groupMessengerDeactivator;

    public GroupEntity deactivateEntity(GroupEntity dto) throws GroupFoundButNotActiveException, GroupNotFoundException, MemberNotFoundException {
        GroupEntity group = groupCrudFinder.findEntityByGroup(dto);

        boolean groupActivity = group.isActive();

        if (groupActivity) {
            return groupCrudSaver.saveEntity(
                    setDefaultValues(group));
        }

        throw new GroupFoundButNotActiveException(GroupCrudStatusEnum.GROUP_FOUND_BUT_NOT_ACTIVE.toString());
    }

    private GroupEntity setDefaultValues(GroupEntity groupEntity) throws MemberNotFoundException, GroupNotFoundException {
        groupEntity.setActive(false);
        groupEntity.setModificationTime(LocalDateTime.now());

//        deactivateGroupMessenger(groupEntity);

        return groupEntity;
    }

    private MessengerEntity deactivateGroupMessenger(GroupEntity groupEntity) throws UserNotFoundException, MessengerNotFoundException, MemberNotFoundException, GroupNotFoundException, MessengerAlreadyExistsException {
        return groupMessengerDeactivator.deactivateMessenger(groupEntity);
    }
}
