package pl.com.devmeet.devmeetcore.domain.user.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyExistsEmailDuplicationException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class UserCrudCreator {

    private UserCrudSaver userSaver;
    private UserCrudFinder userFinder;

    public UserEntity create(UserEntity dto) throws UserAlreadyExistsEmailDuplicationException {
        UserEntity user;
        boolean userActive;

        try {
            user = userFinder.findEntityByIdOrUserEmail(dto);

            userActive = user.isActive();

            if (!userActive && user.getModificationTime() != null) {
                user.setModificationTime(LocalDateTime.now());
                user.setActive(true);
                return saveUserEntity(user);
            }

        } catch (UserNotFoundException e) {
            return saveUserEntity(setDefaultValuesToNewUserEntity(dto));
        }
        throw new UserAlreadyExistsEmailDuplicationException(String.format("User with email: '%s' already exists!", user.getEmail()));
    }

    private UserEntity setDefaultValuesToNewUserEntity(UserEntity user) {
        user.setCreationTime(LocalDateTime.now());
        user.setModificationTime(null);
        user.setActive(false);
        return user;
    }

    private UserEntity saveUserEntity(UserEntity entity) {
        return userSaver.saveEntity(entity);
    }
}
