package pl.com.devmeet.devmeetcore.domain.place.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.place.models.PlaceEntity;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class PlaceCrudFinder {

    private PlaceCrudRepository placeRepository;
    private PlaceMemberFinder memberFinder;

    private MemberEntity findMemberEntity(MemberEntity member) throws MemberNotFoundException, UserNotFoundException {
        return memberFinder.findMember(member);
    }

    public PlaceEntity findPlaceByIdOrPlaceFutures(PlaceEntity placeEntity) throws PlaceNotFoundException {
        Long placeId = placeEntity.getId();
        if (placeId != null)
            return findPlaceById(placeId);
        else
            return findPlaceFeatures(placeEntity);
    }

    public PlaceEntity findPlaceFeatures(PlaceEntity placeEntity) throws PlaceNotFoundException {
        return placeRepository.findByLocation(placeEntity.getLocation())
                .orElseThrow(() -> new PlaceNotFoundException(PlaceCrudStatusEnum.PLACE_NOT_FOUND.toString()));
    }

    public PlaceEntity findPlaceById(Long id) throws PlaceNotFoundException {
        if (id != null)
            return placeRepository.findById(id)
                    .orElseThrow(() -> new PlaceNotFoundException("Place id=" + id + " not found!"));
        else
            throw new PlaceNotFoundException("id cannot be null!");
    }

//    public PlaceDto findPlaceByIdAndMapToDto(Long id) throws PlaceNotFoundException {
//        return mapToDto(findPlaceById(id));
//    }

    public List<PlaceEntity> findAllBySearchingText(String text) {
        return placeRepository.findAllBySearchText(text);
    }

//    public List<PlaceDto> findAllBySearchingTextAndMapToDto(String text) {
//        return mapToDtoList(findAllBySearchingText(text));
//    }

    public List<PlaceEntity> findAllEntities() {
        return placeRepository.findAll();
    }

//    public List<PlaceDto> findAllEntitiesAndMapToDto() {
//        return mapToDtoList(findAllEntities());
//    }

    public boolean isExist(PlaceEntity dto) {
        try {
            return findPlaceById(dto.getId()) != null;
        } catch (PlaceNotFoundException e) {
            return false;
        }
    }

//    public PlaceDto mapToDto(PlaceEntity entity) {
//        return PlaceCrudMapper.map(entity);
//    }
//
//    private List<PlaceDto> mapToDtoList(List<PlaceEntity> entities) {
//        return PlaceCrudMapper.mapDtoList(entities);
//    }
}