package pl.com.devmeet.devmeetcore.domain.member_associated.member.crud;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserCrudService;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

@RequiredArgsConstructor
class MemberUserFinder {

    @NonNull
    private UserCrudService userCrudService;

    public UserEntity findUserEntity(UserEntity dto) throws UserNotFoundException {
        return userCrudService.findByIdOrUserEmail(dto);
    }
}
