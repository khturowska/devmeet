package pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions;

/**
 * Created by IntelliJ IDEA.
 * User: Kamil Ptasinski
 * Date: 15.11.2019
 * Time: 23:48
 */
public class AvailabilityFoundButNotActiveException extends Exception {
    public AvailabilityFoundButNotActiveException(String message) {
        super(message);
    }
}
