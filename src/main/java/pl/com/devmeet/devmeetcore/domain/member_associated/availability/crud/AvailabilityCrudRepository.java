package pl.com.devmeet.devmeetcore.domain.member_associated.availability.crud;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.models.AvailabilityEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;

import java.util.List;
import java.util.Optional;

@Repository
public interface AvailabilityCrudRepository extends JpaRepository<AvailabilityEntity, Long> {

    Optional<AvailabilityEntity> findByMember(MemberEntity member);

    Optional<List<AvailabilityEntity>> findAllByMember(MemberEntity member);
}
