package pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions;

/**
 * Created by IntelliJ IDEA.
 * User: Kamil Ptasinski
 * Date: 16.11.2019
 * Time: 11:08
 */
public class AvailabilityAlreadyExistsException extends Exception {
    public AvailabilityAlreadyExistsException(String message) {
        super(message);
    }
}
