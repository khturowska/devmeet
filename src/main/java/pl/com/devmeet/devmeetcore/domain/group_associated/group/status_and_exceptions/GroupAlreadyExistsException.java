package pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions;

public class GroupAlreadyExistsException extends Exception {

    public GroupAlreadyExistsException() {
        super(GroupCrudStatusEnum.GROUP_ALREADY_EXISTS.toString());
    }

    public GroupAlreadyExistsException(String message) {
        super(message);
    }

    public GroupAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }
}
