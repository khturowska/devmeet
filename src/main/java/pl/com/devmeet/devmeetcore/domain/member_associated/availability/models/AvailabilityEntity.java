package pl.com.devmeet.devmeetcore.domain.member_associated.availability.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "availabilities")
@Entity
public class AvailabilityEntity {

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    private MemberEntity member;

    private LocalDate beginDate;
    private LocalTime beginTime;

    private LocalDate endDate;
    private LocalTime endTime;

    private Boolean freeTime;
    private Boolean remoteWork;

    private LocalDateTime modificationTime;
    private boolean isActive;

    private LocalDateTime creationTime;
}
