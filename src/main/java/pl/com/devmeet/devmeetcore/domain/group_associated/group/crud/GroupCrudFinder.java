package pl.com.devmeet.devmeetcore.domain.group_associated.group.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@NoArgsConstructor
@Builder
class GroupCrudFinder {

    private GroupCrudRepository groupCrudRepository;
    private GroupMemberFinder memberFinder;

    public GroupEntity findById(Long id) throws GroupNotFoundException {
        if (id != null) {
            return groupCrudRepository.findById(id)
                    .orElseThrow(() -> new GroupNotFoundException("Group id=" + id + " not found!"));
        } else
            throw new GroupNotFoundException(GroupCrudStatusEnum.ID_IS_NOT_SPECIFIED.toString() + ": " + id);
    }

    public GroupEntity findEntityByGroup(GroupEntity group) throws GroupNotFoundException {
        Long id = group.getId();

        if (id != null)
            return findById(id);
        else
            return findEntityByGroupName(group.getGroupName());
    }


    public GroupEntity findEntityByGroupName(String groupName) throws GroupNotFoundException {
        Optional<GroupEntity> group = groupCrudRepository.findByGroupName(groupName);

        if (group.isPresent())
            return group.get();
        else
            throw new GroupNotFoundException(GroupCrudStatusEnum.GROUP_NOT_FOUND.toString());
    }

    public boolean isExist(GroupEntity dto) {
        try {
            return findEntityByGroup(dto) != null;
        } catch (GroupNotFoundException e) {
            return false;
        }
    }

    public List<GroupEntity> findAllEntities() {
        return groupCrudRepository.findAll();
    }
}
