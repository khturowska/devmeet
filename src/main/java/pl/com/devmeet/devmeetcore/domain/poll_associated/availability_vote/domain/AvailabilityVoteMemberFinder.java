package pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AvailabilityVoteMemberFinder {

    private MemberCrudService memberCrudService;

    public MemberEntity findMember(MemberEntity dto) throws MemberNotFoundException, UserNotFoundException {
        return memberCrudService.find(dto);
    }
}
