package pl.com.devmeet.devmeetcore.api;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceNotFoundException;

@RestControllerAdvice
class PlaceExceptionApiHandler {

    @ExceptionHandler({PlaceNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String placeNotFoundHandler(PlaceNotFoundException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler({PlaceAlreadyExistsException.class})
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public String placeAlreadyExistHandler(PlaceAlreadyExistsException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler({PlaceFoundButNotActiveException.class})
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public String placeFoundButNotActiveHandler(PlaceFoundButNotActiveException ex) {
        return ex.getMessage();
    }


}
