package pl.com.devmeet.devmeetcore.api;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.MemberAddNewMeetingInGroupService;
import pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.MemberAddToExistingMeetingInGroupService;
import pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.crud.RelationsMemberGroupMeetingCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupApiMapper;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingApiMapper;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingDto;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberApiMapper;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/member-group-meeting")
class MemberGroupMeetingApi {

    private MemberAddNewMeetingInGroupService memberAddNewMeetingInGroupService;
    private MemberAddToExistingMeetingInGroupService memberAddToExistingMeetingInGroupService;
    private RelationsMemberGroupMeetingCrudService relationsMemberGroupMeetingCrudService;

    private MeetingApiMapper meetingApiMapper;
    private MemberApiMapper memberApiMapper;
    private GroupApiMapper groupApiMapper;

    @Autowired
    public MemberGroupMeetingApi(MemberAddNewMeetingInGroupService memberAddNewMeetingInGroupService,
                                 MemberAddToExistingMeetingInGroupService memberAddToExistingMeetingInGroupService,
                                 RelationsMemberGroupMeetingCrudService relationsMemberGroupMeetingCrudService) {

        this.memberAddNewMeetingInGroupService = memberAddNewMeetingInGroupService;
        this.memberAddToExistingMeetingInGroupService = memberAddToExistingMeetingInGroupService;
        this.relationsMemberGroupMeetingCrudService = relationsMemberGroupMeetingCrudService;
        this.meetingApiMapper = new MeetingApiMapper();
        this.memberApiMapper = new MemberApiMapper();
        this.groupApiMapper = new GroupApiMapper();
    }

    //todo Czy taka forma jest do zaakceptowania? Może z frontendu powinny lecieć dwa obiekty: meeting i member
    @SneakyThrows
    @PostMapping("/create-new-meeting")
    public ResponseEntity memberAddNewMeetingInGroupAndReturnMeetingMembers(@RequestBody MeetingDto newMeeting) {
        MeetingEntity created = memberAddNewMeetingInGroupService
                .addNewMeetingInGroup(
                        meetingApiMapper
                                .mapToBackend(newMeeting));
        List<MemberEntity> meetingMembers = relationsMemberGroupMeetingCrudService
                .findAllMeetingMembers(created);

        return new ResponseEntity(memberApiMapper
                .mapToFrontend(meetingMembers),
                HttpStatus.CREATED);
    }

    @SneakyThrows
    @PutMapping("/add-member-to-meeting")
    public ResponseEntity addMemberToExistingMeetingAndReturnMeetingMembers(@RequestParam Long memberId, @RequestParam Long meetingId) {
        MeetingEntity meeting = memberAddToExistingMeetingInGroupService
                .addMemberToExistingMeetingInGroup(memberId, meetingId);
        List<MemberEntity> meetingMembers = relationsMemberGroupMeetingCrudService
                .findAllMeetingMembers(meeting);

        return new ResponseEntity(memberApiMapper
                .mapToFrontend(meetingMembers),
                HttpStatus.OK);
    }

    @SneakyThrows
    @GetMapping("/all-promoter-meetings/{promoterId}")
    public ResponseEntity findAllMeetingsWhereMemberIsAPromoter(@PathVariable("promoterId") Long promoterId) {
        List<MeetingEntity> promoterMeetings = relationsMemberGroupMeetingCrudService
                .findAllMeetingsWhereMemberIsAPromoter(MemberEntity.builder()
                        .id(promoterId)
                        .build());

        return new ResponseEntity(meetingApiMapper
                .mapToFrontend(promoterMeetings),
                HttpStatus.FOUND);
    }

    @SneakyThrows
    @GetMapping("/all-member-meetings/{memberId}")
    public ResponseEntity findAllMeetingWhereIsMember(@PathVariable("memberId") Long memberId) {
        List<MeetingEntity> memberMeetings = relationsMemberGroupMeetingCrudService
                .findAllMeetingWhereIsMember(MemberEntity.builder()
                        .id(memberId)
                        .build());

        return new ResponseEntity(meetingApiMapper
                .mapToFrontend(memberMeetings),
                HttpStatus.FOUND);
    }

    @SneakyThrows
    @GetMapping("/all-group-meetings/{groupId}")
    public ResponseEntity findAllMeetingsInGroup(@PathVariable("groupId") Long groupId) {
        List<MeetingEntity> groupMeetings = relationsMemberGroupMeetingCrudService
                .findAllMeetingsInGroup(GroupEntity.builder()
                        .id(groupId)
                        .build());

        return new ResponseEntity(meetingApiMapper
                .mapToFrontend(groupMeetings),
                HttpStatus.FOUND);
    }

    @SneakyThrows
    @GetMapping("/all-meeting-members/{meetingId}")
    public ResponseEntity findAllMeetingMembers(@PathVariable("meetingId") Long meetingId) {
        List<MemberEntity> meetingMembers = relationsMemberGroupMeetingCrudService
                .findAllMeetingMembers(MeetingEntity.builder()
                        .id(meetingId)
                        .build());

        return new ResponseEntity(memberApiMapper
                .mapToFrontend(meetingMembers),
                HttpStatus.FOUND);
    }
}
