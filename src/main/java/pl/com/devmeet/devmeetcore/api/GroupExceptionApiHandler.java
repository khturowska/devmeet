package pl.com.devmeet.devmeetcore.api;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupArgumentsNotSpecifiedException;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;

@RestControllerAdvice
class GroupExceptionApiHandler {

    @ExceptionHandler({GroupAlreadyExistsException.class})
    @ResponseStatus(HttpStatus.CONFLICT)
    public String groupAlreadyExistsHandler(GroupAlreadyExistsException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler({GroupArgumentsNotSpecifiedException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String groupArgumentsNotSpecifiedHandler(GroupArgumentsNotSpecifiedException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler({GroupFoundButNotActiveException.class})
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public String groupFoundButNotActiveHandler(GroupFoundButNotActiveException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler({GroupNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String groupNotFoundHandler(GroupNotFoundException ex) {
        return ex.getMessage();
    }


}
