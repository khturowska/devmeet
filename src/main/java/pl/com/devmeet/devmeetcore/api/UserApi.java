package pl.com.devmeet.devmeetcore.api;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserCrudService;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;

import java.net.URI;
import java.util.List;

import static java.lang.Boolean.parseBoolean;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/users")
class UserApi {

    private UserCrudService userCrudService;

    @Autowired
    public UserApi(UserCrudService userCrudService) {
        this.userCrudService = userCrudService;
    }

    // get

    @GetMapping
    public List<UserEntity> findAllUsers() {
        return userCrudService.findAll();
    }

    @SneakyThrows
    @GetMapping("{id}")
    public UserEntity getById(@PathVariable Long id) {
        return userCrudService.findById(id);
    }

    @SneakyThrows
    @GetMapping("/email/{email}")
    public UserEntity getByEmail(@PathVariable String email) {
        return userCrudService.findByEmail(email);
    }

    @GetMapping("/is-active/{isActive}")
    public List<UserEntity> getAllIsActive(@PathVariable String isActive) {
        return userCrudService.findAllByIsActive(parseBoolean(isActive));
    }


    // add

    @SneakyThrows
    @PostMapping
    public ResponseEntity<UserEntity> add(@RequestBody UserEntity user) {
        UserEntity added = userCrudService.add(user);
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(added.getId())
                .toUri();
        return ResponseEntity.created(uri).body(added);
    }

    // update

    @SneakyThrows
    @PutMapping("/{id}")
    public ResponseEntity<UserEntity> update(@PathVariable Long id,
                                             @RequestBody UserEntity user) {
        if (!id.equals(user.getId()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "id from path does not match with id in body!");
        return new ResponseEntity<>(userCrudService.update(user), HttpStatus.ACCEPTED);
    }

    // delete

    @SneakyThrows
    @DeleteMapping("/{id}")
    public ResponseEntity<?> removeUserById(@PathVariable Long id) {
        userCrudService.deleteById(id);
        return ResponseEntity.ok().build();
    }

}
