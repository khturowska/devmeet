package pl.com.devmeet.devmeetcore.api;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.com.devmeet.devmeetcore.feature_services.user_and_member.user_activation_after_registration.exceptions.InvalidUUIDStringException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.*;

@RestControllerAdvice
class UserExceptionApiHandler {

    @ExceptionHandler({UserNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String userNotFoundHandler(UserNotFoundException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler({UserFoundButNotActiveException.class})
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public String userFoundButNotActiveHandler(UserFoundButNotActiveException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler({UserAlreadyActiveException.class})
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public String userAlreadyActiveHandler(UserAlreadyActiveException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler({UserAlreadyExistsEmailDuplicationException.class})
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public String userAlreadyExistsHandler(UserAlreadyExistsEmailDuplicationException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler({InvalidUUIDStringException.class})
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public String invalidUUIDStringHandler(InvalidUUIDStringException ex) {
        return ex.getMessage();
    }

}
