package pl.com.devmeet.devmeetcore.api;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberUserNotActiveException;

@RestControllerAdvice
class MemberExceptionApiHandler {

    @ExceptionHandler({MemberAlreadyExistsException.class})
    @ResponseStatus(HttpStatus.CONFLICT)
    public String memberAlreadyExistsHandler(MemberAlreadyExistsException ex) {
        return ex.getMessage();
    }


    @ExceptionHandler({MemberFoundButNotActiveException.class})
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public String memberFoundButNotActiveHandler(MemberFoundButNotActiveException ex) {
        return ex.getMessage();
    }


    @ExceptionHandler({MemberNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String memberNotFoundHandler(MemberNotFoundException ex) {
        return ex.getMessage();
    }


    @ExceptionHandler({MemberUserNotActiveException.class})
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public String memberUserNotActiveHandler(MemberUserNotActiveException ex) {
        return ex.getMessage();
    }

}
