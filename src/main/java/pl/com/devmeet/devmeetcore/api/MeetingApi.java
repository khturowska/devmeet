package pl.com.devmeet.devmeetcore.api;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.crud.MeetingCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingApiMapper;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingDto;
import pl.com.devmeet.devmeetcore.domain.place.crud.PlaceCrudService;

import java.util.List;

import static java.lang.Boolean.parseBoolean;


@CrossOrigin
@RestController
@RequestMapping("/api/v1/meetings")
class MeetingApi {

    private MeetingCrudService meetingCrudService;
    private GroupCrudService groupCrudService;
    private PlaceCrudService placeCrudService;
    private MeetingApiMapper mapperApi = new MeetingApiMapper();

    @Autowired
    public MeetingApi(MeetingCrudService meetingCrudService, GroupCrudService groupCrudService, PlaceCrudService placeCrudService) {
        this.meetingCrudService = meetingCrudService;
        this.groupCrudService = groupCrudService;
        this.placeCrudService = placeCrudService;
    }

//    @SneakyThrows
//    @PostMapping
//    public ResponseEntity<MeetingDto> add(@RequestBody MeetingDto newMeeting) {
//        return new ResponseEntity<>(mapperApi.mapToFrontend(
//                meetingCrudService.add(mapperApi.mapToBackend(newMeeting))), HttpStatus.CREATED);
//    }

    @GetMapping
    private List<MeetingDto> getAll() {
        return mapperApi.mapToFrontend(meetingCrudService.findAll());
    }

    @SneakyThrows
    @GetMapping("{meetingId}")
    public ResponseEntity<MeetingDto> getById(@PathVariable("meetingId") Long meetingId) {
        return new ResponseEntity<>(mapperApi.mapToFrontend(
                meetingCrudService.findById(meetingId)), HttpStatus.OK);
    }

    @SneakyThrows
    @GetMapping("group/{groupId}")
    public List<MeetingDto> getAllByGroup(@PathVariable Long groupId) {
        return mapperApi.mapToFrontend(meetingCrudService.findMeetingsByGroup(groupId));
    }

    @SneakyThrows
    @GetMapping("place/{placeId}")
    public List<MeetingDto> getAllByPlace(@PathVariable Long placeId) {
        return mapperApi.mapToFrontend(meetingCrudService.findMeetingsByPlace(placeId));
    }

    @GetMapping("/is-active/{isActive}")
    public List<MeetingDto> getAllIsActive(@PathVariable String isActive) {
        return mapperApi.mapToFrontend(meetingCrudService.findAllByIsActive(parseBoolean(isActive)));
    }

    @SneakyThrows
    @DeleteMapping("{meetingId}")
    public ResponseEntity<Boolean> deactivate(@PathVariable("meetingId") Long meetingId) {
        return new ResponseEntity<>((meetingCrudService.deactivate(meetingId) != null), HttpStatus.ACCEPTED);
    }

}
