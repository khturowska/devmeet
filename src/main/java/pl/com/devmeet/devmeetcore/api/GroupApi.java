package pl.com.devmeet.devmeetcore.api;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupApiMapper;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupDto;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/groups")
class GroupApi {

    private GroupCrudService group;
    private MemberCrudService member;

    private GroupApiMapper groupApiMapper;

    @Autowired
    private GroupApi(GroupCrudService group, MemberCrudService member) {
        this.group = group;
        this.member = member;
    }

    @GetMapping
    private ResponseEntity<List<GroupDto>> getAllOrFiltered(@RequestParam(required = false) String searchText) {
        return new ResponseEntity<>(groupApiMapper
                .mapToFrontend(group.findBySearchText(searchText)),
                HttpStatus.OK);
    }

    @SneakyThrows
    @GetMapping("{id}")
    private ResponseEntity<GroupDto> getById(@PathVariable Long id) {
        return new ResponseEntity<>(groupApiMapper
                .mapToFrontend(group.findById(id)),
                HttpStatus.OK);
    }

//    @Deprecated
//    @SneakyThrows
//    @PostMapping
//    private ResponseEntity<GroupEntity> add(@RequestBody GroupEntity newGroup) {
//        this.group.add(newGroup);
//        URI uri = ServletUriComponentsBuilder
//                .fromCurrentRequest()
//                .path("/{id}")
//                .buildAndExpand(newGroup.getId())
//                .toUri();
//        return ResponseEntity.created(uri).body(newGroup);
//    }

    @SneakyThrows
    @PutMapping
    private ResponseEntity<GroupDto> update(@RequestBody GroupEntity updatedGroup) {
//        if (!id.equals(updatedGroup.getId()))
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "id from path does not match with id in body!");
        GroupEntity updated = group.update(updatedGroup);
        return new ResponseEntity<>(groupApiMapper
                .mapToFrontend(updated),
                HttpStatus.OK);
    }

    @SneakyThrows
    @DeleteMapping("/{id}")
    private HttpStatus deactivateById(@PathVariable Long id) {
        GroupEntity deleted = group.delete(group.findById(id));
        return HttpStatus.OK;
    }
}
