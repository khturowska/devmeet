package pl.com.devmeet.devmeetcore.api;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.com.devmeet.devmeetcore.domain.place.crud.PlaceCrudService;
import pl.com.devmeet.devmeetcore.domain.place.models.PlaceEntity;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/places")
class PlaceApi {

    private PlaceCrudService placeService;

    @Autowired
    PlaceApi(PlaceCrudService placeService) {
        this.placeService = placeService;
    }

    @GetMapping
    public List<PlaceEntity> getPlaces() {
        return placeService.findAll();
    }

    @SneakyThrows
    @GetMapping("{id}")
    public PlaceEntity getById(@PathVariable Long id) {
        return placeService.findPlaceById(id);
    }

}
