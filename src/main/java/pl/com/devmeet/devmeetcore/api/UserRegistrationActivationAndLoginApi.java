package pl.com.devmeet.devmeetcore.api;

import com.vaadin.flow.component.UI;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.com.devmeet.devmeetcore.feature_services.user_and_member.login.UserLoginService;
import pl.com.devmeet.devmeetcore.feature_services.user_and_member.user_activation_after_registration.UserActivationService;
import pl.com.devmeet.devmeetcore.feature_services.user_and_member.user_registration.DevMeetRegistrationPasswordValidator;
import pl.com.devmeet.devmeetcore.feature_services.user_and_member.user_registration.UserRegistrationService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberApiDto;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberApiMapper;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.user.models.UserApiDto;
import pl.com.devmeet.devmeetcore.domain.user.models.UserApiMapper;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.login_and_registration.LogInOrRegisterMainView;

import javax.validation.Valid;

@CrossOrigin
@RestController
@RequestMapping(value = UserRegistrationActivationAndLoginApi.apiUrl)
public class UserRegistrationActivationAndLoginApi {

    public static final String apiUrl = "/api/v1/user-register-activation-login";
    public static final String vaadinEndpointPrefix = "/v";

    private UserRegistrationService userService;
    private UserActivationService activatorService;
    private UserLoginService userLoginService;

    private DevMeetRegistrationPasswordValidator passwordValidator;

    private UserApiMapper userApiMapper;
    private MemberApiMapper memberApiMapper;

    @Autowired
    public UserRegistrationActivationAndLoginApi(UserRegistrationService userService,
                                                 UserActivationService activatorService,
                                                 UserLoginService userLoginService) {
        this.userService = userService;
        this.activatorService = activatorService;
        this.userLoginService = userLoginService;
        this.userApiMapper = new UserApiMapper();
        this.memberApiMapper = new MemberApiMapper();
    }

    @SneakyThrows
    @PostMapping(value = "/{email}")
    public ResponseEntity<UserApiDto> register(@PathVariable String email) {
        UserEntity user = UserEntity.builder()
                .email(email)
                .build();
        return new ResponseEntity<>(userApiMapper.mapToFrontend(userService.registerNotActiveUserUsingApiWithInitialPasswordAndSendEmailToUser(user)), HttpStatus.OK);
    }

    @SneakyThrows
    @GetMapping(value = "/{email}/{userKey}")
    public ResponseEntity<?> activate(@Valid @PathVariable String email,
                                      @PathVariable String userKey) {
        activatorService.activateUser(email, userKey);
        return ResponseEntity.ok().build();
    }

    @SneakyThrows
    @GetMapping(vaadinEndpointPrefix + "/{email}/{userKey}")
    private void activateUsingVaadin(@Valid @PathVariable String email,
                                    @PathVariable String userKey){
        activatorService.activateUser(email, userKey);
        UI.getCurrent().navigate("/" + LogInOrRegisterMainView.URL);
    }

    @SneakyThrows
    @PostMapping("/login")
    public ResponseEntity<MemberApiDto> login(@RequestBody UserApiDto userForm) {
        MemberEntity usersMember = userLoginService.loginUserAndGetMember(userApiMapper.mapToBackend(userForm));
        return ResponseEntity
                .ok(memberApiMapper.mapToFrontend(usersMember));
    }
}
