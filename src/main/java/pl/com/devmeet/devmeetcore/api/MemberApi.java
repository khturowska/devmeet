package pl.com.devmeet.devmeetcore.api;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberApiMapper;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberApiDto;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/member")
class MemberApi {

    private MemberCrudService memberService;
    private MemberApiMapper apiMapper;

    @Autowired
    private MemberApi(MemberCrudService memberService) {
        this.memberService = memberService;
        apiMapper = new MemberApiMapper();
    }

    @SneakyThrows
    @GetMapping
    public ResponseEntity<MemberApiDto> findMemberByUser(@RequestParam Long userId) {
        return ResponseEntity
                .ok()
                .body(apiMapper.mapToFrontend(memberService.findByUserId(userId)));
    }

    @GetMapping("/all")
    public ResponseEntity<List<MemberApiDto>> findAllMembers() {
        return ResponseEntity
                .ok()
                .body(apiMapper.mapToFrontend(memberService.findAll()));
    }

    @SneakyThrows
    @GetMapping("{memberId}")
    public ResponseEntity<MemberApiDto> findMemberById(@PathVariable("memberId") Long memberId) {
        return ResponseEntity
                .ok()
                .body(apiMapper.mapToFrontend(
                        memberService.findById(memberId)));
    }

    @SneakyThrows
    @PutMapping
    public ResponseEntity<MemberApiDto> updateMemberById(@RequestBody MemberApiDto member) {
        return ResponseEntity
                .accepted()
                .body(apiMapper.mapToFrontend(
                        memberService.update(
                                apiMapper.mapToBackend(member))));
    }

    @SneakyThrows
    @DeleteMapping("/{memberId}")
    public ResponseEntity deactivateMember(@PathVariable("memberId") Long memberId) {
        return ResponseEntity
                .accepted()
                .body(memberService.deactivateById(memberId));
    }
}
