package pl.com.devmeet.devmeetcore.api;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.com.devmeet.devmeetcore.feature_services.member_and_group.MemberAddNewGroupService;
import pl.com.devmeet.devmeetcore.feature_services.member_and_group.MemberAddToExistingGroupService;
import pl.com.devmeet.devmeetcore.feature_services.member_and_group.crud.RelationsMemberGroupCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupApiMapper;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupDto;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberApiMapper;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/member-group")
class MemberGroupApi {

    private MemberAddNewGroupService memberAddNewGroupService;
    private MemberAddToExistingGroupService memberAddToExistingGroupService;
    private RelationsMemberGroupCrudService relationsMemberGroupCrudService;

    private GroupApiMapper groupApiMapper;
    private MemberApiMapper memberApiMapper;

    @Autowired
    public MemberGroupApi(MemberAddNewGroupService memberAddNewGroupService,
                          MemberAddToExistingGroupService memberAddToExistingGroupService,
                          RelationsMemberGroupCrudService relationsMemberGroupCrudService) {

        this.memberAddNewGroupService = memberAddNewGroupService;
        this.memberAddToExistingGroupService = memberAddToExistingGroupService;
        this.relationsMemberGroupCrudService = relationsMemberGroupCrudService;
        this.groupApiMapper = new GroupApiMapper();
        this.memberApiMapper = new MemberApiMapper();
    }

    @SneakyThrows
    @PostMapping("/create-new-group")
    public ResponseEntity createNewGroupAndAddMemberToIt(@RequestParam Long memberId, @RequestBody GroupDto newGroup) {
        GroupEntity createdGroup = memberAddNewGroupService
                .memberCreateNewGroup(memberId, groupApiMapper.mapToBackend(newGroup));
        List<MemberEntity> groupMembers = relationsMemberGroupCrudService
                .findMembersInGroup(createdGroup);

        return new ResponseEntity(memberApiMapper
                .mapToFrontend(groupMembers),
                HttpStatus.CREATED);
    }

    @SneakyThrows
    @PutMapping("/add-member-to-group")
    public ResponseEntity addMemberToExistingGroup(@RequestParam Long memberId, @RequestParam Long groupId) {
        GroupEntity updatedGroup = memberAddToExistingGroupService
                .addMemberToExistingGroup(groupId, memberId);
        List<MemberEntity> groupMembers = relationsMemberGroupCrudService
                .findMembersInGroup(updatedGroup);

        return new ResponseEntity(memberApiMapper
                .mapToFrontend(groupMembers),
                HttpStatus.OK);
    }

    @SneakyThrows
    @GetMapping("/all-member-groups/{memberId}")
    public ResponseEntity findAllMemberGroups(@PathVariable("memberId") Long memberId) {
        List<GroupEntity> membersGroupList = relationsMemberGroupCrudService
                .findInWhatGroupsIsAMember(MemberEntity.builder()
                        .id(memberId)
                        .build());

        return new ResponseEntity(groupApiMapper
                .mapToFrontend(membersGroupList),
                HttpStatus.FOUND);
    }

    @SneakyThrows
    @GetMapping("/all-group-members/{groupId}")
    public ResponseEntity findAllGroupMembers(@PathVariable("groupId") Long groupId) {
        List<MemberEntity> listOfGroupMembers = relationsMemberGroupCrudService
                .findMembersInGroup(GroupEntity.builder()
                        .id(groupId)
                        .build());

        return new ResponseEntity(memberApiMapper
                .mapToFrontend(listOfGroupMembers),
                HttpStatus.FOUND);
    }
}
