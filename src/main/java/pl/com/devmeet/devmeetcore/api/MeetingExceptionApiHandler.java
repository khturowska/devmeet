package pl.com.devmeet.devmeetcore.api;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingNotFoundException;

@RestControllerAdvice
class MeetingExceptionApiHandler {

    @ExceptionHandler({MeetingAlreadyExistsException.class})
    @ResponseStatus(HttpStatus.CONFLICT)
    public String meetingAlreadyExistsHandler(MeetingAlreadyExistsException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler({MeetingFoundButNotActiveException.class})
    @ResponseStatus(HttpStatus.CONFLICT)
    public String meetingFoundButNotActiveHandler(MeetingFoundButNotActiveException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler({MeetingNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String meetingNotFoundHandler(MeetingNotFoundException ex) {
        return ex.getMessage();
    }
}
