package pl.com.devmeet.devmeetcore.api;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.crud.AvailabilityCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.models.AvailabilityApiMapper;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.models.AvailabilityDto;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.models.AvailabilityEntity;

import java.net.URI;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/availability")
class AvailabilityApi {

    private AvailabilityCrudService availabilityService;
    private AvailabilityApiMapper mapperApi = new AvailabilityApiMapper();

    @Autowired
    public AvailabilityApi(AvailabilityCrudService availabilityService) {
        this.availabilityService = availabilityService;
    }

    @SneakyThrows
    @PostMapping
    public ResponseEntity<AvailabilityDto> addNewAvailability(@RequestBody AvailabilityDto newAvailability) {
        AvailabilityEntity added = availabilityService.add(mapperApi.mapToBackend(newAvailability));
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/id")
                .buildAndExpand(added.getId())
                .toUri();
        return ResponseEntity
                .created(uri)
                .body(mapperApi.mapToFrontend(added));
    }

    @SneakyThrows
    @GetMapping("/{availabilityId}")
    public ResponseEntity<AvailabilityDto> getAvailabilityById(@PathVariable("availabilityId") Long availabilityId) {
        return ResponseEntity
                .accepted()
                .body(mapperApi.mapToFrontend(
                        availabilityService.findById(availabilityId)));
    }

    @SneakyThrows
    @GetMapping     //("{memberId}")
    public ResponseEntity<List<AvailabilityDto>> getMemberAvailabilities(@RequestParam Long memberId) {
        return ResponseEntity
                .accepted()
                .body(mapperApi.mapListToFrontend(
                        availabilityService.findAllMemberAvailabilitiesByMemberId(memberId)));
    }

    @SneakyThrows
    @GetMapping("/all")
    public ResponseEntity<List<AvailabilityDto>> getAllAvailabilities() {
        return ResponseEntity
                .accepted()
                .body(mapperApi.mapListToFrontend(availabilityService.findAll()));
    }

    @SneakyThrows
    @PutMapping
    public ResponseEntity<AvailabilityDto> updateAvailability(@RequestBody AvailabilityDto apiDto) {
        return ResponseEntity
                .accepted()
                .body(mapperApi.mapToFrontend(
                        availabilityService.update(
                                mapperApi.mapToBackend(apiDto))));
    }

    @SneakyThrows
    @DeleteMapping("/{availabilityId}")
    public ResponseEntity<Boolean> deleteAvailability(@PathVariable("availabilityId") Long availabilityId) {
        return ResponseEntity
                .ok()
                .body(availabilityService.deleteById(availabilityId) != null);
    }
}
