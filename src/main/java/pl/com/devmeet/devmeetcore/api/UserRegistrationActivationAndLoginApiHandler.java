package pl.com.devmeet.devmeetcore.api;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.com.devmeet.devmeetcore.feature_services.user_and_member.login.exceptions.WrongUserPasswordLoginException;
import pl.com.devmeet.devmeetcore.feature_services.user_and_member.user_activation_after_registration.exceptions.InvalidUUIDStringException;

@RestControllerAdvice
class UserRegistrationActivationAndLoginApiHandler {

    @ExceptionHandler(WrongUserPasswordLoginException.class)
    public String wrongUserPasswordLoginHandler(WrongUserPasswordLoginException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler(InvalidUUIDStringException.class)
    public String invalidUUSIDString(InvalidUUIDStringException ex) {
        return ex.getMessage();
    }
}
