package pl.com.devmeet.devmeetcore.config;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;

import java.net.InetAddress;

@Slf4j
@Getter
@Configuration
public class AddressAndPortConfigurationInfoService {

    public static final String PROD_PROF_NAME = "prod";
    public static final String DEV_PROF_NAME = "dev";
    public static final String HEROKU_PROF_NAME = "heroku";

    private String serverAddressStr;
    private String serverPortNumberStr;
    private String activeProfileName;

    @Profile(AddressAndPortConfigurationInfoService.PROD_PROF_NAME)
    @Bean
    @Autowired
    public String prodHostConfig(ServerProperties serverProperties, Environment env) {
        InetAddress serverAddress = serverProperties.getAddress();
        this.serverAddressStr = serverAddress != null ? serverAddress.toString() : getLocalServerAddressFromInterface();
        this.serverPortNumberStr = serverProperties.getPort() == null ? "8080" : serverProperties.getPort().toString();
        this.activeProfileName = AddressAndPortConfigurationInfoService.PROD_PROF_NAME;
        printActiveProfileName(AddressAndPortConfigurationInfoService.PROD_PROF_NAME);
        printDevMeetAddressAndPortConfiguration(serverAddressStr, serverPortNumberStr);
        return AddressAndPortConfigurationInfoService.PROD_PROF_NAME;
    }

    @Profile(AddressAndPortConfigurationInfoService.DEV_PROF_NAME)
    @Bean
    @Autowired
    public String devHostConfig(ServerProperties serverProperties, Environment env) {
        this.serverAddressStr = getLocalServerAddressFromInterface();
        this.serverPortNumberStr = serverProperties.getPort() == null ? "8080" : serverProperties.getPort().toString();
        this.activeProfileName = AddressAndPortConfigurationInfoService.DEV_PROF_NAME;
        printActiveProfileName(AddressAndPortConfigurationInfoService.DEV_PROF_NAME);
        printDevMeetAddressAndPortConfiguration(serverAddressStr, serverPortNumberStr);
        return AddressAndPortConfigurationInfoService.DEV_PROF_NAME;
    }

    @Profile(AddressAndPortConfigurationInfoService.HEROKU_PROF_NAME)
    @Bean
    @Autowired
    public String herokuHostConfig(ServerProperties serverProperties, Environment env) {
        String serverUrlStr = env.getProperty("heroku.address");
        this.serverAddressStr = serverUrlStr != null ? serverUrlStr : getLocalServerAddressFromInterface();
        this.activeProfileName = AddressAndPortConfigurationInfoService.HEROKU_PROF_NAME;
        this.serverPortNumberStr = env.getProperty("server.port");
        printActiveProfileName(AddressAndPortConfigurationInfoService.HEROKU_PROF_NAME);
        printDevMeetAddressAndPortConfiguration(serverAddressStr, serverPortNumberStr);
        return AddressAndPortConfigurationInfoService.HEROKU_PROF_NAME;
    }

    private String getLocalServerAddressFromInterface() {
        return new HostAddressGetter().getIpv4AddressString();
    }

    private void printActiveProfileName(String activeProfileName) {
        log.info(String.format("======= Loaded configuration for %s profile =======", activeProfileName.toUpperCase()));
    }

    private void printDevMeetAddressAndPortConfiguration(String address, String port) {
        log.info(String.format("Loaded server configuration: server.address=%s and server.port=%s", address, port));
    }
}
