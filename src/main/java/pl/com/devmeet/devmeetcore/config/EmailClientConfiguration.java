package pl.com.devmeet.devmeetcore.config;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Slf4j
@Configuration
public class EmailClientConfiguration {

    @Getter
    private String login = null;
    private String password = null;

    @Autowired
    private Environment env;

    @Bean
    public JavaMailSenderImpl mailSender() {
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
        this.login = getEmailLoginFromActiveProfileProperties();
        this.password = getEmailPasswordFromActiveProfileProperties();

        javaMailSender.setDefaultEncoding("UTF-8");
        javaMailSender.setHost("smtp.gmail.com");
        javaMailSender.setUsername(login);
        javaMailSender.setPassword(password);

        javaMailSender.setPort(587);
        javaMailSender.setProtocol("smtp");

        Properties mailProperties = new Properties();
        mailProperties.setProperty("mail.smtp.auth", "true");
        mailProperties.setProperty("mail.smtp.starttls.enable", "true");
        mailProperties.setProperty("mail.smtp.connectiontimeout", "6000");
        mailProperties.setProperty("mail.smtp.timeout", "6000");
        mailProperties.setProperty("mail.smtp.writetimeout", "6000");

        javaMailSender.setJavaMailProperties(mailProperties);
        emailClientConfigurationInfo();
        return javaMailSender;
    }

    private void emailClientConfigurationInfo(){
        String successInfo = String.format("Mail client initialization for login: %s", login);
        log.info(successInfo);
    }

    private String getEmailLoginFromActiveProfileProperties() {
        return env.getProperty("email.login");
    }

    private String getEmailPasswordFromActiveProfileProperties() {
        return env.getProperty("email.password");
    }

}
