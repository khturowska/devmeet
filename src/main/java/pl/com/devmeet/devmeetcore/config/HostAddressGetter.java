package pl.com.devmeet.devmeetcore.config;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.net.*;
import java.util.Enumeration;

@Slf4j
@Getter
public class HostAddressGetter {

    private String hostInterfaceName;
    private Inet4Address ipv4HostAddress;
    private Inet6Address ipv6HostAddress;

    public HostAddressGetter() {
        try {
            extractAddresses();
        } catch (SocketException e) {
            log.error(String.format("Can't read IPv4 and IPv6 addresses from network interface", e.getMessage()));
        }
//        log.info(String.format("Server run on IPv4:%s and IPv6:%s", getIpv4AddressString(), getIpv6AddressString()));
        log.info(String.format("Server run on IPv4:%s and IPv6:%s", ipv4HostAddress.getHostAddress(), ipv6HostAddress.getHostAddress()));
    }

    public String getIpv4AddressString() {
        String ipv4 = ipv4HostAddress.getHostAddress();
        ipv4 = deleteSlash(deleteInterfaceName(ipv4));
        return ipv4;
    }

    public String getIpv6AddressString() {
        String ipv6 = ipv6HostAddress.getHostAddress();
        ipv6 = deleteInterfaceName(deleteSlash(ipv6));
        return ipv6;
    }

    private String deleteSlash(String str) {
        int slashIndex = str.indexOf('/');
        return slashIndex >= 0 ? str.substring(slashIndex) : str;
    }

    private String deleteInterfaceName(String str) {
        int percentIndex = str.indexOf('%');
        return percentIndex >= 0 ? str.substring(0, percentIndex) : str;
    }

    private void extractAddresses() throws SocketException {
        Enumeration<InetAddress> iterAddress;
        InetAddress address;
        NetworkInterface network = getUpNetworkInterface();

        iterAddress = network.getInetAddresses();

        while (iterAddress.hasMoreElements()) {
            address = iterAddress.nextElement();

            if (address.isAnyLocalAddress())
                continue;

            else if (address.isLoopbackAddress())
                continue;

            else if (address.isMulticastAddress())
                continue;

            else {
                if (address instanceof Inet4Address) {
                    this.ipv4HostAddress = (Inet4Address) address;
                    this.hostInterfaceName = hostInterfaceName.isEmpty() ? ipv4HostAddress.getCanonicalHostName() : hostInterfaceName;
                }
                if (address instanceof Inet6Address) {
                    this.ipv6HostAddress = (Inet6Address) address;
                    this.hostInterfaceName = ipv6HostAddress.getScopedInterface().getName();
                }
            }
        }
    }

    private NetworkInterface getUpNetworkInterface() throws SocketException {
        Enumeration<NetworkInterface> iterNetwork;
        NetworkInterface network;
        iterNetwork = NetworkInterface.getNetworkInterfaces();

        while (iterNetwork.hasMoreElements()) {
            network = iterNetwork.nextElement();

            if (!network.isUp())
                continue;

            if (network.isLoopback())
                continue;

            return network;
        }
        return null;
    }
}
