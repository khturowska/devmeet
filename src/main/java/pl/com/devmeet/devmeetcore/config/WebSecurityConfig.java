package pl.com.devmeet.devmeetcore.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.AppHomeView;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.login_and_registration.AfterRegisterInfoPage;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.login_and_registration.LogInOrRegisterMainView;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/", "/home",
                        "/" + AfterRegisterInfoPage.URL,
//                        "/app/**",
//                        "/api/**",
                        "/" + AppHomeView.URL).permitAll()
                .anyRequest().authenticated()

                .and()
                .formLogin()
                .loginPage("/" + LogInOrRegisterMainView.URL)
                .loginProcessingUrl("/" + LogInOrRegisterMainView.URL)
                .permitAll()

                .and()
                .logout()
                .permitAll()

                .and()
                .csrf()
                .disable();
    }

    /**
     * Allows access to static resources, bypassing Spring security.
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(
                // Vaadin Flow static resources //
                "/VAADIN/**",

                // the standard favicon URI
                "/favicon.ico",

                // the robots exclusion standard
                "/robots.txt",

                // web application manifest //
                "/manifest.webmanifest",
                "/sw.js",
                "/offline-page.html",

                // (development mode) static resources //
                "/frontend/**",

                // (development mode) webjars //
                "/webjars/**",

                // (production mode) static resources //
                "/frontend-es5/**", "/frontend-es6/**",

                "/images/**", "/icons/**",
                "/api/**");
    }
}
