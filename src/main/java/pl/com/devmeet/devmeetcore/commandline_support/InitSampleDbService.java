package pl.com.devmeet.devmeetcore.commandline_support;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.com.devmeet.devmeetcore.feature_services.user_and_member.user_registration.UserPasswordEncryptor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.place.crud.PlaceCrudService;
import pl.com.devmeet.devmeetcore.domain.place.models.PlaceEntity;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserCrudService;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyActiveException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyExistsEmailDuplicationException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
class InitSampleDbService {

    private List<String> initDbReportList;

    private UserCrudService userService;
    private MemberCrudService memberService;
    private PlaceCrudService placeService;
    private GroupCrudService groupService;

    @Autowired
    public InitSampleDbService(UserCrudService userService,
                               MemberCrudService memberService,
                               PlaceCrudService placeService,
                               GroupCrudService groupService) {

        this.userService = userService;
        this.memberService = memberService;
        this.placeService = placeService;
        this.groupService = groupService;
    }

    public String init() {
        initDbReportList = new ArrayList<>();
        initDbReportList.addAll(insertUsers());
        initDbReportList.addAll(insertPlaces());
        initDbReportList.addAll(initGroups());

        return createPreparationReport(initDbReportList);
    }

    private String createPreparationReport(List<String> initDbReportList) {
        String initSampleDbInfo = "\n\n###\tInit sample db creation report\t###\n";
        String applicationReadyToUseInfo = "\n###\tDevMeet application ready to serve\t###\n";
        StringBuilder sb = new StringBuilder();

        sb.append(initSampleDbInfo);
        initDbReportList.forEach(r -> sb.append("\n* " + r));
        sb.append(applicationReadyToUseInfo);

        return sb.toString();
    }

    @SneakyThrows
    private List<String> insertUsers() {
        List<String> creationResult = new ArrayList<>();
        String userEmail = "test.pt4q@gmail.com";
        String testPassword = "Qwerty1234";

        UserEntity adminUserEntity = UserEntity.builder()
                .email(userEmail)
                .password(new UserPasswordEncryptor().encryptPassword(testPassword))
                .activationKey(UUID.randomUUID())
                .build();

        try {
            adminUserEntity = userService.activate(userService.add(adminUserEntity));
            creationResult.add(String.format("User(id=%d) ready", adminUserEntity.getId()));
        } catch (UserAlreadyExistsEmailDuplicationException | UserAlreadyActiveException e) {
            adminUserEntity = userService.findByEmail(userEmail);
            creationResult.add(String.format("User(id=%d) ready (already exists)", adminUserEntity.getId()));
        }

        MemberEntity adminMember = MemberEntity.builder()
                .user(adminUserEntity)
                .creationTime(LocalDateTime.now())
                .nick("admin")
                .build();

        MemberEntity createdMember;
        try {
            createdMember = memberService.add(adminMember);
            creationResult.add(String.format("Member(id=%d) for User(id=%d) ready", createdMember.getId(), createdMember.getUser().getId()));
        } catch (MemberAlreadyExistsException e) {
            createdMember = memberService.findByUser(adminUserEntity);
            creationResult.add(String.format("Member(id=%d) for User(id=%d) ready (already exists)", createdMember.getId(), createdMember.getUser().getId()));
        }
        return creationResult;
    }

    @SneakyThrows
    private List<String> insertPlaces() {
        List<String> creationResult = new ArrayList<>();
        PlaceEntity place1 = PlaceEntity.builder()
                .placeName("Centrum Zarządzania Innowacjami i Transferem Technologii Politechniki Warszawskiej")
                .description("Openspace koło Metra Politechniki")
                .website("cziitt.pw.edu.pl")
                .location("Rektorska 4, 00-614 Warszawa")
                .build();

        PlaceEntity place2 = PlaceEntity.builder()
                .placeName("Google for Startups – Koneser")
                .description("Google Campus Warsaw")
                .website("https://www.campus.co/warsaw")
                .location("Plac Konesera 03-736 Warszawa (+48) 22 128 44 38")
                .build();

        PlaceEntity place3 = PlaceEntity.builder()
                .placeName("Wydział Matematyki, Informatyki i Mechaniki Uniwersytetu Warszawskiego – wydział Uniwersytetu Warszawskiego")
                .description("MeetUp tup! tup! tup! jeb!")
                .website("https://www.mimuw.edu.pl")
                .location("Stefana Banacha 2, 02-097 Warszawa")
                .build();

        PlaceEntity place4 = PlaceEntity.builder()
                .placeName("Centrum konferencyjne FOCUS")
                .description("budynek z drzewem na piętrze")
                .website("http://www.budynekfocus.com/pl")
                .location("Aleja Armii Ludowej 26, 00-609 Warszawa")
                .build();

        PlaceEntity place5 = PlaceEntity.builder()
                .placeName("Startberry")
                .description("We build bridges between #startups and #corporations Partners: EY, F11, Microsoft #MVP #hackathon #venturebuilder #vc #startberry")
                .website("https://twitter.com/startberrypl")
                .location("Grochowska 306/308, 03-840 Warszawa")
                .build();

        List<PlaceEntity> placesList = new ArrayList<>();
        placesList.add(place1);
        placesList.add(place2);
        placesList.add(place3);
        placesList.add(place4);
        placesList.add(place5);

        placesList
                .forEach(place -> {
                    try {
                        place = placeService.add(place);
                        creationResult.add(String.format("Place (id=%d) ready", place.getId()));
                    } catch (PlaceAlreadyExistsException e) {
                        creationResult.add(String.format("Place (id=%d) ready (already exists)", place.getId()));
                    }
                });
        return creationResult;
    }

    @SneakyThrows
    private List<String> initGroups() {
        List<String> creationResult = new ArrayList<>();
        GroupEntity group1 = GroupEntity.builder()
                .groupName("Java test group")
                .website("www.testWebsite.com")
                .description("Welcome to test group")
                .build();

        GroupEntity group2 = GroupEntity.builder()
                .groupName("DevMeet developers")
                .website("https://bitbucket.org/khturowska/devmeet/")
                .description("Awesome team")
                .build();

        List<GroupEntity> groupList = new ArrayList<>();
        groupList.add(group1);
        groupList.add(group2);

        groupList
                .forEach(group -> {
                    try {
                        group = groupService.add(group);
                        creationResult.add(String.format("Group (id=%d) ready", group.getId()));
                    } catch (GroupAlreadyExistsException e) {
                        try {
                            group = groupService.findByGroup(group);
                            creationResult.add(String.format("Group (id=%d) ready (already exists)", group.getId()));
                        } catch (GroupNotFoundException groupNotFoundException) {
                            groupNotFoundException.printStackTrace();
                        }
                    }
                });
        return creationResult;
    }
}
