package pl.com.devmeet.devmeetcore.commandline_support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.com.devmeet.devmeetcore.config.EmailClientConfiguration;
import pl.com.devmeet.devmeetcore.email.EmailSenderService;
import pl.com.devmeet.devmeetcore.email.Mail;

import java.time.LocalDateTime;

@Component
public class DevMeetCommandLineRunner implements CommandLineRunner {

    private InitSampleDbService initSampleDbService;

    private EmailSenderService emailSenderService;
    private EmailClientConfiguration emailClientConfiguration;

    @Autowired
    public DevMeetCommandLineRunner(InitSampleDbService initSampleDbService,
                                    EmailSenderService emailSenderService,
                                    EmailClientConfiguration emailClientConfiguration) {

        this.initSampleDbService = initSampleDbService;
        this.emailSenderService = emailSenderService;
        this.emailClientConfiguration = emailClientConfiguration;
    }

    @Override
    public void run(String... args) throws Exception {
        String environmentReport = createEnvironmentReport();
        String creationReport = initSampleDbService.init();
        String report = new StringBuilder()
                .append(environmentReport)
                .append(creationReport)
                .toString();

        sendEmail(report);
    }

    private String createEnvironmentReport() {
        String machineReport = new StringBuilder()
                .append("\n").append(LocalDateTime.now()).append("\tDevmeet run at ")
                .append("\nuserName: ").append(System.getProperty("user.name"))
                .append("\nJavaHotSpotVendor: ").append(System.getProperty("java.vendor"))
                .append("\nJavaHotSpotVersion: ").append(System.getProperty("java.version"))
                .append("\nosName: ").append(System.getProperty("os.name"))
                .append("\nosVersion: ").append(System.getProperty("os.version"))
                .append("\nosArchitecture: ").append(System.getProperty("os.arch"))
                .toString();
        return machineReport;
    }

    private void sendEmail(String content) {
        Mail runInfoMail = Mail.builder()
                .from("no-reply@devmeet.com")
                .to(emailClientConfiguration.getLogin())
                .subject("Devmeet run at" + LocalDateTime.now())
                .content(content)
                .build();
        emailSenderService.sendSimpleMessage(runInfoMail);
    }
}
