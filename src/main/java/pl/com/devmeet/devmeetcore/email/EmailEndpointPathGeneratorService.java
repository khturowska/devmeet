package pl.com.devmeet.devmeetcore.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.com.devmeet.devmeetcore.config.AddressAndPortConfigurationInfoService;


@Component
public class EmailEndpointPathGeneratorService {

    private final AddressAndPortConfigurationInfoService environmentInfoService;

    @Autowired
    public EmailEndpointPathGeneratorService(AddressAndPortConfigurationInfoService environmentInfoService) {
        this.environmentInfoService = environmentInfoService;
    }

    public String generateEndpointPath(String endpoint) {
        String activeProfile = environmentInfoService.getActiveProfileName();
        String generatedUri = generateUriWithAddressAndPortBasedOnActiveProfile(endpoint, activeProfile);

        return generatedUri;
    }

    private String generateUriWithAddressAndPortBasedOnActiveProfile(String endpoint, String activeProfile) {
        String serverAddress = environmentInfoService.getServerAddressStr();
        String portNumber = environmentInfoService.getServerPortNumberStr();
        String uri = null;
        int addrLen = serverAddress.length();

        if (activeProfile.equals(AddressAndPortConfigurationInfoService.DEV_PROF_NAME)
                || activeProfile.equals(AddressAndPortConfigurationInfoService.PROD_PROF_NAME)) {
            uri = "http://" + serverAddress + ":" + portNumber + endpoint;
        } else if (activeProfile.equals(AddressAndPortConfigurationInfoService.HEROKU_PROF_NAME)) {

            if (serverAddress.charAt(addrLen - 1) == '/')
                serverAddress = serverAddress.substring(0, addrLen - 1);

            uri = serverAddress + endpoint;
        }
        return uri;
    }
}
