package pl.com.devmeet.devmeetcore.email;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import pl.com.devmeet.devmeetcore.threads.ThreadNames;

@Slf4j
@Service
public class EmailSenderService {

    private JavaMailSender emailSender;
    private EmailEndpointPathGeneratorService endpointPathGeneratorService;

    @Autowired
    public EmailSenderService(JavaMailSender emailSender, EmailEndpointPathGeneratorService endpointPathGeneratorService) {
        this.emailSender = emailSender;
        this.endpointPathGeneratorService = endpointPathGeneratorService;
    }

    public void sendSimpleMessage(final Mail mail) {
        Thread thread = new Thread(() -> {
            SimpleMailMessage msg = new SimpleMailMessage();
            msg.setTo(mail.getTo());
            msg.setSubject(mail.getSubject());
            msg.setFrom(mail.getFrom());
            msg.setText(mail.getContent());
            try {
                emailSender.send(msg);
            } catch (Exception e) {
                log.warn("Mail sender error: " + e.getMessage());
            }
        });
        thread.setName(ThreadNames.EMAIL_SEND);
        thread.start();
    }

    public String generateServerAddressAndPortEndpointPathString(String endpointPath){
        return endpointPathGeneratorService.generateEndpointPath(endpointPath);
    }
}