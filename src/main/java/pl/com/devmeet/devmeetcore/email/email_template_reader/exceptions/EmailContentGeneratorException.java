package pl.com.devmeet.devmeetcore.email.email_template_reader.exceptions;

public class EmailContentGeneratorException extends Exception {
    public EmailContentGeneratorException() {
    }

    public EmailContentGeneratorException(String message) {
        super(message);
    }

    public EmailContentGeneratorException(String message, Throwable cause) {
        super(message, cause);
    }
}
