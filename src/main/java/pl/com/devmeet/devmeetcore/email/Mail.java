package pl.com.devmeet.devmeetcore.email;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Mail {
    private String from;
    private String to;
    private String subject;
    //    private String activationKey;
//    private String initialPassword;
    private String content;
}
