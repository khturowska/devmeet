package pl.com.devmeet.devmeetcore.email.email_template_reader;

import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.email.email_template_reader.exceptions.EmailContentGeneratorException;
import pl.com.devmeet.devmeetcore.email.email_template_reader.exceptions.ResourcesTemplateReaderErrorException;

import java.util.Map;

@NoArgsConstructor
public class EmailContentGenerator {

    private String pathToTemplate;

    public EmailContentGenerator(String pathToTemplate) {
        this.pathToTemplate = pathToTemplate;
    }

    public String generateEmailContent(Map<String, String> parameters) throws ResourcesTemplateReaderErrorException, EmailContentGeneratorException {
        String template = new ResourcesTemplateReader(pathToTemplate).readFile();
        return generateEmailContentFromTemplateAndParams(template, parameters);
    }

    public String generateEmailContentFromTemplateAndParams(String templateString, Map<String, String> parameters) throws EmailContentGeneratorException {
        checkTemplate(templateString);
        checkParamsSize(parameters);

        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            templateString = templateString.replace("{" + entry.getKey() + "}", entry.getValue());
        }
        return templateString;
    }

    private void checkTemplate(String template) throws EmailContentGeneratorException {
        if (template.isEmpty())
            throw new EmailContentGeneratorException("Template: " + template);
    }

    private void checkParamsSize(Map<String, String> stringStringMap) throws EmailContentGeneratorException {
        if (stringStringMap.size() == 0)
            throw new EmailContentGeneratorException("Params: 0");
    }
}
