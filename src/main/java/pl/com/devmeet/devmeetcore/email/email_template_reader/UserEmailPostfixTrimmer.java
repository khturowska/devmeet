package pl.com.devmeet.devmeetcore.email.email_template_reader;

public class UserEmailPostfixTrimmer {

    public String trim(String userEmail) {
        StringBuilder sb = new StringBuilder();
        Character readedChar = null;
        int charIndex = 0;
        do {
            if (readedChar != null)
                sb.append(readedChar);

            readedChar = userEmail.charAt(charIndex);
            charIndex++;

        } while (readedChar != '@');
        return sb.toString();
    }
}
