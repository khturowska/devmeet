package pl.com.devmeet.devmeetcore.email.email_template_reader.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
class EmailExceptionHandler {

    @ExceptionHandler({EmailContentGeneratorException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String emailContentGeneratorHandler(EmailContentGeneratorException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler({ResourcesTemplateReaderErrorException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String resourcesTemplateReaderFileNotFoundException(ResourcesTemplateReaderErrorException ex) {
        return ex.getMessage();
    }


}
