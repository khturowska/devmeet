package pl.com.devmeet.devmeetcore.email.email_template_reader;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.com.devmeet.devmeetcore.email.email_template_reader.exceptions.ResourcesTemplateReaderErrorException;

import java.io.*;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
public class ResourcesTemplateReader {

    @NonNull
    private String pathToFile;

    public String readFile() throws ResourcesTemplateReaderErrorException {
        String fileString = null;
        try {
            InputStream is = getClass().getResourceAsStream(pathToFile);
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            fileString = br.lines()
                    .map(l -> l + "\n")
                    .collect(Collectors.joining());
            fileString = removeLastEnterSignFromString(fileString);
            br.close();
            isr.close();
            is.close();
            log.info(String.format("%s file read successfully", pathToFile));
        } catch (NullPointerException | IOException e) {
            log.error(String.format("Can't read template %s because: %s", pathToFile, e));
            throw new ResourcesTemplateReaderErrorException(e.getMessage());
        }
        return fileString;
    }

    private String removeLastEnterSignFromString(String string){
        return string
                .substring(0, (string.length() - 1));
    }
}
