package pl.com.devmeet.devmeetcore.email.email_template_reader.exceptions;

public class ResourcesTemplateReaderErrorException extends Exception {

    public ResourcesTemplateReaderErrorException() {
    }

    public ResourcesTemplateReaderErrorException(String message) {
        super(message);
    }

    public ResourcesTemplateReaderErrorException(String message, Throwable cause) {
        super(message, cause);
    }
}
