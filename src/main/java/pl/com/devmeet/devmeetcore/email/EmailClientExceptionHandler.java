package pl.com.devmeet.devmeetcore.email;

import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.MailParseException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
class EmailClientExceptionHandler {

    @ExceptionHandler(value = MailParseException.class)
    public void handleMailParseException(MailParseException mailParseException){
        log.error(mailParseException.getMessage());
    }
}
