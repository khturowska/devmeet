package pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.menu_new;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.feature_services.member_and_group.MemberAddNewGroupService;
import pl.com.devmeet.devmeetcore.feature_services.member_and_group.exceptions.MemberAddNewGroupServiceException;
import pl.com.devmeet.devmeetcore.feature_services.member_and_group.exceptions.RelationsMemberGroupCrudServiceException;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.AppHomeView;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.member_side_panel.MemberGroupsViewDiv;

@Route(value = NewGroupViewDiv.URL, layout = AppHomeView.class)
@PageTitle(NewGroupViewDiv.PAGE_TITLE)
public class NewGroupViewDiv extends Div {

    public static final String URL = AppHomeView.URL + "/new-group";
    static final String PAGE_TITLE = "Create new group";

    private TextField groupNameTextField = new TextField("Group name");
    private TextField groupWebsiteTextField = new TextField("Website");
    private TextArea groupDescriptionTextField = new TextArea("Description");
    private NumberField groupMembersLimitNumberField = new NumberField("Members limit");

    private Button saveGroupButton = new Button("save");
    private Button cancelButton = new Button("cancel");

    private final MemberAddNewGroupService memberAddNewGroupService;
    private Binder<GroupEntity> groupBinder = new Binder<>(GroupEntity.class);
    private GroupEntity groupEntity = new GroupEntity();
    private MemberEntity memberEntity;

    @Autowired
    public NewGroupViewDiv(MemberAddNewGroupService memberAddNewGroupService) {
        this.memberAddNewGroupService = memberAddNewGroupService;
        memberEntity = getMemberEntityFromContext();
        configBinder();
        saveButtonConfig();
        cancelButtonConfig();
        add(createGroupForm());
    }

    private MemberEntity getMemberEntityFromContext() {
        return ComponentUtil.getData(UI.getCurrent(), MemberEntity.class);
    }

    private Component createGroupForm() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        FormLayout formLayout = new FormLayout();
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSizeFull();
        verticalLayout.add(
                new H3(PAGE_TITLE),
                groupNameTextField,
                groupWebsiteTextField,
                groupDescriptionTextField,
                groupMembersLimitNumberField,
                new HorizontalLayout(saveGroupButton, cancelButton));
        formLayout.add(verticalLayout);
        horizontalLayout.add(formLayout);
        horizontalLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);
        return horizontalLayout;
    }

    private void configTextAndNumberFields() {
        groupNameTextField.setAutofocus(true);
        groupNameTextField.setInvalid(true);
    }

    private void configBinder() {
        this.groupBinder.setBean(groupEntity);
        this.groupBinder
                .forField(groupNameTextField)
                .asRequired()
                .withValidator(s -> s.length() > 3 && s.matches("\\S+.{2,}"), "Group name must have more than 3 characters and cannot start with a space")
                .bind(GroupEntity::getGroupName, GroupEntity::setGroupName);
        this.groupBinder
                .forField(groupWebsiteTextField)
                .bind(GroupEntity::getWebsite, GroupEntity::setWebsite);
        this.groupBinder
                .forField(groupDescriptionTextField)
                .bind(GroupEntity::getDescription, GroupEntity::setDescription);
        this.groupBinder
                .forField(groupMembersLimitNumberField)
                .asRequired()
                .withConverter(d -> d != null ? d.intValue() : 5, i -> i != null ? i.doubleValue() : 5)
                .withValidator(i -> i > 1, "Members limit must be greater than 1")
                .bind(GroupEntity::getMembersLimit, GroupEntity::setMembersLimit);
    }

    private void saveButtonConfig() {
        this.saveGroupButton.addClickListener(buttonClickEvent -> {
            if (groupBinder.isValid()) {
                groupEntity = groupBinder.getBean();
                try {
                    groupEntity = memberAddNewGroupService.memberCreateNewGroup(memberEntity.getId(), groupEntity);
                    navigateToMemberGroupsView();
                } catch (MemberNotFoundException | GroupAlreadyExistsException | RelationsMemberGroupCrudServiceException | MemberAddNewGroupServiceException e) {
                    Notification.show(String.format("Something went wrong: %s", e.getMessage()));
                }
            } else
                Notification.show("Incorrectly filled fields!", 5000, Notification.Position.MIDDLE);
        });
    }

    private void cancelButtonConfig() {
        this.cancelButton.addClickListener(buttonClickEvent -> {
            navigateToMemberGroupsView();
        });
    }

    private void navigateToMemberGroupsView() {
        UI.getCurrent().navigate(MemberGroupsViewDiv.URL);
    }
}
