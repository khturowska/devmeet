package pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.member_side_panel;

import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingApiMapper;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingDto;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.crud.RelationsMemberGroupMeetingCrudService;
import pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.exceptions.RelationsMemberGroupMeetingServiceException;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.AppHomeView;

import java.util.Collections;
import java.util.List;

@Route(value = MemberMeetingsViewDiv.URL, layout = AppHomeView.class)
@PageTitle(MemberMeetingsViewDiv.PAGE_TITLE)
//@CssImport("./styles/views/about/about-view.css")
public class MemberMeetingsViewDiv extends Div {

    private static final String ID = "member-meetings";
    public static final String URL = AppHomeView.URL + "/" + ID;
    static final String PAGE_TITLE = "Your meetings";

    private RelationsMemberGroupMeetingCrudService relationsMemberGroupMeetingCrudService;
    private MemberEntity member;

    private Grid<MeetingDto> meetingsGrid = new Grid<>();

    @Autowired
    public MemberMeetingsViewDiv(RelationsMemberGroupMeetingCrudService relationsMemberGroupMeetingCrudService) {
        setId(ID);
        this.relationsMemberGroupMeetingCrudService = relationsMemberGroupMeetingCrudService;
        this.member = getMemberFromContext();
        List<MeetingEntity> memberMeetings = findMemberMeetings(member);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(configAndCreateAGridWithMeetings(mapMeetingsToFrontend(memberMeetings)));
        verticalLayout.setSizeFull();

        setSizeFull();
        add(verticalLayout);
    }

    private MemberEntity getMemberFromContext() {
        return ComponentUtil.getData(UI.getCurrent(), MemberEntity.class);
    }

    private List<MeetingEntity> findMemberMeetings(MemberEntity member){
        try {
            return relationsMemberGroupMeetingCrudService.findAllMeetingWhereIsMember(member);
        } catch (RelationsMemberGroupMeetingServiceException e) {
            return Collections.emptyList();
        }
    }

    private Grid configAndCreateAGridWithMeetings(List<MeetingDto> meetingDtos) {
        defineGridColumns();
        setGridItemClickAction();

        this.meetingsGrid.addThemeVariants(GridVariant.LUMO_COLUMN_BORDERS, GridVariant.LUMO_ROW_STRIPES);
        this.meetingsGrid.setItems(meetingDtos);
        return meetingsGrid;
    }

    private void defineGridColumns() {
        this.meetingsGrid.addColumn(MeetingDto::getMeetingName)
                .setHeader("Meeting name")
                .setSortable(true);
        this.meetingsGrid.addColumn(MeetingDto::getBegin)
                .setHeader("Begin")
                .setSortable(true);
//        this.meetingsGrid.addColumn(MeetingDto::getBeginTime)
//                .setHeader("Begin time")
//                .setSortable(true);
        this.meetingsGrid.addColumn(MeetingDto::getEnd)
                .setHeader("End")
                .setSortable(true);
//        this.meetingsGrid.addColumn(MeetingDto::getEndTime)
//                .setHeader("End time")
//                .setSortable(true);
        this.meetingsGrid.addColumn(MeetingDto::getGroup)
                .setHeader("Group")
                .setSortable(true);
        this.meetingsGrid.addColumn(MeetingDto::getPromoter)
                .setHeader("Promoter")
                .setSortable(true);
        this.meetingsGrid.addColumn(MeetingDto::getPlace)
                .setHeader("Place")
                .setSortable(true);
    }

    private String createLinkWithParam(String url, String paramName, Long id) {
        return url + "?" + paramName + "=" + id.toString();
    }

    private void setGridItemClickAction() {

    }

    private List<MeetingDto> mapMeetingsToFrontend(List<MeetingEntity> memberMeetings){
        return new MeetingApiMapper().mapToFrontend(memberMeetings);
    }
}
