package pl.com.devmeet.devmeetcore.vaadin_ui.admins;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.crud.MeetingCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingEntity;

import java.util.ArrayList;
import java.util.List;

@Route(MeetingsGui.ADMIN_MEETINGS_VIEW_PATH)
class MeetingsGui extends VerticalLayout {

    public static final String ADMIN_MEETINGS_VIEW_PATH = "admin/meetings";

    private MenuBar menuBar;
    private MeetingCrudService meeting;
    private List<MeetingEntity> meetingList;

    // vaadin components
    private Grid<MeetingEntity> meetingGrid;

    MeetingsGui(MenuBar menuBar, MeetingCrudService meeting) {
        this.menuBar = menuBar;
        this.meeting = meeting;
        meetingList = new ArrayList<>();
        meetingList = meeting.findAll();
        setDefaultHorizontalComponentAlignment(FlexComponent.Alignment.CENTER);
        Notification.show("Meetings", 2000, Notification.Position.MIDDLE);
        meetingGrid = new Grid<>(MeetingEntity.class);
        meetingGrid.removeColumnByKey("beginTime");
        meetingGrid.removeColumnByKey("endTime");
        meetingGrid.removeColumnByKey("active");
        meetingGrid.removeColumnByKey("creationTime");
        meetingGrid.removeColumnByKey("meetingNumber");
        meetingGrid.addThemeVariants(
                GridVariant.LUMO_NO_BORDER,
                GridVariant.LUMO_NO_ROW_BORDERS,
                GridVariant.LUMO_ROW_STRIPES);
        meetingGrid.getColumns().forEach(c -> c.setAutoWidth(true));
        refreshGrid(meetingList);
        add(menuBar, meetingGrid);
    }

    private void refreshGrid(List<MeetingEntity> meetingList) {
        meetingGrid.setItems(meetingList);
        meetingGrid.getDataProvider().refreshAll();
    }


}
