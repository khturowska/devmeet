package pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.menu_new;

import lombok.AllArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
class NewMeetingValidator {

    private LocalDateTime begin;
    private LocalDateTime end;

    public boolean validate() {
        return endGreaterThanBegin()
                && beginGreaterThanNow();
    }

    private boolean endGreaterThanBegin() {
        return end.isAfter(begin);
    }

    private boolean beginGreaterThanNow() {
        return begin.isAfter(LocalDateTime.now());
    }
}
