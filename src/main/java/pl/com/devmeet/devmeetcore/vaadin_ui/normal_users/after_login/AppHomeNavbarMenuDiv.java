package pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login;

import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.contextmenu.SubMenu;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.router.RouterLink;
import lombok.Getter;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.menu_account.EditMemberProfileViewDiv;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.menu_find.FindGroupViewDiv;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.menu_find.FindMeetingViewDiv;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.menu_new.NewGroupViewDiv;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.menu_new.NewMeetingViewDiv;

class AppHomeNavbarMenuDiv extends Div {

    @Getter
    private MenuBar menuBar;

    public AppHomeNavbarMenuDiv() {
        this.menuBar = new MenuBar();
        menuBar.setId("app-menu");
        menuBar.setOpenOnHover(true);
        createMenuBar();
    }

    private void createMenuBar() {
        MenuItem newObjects = menuBar.addItem("New");
        createNewObjectsMenu(newObjects);
        MenuItem findObjects = menuBar.addItem("Find");
        createFindObjectsMenu(findObjects);
        MenuItem account = menuBar.addItem("Account");
        createAccountMenu(account);
    }

    private void createNewObjectsMenu(MenuItem menuItem) {
        SubMenu newObjectsMenu = menuItem.getSubMenu();
        newObjectsMenu.addItem(new RouterLink("Create new group", NewGroupViewDiv.class));
        newObjectsMenu.addItem(new RouterLink("Create new meeting", NewMeetingViewDiv.class));
    }

    private void createFindObjectsMenu(MenuItem menuItem) {
        SubMenu findObjectsMenu = menuItem.getSubMenu();
        findObjectsMenu.addItem(new RouterLink("Find groups", FindGroupViewDiv.class));
        findObjectsMenu.addItem(new RouterLink("Find meetings", FindMeetingViewDiv.class));
    }

    private void createAccountMenu (MenuItem menuItem){
        SubMenu accountMenu = menuItem.getSubMenu();
        accountMenu.addItem(new RouterLink("Edit Profile", EditMemberProfileViewDiv.class));
    }

}
