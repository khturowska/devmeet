package pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.TabsVariant;
import com.vaadin.flow.router.RouterLink;
import lombok.Getter;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.member_side_panel.MemberMeetingsViewDiv;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.member_side_panel.MemberGroupsViewDiv;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.member_side_panel.MemberPlacesViewDiv;

class AppHomeDrawerComponent {

    private String appName;
    @Getter
    private final Tabs menu;
    @Getter
    private Component drawerComponent;

    AppHomeDrawerComponent(String appName) {
        this.appName = appName;
        menu = createDrawerMenu();
        drawerComponent = createDrawerContent(menu);
    }

    private Component createDrawerContent(Tabs menu) {
        VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();
        layout.setPadding(false);
        layout.setSpacing(false);
        layout.getThemeList().set("spacing-s", true);
        layout.setAlignItems(FlexComponent.Alignment.STRETCH);
        layout.add(createLogoAndAppName(appName), menu);
        return layout;
    }

    private Component createLogoAndAppName(String appName) {
        HorizontalLayout logoLayout = new HorizontalLayout();
        logoLayout.setId("logo");
        logoLayout.setAlignItems(FlexComponent.Alignment.CENTER);
        logoLayout.add(new Image("images/logo.png", appName + " logo"));
        logoLayout.add(new H1(appName));
        return logoLayout;
    }

    private Tabs createDrawerMenu() {
        final Tabs tabs = new Tabs();
        tabs.setOrientation(Tabs.Orientation.VERTICAL);
        tabs.addThemeVariants(TabsVariant.LUMO_MINIMAL);
        tabs.setId("tabs");
        tabs.add(createMenuItems());
        return tabs;
    }

    private Component[] createMenuItems() {
        return new Tab[]{
//                createTab("Groups", GroupView.class),
                createTab("Groups", MemberGroupsViewDiv.class),
                createTab("Meetings", MemberMeetingsViewDiv.class),
                createTab("Places", MemberPlacesViewDiv.class)
        };
    }

    private static Tab createTab(String text, Class<? extends Component> navigationTarget) {
        final Tab tab = new Tab();
        tab.add(new RouterLink(text, navigationTarget));
        ComponentUtil.setData(tab, Class.class, navigationTarget);
        return tab;
    }
}
