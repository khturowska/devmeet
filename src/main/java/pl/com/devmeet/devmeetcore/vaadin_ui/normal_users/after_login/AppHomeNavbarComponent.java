package pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import lombok.Getter;

@Getter
class AppHomeNavbarComponent {

    private H1 currentPageTitle;
    private Component navbarComponent;
    private AppHomeNavbarMenuDiv navbarMenu;

    public AppHomeNavbarComponent() {
//        this.currentViewTitle = currentViewTitle;
        this.currentPageTitle = new H1();
        this.navbarMenu = new AppHomeNavbarMenuDiv();
        this.navbarComponent = createHeaderContent();
    }

    public void setCurrentPageTitle(String text){
        currentPageTitle.setText(text);
    }

    private Component createHeaderContent() {
        HorizontalLayout layout = new HorizontalLayout();
        layout.setId("header");
        layout.getThemeList().set("dark", true);
        layout.setWidthFull();
        layout.setSpacing(false);
        layout.setAlignItems(FlexComponent.Alignment.CENTER);
        layout.add(new DrawerToggle());
//        currentViewTitle = new H1();
        layout.add(currentPageTitle);
        layout.add(createMenuComponent());
        layout.add(new Image("images/user.svg", "Avatar"));
        return layout;
    }

    private MenuBar createMenuComponent(){
        return navbarMenu.getMenuBar();
    }
}
