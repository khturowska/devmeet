package pl.com.devmeet.devmeetcore.vaadin_ui.admins;

import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserCrudService;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;

import java.util.ArrayList;
import java.util.List;

@Route(UsersGui.ADMIN_USERS_VIEW_PATH)
class UsersGui extends VerticalLayout {

    public static final String ADMIN_USERS_VIEW_PATH = "admin/users";

    private MenuBar menuBar;
    private UserCrudService userService;
    private List<UserEntity> userList;

    // vaadin components
    private Grid<UserEntity> userGrid;
    private TextField textFieldEmail;
    private ComboBox<String> comboBoxIsActive;

    public UsersGui(MenuBar menuBar, UserCrudService userService) {
        this.menuBar = menuBar;
        this.userService = userService;
        userList = new ArrayList<>();
        userList = userService.findAll();
        setDefaultHorizontalComponentAlignment(FlexComponent.Alignment.CENTER);
        Notification.show("Users", 2000, Notification.Position.MIDDLE);

        FormLayout filtersLayout = new FormLayout();
        comboBoxIsActive = new ComboBox<>("Activated");
        comboBoxIsActive.setItems("yes", "no");
        comboBoxIsActive.addValueChangeListener(e -> filterUserList());
        comboBoxIsActive.setClearButtonVisible(true);
        textFieldEmail = new TextField("Email", e -> filterUserList());
        textFieldEmail.setPlaceholder("filter");
        textFieldEmail.setValueChangeMode(ValueChangeMode.LAZY);
        textFieldEmail.setValueChangeTimeout(1000);
        textFieldEmail.setClearButtonVisible(true);
        filtersLayout.add(textFieldEmail, comboBoxIsActive);

        userGrid = new Grid<>(UserEntity.class);
//        userGrid.removeColumnByKey("password");
//        userGrid.removeColumnByKey("active");
        userGrid.removeColumnByKey("creationTime");
        userGrid.removeColumnByKey("modificationTime");

        userGrid.addThemeVariants(
                GridVariant.LUMO_NO_BORDER,
                GridVariant.LUMO_NO_ROW_BORDERS,
                GridVariant.LUMO_ROW_STRIPES);
        userGrid.getColumns().forEach(c -> c.setAutoWidth(true));
        refreshGrid(userList);

        add(menuBar, filtersLayout, userGrid);
    }

    private void filterUserList() {
        if (textFieldEmail.getValue() != null
                && !textFieldEmail.getValue().trim().isEmpty()) {
            comboBoxIsActive.clear();
            userList = userService.findByEmailLike(textFieldEmail.getValue());
        } else if (comboBoxIsActive.getValue() != null
                && textFieldEmail.getValue().trim().isEmpty()) {
            if (comboBoxIsActive.getValue().equals("yes"))
                userList = userService.findAllByIsActive(true);
            if (comboBoxIsActive.getValue().equals("no"))
                userList = userService.findAllByIsActive(false);
        } else userList = userService.findAll();
        refreshGrid(userList);
    }

    private void refreshGrid(List<UserEntity> userList) {
        userGrid.setItems(userList);
        userGrid.getDataProvider().refreshAll();
    }
}
