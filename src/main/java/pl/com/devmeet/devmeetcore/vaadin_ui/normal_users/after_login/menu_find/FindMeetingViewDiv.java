package pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.menu_find;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.AppHomeView;

@Route(value = FindMeetingViewDiv.URL, layout = AppHomeView.class)
@PageTitle(FindMeetingViewDiv.PAGE_TITLE)
public class FindMeetingViewDiv extends Div {

    public static final String URL = AppHomeView.URL + "/find-meeting";
    static final String PAGE_TITLE = "Find meeting";

    public FindMeetingViewDiv() {
        HorizontalLayout layout = new HorizontalLayout();
        layout.add(
                new H1("find meeting view")
        );
        add(layout);
    }
}
