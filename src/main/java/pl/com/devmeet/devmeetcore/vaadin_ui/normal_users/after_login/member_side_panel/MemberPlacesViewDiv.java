package pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.member_side_panel;

import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.AppHomeView;

@Route(value = MemberPlacesViewDiv.URL, layout = AppHomeView.class)
@PageTitle(MemberPlacesViewDiv.PAGE_TITLE)
@CssImport("./styles/views/about/about-view.css")
public class MemberPlacesViewDiv extends Div {

    private static final String ID = "member-places";
    public static final String URL = AppHomeView.URL + "/" + ID;
    static final String PAGE_TITLE = "Your places";

    public MemberPlacesViewDiv() {
        setId(ID);
        add(new Label("Member Places view"));
    }

}
