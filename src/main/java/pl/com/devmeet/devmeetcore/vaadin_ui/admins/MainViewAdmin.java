package pl.com.devmeet.devmeetcore.vaadin_ui.admins;

import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;

@Route(MainViewAdmin.ADMIN_MAIN_VIEW_PATH)
@Theme(value = Lumo.class, variant = Lumo.DARK)
public class MainViewAdmin extends VerticalLayout {

    public static final String ADMIN_MAIN_VIEW_PATH = "admin";

    private MenuBar menuBar;

    // vaadin components
    private H1 header;

    MainViewAdmin(MenuBar menuBar) {
        this.menuBar = menuBar;
        setDefaultHorizontalComponentAlignment(FlexComponent.Alignment.CENTER);
        header = new H1("devmeet app - home");
        add(menuBar, header);
    }


}
