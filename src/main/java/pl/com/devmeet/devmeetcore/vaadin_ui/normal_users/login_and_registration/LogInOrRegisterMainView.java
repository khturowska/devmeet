package pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.login_and_registration;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Route(value = LogInOrRegisterMainView.URL)
@PageTitle(value = LogInOrRegisterMainView.PAGE_TITLE)
public class LogInOrRegisterMainView extends VerticalLayout {

    public static final String URL = "login-or-register";
    static final String PAGE_TITLE = "Welcome to DevMeet";

    private H1 welcomeText = new H1();

    private LogInViewDiv login;
    private RegisterViewDiv signIn;
    private Div pages;

    @Autowired
    public LogInOrRegisterMainView(VaadinRegisterService vaadinRegisterService, VaadinLoginService vaadinLoginService) {
        this.login = new LogInViewDiv(vaadinLoginService);
        this.signIn = new RegisterViewDiv(vaadinRegisterService);
        this.pages = new Div(login, signIn);

        addClassName(URL);
        setSizeFull();

        setJustifyContentMode(JustifyContentMode.START);
        setAlignItems(Alignment.CENTER);

        add(welcomeText);
        add(makeTabs());
        add(pages);
    }

    private Tabs makeTabs() {
        Tab loginTab = new Tab("Log in");
        Tab signInTab = new Tab("Register");

        Map<Tab, Component> tabsToPages = new HashMap<>();
        tabsToPages.put(loginTab, login);
        tabsToPages.put(signInTab, signIn);

        Tabs tabs = new Tabs(loginTab, signInTab);

        Set<Component> pagesShown = Stream.of(login)
                .collect(Collectors.toSet());

        tabs.addSelectedChangeListener(event -> {
            pagesShown.forEach(page -> page.setVisible(false));
            pagesShown.clear();
            Component selectedPage = tabsToPages.get(tabs.getSelectedTab());
            selectedPage.setVisible(true);
            pagesShown.add(selectedPage);
        });

        return tabs;
    }

}
