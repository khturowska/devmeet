package pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login;

import com.vaadin.flow.component.notification.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

@Component
public class AppHomeService {

    private MemberCrudService memberCrudService;

    @Autowired
    public AppHomeService(MemberCrudService memberCrudService) {
        this.memberCrudService = memberCrudService;
    }

    public MemberEntity loadTestMemberFromContext(MemberEntity member) {
        String memberLoadingError;
        if (member == null) {
            try {
                return memberCrudService.findByUser(UserEntity.builder().email("test.pt4q@gmail.com").build());
            } catch (MemberNotFoundException | UserNotFoundException e) {
                memberLoadingError = String.format("Security error. User or Member not found!");
                Notification.show(memberLoadingError, 10000, Notification.Position.MIDDLE);
            } catch (NullPointerException e) {
                memberLoadingError = String.format("Security error. Member loaded from context is null!");
                Notification.show(memberLoadingError, 10000, Notification.Position.MIDDLE);
            }
        }
        return member;
    }
}
