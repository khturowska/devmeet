package pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;

import java.util.Optional;

@JsModule("./styles/shared-styles.js")
@CssImport("./styles/views/main/main-view.css")
//@PWA(name = DevMeetHomeView.APP_NAME, shortName = DevMeetHomeView.APP_NAME, enableInstallPrompt = true)
@PageTitle(AppHomeView.PAGE_TITLE)
@Route(AppHomeView.URL)
public class AppHomeView extends AppLayout {

    public static final String PAGE_TITLE = "DevMeet Home";
    public static final String URL = "app";
    public static final String APP_NAME = "DevMeet";

    private AppHomeService memberHomeService;
//    private MemberEntity member;

    private AppHomeDrawerComponent drawerComponent;
    private AppHomeNavbarComponent navbarComponent;

    @Autowired
    public AppHomeView(AppHomeService memberHomeService) {
        this.memberHomeService = memberHomeService;
//        this.member = setMemberAndSendToContextOnlyForTests();

        setPrimarySection(Section.DRAWER);
        navbarComponent = new AppHomeNavbarComponent();
        drawerComponent = new AppHomeDrawerComponent(APP_NAME);

        addToNavbar(navbarComponent.getNavbarComponent());
        addToDrawer(drawerComponent.getDrawerComponent());
    }

    private MemberEntity getMemberFromContext(){
        MemberEntity memberEntity = ComponentUtil.getData(UI.getCurrent(), MemberEntity.class);
        return memberEntity;
    }

    private MemberEntity setMemberAndSendToContextOnlyForTests(){
        MemberEntity testMember = memberHomeService.loadTestMemberFromContext(ComponentUtil.getData(UI.getCurrent(), MemberEntity.class));
        ComponentUtil.setData(UI.getCurrent(), MemberEntity.class, testMember);

        return testMember;
    }

    @Override
    protected void afterNavigation() {
        super.afterNavigation();
        Tabs menu = drawerComponent.getMenu();
        getTabForComponent(menu, getContent()).ifPresent(menu::setSelectedTab);
        navbarComponent.setCurrentPageTitle(getCurrentPageTitle());
    }

    private Optional<Tab> getTabForComponent(Tabs menu, Component component) {
        return menu.getChildren()
                .filter(tab -> ComponentUtil.getData(tab, Class.class)
                        .equals(component.getClass()))
                .findFirst().map(Tab.class::cast);
    }

    private String getCurrentPageTitle() {
        return getContent().getClass().getAnnotation(PageTitle.class).value();
    }

}
