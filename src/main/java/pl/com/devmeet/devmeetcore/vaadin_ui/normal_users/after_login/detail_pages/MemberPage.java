package pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.detail_pages;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.router.*;
import org.springframework.beans.factory.annotation.Autowired;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.AppHomeView;

import java.util.List;
import java.util.Map;

@Route(value = MemberPage.URL, layout = AppHomeView.class)
@PageTitle(MemberPage.PAGE_TITLE)
public class MemberPage extends Div implements HasUrlParameter<String> {

    public static final String URL = AppHomeView.URL + "/member";
    public static final String PAGE_TITLE = "Member";
    public static final String QUERY_PARAM_ID_NAME = "memberId";

    private H3 message = new H3();

    private MemberEntity member = new MemberEntity();
    private MemberCrudService memberCrudService;

    @Autowired
    public MemberPage(MemberCrudService memberCrudService) {
        this.memberCrudService = memberCrudService;

        message.setText("Look at page title");
        add(message);
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, @OptionalParameter String urlQuery) {
        Location location = beforeEvent.getLocation();
        QueryParameters queryParameters = location.getQueryParameters();
        Map<String, List<String>> parametersMap = queryParameters.getParameters();

        if (parametersMap.containsKey(QUERY_PARAM_ID_NAME)) {
            try {
                Long id = Long.valueOf(parametersMap.get(QUERY_PARAM_ID_NAME).get(0));
                this.member = memberCrudService.findById(id);
                setPageTitle(member.getNick());

            } catch (MemberNotFoundException e) {
                Notification.show(e.getMessage());
            }
        }
    }

    private void setPageTitle(String nick) {
        UI.getCurrent()
                .getPage()
                .setTitle(String.format("Member: %s", nick));
    }
}
