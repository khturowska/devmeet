package pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.login_and_registration;

import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.AppHomeView;

import java.util.Collections;
import java.util.Optional;

@PageTitle(LogInViewDiv.PAGE_TITLE)
@Route(LogInViewDiv.URL)
class LogInViewDiv extends Div implements BeforeEnterObserver {

    public static final String URL = "login-view";
    public static final String PAGE_TITLE = "Log in";

    private LoginForm login = new LoginForm();
    private VerticalLayout layout = new VerticalLayout();

    private VaadinLoginService vaadinLoginService;

    public LogInViewDiv(VaadinLoginService vaadinLoginService) {
        this.vaadinLoginService = vaadinLoginService;
        addClassName(URL + "-div");

        setSizeFull();

        layout.setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);
        layout.setAlignItems(FlexComponent.Alignment.CENTER);

        addLoginAction();
        addForgotPasswordAction();

        login.setForgotPasswordButtonVisible(false);

        layout.add(login);
        add(layout);
    }

    private void addLoginAction() {
        login.addLoginListener(event -> {
            String userEmail = event.getUsername();
            String userPassword = event.getPassword();

            UserEntity userFromForm = UserEntity.builder()
                    .email(userEmail)
                    .password(userPassword)
                    .build();

            Optional<MemberEntity> member = vaadinLoginService.loginUserAndGetMember(userFromForm);
            if (member.isPresent())
                ifLoginSuccess(member.get());
            else {
                ifLoginError();
            }
        });
    }

    private void ifLoginSuccess(MemberEntity member) {
        ComponentUtil.setData(UI.getCurrent(), MemberEntity.class, member);
        UI.getCurrent().navigate(AppHomeView.URL);
    }

    private void ifLoginError() {
        login.setError(true);
        login.setForgotPasswordButtonVisible(true);
        login.setEnabled(true);
    }

    private void addForgotPasswordAction() {
        login.addForgotPasswordListener(event -> {
        });
    }

    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        if (!event.getLocation()
                .getQueryParameters()
                .getParameters()
                .getOrDefault("error", Collections.emptyList())
                .isEmpty()) {
            login.setError(true);
        }
    }
}
