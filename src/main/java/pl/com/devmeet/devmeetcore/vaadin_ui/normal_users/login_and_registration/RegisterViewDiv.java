package pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.login_and_registration;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.validator.EmailValidator;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;

import java.util.Optional;

@PageTitle(RegisterViewDiv.PAGE_TITLE)
@Route(RegisterViewDiv.URL)
class RegisterViewDiv extends Div {

    public static final String URL = "sign-in-view";
    public static final String PAGE_TITLE = "Register";

    private TextField emailTextField = new TextField();
    private PasswordField passwordTextField = new PasswordField();
    private PasswordField retypePasswordTextField = new PasswordField();

    private Button registerButton = new Button("register");
    private Button cleanFieldsButton = new Button("clean");

    private final VaadinRegisterService vaadinRegisterService;
    private Binder<UserEntity> userBinder = new Binder<>(UserEntity.class);
    private UserEntity user = new UserEntity();

    @Autowired
    public RegisterViewDiv(VaadinRegisterService vaadinRegisterService) {
        this.vaadinRegisterService = vaadinRegisterService;

        addClassName(URL + "-div");
        setSizeFull();

        VerticalLayout mainLayout = new VerticalLayout();
        HorizontalLayout textLayout = new HorizontalLayout();
        VerticalLayout formLayout = new VerticalLayout();
        HorizontalLayout actionButtonsLayout = new HorizontalLayout();

        mainLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);
        mainLayout.setAlignItems(FlexComponent.Alignment.CENTER);
        mainLayout.setSizeFull();

//        textLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);
        textLayout.setAlignItems(FlexComponent.Alignment.START);
        textLayout.setSizeFull();

        formLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.START);
        formLayout.setAlignItems(FlexComponent.Alignment.CENTER);
        formLayout.setSizeFull();

        actionButtonsLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);
        actionButtonsLayout.setAlignItems(FlexComponent.Alignment.AUTO);
        actionButtonsLayout.setSizeFull();

        H2 logInTextHeader = new H2("Sign in");
        textLayout.add(logInTextHeader);

        configTextInputs();
        formLayout.add(logInTextHeader, emailTextField, passwordTextField, retypePasswordTextField);

        configButtons();
        actionButtonsLayout.add(registerButton, cleanFieldsButton);

        FormLayout signInForm = new FormLayout();
        signInForm.add(formLayout, actionButtonsLayout);

        configBinder();

        mainLayout.add(signInForm);
        add(mainLayout);
    }

    private void configTextInputs() {
        emailTextField.setLabel("Email");
        emailTextField.setSizeFull();
        emailTextField.setRequired(true);

        passwordTextField.setLabel("Password");
        passwordTextField.setSizeFull();
        passwordTextField.setRequired(true);

        retypePasswordTextField.setLabel("Retype password");
        retypePasswordTextField.setSizeFull();
        retypePasswordTextField.setRequired(true);
    }

    private void configBinder() {
        this.userBinder.setBean(this.user);

        String passwordFieldsValidatorMessage = (String.format("Password must have minimum %d, maximum %d chars, ",
                vaadinRegisterService.getPasswordValidator().PASSWORD_MIN_LENGTH,
                vaadinRegisterService.getPasswordValidator().PASSWORD_MAX_LENGTH))
                + (vaadinRegisterService.getPasswordValidator().SPECIAL_CHARS ? "with special characters" : "without special characters");

        this.userBinder
                .forField(emailTextField)
                .withValidator(new EmailValidator("This doesn't look like a valid email address"))
                .asRequired()
                .bind(UserEntity::getEmail, UserEntity::setEmail);
        this.userBinder
                .forField(passwordTextField)
                .withValidator(pass -> vaadinRegisterService.getPasswordValidator().validatePassword(pass),
                        passwordFieldsValidatorMessage)
                .asRequired();
        this.userBinder
                .forField(retypePasswordTextField)
                .withValidator(retypedPass -> passwordTextField.getValue().equals(retypedPass)
                                && vaadinRegisterService.getPasswordValidator().validatePassword(retypedPass),
                        passwordFieldsValidatorMessage)
                .asRequired()
                .bind(UserEntity::getPassword, UserEntity::setPassword);
    }

    private void configButtons() {
        registerButton.setSizeFull();
        registerButton.addClickShortcut(Key.ENTER);
        registerButton.addClickListener(event -> {
//            UserEntity userForm = userBinder.getBean();
            if (userBinder.isValid()) {
                tryRegisterUserAndIfSuccessRedirectToSignInInfoPageElseRenderErrNotification(user);
            }
        });
//        setRegisterButtonDisabledIfFieldsAreInvalid();

        cleanFieldsButton.setSizeFull();
        cleanFieldsButton.addClickShortcut(Key.ESCAPE);
        cleanFieldsButton.addClickListener(event -> {
            clearUserBinderFields();
        });
    }

    private void setRegisterButtonDisabledIfFieldsAreInvalid(){
        userBinder.addStatusChangeListener(event -> registerButton.setEnabled(userBinder.isValid()));
    }

    private void tryRegisterUserAndIfSuccessRedirectToSignInInfoPageElseRenderErrNotification(UserEntity userForm) {
        Optional<UserEntity> registered = vaadinRegisterService.registerUser(userForm);
        if (registered.isPresent()) {
            UI.getCurrent().navigate(AfterRegisterInfoPage.URL);
        } else {
            String emailDuplication = String
                    .format("Email %s already exists. If you're forgot password, contact with administrator",
                            userForm.getEmail());
//            clearUserBinderFields();
            Notification.show(emailDuplication, 10000, Notification.Position.MIDDLE);
        }
    }

    private void clearUserBinderFields() {
        userBinder.readBean(null);
    }
}
