package pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.menu_account;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.AppHomeView;

@Route(value = EditMemberProfileViewDiv.URL, layout = AppHomeView.class)
@PageTitle(EditMemberProfileViewDiv.PAGE_TITLE)
public class EditMemberProfileViewDiv extends Div {

    public static final String URL = AppHomeView.URL + "/edit-member";
    static final String PAGE_TITLE = "Edit member";

    private EmailField userEmailField =new EmailField("email");
    private PasswordField passwordTextField = new PasswordField();
    private PasswordField retypePasswordTextField = new PasswordField();
    private Button saveUserButton = new Button();
    private Button cancelUserButton = new Button();

    private TextField nickTextField = new TextField("Member nickname");
    private Button saveMemberButton = new Button();
    private Button cancelMemberButton = new Button();

    private Binder<MemberEntity> groupBinder = new Binder<>(MemberEntity.class);
    private MemberEntity groupEntity = new MemberEntity();

    public EditMemberProfileViewDiv() {

        createButtonsConfig();
        add(createFormAccordion());
    }

    private Component createFormAccordion() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        Accordion accordion = new Accordion();
        accordion.add("Edit user", createUserForm());
        accordion.add("Edit member", createMemberForm());
        accordion.close();
        horizontalLayout.add(accordion);
        horizontalLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);
        return horizontalLayout;
    }

    private Component createUserForm() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        FormLayout formLayout = new FormLayout();
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSizeFull();
        verticalLayout.add(
                new H3("Edit user"),
                userEmailField,
                passwordTextField,
                retypePasswordTextField,
                new HorizontalLayout(saveUserButton, cancelUserButton));
        formLayout.add(verticalLayout);
        horizontalLayout.add(formLayout);
        horizontalLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);
        return horizontalLayout;
    }

    private Component createMemberForm() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        FormLayout formLayout = new FormLayout();
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSizeFull();
        verticalLayout.add(
                new H3("Edit member"),
                nickTextField,
                new HorizontalLayout(saveMemberButton, cancelMemberButton));
        formLayout.add(verticalLayout);
        horizontalLayout.add(formLayout);
        horizontalLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);
        return horizontalLayout;
    }

    private void createButtonsConfig() {
        String saveButtonLabel = "save";
        String cancelButtonLabel = "cancel";
        createUserSaveButtonConfig(saveButtonLabel);
        createUserCancelButtonConfig(cancelButtonLabel);
        createMemberSaveButtonConfig(saveButtonLabel);
        createMemberCancelButtonConfig(cancelButtonLabel);
    }

    private void createUserSaveButtonConfig(String label) {
        saveUserButton.setText(label);
    }

    private void createUserCancelButtonConfig(String label) {
        cancelUserButton.setText(label);
    }

    private void createMemberSaveButtonConfig(String label) {
        saveMemberButton.setText(label);
    }

    private void createMemberCancelButtonConfig(String label) {
        cancelMemberButton.setText(label);
    }
}
