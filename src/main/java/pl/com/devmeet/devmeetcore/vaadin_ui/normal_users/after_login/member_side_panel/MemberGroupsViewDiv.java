package pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.member_side_panel;

import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupDto;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupApiMapper;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.feature_services.member_and_group.crud.RelationsMemberGroupCrudService;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.AppHomeView;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.detail_pages.GroupPage;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.detail_pages.MemberPage;

import java.util.HashMap;
import java.util.List;

@Route(value = MemberGroupsViewDiv.URL, layout = AppHomeView.class)
@PageTitle(MemberGroupsViewDiv.PAGE_TITLE)
public class MemberGroupsViewDiv extends Div {

    private static final String ID = "member-groups";
    public static final String URL = AppHomeView.URL + "/" + ID;
    static final String PAGE_TITLE = "Your groups";

    private RelationsMemberGroupCrudService relationsMemberGroupCrudService;
    private MemberEntity member;

    private Grid<GroupDto> groupsGrid = new Grid<>();

    @Autowired
    public MemberGroupsViewDiv(RelationsMemberGroupCrudService relationsMemberGroupCrudService) {
        setId(ID);
        this.relationsMemberGroupCrudService = relationsMemberGroupCrudService;
        this.member = getMemberFromContext();
        List<GroupEntity> memberGroups = findMemberGroups(member);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(configAndCreateAGridWithGroups(mapGroupsToFrontend(memberGroups)));
        verticalLayout.setSizeFull();

        setSizeFull();
        add(verticalLayout);
    }

    private MemberEntity getMemberFromContext() {
        return ComponentUtil.getData(UI.getCurrent(), MemberEntity.class);
    }

    private Grid configAndCreateAGridWithGroups(List<GroupDto> groupDtos) {
        defineGridColumns();
        setGridItemClickAction();
        this.groupsGrid.addThemeVariants(GridVariant.LUMO_COLUMN_BORDERS, GridVariant.LUMO_ROW_STRIPES);
        this.groupsGrid.setItems(groupDtos);
        return groupsGrid;
    }

    private void defineGridColumns() {
        this.groupsGrid.addColumn(new ComponentRenderer<>(g -> new Anchor(createLinkWithParam(GroupPage.URL, GroupPage.QUERY_PARAM_ID_NAME, g.getId()), g.getGroupName())))
                .setHeader("Group name")
                .setSortable(true);
        this.groupsGrid.addColumn(GroupDto::getDescription)
                .setHeader("Description");
        this.groupsGrid.addColumn(new ComponentRenderer<>(m -> new Anchor(createLinkWithParam(MemberPage.URL, MemberPage.QUERY_PARAM_ID_NAME, m.getFounder().getId()), m.getFounder().getNick())))
                .setHeader("Founder")
                .setSortable(true);
        this.groupsGrid.addColumn(GroupDto::getCreationTime)
                .setHeader("Creation time")
                .setSortable(true);
    }

    private String createLinkWithParam(String url, String paramName, Long id) {
        return url + "?" + paramName + "=" + id.toString();
    }

    private void setGridItemClickAction() {
        this.groupsGrid.addItemClickListener(listener -> {
            GroupDto currentSelectedGroup = listener.getItem();
            QueryParameters parameters = QueryParameters.simple(new HashMap<String, String>() {{
                put(GroupPage.QUERY_PARAM_ID_NAME, currentSelectedGroup.getId().toString());
            }});
            UI.getCurrent().navigate(GroupPage.URL, parameters);
        });
    }

    private List<GroupEntity> findMemberGroups(MemberEntity member) {
        return relationsMemberGroupCrudService.findInWhatGroupsIsAMember(member);
    }

    private List<GroupDto> mapGroupsToFrontend(List<GroupEntity> groupEntities) {
        return new GroupApiMapper().mapToFrontend(groupEntities);
    }
}
