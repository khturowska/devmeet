package pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.login_and_registration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.com.devmeet.devmeetcore.feature_services.user_and_member.user_registration.DevMeetRegistrationPasswordValidator;
import pl.com.devmeet.devmeetcore.feature_services.user_and_member.user_registration.UserRegistrationService;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyExistsEmailDuplicationException;

import java.util.Optional;

@Component
class VaadinRegisterService {

    private UserRegistrationService userRegistrationService;
    private DevMeetRegistrationPasswordValidator passwordValidator;

    @Autowired
    public VaadinRegisterService(UserRegistrationService userRegistrationService) {
        this.userRegistrationService = userRegistrationService;
        this.passwordValidator = new DevMeetRegistrationPasswordValidator();
    }

    public DevMeetRegistrationPasswordValidator getPasswordValidator() {
        return passwordValidator;
    }

    public Optional<UserEntity> registerUser(UserEntity userForm) {
        try {
            return Optional.of(userRegistrationService.registerNotActiveUserUsingVaadinAndSendEmailToUser(userForm));
        } catch (UserAlreadyExistsEmailDuplicationException e) {
            return Optional.empty();
        }
    }
}
