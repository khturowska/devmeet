package pl.com.devmeet.devmeetcore.vaadin_ui.admins;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.stereotype.Component;

@Component
@UIScope
class MenuBar extends HorizontalLayout {

    // vaadin components
    private Image imageLogo;
    private Icon meetingIcon;
    private Icon placeIcon;
    private Icon groupIcon;
    private Icon userIcon;
    private Button btnHome;
    private Button btnMeetings;
    private Button btnPlaces;
    private Button btnGroups;
    private Button btnUsers;

    MenuBar() {
        getIcons();
        getButtons();

    }

    private void getButtons() {

        btnHome = new Button(new Image("img/logo.png", "Home"),
                e -> UI.getCurrent().navigate("admin"));
        btnMeetings = new Button("Meetings", meetingIcon,
                e -> UI.getCurrent().navigate("admin/meetings"));
        btnPlaces = new Button("Places", placeIcon,
                e -> UI.getCurrent().navigate("admin/places"));
        btnGroups = new Button("Groups", groupIcon,
                e -> UI.getCurrent().navigate("admin/groups"));
        btnUsers = new Button("Users", userIcon,
                e -> UI.getCurrent().navigate("admin/users"));

        setButtonsSize("50px");
        add(btnHome, btnMeetings, btnPlaces, btnGroups, btnUsers);
    }

    private void setButtonsSize(String height) {
        btnHome.setHeight(height);
        btnMeetings.setHeight(height);
        btnPlaces.setHeight(height);
        btnGroups.setHeight(height);
        btnUsers.setHeight(height);
    }

    private void getIcons() {
        imageLogo = getLogo();
        meetingIcon = getIcon(VaadinIcon.HANDSHAKE);
        placeIcon = getIcon(VaadinIcon.WORKPLACE);
        groupIcon = getIcon(VaadinIcon.GROUP);
        userIcon = getIcon(VaadinIcon.USER);
    }

    private Icon getIcon(VaadinIcon ico) {
        Icon icon = new Icon(ico);
        icon.setSize("45px");
        return icon;
    }

    private Image getLogo() {
        //https://stackoverflow.com/questions/57553973/where-should-i-place-my-vaadin-10-static-files/
        Image imageLogo = new Image("img/logo.png", "logo");
        imageLogo.getStyle().set("borderRadius", "5%");
        return imageLogo;
    }


}
