package pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.detail_pages;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.router.*;
import org.springframework.beans.factory.annotation.Autowired;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.crud.MeetingCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingNotFoundException;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.AppHomeView;

import java.util.List;
import java.util.Map;

@Route(value = MeetingPage.URL, layout = AppHomeView.class)
@PageTitle(MeetingPage.PAGE_TITLE)
public class MeetingPage extends Div implements HasUrlParameter<String> {

    public static final String URL = AppHomeView.URL + "/meeting";
    public static final String PAGE_TITLE = "Meeting";
    public static final String QUERY_PARAM_ID_NAME = "meetingId";

    private H3 message = new H3();

    private MeetingEntity meeting = new MeetingEntity();
    private MeetingCrudService meetingCrudService;

    @Autowired
    public MeetingPage(MeetingCrudService meetingCrudService) {
        this.meetingCrudService = meetingCrudService;

        message.setText("Look at page title");
        add(message);
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, @OptionalParameter String urlQuery) {
        Location location = beforeEvent.getLocation();
        QueryParameters queryParameters = location.getQueryParameters();
        Map<String, List<String>> parametersMap = queryParameters.getParameters();

        if (parametersMap.containsKey(QUERY_PARAM_ID_NAME)) {
            try {
                Long id = Long.valueOf(parametersMap.get(QUERY_PARAM_ID_NAME).get(0));
                meeting = meetingCrudService.findById(id);
                setPageTitle(
                        meeting.getMeetingName(),
                        meeting.getGroup().getGroupName(),
                        meeting.getPromoter().getNick(),
                        meeting.getBegin().toString());
//                        (meeting.getBeginDate().toString() + "T" + meeting.getBeginTime().toString()));

            } catch (MeetingNotFoundException e) {
                Notification.show(e.getMessage());
            }
        }
    }

    private void setPageTitle(String meetingName, String groupName, String founder, String beginDateTime) {
        UI.getCurrent()
                .getPage()
                .setTitle(String.format("Meeting name: %s | group name: %s | fouder: %s | begin: %s", meetingName, groupName, founder, beginDateTime));
    }
}
