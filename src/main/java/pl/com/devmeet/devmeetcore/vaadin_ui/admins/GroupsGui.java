package pl.com.devmeet.devmeetcore.vaadin_ui.admins;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.router.Route;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;

import java.util.ArrayList;
import java.util.List;

@Route(GroupsGui.ADMIN_GROUPS_VIEW_PATH)
class GroupsGui extends VerticalLayout {

    public static final String ADMIN_GROUPS_VIEW_PATH = "admin/groups";
    private MenuBar menuBar;
    private GroupCrudService group;
    private List<GroupEntity> groupList;

    // vaadin components
    private Grid<GroupEntity> groupGrid;

    public GroupsGui(MenuBar menuBar, GroupCrudService group) {
        this.menuBar = menuBar;
        this.group = group;
        groupList = new ArrayList<>();
        groupList = group.findAll();
        setDefaultHorizontalComponentAlignment(FlexComponent.Alignment.CENTER);
        Notification.show("Groups", 2000, Notification.Position.MIDDLE);
        RadioButtonGroup<String> radioButtonGroup = new RadioButtonGroup<>();
        radioButtonGroup.setItems("active", "not active", "all");
        radioButtonGroup.setValue("all");
        groupGrid = new Grid<>(GroupEntity.class);
        groupGrid.removeColumnByKey("id");
        groupGrid.removeColumnByKey("creationTime");
        groupGrid.removeColumnByKey("website");
        groupGrid.removeColumnByKey("description");
        groupGrid.removeColumnByKey("modificationTime");
        groupGrid.removeColumnByKey("active");
        groupGrid.addThemeVariants(
                GridVariant.LUMO_NO_BORDER,
                GridVariant.LUMO_NO_ROW_BORDERS,
                GridVariant.LUMO_ROW_STRIPES);
        groupGrid.getColumns().forEach(c -> c.setAutoWidth(true));
        refreshGrid(groupList);
        radioButtonGroup.addValueChangeListener(e -> {
            if (e.getValue().equals("active"))
                groupList = group.findByActive(true);
            else if (e.getValue().equals("not active"))
                groupList = group.findByActive(false);
            else groupList = group.findAll();
            refreshGrid(groupList);
        });
        add(menuBar, radioButtonGroup, groupGrid);
    }

    private void refreshGrid(List<GroupEntity> groupList) {
        groupGrid.setItems(groupList);
        groupGrid.getDataProvider().refreshAll();
    }
}
