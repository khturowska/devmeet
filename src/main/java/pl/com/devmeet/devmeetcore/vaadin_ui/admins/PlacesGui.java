package pl.com.devmeet.devmeetcore.vaadin_ui.admins;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import pl.com.devmeet.devmeetcore.domain.place.crud.PlaceCrudService;
import pl.com.devmeet.devmeetcore.domain.place.models.PlaceEntity;

import java.util.ArrayList;
import java.util.List;

@Route(PlacesGui.ADMIN_PLACES_VIEW_PATH)
class PlacesGui extends VerticalLayout {

    public static final String ADMIN_PLACES_VIEW_PATH = "admin/places";

    private MenuBar menuBar;
    private PlaceCrudService placeService;
    private List<PlaceEntity> placeList;

    // vaadin components
    private Grid<PlaceEntity> placeGrid;

    public PlacesGui(MenuBar menuBar, PlaceCrudService placeService) {
        this.menuBar = menuBar;
        this.placeService = placeService;
        placeList = new ArrayList<>();
        placeList = placeService.findAll();
        setDefaultHorizontalComponentAlignment(FlexComponent.Alignment.CENTER);
        Notification.show("Places", 2000, Notification.Position.MIDDLE);

        placeGrid = new Grid<>(PlaceEntity.class);
        placeGrid.removeColumnByKey("id");
        placeGrid.removeColumnByKey("creationTime");
        placeGrid.removeColumnByKey("modificationTime");
        placeGrid.removeColumnByKey("active");
        placeGrid.addThemeVariants(
                GridVariant.LUMO_NO_BORDER,
                GridVariant.LUMO_NO_ROW_BORDERS,
                GridVariant.LUMO_ROW_STRIPES);
        placeGrid.getColumns().forEach(c -> c.setAutoWidth(true));
        refreshGrid(placeList);
        add(menuBar, placeGrid);
    }

    private void refreshGrid(List<PlaceEntity> placeList) {
        placeGrid.setItems(placeList);
        placeGrid.getDataProvider().refreshAll();
    }
}
