package pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.menu_new;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.place.crud.PlaceCrudService;
import pl.com.devmeet.devmeetcore.domain.place.models.PlaceEntity;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceNotFoundException;
import pl.com.devmeet.devmeetcore.feature_services.member_and_group.crud.RelationsMemberGroupCrudService;
import pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.MemberAddNewMeetingInGroupService;
import pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.exceptions.MemberAddNewMeetingInGroupServiceException;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.AppHomeView;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.member_side_panel.MemberMeetingsViewDiv;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Route(value = NewMeetingViewDiv.URL, layout = AppHomeView.class)
@PageTitle(NewMeetingViewDiv.PAGE_TITLE)
public class NewMeetingViewDiv extends Div {

    public static final String URL = AppHomeView.URL + "/new-meeting";
    static final String PAGE_TITLE = "Create new meeting";

    private TextField meetingNameTextField = new TextField("Meeting title");
    private DateTimePicker beginDateTimePicker = new DateTimePicker("Begin");
    private DateTimePicker endDateTimePicker = new DateTimePicker("End");
    private ComboBox<String> groupComboBox = new ComboBox("Group");
    private ComboBox<String> placeComboBox = new ComboBox("Place");

    private Button saveMeetingButton = new Button("save");
    private Button cancelButton = new Button("cancel");

    private MemberAddNewMeetingInGroupService memberAddNewMeetingInGroupService;
    private RelationsMemberGroupCrudService relationsMemberGroupCrudService;
    private PlaceCrudService placeCrudService;
    private Binder<MeetingEntity> meetingBinder = new Binder<>(MeetingEntity.class);
    private MeetingEntity meeting = new MeetingEntity();
    private MemberEntity member;
    private List<GroupEntity> memberGroups;
    private List<PlaceEntity> places;

    @Autowired
    public NewMeetingViewDiv(MemberAddNewMeetingInGroupService memberAddNewMeetingInGroupService,
                             RelationsMemberGroupCrudService relationsMemberGroupCrudService,
                             PlaceCrudService placeCrudService) {
        this.memberAddNewMeetingInGroupService = memberAddNewMeetingInGroupService;
        this.relationsMemberGroupCrudService = relationsMemberGroupCrudService;
        this.placeCrudService = placeCrudService;

        this.member = getMemberEntityFromContext();
        this.memberGroups = findAllMemberGroups(member);
        this.places = findAllPlaces();

        configMeetingNameField();
        configGroupComboBox(memberGroups);
        configPlaceComboBox(places);

        saveButtonConfig();
        cancelButtonConfig();
        configBinder(memberGroups);
        configDateTimePickers();
        add(createMeetingForm());
    }

    private MemberEntity getMemberEntityFromContext() {
        return ComponentUtil.getData(UI.getCurrent(), MemberEntity.class);
    }

    private List<GroupEntity> findAllMemberGroups(MemberEntity member) {
        return relationsMemberGroupCrudService.findInWhatGroupsIsAMember(member);
    }

    private List<PlaceEntity> findAllPlaces() {
        return placeCrudService.findAll();
    }

    private Component createMeetingForm() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        FormLayout formLayout = new FormLayout();
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSizeFull();
        verticalLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);
        HorizontalLayout buttonsLayout = new HorizontalLayout(saveMeetingButton, cancelButton);
        buttonsLayout.setWidthFull();
        verticalLayout.add(
                new H3(PAGE_TITLE),
                meetingNameTextField,
                beginDateTimePicker,
                endDateTimePicker,
                groupComboBox,
                placeComboBox,
                buttonsLayout);
        formLayout.add(verticalLayout);
        horizontalLayout.add(formLayout);
        horizontalLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);
        return horizontalLayout;
    }

    private void configMeetingNameField(){
        this.meetingNameTextField.setWidthFull();
    }

    private void configDateTimePickers() {
        String time = "time";
        String date = "date";
        this.beginDateTimePicker.setDatePlaceholder(date);
        this.beginDateTimePicker.setTimePlaceholder(time);
        this.endDateTimePicker.setDatePlaceholder(date);
        this.endDateTimePicker.setTimePlaceholder(time);
    }

    private void configGroupComboBox(List<GroupEntity> memberGroups) {
        this.groupComboBox.setItems(memberGroups
                .stream()
                .map(GroupEntity::getGroupName)
                .collect(Collectors.toList()));
        groupComboBox.setRequired(true);
        groupComboBox.setWidthFull();
//        groupComboBox.setAllowCustomValue(true);
//        groupComboBox.addCustomValueSetListener(listener -> {
//            String selected = listener.getDetail();
//            System.out.println(selected);
//        });
//        groupComboBox.addValueChangeListener(listener -> {
//            String selected = listener.getValue();
//            System.out.println(selected);
//        });
    }

    private void configPlaceComboBox(List<PlaceEntity> memberPlaces) {
        this.placeComboBox.setItems(memberPlaces
                .stream()
                .map(PlaceEntity::getPlaceName)
                .collect(Collectors.toList()));
        placeComboBox.setRequired(true);
        placeComboBox.setWidthFull();
    }

    private void configBinder(List<GroupEntity> memberGroups) {
        this.meetingBinder.setBean(meeting);
        this.meetingBinder
                .forField(meetingNameTextField)
                .asRequired("Meeting must have name")
                .bind(MeetingEntity::getMeetingName, MeetingEntity::setMeetingName);
//        this.meetingBinder
//                .forField(beginDateTimePicker)
//                .bind(
//                        meeting -> LocalDateTime.of(meeting.getBeginDate(), meeting.getBeginTime()),
//                        (meeting, localDateTime) -> {
//                            meeting.setBeginDate(localDateTime.toLocalDate());
//                            meeting.setBeginTime(localDateTime.toLocalTime());
//                        });
//        this.meetingBinder
//                .forField(endDateTimePicker)
//                .bind(
//                        meeting -> LocalDateTime.of(meeting.getEndDate(), meeting.getEndTime()),
//                        (meeting, localDateTime) -> {
//                            meeting.setEndDate(localDateTime.toLocalDate());
//                            meeting.setEndTime(localDateTime.toLocalTime());
//                        });
    }

    private void saveButtonConfig() {
        this.saveMeetingButton.addClickListener(buttonClickEvent -> {
            if (meetingBinder.isValid() && !groupComboBox.isInvalid() && validateDateTimePickers()) {
                this.meeting = buildMeeting(meetingBinder.getBean());
                try {
                    this.meeting = memberAddNewMeetingInGroupService.addNewMeetingInGroup(meeting);
                    navigateToMemberGroupMeetings();
                } catch (MemberNotFoundException | GroupNotFoundException | PlaceNotFoundException | MeetingAlreadyExistsException | MemberAddNewMeetingInGroupServiceException e) {
                    Notification.show(String.format("Something went wrong: %s", e.getMessage()));
                }
            } else
                Notification.show("Incorrectly filled fields!", 5000, Notification.Position.MIDDLE);
        });
    }

    private MeetingEntity buildMeeting (MeetingEntity meetingEntity){
        meetingEntity.setPromoter(member);
        meetingEntity.setGroup(findSelectedGroupByName(groupComboBox.getValue(), memberGroups));
        meetingEntity.setPlace(findSelectedPlaceByName(placeComboBox.getValue(), places));
        meetingEntity.setBegin(beginDateTimePicker.getOptionalValue().get());
        meetingEntity.setEnd(endDateTimePicker.getOptionalValue().get());
//        meetingEntity.setBeginTime(beginDateTimePicker.getValue().toLocalTime());
//        meetingEntity.setBeginDate(beginDateTimePicker.getValue().toLocalDate());
//        meetingEntity.setEndTime(endDateTimePicker.getValue().toLocalTime());
//        meetingEntity.setEndDate(endDateTimePicker.getValue().toLocalDate());
        return meetingEntity;
    }

    private boolean validateDateTimePickers() {
        Optional<LocalDateTime> begin = beginDateTimePicker.getOptionalValue();
        Optional<LocalDateTime> end = endDateTimePicker.getOptionalValue();
        return begin.isPresent() && end.isPresent() ?
                new NewMeetingValidator(begin.get(), end.get()).validate()
                : false;
    }

    private GroupEntity findSelectedGroupByName(String groupName, List<GroupEntity> groups) {
        return groups
                .stream()
                .filter(g -> g.getGroupName().equals(groupName))
                .findFirst()
                .get();
    }

    private PlaceEntity findSelectedPlaceByName(String placeName, List<PlaceEntity> places) {
        return places
                .stream()
                .filter(p -> p.getPlaceName().equals(placeName))
                .findFirst()
                .get();
    }

    private void cancelButtonConfig() {
        this.cancelButton.addClickListener(buttonClickEvent -> {
            navigateToMemberGroupMeetings();
        });
    }

    private void navigateToMemberGroupMeetings() {
        UI.getCurrent().navigate(MemberMeetingsViewDiv.URL);
    }
}
