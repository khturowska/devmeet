package pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.login_and_registration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.com.devmeet.devmeetcore.feature_services.user_and_member.login.UserLoginService;
import pl.com.devmeet.devmeetcore.feature_services.user_and_member.login.exceptions.WrongUserPasswordLoginException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.util.Optional;

@Slf4j
@Component
class VaadinLoginService {

    private UserLoginService loginService;

    @Autowired
    public VaadinLoginService(UserLoginService loginService) {
        this.loginService = loginService;
    }

    public Optional<MemberEntity> loginUserAndGetMember(UserEntity userFromLoginForm) {
        try {
            return Optional.of(loginService.loginUserAndGetMember(userFromLoginForm));
        } catch (UserNotFoundException | WrongUserPasswordLoginException | UserFoundButNotActiveException e) {
            String errorMessage = e.getMessage();
            log.warn(String.format("User %s tried to log in but error occurred: %s",
                    userFromLoginForm.getEmail(),
                    errorMessage));
            return Optional.empty();
        }
    }
}
