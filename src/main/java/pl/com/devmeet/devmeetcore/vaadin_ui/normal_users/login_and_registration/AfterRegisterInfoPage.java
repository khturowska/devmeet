package pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.login_and_registration;

import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route(value = AfterRegisterInfoPage.URL)
public class AfterRegisterInfoPage extends VerticalLayout {
    public static final String URL = "register-success";

    public AfterRegisterInfoPage() {
        add(new H1("Mail with registration link was send to you"),
                new Paragraph("Please confirm your email."));
    }
}
