package pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.detail_pages;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.router.*;
import org.springframework.beans.factory.annotation.Autowired;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.after_login.AppHomeView;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

@Route(value = GroupPage.URL, layout = AppHomeView.class)
@PageTitle(GroupPage.PAGE_TITLE)
public class GroupPage extends Div implements HasUrlParameter<String> {

    public static final String URL = AppHomeView.URL + "/group";
    public static final String PAGE_TITLE = "Group";
    public static final String QUERY_PARAM_ID_NAME = "groupId";

    private H3 message = new H3();

    private GroupEntity group = new GroupEntity();
    private GroupCrudService groupCrudService;

    @Autowired
    public GroupPage(GroupCrudService groupCrudService) {
        this.groupCrudService = groupCrudService;

        message.setText("Look at page title");
        add(message);
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, @OptionalParameter String urlQuery) {
        Location location = beforeEvent.getLocation();
        QueryParameters queryParameters = location.getQueryParameters();
        Map<String, List<String>> parametersMap = queryParameters.getParameters();

        if (parametersMap.containsKey(QUERY_PARAM_ID_NAME)) {
            try {
                Long id = Long.valueOf(parametersMap.get(QUERY_PARAM_ID_NAME).get(0));
                group = groupCrudService.findById(id);
                setPageTitle(group.getGroupName(), group.getFounder().getNick(), group.getCreationTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

            } catch (GroupNotFoundException e) {
                Notification.show(e.getMessage());
            }
        }
    }

    private void setPageTitle(String newTitle, String founder, String createdAt) {
        UI.getCurrent()
                .getPage()
                .setTitle(String.format("Group name: %s | fouder: %s | created: %s", newTitle, founder, createdAt));
    }
}
