package pl.com.devmeet.devmeetcore.vaadin_ui;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.lumo.Lumo;
import pl.com.devmeet.devmeetcore.vaadin_ui.normal_users.login_and_registration.LogInOrRegisterMainView;

@Route
class MainView extends VerticalLayout {

    private Button normalUserButton = new Button("Normal User");
    private Button changeTheme = new Button("Light");
    private boolean darkTheme = true;

    private Text normUserInfoText = new Text("Application");
    private Text changeThemeInfoText = new Text("Change theme");

    public MainView() {
        Div normUserLine = new Div();
        Div adminUserLine = new Div();
        Div themeSetting = new Div();

        normUserLine.add(normalUserButton, normUserInfoText);
        themeSetting.add(changeTheme, changeThemeInfoText);

        normalUserButtonConfig();
        changeTheme();

        add(
                normUserLine,
                adminUserLine,
                themeSetting
        );
    }

    private void changeTheme() {
        changeTheme.addClickListener(e -> {
            if (!darkTheme) {
                UI.getCurrent().getElement().setAttribute("theme", Lumo.DARK);
                changeTheme.setText("Light");
                darkTheme = true;
            } else {
                UI.getCurrent().getElement().setAttribute("theme", Lumo.LIGHT);
                changeTheme.setText("Dark");
                darkTheme = false;
            }
        });
    }

    //
    void normalUserButtonConfig() {
        normalUserButton.addClickListener(e ->
                UI.getCurrent().navigate(LogInOrRegisterMainView.URL));
    }
}
