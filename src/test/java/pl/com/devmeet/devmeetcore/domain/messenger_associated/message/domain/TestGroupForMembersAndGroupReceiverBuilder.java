package pl.com.devmeet.devmeetcore.domain.messenger_associated.message.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudRepository;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerEntity;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerRepository;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
class TestGroupForMembersAndGroupReceiverBuilder {

    private GroupCrudRepository groupRepository;
    private MessengerRepository messengerRepository;

    private GroupEntity groupEntity;
    private MessengerEntity messengerEntity;

    public void build() {
        initGroup();
        initMessenger();
    }

    private void initGroup() {
        this.groupEntity = GroupEntity.builder()
                .groupName("Java test group")
                .website("www.testWebsite.com")
                .description("Welcome to test group")
                .membersLimit(5)
                .creationTime(null)
                .modificationTime(null)
                .isActive(false)
                .build();
    }

    private void initMessenger() {
        this.messengerEntity = MessengerEntity.builder()
                .group(groupEntity)
                .build();
    }
}
