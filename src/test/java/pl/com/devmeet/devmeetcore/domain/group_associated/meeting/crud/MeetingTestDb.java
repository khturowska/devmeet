package pl.com.devmeet.devmeetcore.domain.group_associated.meeting.crud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.place.crud.PlaceCrudService;
import pl.com.devmeet.devmeetcore.domain.place.models.PlaceEntity;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

@Builder
@NoArgsConstructor
@AllArgsConstructor
class MeetingTestDb {

    private MeetingCrudService meetingCrudService;
    private GroupCrudService groupCrudService;
    private PlaceCrudService placeCrudService;

    public GroupEntity initTestGroup(GroupEntity groupEntity) throws GroupAlreadyExistsException {
        return groupCrudService.add(groupEntity);
    }

    public PlaceEntity initTestPlace(PlaceEntity placeEntity) throws MemberNotFoundException, UserNotFoundException, PlaceAlreadyExistsException {
        return placeCrudService.add(placeEntity);
    }

    public boolean initTestDB(GroupEntity groupEntity, PlaceEntity placeEntity) throws GroupAlreadyExistsException, MemberNotFoundException, UserNotFoundException, PlaceAlreadyExistsException {
        return initTestGroup(groupEntity) != null
                && initTestPlace(placeEntity) != null;
    }
}
