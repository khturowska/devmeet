package pl.com.devmeet.devmeetcore.domain.member_associated.availability.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import pl.com.devmeet.devmeetcore.DevmeetApplication;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.models.AvailabilityDto;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.models.AvailabilityEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberUserNotActiveException;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserCrudService;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyActiveException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyExistsEmailDuplicationException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.net.URI;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = DevmeetApplication.class
)
@AutoConfigureMockMvc
//@TestPropertySource(locations = "classpath:integrationtest-application.properties")
public class AvailabilityApiComponentTest {

    String postEndPointString;
    @Autowired
    private ObjectMapper objectMapper;
    @LocalServerPort
    private int randomServerPort;
    private RestTemplate restTemplate;
    @Autowired
    private MemberCrudService memberCrudService;
    @Autowired
    private UserCrudService userCrudService;
    private AvailabilityEntity testAvailabilityEntity;
    private MemberEntity testMemberEntity;
    private UserEntity testUserEntity;
    private AvailabilityDto frontendTestAvailability;

    @Before
    public void setUp() throws Exception {
        this.restTemplate = new RestTemplate();
        initTestDB();

        this.postEndPointString = "/api/v1/availability";

//        testAvailabilityEntity = AvailabilityEntity.builder()
//                .member(testMemberEntity)
//                .beginDateTime(new DateTime(2020, 3, 3, 15, 0, 0))
//                .endDateTime(new DateTime(2020, 3, 3, 16, 0, 0))
//                .remoteWork(true)
//                .creationTime(null)
//                .modificationTime(null)
//                .isActive(true)
//                .build();

        this.frontendTestAvailability = AvailabilityDto.builder()
                .memberId(testMemberEntity.getId())
                .remoteWork(false)
                .freeTime(true)
                .beginDate("2020/04/05")
                .beginTime("17:30")
                .endDate("2020/04/05")
                .endTime("21:00")
                .build();

    }

    private boolean initTestDB() {
        this.testUserEntity = UserEntity.builder()
                .email("testavailabilityapiuser@gmail.com")
                .password("multiPass")
                .isActive(true)
                .build();

        this.testMemberEntity = MemberEntity.builder()
                .user(testUserEntity)
                .nick("testAvailabilityApiMember")
                .build();

        try {
            this.testUserEntity = userCrudService
                    .findByIdOrUserEmail(
                            userCrudService.activate(
                                    userCrudService.add(testUserEntity)
                            )
                    );
        } catch (UserNotFoundException | UserAlreadyActiveException e) {
            e.printStackTrace();
        } catch (UserAlreadyExistsEmailDuplicationException e) {
            System.out.println("Availability TEST: User (id=" + testUserEntity.getId() + ") already exists");
        }

        try {
            this.testMemberEntity = memberCrudService
                    .find(memberCrudService.add(testMemberEntity));
        } catch (MemberNotFoundException | UserNotFoundException | MemberUserNotActiveException e) {
            e.printStackTrace();
        } catch (MemberAlreadyExistsException e) {
            System.out.println("Availability TEST: Member (id=" + testUserEntity.getId() + ") already exists");
        }

        return testUserEntity.getId() != null
                && testMemberEntity.getId() != null;
    }

    private ResponseEntity<AvailabilityDto> createAvailabilityUsingRestApi() throws Exception {
        URI uri = new URI("http://localhost:" + randomServerPort + postEndPointString);
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AvailabilityDto> requestEntity = new HttpEntity<>(frontendTestAvailability);
        return restTemplate.exchange(uri.toString(), HttpMethod.POST, requestEntity, AvailabilityDto.class);
    }

    // todo zmieniać konfigurację spring security na czas testów lub uwzględnić autoryzację Usera
    @Ignore
    @Test
    @SneakyThrows
    public void WHEN_create_Availability_using_AvailabilityApiMapper_from_other_timezone_THEN_return_Availability_with_time_correction() {
        ResponseEntity<AvailabilityDto> postAvailability = createAvailabilityUsingRestApi();

        assertThat(postAvailability.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(postAvailability.getBody().getId()).isNotNull();
    }
//
//    @Test
//    @SneakyThrows
//    public void WHEN_try_to_find_existing_Member_Availabilities_return_Availabilities(){
//
//    }
}