package pl.com.devmeet.devmeetcore.domain.place.crud;

import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudRepository;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberRepository;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerRepository;
import pl.com.devmeet.devmeetcore.domain.place.models.PlaceEntity;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserRepository;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@RunWith(SpringRunner.class)
public class PlaceCrudServiceTest {

    @Autowired
    private PlaceCrudRepository repository;
    @Autowired
    private MemberRepository memberRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MessengerRepository messengerRepository;
    @Autowired
    private GroupCrudRepository groupCrudRepository;

//    private PlaceCrudService placeCrudService;

    private PlaceEntity testPlaceEntity1;
    private PlaceEntity testPlaceEntity2;
    private MemberEntity testMemberEntity;
    private UserEntity testUserEntity;

    @Before
    public void setUp() {

        testUserEntity = UserEntity.builder()
                .email("testplaceuser@gmail.com")
                .password("multiPass")
                .isActive(true)
                .build();

        testMemberEntity = MemberEntity.builder()
                .user(testUserEntity)
                .nick("serpentyna123")
                .build();

        testPlaceEntity1 = PlaceEntity.builder()
                .placeName("FOCUS")
                .description("Centrum konferencyjne FOCUS - budynek z drzewem na piętrze")
                .website("http://www.budynekfocus.com/pl")
                .location("Aleja Armii Ludowej 26, 00-609 Warszawa")
                .creationTime(null)
                .modificationTime(null)
                .isActive(true)
                .build();

        testPlaceEntity2 = PlaceEntity.builder()
                .placeName("Wydział Matematyki, Informatyki i Mechaniki Uniwersytetu Warszawskiego – wydział Uniwersytetu Warszawskiego")
                .description("MeetUp tup! tup! tup! jeb!")
                .website("https://www.mimuw.edu.pl/")
                .location("Stefana Banacha 2, 02-097 Warszawa")
                .creationTime(null)
                .modificationTime(null)
                .isActive(true)
                .build();


    }

    private PlaceCrudService initPlaceCrudFacade() {
        return new PlaceCrudService(repository, memberRepository, userRepository, messengerRepository, groupCrudRepository);
    }

    @SneakyThrows
    @Test
    public void WHEN_try_to_create_non_existing_place_THEN_return_place() {
        PlaceCrudService placeCrudService = initPlaceCrudFacade();
        PlaceEntity created = placeCrudService.add(testPlaceEntity1);

        assertThat(created.getPlaceName()).isEqualTo(testPlaceEntity1.getPlaceName());
        assertThat(created.getDescription()).isEqualTo(testPlaceEntity1.getDescription());
        assertThat(created.getWebsite()).isEqualTo(testPlaceEntity1.getWebsite());
        assertThat(created.getLocation()).isEqualTo(testPlaceEntity1.getLocation());

        assertThat(created.getCreationTime()).isNotNull();
        assertThat(created.getModificationTime()).isNull();
        assertThat(created.isActive()).isTrue();
    }

    @SneakyThrows
    @Test
    public void WHEN_try_to_create_existing_place_THEN_EntityAlreadyExistsException() {
        PlaceCrudService placeCrudService = initPlaceCrudFacade();
        try {
            placeCrudService.add(testPlaceEntity1);
        } catch (PlaceAlreadyExistsException e) {
            Assert.fail();
        }
        try {
            placeCrudService.add(testPlaceEntity1);
            Assert.fail();
        } catch (PlaceAlreadyExistsException e) {
            assertThat(e)
                    .isInstanceOf(PlaceAlreadyExistsException.class)
                    .hasMessage(PlaceCrudStatusEnum.PLACE_ALREADY_EXISTS.toString());
        }
    }

    @SneakyThrows
    @Test
    public void WHEN_found_place_THEN_return_place() {
        PlaceEntity found;
        PlaceCrudService placeCrudService = initPlaceCrudFacade();
        PlaceEntity created = placeCrudService.add(testPlaceEntity1);

        found = placeCrudService.findPlaceById(created.getId());
        assertThat(found).isNotNull();
    }

    @SneakyThrows
    @Test
    public void WHEN_try_to_find_non_existing_place_THEN_return_EntityNotFoundException() {
        PlaceCrudService placeCrudService = initPlaceCrudFacade();
        try {
            placeCrudService.findPlaceByIdOrFeatures(testPlaceEntity1);
            Assert.fail();
        } catch (PlaceNotFoundException e) {
            assertThat(e)
                    .isInstanceOf(PlaceNotFoundException.class)
                    .hasMessage(PlaceCrudStatusEnum.PLACE_NOT_FOUND.toString());
        }
    }

    @SneakyThrows
    @Test
    public void WHEN_try_to_find_all_places_THEN_return_places() {
        List<PlaceEntity> found;
        PlaceCrudService placeCrudService = initPlaceCrudFacade();
        placeCrudService.add(testPlaceEntity1);
        placeCrudService.add(testPlaceEntity2);
        found = placeCrudService.findAll();

        assertThat(found.size()).isGreaterThanOrEqualTo(2);
    }

    @SneakyThrows
    @Test
    public void WHEN_try_to_update_existing_place_THEN_return_place() {
        PlaceCrudService placeCrudService = initPlaceCrudFacade();
        PlaceEntity created = placeCrudService.add(testPlaceEntity1);
        PlaceEntity updateForCreated = placeUpdatedValues(placeCrudService.findPlaceByIdOrFeatures(testPlaceEntity1));
        PlaceEntity updated = placeCrudService.update(updateForCreated);

        assertThat(updated.getPlaceName()).isEqualTo(created.getPlaceName());
        assertThat(updated.getWebsite()).isEqualTo("www.pw.pl");
        assertThat(updated.getDescription()).isEqualTo("openspace");
        assertThat(updated.getLocation()).isEqualTo(created.getLocation());
        assertThat(updated.getCreationTime()).isEqualTo(created.getCreationTime());
        assertThat(updated.getModificationTime()).isNotNull();
        assertThat(updated.isActive()).isEqualTo(created.isActive());
    }

    private PlaceEntity placeUpdatedValues(PlaceEntity testPlaceEntity) {
        return PlaceEntity.builder()
                .id(testPlaceEntity.getId())
                .website("www.pw.pl")
                .description("openspace")
                .build();
    }

    @SneakyThrows
    @Test
    public void WHEN_try_to_update_non_existing_place_THEN_return_EntityNotFoundException() {
        PlaceCrudService placeCrudService = initPlaceCrudFacade();
        try {
            placeCrudService.update(placeUpdatedValues(testPlaceEntity1));
        } catch (PlaceNotFoundException e) {
            assertThat(e)
                    .isInstanceOf(PlaceNotFoundException.class);
        }
    }

    @SneakyThrows
    @Test
    public void WHEN_delete_existing_place_THEN_return_place() {
        PlaceCrudService placeCrudService = initPlaceCrudFacade();
        PlaceEntity created = placeCrudService.add(testPlaceEntity1);
        PlaceEntity deleted = placeCrudService.delete(created);

        assertThat(deleted).isNotNull();
        assertThat(deleted.isActive()).isFalse();
        assertThat(deleted.getCreationTime()).isEqualTo(created.getCreationTime());
        assertThat(deleted.getModificationTime()).isNotNull();
    }

    @SneakyThrows
    @Test
    public void WHEN_try_to_delete_non_existing_place_THEN_return_EntityNotFoundException() {
        PlaceCrudService placeCrudService = initPlaceCrudFacade();
        try {
            placeCrudService.delete(testPlaceEntity1);
        } catch (PlaceNotFoundException e) {
            assertThat(e)
                    .isInstanceOf(PlaceNotFoundException.class);
        }
    }
}