package pl.com.devmeet.devmeetcore.domain.messenger_associated.message.domain;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by IntelliJ IDEA.
 * User: Kamil Ptasinski
 * Date: 02.12.2019
 * Time: 18:09
 */
@RequiredArgsConstructor
class TestMessagesGenerator {

    @NonNull
    private String testMessage;

    private int index = -1;

    public List<MessageEntity> generateConversation(MessengerEntity sender, MessengerEntity receiver, int numberOfMessages) {
        boolean memberReceiver = checkIsMemberReceiver(receiver);
        boolean groupReceiver = checkIsGroupReceiver(receiver);

        if (memberReceiver && !groupReceiver)
            return generateMemberToMemberConversation(sender, receiver, numberOfMessages);

        else if (groupReceiver && !memberReceiver)
            return generateMemberToGroupConversation(sender, receiver, numberOfMessages);

        else
            return null;
    }

    private boolean checkIsMemberReceiver(MessengerEntity receiver) {
        try {
            return receiver.getMember() != null;
        } catch (NullPointerException e) {
            return false;
        }
    }

    private boolean checkIsGroupReceiver(MessengerEntity receiver) {
        try {
            return receiver.getGroup() != null;
        } catch (NullPointerException e) {
            return false;
        }
    }

    private List<MessageEntity> generateMemberToMemberConversation(MessengerEntity sender, MessengerEntity receiver, int numberOfMessages) {
        return testMessagesGenerator(sender.getMember().getNick(), receiver.getMember().getNick(), numberOfMessages).stream()
                .map(message ->
                        messageBuilderWithSwitching(sender, receiver, message, index++)
                )
                .collect(Collectors.toList());
    }

    private MessageEntity messageBuilderWithSwitching(MessengerEntity sender, MessengerEntity receiver, String message, int objectIndex) {
        return objectIndex % 2 == 0 ?
                MessageEntity.builder()
                        .sender(receiver)
                        .receiver(sender)
                        .message(message)
                        .build()
                :
                MessageEntity.builder()
                        .sender(sender)
                        .receiver(receiver)
                        .message(message)
                        .build();
    }

    private List<MessageEntity> generateMemberToGroupConversation(MessengerEntity sender, MessengerEntity receiver, int numberOfMessages) {
        return testMessagesGenerator(sender.getMember().getNick(), receiver.getGroup().getGroupName(), numberOfMessages).stream()
                .map(message ->
                        simpleMessageBuilder(sender, receiver, message)
                )
                .collect(Collectors.toList());
    }

    private MessageEntity simpleMessageBuilder(MessengerEntity sender, MessengerEntity receiver, String message) {
        return MessageEntity.builder()
                .sender(sender)
                .receiver(receiver)
                .message(message)
                .build();
    }

    private List<String> testMessagesGenerator(String senderNickname, String receiverNickname, int numberOfMessages) {
        List<String> result = new ArrayList<>();
        int index = 0;

        while (index < numberOfMessages) {
            if (testMessage != null && !testMessage.equals(""))
                result.add(generateStringMessage(senderNickname, receiverNickname, index));
            else
                result.add("");

            index++;
        }

        return result;
    }

    private String generateStringMessage(String senderNickname, String receiverNickname, int messageIndex) {
        return String
                .format("'%s' to '%s' test message number: %d \nMessage: %s", senderNickname, receiverNickname, messageIndex, testMessage);
    }
}
