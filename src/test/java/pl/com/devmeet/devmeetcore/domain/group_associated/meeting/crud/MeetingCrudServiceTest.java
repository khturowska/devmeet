package pl.com.devmeet.devmeetcore.domain.group_associated.meeting.crud;

//@Ignore
//@DataJpaTest
//@RunWith(SpringRunner.class)
public class MeetingCrudServiceTest {

//    @Autowired
//    private GroupCrudRepository groupCrudRepository;
//
//    @Autowired
//    private MeetingCrudRepository meetingCrudRepository;
//
//    @Autowired
//    private PlaceCrudRepository placeCrudRepository;
//
//    @Autowired
//    private MemberRepository memberRepository;
//
//    @Autowired
//    private UserRepository userRepository;
//
//    @Autowired
//    private MessengerRepository messengerRepository;
//
//    private GroupCrudService groupCrudService;
//    private MeetingCrudService meetingCrudService;
//    private PlaceCrudService placeCrudService;
//
//    private MeetingDto meetingDto;
//    private MeetingDto secondMeeting;
//    private GroupDto groupDto;
//    private PlaceDto placeDto;
//
//    List<MeetingDto> meetingDtosList = new ArrayList<>();
//
//    @Before
//    public void setUp() {
//
//        groupDto = new GroupDto().builder()
//                .groupName("Dancing group")
//                .description("We dance only salsa")
//                .isActive(true)
//                .build();
//
//        placeDto = new PlaceDto().builder()
//                .placeName("Warsaw City")
//                .build();
//
//        meetingDto = new MeetingDto().builder()
//                .meetingNumber(1)
//                .beginTime(DateTime.now())
//                .endTime(DateTime.now().plusHours(2))
//                .group(groupDto)
//                .isActive(true)
//                .place(placeDto)
//                .build();
//
//        secondMeeting = new MeetingDto().builder()
//                .meetingNumber(2)
//                .beginTime(DateTime.now())
//                .endTime(DateTime.now().plusHours(2))
//                .group(groupDto)
//                .isActive(true)
//                .place(placeDto)
//                .build();
//
//        groupCrudService = initGroupCruFacade();
//        placeCrudService = initPlaceCrudFacade();
//        meetingCrudService = initMeetingCrudFacade();
//
//    }
//
//    private MeetingDto createMeeting() throws MeetingAlreadyExistsException {
//        return initMeetingCrudFacade().add(meetingDto);
//    }
//
//    private MeetingDto createSecondMeeting() throws MeetingAlreadyExistsException {
//        return initMeetingCrudFacade().add(secondMeeting);
//    }
//
//    private GroupDto createGroup() throws GroupAlreadyExistsException, UserNotFoundException, GroupNotFoundException, MemberNotFoundException, MessengerAlreadyExistsException, MessengerArgumentNotSpecified {
//        return initGroupCruFacade().add(groupDto);
//    }
//
//    private PlaceDto createPlace() throws PlaceAlreadyExistsException, MemberNotFoundException, UserNotFoundException {
//        return initPlaceCrudFacade().add(placeDto);
//    }
//
//
//    private GroupCrudService initGroupCruFacade() {
//        return new GroupCrudService(groupCrudRepository, memberRepository, userRepository, messengerRepository);
//    }
//
//    private PlaceCrudService initPlaceCrudFacade() {
//        return new PlaceCrudService(placeCrudRepository, memberRepository, userRepository, messengerRepository, groupCrudRepository);
//    }
//
//    private MeetingCrudService initMeetingCrudFacade() {
//        return new MeetingCrudService(meetingCrudRepository);
//    }
//
//    @Test
//    public void when_create_non_existing_meeting_then_create_meeting() throws MeetingAlreadyExistsException {
//
//        MeetingDto createdMeetingDto = createMeeting();
//        assertThat(createdMeetingDto).isNotNull();
//        assertThat(createdMeetingDto.getGroup().getGroupName()).isEqualTo("Dancing group");
//        assertThat(createdMeetingDto.getMeetingNumber()).isEqualTo(1);
//    }
//
//    @Test
//    public void when_create_existing_meeting_then_throw_an_exception() {
//
//        MeetingDto existingMeetingDto = null;
//
//        try {
//            existingMeetingDto = createMeeting();
//        } catch (MeetingAlreadyExistsException e) {
//            Assert.fail();
//        }
//
//        try {
//            meetingCrudService.add(existingMeetingDto);
//            Assert.fail();
//        } catch (MeetingAlreadyExistsException e) {
//            assertThat(e)
//                    .hasMessage("Meeting already exists");
//        }
//    }
//
//    @Test
//    public void when_try_to_read_existing_meeting_then_read() throws MeetingNotFoundException, MeetingAlreadyExistsException {
//        MeetingDto createdMeetingDto = createMeeting();
//        MeetingDto foundMeeting = meetingCrudService.find(createdMeetingDto);
//        assertThat(foundMeeting).isNotNull();
//        assertThat(foundMeeting.getGroup().getGroupName()).isEqualTo("Dancing group");
//        assertThat(foundMeeting.getMeetingNumber()).isEqualTo(1);
//    }
//
//    @Test
//    public void when_try_to_readAll_meetings_for_given_group_then_readAll() throws MeetingNotFoundException, GroupAlreadyExistsException, MeetingAlreadyExistsException, UserNotFoundException, MemberNotFoundException, GroupNotFoundException, MessengerAlreadyExistsException, MessengerArgumentNotSpecified {
//
//        GroupDto groupDto = createGroup();
//        MeetingDto createdMeetingDto = createMeeting();
//        MeetingDto createdSecondDto = createSecondMeeting();
//
//        meetingDtosList.add(createdMeetingDto);
//        meetingDtosList.add(createdSecondDto);
//
//        List<MeetingDto> meetingEntityList = meetingCrudService.readAll(groupDto);
//        assertThat(meetingEntityList).isNotNull();
//        assertThat(meetingEntityList.size()).isEqualTo(2);
//        assertThat(meetingEntityList.get(0).getMeetingNumber()).isEqualTo(1);
//        assertThat(meetingEntityList.get(1).getMeetingNumber()).isEqualTo(2);
//    }
//
//    @Test
//    public void when_try_to_update_existing_meeting_then_update() throws MeetingNotFoundException, MeetingAlreadyExistsException {
//
//        MeetingDto oldMeetingDto = createMeeting();
//
//        MeetingDto newDto = MeetingDto.builder()
//                .beginTime(DateTime.now())
//                .endTime(DateTime.now().plusHours(4))
//                .group(groupDto)
//                .isActive(true)
//                .place(placeDto)
//                .build();
//
//        MeetingDto updatedMeetingDto = meetingCrudService.update(newDto);
//        assertThat(updatedMeetingDto.getMeetingNumber()).isEqualTo(1);
//        assertThat(updatedMeetingDto.getEndTime().toLocalDate()).isEqualTo(DateTime.now().plusHours(4).toLocalDate());
//    }
//
//    @Test
//    public void when_try_to_update_existing_meeting_with_meeting_number_of_other_entity_then_update_with_previous_meeting_number() throws MeetingNotFoundException, MeetingAlreadyExistsException { //nie może być dwóch meetingów o tym samym numerze
//
//        MeetingDto oldMeetingDto = createMeeting();
//
//        MeetingDto newDto = MeetingDto.builder()
//                .meetingNumber(2)
//                .beginTime(DateTime.now())
//                .endTime(DateTime.now().plusHours(4))
//                .group(groupDto)
//                .isActive(true)
//                .place(placeDto)
//                .build();
//
//        MeetingDto updatedMeetingDto = meetingCrudService.update(newDto);
//        assertThat(updatedMeetingDto.getMeetingNumber()).isEqualTo(1);
//        assertThat(updatedMeetingDto.getEndTime().toLocalDate()).isEqualTo(DateTime.now().plusHours(4).toLocalDate());
//    }
//
//    @Test
//    public void when_try_to_delete_existing_meeting_then_delete_meeting() throws MeetingAlreadyExistsException, MeetingNotFoundException {
//
//        MeetingDto deletedMeeting = createMeeting();
//        MeetingDto meetingDto = meetingCrudService.delete(deletedMeeting);
//        assertThat(meetingDto.isActive()).isFalse();
//        assertThat(meetingDto.getEndTime()).isNull();
//        assertThat(meetingDto.getBeginTime()).isNull();
//
//    }
//
//    @Test
//    public void when_try_to_delete_non_existing_meeting_then_throw_an_exception() {
//
//        try {
//            meetingCrudService.delete(meetingDto);
//            Assert.fail();
//        } catch (MeetingNotFoundException e) {
//            assertThat(e)
//                    .hasMessage("Meeting is not found in our database");
//        }
//    }
//
//    @Test
//    public void findEntity() throws MeetingAlreadyExistsException, MeetingNotFoundException {
//        MeetingDto meetingDto = createMeeting();
//        MeetingEntity foundMeeting = meetingCrudService.findEntity(meetingDto);
//        assertThat(foundMeeting).isNotNull();
//        assertThat(foundMeeting.getMeetingNumber()).isEqualTo(1);
//    }
//
//    @Test
//    public void findEntities() throws MeetingNotFoundException, GroupAlreadyExistsException, MeetingAlreadyExistsException, UserNotFoundException, MemberNotFoundException, GroupNotFoundException, MessengerAlreadyExistsException, MessengerArgumentNotSpecified {
//        GroupDto groupDto = createGroup();
//        MeetingDto createdMeetingDto = createMeeting();
//        MeetingDto createdSecondDto = createSecondMeeting();
//
//        meetingDtosList.add(createdMeetingDto);
//        meetingDtosList.add(createdSecondDto);
//        List<MeetingEntity> meetingEntityList = meetingCrudService.findEntities(groupDto);
//        assertThat(meetingEntityList).isNotNull();
//        assertThat(meetingEntityList.size()).isEqualTo(2);
//        assertThat(meetingEntityList.get(0).getMeetingNumber()).isEqualTo(1);
//        assertThat(meetingEntityList.get(1).getMeetingNumber()).isEqualTo(2);
//    }
//
//    @Test
//    public void WHEN_meeting_exists_THEN_return_true() throws MeetingAlreadyExistsException {
//
//        MeetingDto meetingDto = createMeeting();
//
//        boolean memberExists = meetingCrudService.isExist(meetingDto);
//        assertThat(memberExists).isTrue();
//    }
//
//    @Test
//    public void WHEN_member_does_not_exist_THEN_return_false() {
//        boolean memberDoesNotExist = meetingCrudService.isExist(meetingDto);
//        assertThat(memberDoesNotExist).isFalse();
//    }
}