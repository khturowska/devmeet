package pl.com.devmeet.devmeetcore.domain.user;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserCrudService;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserRepository;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyActiveException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyExistsEmailDuplicationException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@RunWith(SpringRunner.class)
public class UserCrudServiceTest {

    @Autowired
    private UserRepository userRepository;

    private UserEntity testDto = UserEntity.builder()
            .email("test@test.pl")
            .password("testPass")
            .isActive(true)
            .build();

    private UserCrudService initFacade() {
        return new UserCrudService(userRepository);
    }

    private UserEntity createTestUser() throws UserAlreadyExistsEmailDuplicationException {
        testDto = initFacade().add(testDto);
        return testDto;
    }

    @Test
    public void WHEN_create_new_not_existing_user_THEN_return_UserDto() throws UserAlreadyExistsEmailDuplicationException {
        UserEntity created = createTestUser();

        assertThat(created).isNotNull();
        assertThat(created.getId()).isNotNull();
        assertThat(created.getCreationTime()).isNotNull();
        assertThat(created.getModificationTime()).isNull();
        assertThat(created.isActive()).isFalse();
    }

    @Test
    public void WHEN_try_to_create_user_that_already_exist_THEN_return_UserAlreadyExistsException() throws UserAlreadyExistsEmailDuplicationException {
        createTestUser();

        try {
            createTestUser();
            Assert.fail();
        } catch (Exception e) {
            assertThat(e)
                    .isInstanceOf(UserAlreadyExistsEmailDuplicationException.class);
        }
    }

    @Test
    public void WHEN_find_existing_user_THEN_return_UserDto() throws UserAlreadyExistsEmailDuplicationException, UserNotFoundException {
        createTestUser();
        UserEntity found = initFacade().findByIdOrUserEmail(testDto);
        assertThat(found).isNotNull();
    }

    @Test
    public void WHEN_try_to_find_user_who_does_not_exist_THEN_return_UserNotFoundException() {
        try {
            initFacade().findByIdOrUserEmail(testDto);
            Assert.fail();
        } catch (Exception e) {
            assertThat(e)
                    .isInstanceOf(UserNotFoundException.class);
        }
    }

    @Test
    public void WHEN_user_exists_THEN_return_true() throws UserAlreadyExistsEmailDuplicationException {
        createTestUser();
        assertThat(initFacade().isExist(testDto)).isTrue();
    }

    @Test
    public void WHEN_user_not_exists_THEN_return_false() {
        assertThat(initFacade().isExist(testDto)).isFalse();
    }

    @Test
    public void WHEN_activate_user_RETURN_UserDto() throws UserAlreadyExistsEmailDuplicationException, UserNotFoundException, UserAlreadyActiveException {
        UserEntity created = createTestUser();
        UserEntity activated = initFacade().activate(testDto);

        assertThat(created).isNotNull();

        assertThat(activated.isActive()).isTrue();
        assertThat(activated.getModificationTime()).isNotNull();
    }

    @Test
    public void WHEN_try_to_activate_activated_user_THEN_return_UserAlreadyActiveException() throws UserAlreadyExistsEmailDuplicationException, UserNotFoundException, UserAlreadyActiveException {
        createTestUser();
        UserCrudService userFacade = initFacade();
        userFacade.activate(testDto);

        try {
            userFacade.activate(testDto);
            Assert.fail();
        } catch (Exception e) {
            assertThat(e)
                    .isInstanceOf(UserAlreadyActiveException.class);
        }
    }

    @Test
    public void WHEN_check_activated_user_is_active_THEN_return_true() throws UserAlreadyExistsEmailDuplicationException, UserNotFoundException, UserAlreadyActiveException {
        createTestUser();
        UserCrudService userFacade = initFacade();
        userFacade.activate(testDto);

        assertThat(userFacade.isUserActive(testDto)).isTrue();
    }

    @Test
    public void WHEN_check_not_activated_user_is_active_THEN_return_false() throws UserAlreadyExistsEmailDuplicationException, UserNotFoundException {
        createTestUser();
        assertThat(initFacade().isUserActive(testDto)).isFalse();
    }

    @Test
    public void WHEN_try_to_activate_not_existing_user_THEN_return_UserNotFoundException() {
        try {
            initFacade().activate(testDto);
            Assert.fail();
        } catch (Exception e) {
            assertThat(e)
                    .isInstanceOf(UserNotFoundException.class);
        }
    }

    private UserEntity userToUpdate() {
        return UserEntity.builder()
                .id(testDto.getId())
                .email("cos@testcos.com")
                .password("134679")
                .build();
    }

    @Test
    public void WHEN_try_to_update_activated_existing_user_THEN_return_UserDto() throws UserAlreadyExistsEmailDuplicationException, UserNotFoundException, UserAlreadyActiveException, UserFoundButNotActiveException {
        UserEntity created = createTestUser();
        UserCrudService userFacade = initFacade();
        UserEntity activated = userFacade.activate(testDto);
        UserEntity updated = userFacade.update(userToUpdate());

        assertThat(activated.getCreationTime().equals(updated.getCreationTime())).isTrue();
        assertThat(updated.getModificationTime()).isNotNull();
    }

    @Test
    public void WHEN_try_to_update_not_activated_user_THEN_return_UserFoundButNotActiveException() throws UserAlreadyExistsEmailDuplicationException, UserNotFoundException {
        UserEntity created = createTestUser();
        try {
            initFacade().update(userToUpdate());
            Assert.fail();
        } catch (Exception e) {
            assertThat(e)
                    .isInstanceOf(UserFoundButNotActiveException.class);
        }
    }

    @Test
    public void WHEN_try_to_update_not_existing_user_THEN_return_UserNotFoundException() {
        try {
            initFacade().update(userToUpdate());
            Assert.fail();
        } catch (Exception e) {
            assertThat(e)
                    .isInstanceOf(UserNotFoundException.class);
        }
    }

    @Test
    public void WHEN_delete_existing_and_activated_user_THEN_return_UserDto() throws UserAlreadyExistsEmailDuplicationException, UserNotFoundException, UserAlreadyActiveException, UserFoundButNotActiveException {
        createTestUser();
        UserCrudService userFacade = initFacade();
        userFacade.activate(testDto);
        UserEntity deleted = userFacade.deactivate(testDto);

        assertThat(deleted.isActive()).isFalse();
        assertThat(deleted.getModificationTime()).isNotNull();
    }

    @Test
    public void WHEN_try_to_delete_existing_but_not_activated_user_THEN_return_UserFoundButNotActive() throws UserAlreadyExistsEmailDuplicationException, UserNotFoundException {
        createTestUser();
        try {
            initFacade().deactivate(testDto);
            Assert.fail();
        } catch (UserFoundButNotActiveException e) {
            assertThat(e)
                    .isInstanceOf(UserFoundButNotActiveException.class);
        }
    }

    @Test
    public void WHEN_try_to_delete_not_existing_user_THEN_return_UserNotFoundException() {
        try {
            initFacade().deactivate(testDto);
        } catch (Exception e) {
            assertThat(e)
                    .isInstanceOf(UserNotFoundException.class);
        }

    }
}