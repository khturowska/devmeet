package pl.com.devmeet.devmeetcore.domain.member_associated.availability.models;

import org.junit.Before;
import org.junit.Test;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.assertj.core.api.Assertions.assertThat;

public class AvailabilityApiMapperTest {

    private AvailabilityApiMapper apiMapper;

    private AvailabilityDto testDto;
    private AvailabilityEntity testEntity;

    private LocalDate entityBeginDate;
    private LocalTime entityBeginTime;
    private LocalDate entityEndDate;
    private LocalTime entityEndTime;

    private String dtoBeginDate;
    private String dtoBeginTime;
    private String dtoEndDate;
    private String dtoEndTime;

    @Before
    public void setUp() throws Exception {
        apiMapper = new AvailabilityApiMapper();

        dtoBeginDate = "2020/04/05";
        dtoBeginTime = "17:30";
        dtoEndDate = "2020/04/05";
        dtoEndTime = "21:00";

        entityBeginDate = LocalDate.of(2020, 4, 5);
        entityBeginTime = LocalTime.of(17, 30);
        entityEndDate = LocalDate.of(2020, 4, 5);
        entityEndTime = LocalTime.of(21, 0);

        testDto = AvailabilityDto.builder()
                .id(101l)
                .memberId(10l)
                .remoteWork(false)
                .freeTime(true)
                .beginDate(dtoBeginDate)
                .beginTime(dtoBeginTime)
                .endDate(dtoEndDate)
                .endTime(dtoEndTime)
                .build();

        testEntity = AvailabilityEntity.builder()
                .id(101l)
                .member(MemberEntity.builder().id(10l).build())
                .remoteWork(false)
                .freeTime(true)
                .beginDate(entityBeginDate)
                .beginTime(entityBeginTime)
                .endDate(entityEndDate)
                .endTime(entityEndTime)
                .build();
    }

    @Test
    public void mapToBackend() {
        AvailabilityEntity mappedEntity = apiMapper.mapToBackend(testDto);
        assertThat(mappedEntity.getBeginDate()).isEqualTo(entityBeginDate);
        assertThat(mappedEntity.getBeginTime()).isEqualTo(entityBeginTime);
        assertThat(mappedEntity.getEndDate()).isEqualTo(entityEndDate);
        assertThat(mappedEntity.getEndTime()).isEqualTo(entityEndTime);
        assertThat(mappedEntity.getBeginTime()).isNotEqualTo(mappedEntity.getEndTime());
    }

    @Test
    public void mapToFrontend() {
        AvailabilityDto mappedDto = apiMapper.mapToFrontend(testEntity);
        assertThat(mappedDto.getBeginDate()).isEqualTo(entityBeginDate.getYear() + "-" + numberFormatter(entityBeginDate.getMonthValue()) + "-" + numberFormatter(entityBeginDate.getDayOfMonth()));
        assertThat(mappedDto.getBeginTime()).isEqualTo(numberFormatter(entityBeginTime.getHour()) + ":" + numberFormatter(entityBeginTime.getMinute()));
        assertThat(mappedDto.getEndDate()).isEqualTo(entityEndDate.getYear() + "-" + numberFormatter(entityEndDate.getMonthValue()) + "-" + numberFormatter(entityEndDate.getDayOfMonth()));
        assertThat(mappedDto.getEndTime()).isEqualTo(numberFormatter(entityEndTime.getHour()) + ":" + numberFormatter(entityEndTime.getMinute()));
        assertThat(mappedDto.getBeginTime()).isNotEqualTo(mappedDto.getEndTime());
    }

    private String numberFormatter(Integer number) {
        if (number < 10)
            return "0" + number.toString();
        else
            return number.toString();
    }
}