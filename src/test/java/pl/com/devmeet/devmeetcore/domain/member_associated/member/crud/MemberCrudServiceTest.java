package pl.com.devmeet.devmeetcore.domain.member_associated.member.crud;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudRepository;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.*;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerRepository;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerArgumentNotSpecified;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserCrudService;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserRepository;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyActiveException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyExistsEmailDuplicationException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@RunWith(SpringRunner.class)
public class MemberCrudServiceTest {

    @Autowired
    private MemberRepository memberRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MessengerRepository messengerRepository;
    @Autowired
    private GroupCrudRepository groupCrudRepository;

    private UserEntity testUserEntity;
    private MemberEntity testMemberEntity;

    private MemberCrudService memberCrudService;

    private UserCrudService userCrudService;

    @Before
    public void setUp() {
        testUserEntity = UserEntity.builder()
                .email("test@test.pl")
                .password("testPass")
                .isActive(true)
                .build();

        testMemberEntity = MemberEntity.builder()
                .user(testUserEntity)
                .nick("Wasacz")
                .creationTime(null)
                .modificationTime(null)
                .isActive(false)
                .build();

        memberCrudService = initMemberCrudFacade();
    }

    private UserCrudService initUserCrudFacade() {
        return new UserCrudService(userRepository);
    }

    private MemberEntity createMember() throws UserNotFoundException, MemberAlreadyExistsException, GroupNotFoundException, MemberNotFoundException, MessengerAlreadyExistsException, MessengerArgumentNotSpecified, MemberUserNotActiveException {
        return initMemberCrudFacade().add(testMemberEntity);
    }

    private MemberCrudService initMemberCrudFacade() {
        return new MemberCrudService(memberRepository, userRepository, messengerRepository, groupCrudRepository);
    }

    private UserEntity initTestDatabaseByAddingUser() {
        UserCrudService userCrudService = initUserCrudFacade();

        try {
            return userCrudService
                    .findByIdOrUserEmail(
                            userCrudService.activate(
                                    userCrudService.add(testUserEntity)
                            )
                    );
        } catch (UserNotFoundException | UserAlreadyExistsEmailDuplicationException | UserAlreadyActiveException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Test
    public void INIT_TEST_DB() {
        UserEntity createdUser = initTestDatabaseByAddingUser();
        assertThat(createdUser).isNotNull();
    }

    @Test
    public void WHEN_try_to_create_member_for_not_activated_user_THEN_return_MemberUserNotActiveException() throws UserAlreadyExistsEmailDuplicationException, UserNotFoundException {
        UserCrudService userCrudService = initUserCrudFacade();
        userCrudService.add(testUserEntity);
        try {
            initMemberCrudFacade().add(testMemberEntity);
            Assert.fail();
        } catch (MemberAlreadyExistsException e) {
            Assert.fail();
        } catch (MemberUserNotActiveException e) {
            assertThat(e)
                    .isInstanceOf(MemberUserNotActiveException.class);
        }
    }

    @Test
    public void WHEN_creating_non_existing_member_for_activated_user_THEN_create_new_member() throws UserNotFoundException, MemberAlreadyExistsException, MemberNotFoundException, GroupNotFoundException, MessengerArgumentNotSpecified, MessengerAlreadyExistsException, MemberUserNotActiveException {
        UserEntity createdUser = initTestDatabaseByAddingUser();
        MemberEntity member = createMember();
        MemberEntity memberEntity = initMemberCrudFacade().find(testMemberEntity);

        assertThat(member).isNotNull();
        assertThat(member.getNick()).isEqualTo(testMemberEntity.getNick());
        assertThat(member.getCreationTime()).isNotNull();
        assertThat(member.getModificationTime()).isNull();
        assertThat(member.isActive()).isTrue();

        assertThat(memberEntity.getUser()).isEqualToComparingFieldByFieldRecursively(createdUser);
    }

    @Test
    public void WHEN_try_to_create_existing_member_for_activated_user_THEN_throw_exception() {
        initTestDatabaseByAddingUser();
        MemberEntity createdMember = null;

        try {
            createdMember = createMember();
        } catch (UserNotFoundException | MemberAlreadyExistsException | GroupNotFoundException | MemberNotFoundException | MessengerAlreadyExistsException | MessengerArgumentNotSpecified | MemberUserNotActiveException e) {
            Assert.fail();
        }

        try {
            memberCrudService.add(createdMember);
            Assert.fail();
        } catch (MemberAlreadyExistsException e) {
            assertThat(e)
                    .isInstanceOf(MemberAlreadyExistsException.class)
                    .hasMessage(MemberCrudStatusEnum.MEMBER_ALREADY_EXIST.toString());
        } catch (MemberUserNotActiveException | UserNotFoundException e) {
            Assert.fail();
        }
    }

    @Test
    public void WHEN_find_existing_member_THEN_return_memberDto() throws UserNotFoundException, MemberAlreadyExistsException, MemberNotFoundException, GroupNotFoundException, MessengerArgumentNotSpecified, MessengerAlreadyExistsException, MemberUserNotActiveException {
        initTestDatabaseByAddingUser();
        UserEntity foundUser = initUserCrudFacade().findByIdOrUserEmail(testUserEntity);
        createMember();

        MemberEntity foundMemberEntity = memberCrudService.find(testMemberEntity);

        assertThat(foundMemberEntity).isNotNull();
        assertThat(foundMemberEntity.getNick()).isEqualTo(testMemberEntity.getNick());

        assertThat(foundMemberEntity.getUser()).isEqualToComparingFieldByFieldRecursively(foundUser);
    }

    @Test
    public void WHEN_try_to_find_member_who_does_not_exist_THEN_return_MemberNotFoundException() throws UserNotFoundException {
        initTestDatabaseByAddingUser();

        try {
            memberCrudService.find(testMemberEntity);
            Assert.fail();
        } catch (MemberNotFoundException e) {
            assertThat(e)
                    .isInstanceOf(MemberNotFoundException.class)
                    .hasMessage(MemberCrudStatusEnum.MEMBER_NOT_FOUND.toString());
        }
    }

    @Test
    public void WHEN_member_exists_THEN_return_true() throws UserNotFoundException, MemberAlreadyExistsException, MemberNotFoundException, GroupNotFoundException, MessengerAlreadyExistsException, MessengerArgumentNotSpecified, MemberUserNotActiveException {
        initTestDatabaseByAddingUser();
        createMember();

        boolean memberExists = memberCrudService.isExist(testMemberEntity);
        assertThat(memberExists).isTrue();
    }

    @Test
    public void WHEN_member_does_not_exist_THEN_return_false() {
        boolean memberDoesNotExist = memberCrudService.isExist(testMemberEntity);
        assertThat(memberDoesNotExist).isFalse();
    }

    @Test
    public void WHEN_try_to_update_existing_member_THEN_updated_member() throws UserNotFoundException, MemberAlreadyExistsException, MemberNotFoundException, MemberFoundButNotActiveException, GroupNotFoundException, MessengerArgumentNotSpecified, MessengerAlreadyExistsException, MemberUserNotActiveException {
        initTestDatabaseByAddingUser();
        UserEntity foundUser = initUserCrudFacade().findByIdOrUserEmail(testUserEntity);
        MemberEntity createdMember = createMember();
        MemberEntity updateForMember = updateTestMemberDto(createdMember);

        MemberEntity updatedMember = memberCrudService.update(updateForMember);

        assertThat(updatedMember.getUser()).isEqualToComparingFieldByFieldRecursively(foundUser);

        assertThat(updatedMember.isActive()).isTrue();
        assertThat(updatedMember.getNick()).isEqualTo(updateForMember.getNick());
        assertThat(updatedMember.getCreationTime()).isNotNull();
        assertThat(updatedMember.getModificationTime()).isNotNull();
    }

    private MemberEntity updateTestMemberDto(MemberEntity testMemberEntity) {
        return doSomeIllegalOperations(MemberEntity.builder()
                .id(testMemberEntity.getId())
                .nick("changedNick")
                .build());
    }

    private MemberEntity doSomeIllegalOperations(MemberEntity testMemberEntity) {
        testMemberEntity.setActive(false);
        testMemberEntity.setCreationTime(null);
        testMemberEntity.setModificationTime(LocalDateTime.now());
        return testMemberEntity;
    }

    @Test
    public void WHEN_try_to_update_existing_but_not_active_member_THEN_return_exception() throws MemberNotFoundException, UserNotFoundException, MemberAlreadyExistsException, MemberFoundButNotActiveException, GroupNotFoundException, MessengerArgumentNotSpecified, MessengerAlreadyExistsException, MessengerNotFoundException, MemberUserNotActiveException {
        initTestDatabaseByAddingUser();
        MemberEntity created = memberCrudService.add(testMemberEntity);
        memberCrudService.deactivate(testMemberEntity);

        try {
            memberCrudService.update(updateTestMemberDto(created));
            Assert.fail();
        } catch (MemberFoundButNotActiveException e) {
            assertThat(e)
                    .isInstanceOf(MemberFoundButNotActiveException.class)
                    .hasMessage(MemberCrudStatusEnum.MEMBER_FOUND_BUT_NOT_ACTIVE.toString());
        }
    }

    @Test
    public void WHEN_try_to_update_not_existing_member_THEN_return_MemberNotFoundException() throws MemberFoundButNotActiveException {
        initTestDatabaseByAddingUser();

        try {
            memberCrudService.update(updateTestMemberDto(testMemberEntity));
            Assert.fail();
        } catch (MemberNotFoundException e) {
            assertThat(e)
                    .isInstanceOf(MemberNotFoundException.class)
                    .hasMessage(MemberCrudStatusEnum.ID_NOT_SPECIFIED.toString());
        }
    }

    @Test
    public void WHEN_try_to_delete_existing_member_THEN_delete_member() throws MemberFoundButNotActiveException, MemberNotFoundException, UserNotFoundException, MemberAlreadyExistsException, GroupNotFoundException, MessengerArgumentNotSpecified, MessengerAlreadyExistsException, MessengerNotFoundException, MemberUserNotActiveException {
        initTestDatabaseByAddingUser();
        createMember();

        MemberEntity isMemberDeleted = memberCrudService.deactivate(testMemberEntity);
        assertThat(isMemberDeleted.isActive()).isFalse();
    }

    @Test
    public void WHEN_try_to_delete_non_existing_member_THEN_throw_EntityNotFoundException() throws UserNotFoundException, MemberFoundButNotActiveException, MessengerAlreadyExistsException, MessengerNotFoundException, GroupNotFoundException {
        initTestDatabaseByAddingUser();

        try {
            memberCrudService.deactivate(testMemberEntity);
            Assert.fail();
        } catch (MemberNotFoundException e) {
            assertThat(e)
                    .isInstanceOf(MemberNotFoundException.class)
                    .hasMessage(MemberCrudStatusEnum.MEMBER_NOT_FOUND.toString());
        }
    }
}