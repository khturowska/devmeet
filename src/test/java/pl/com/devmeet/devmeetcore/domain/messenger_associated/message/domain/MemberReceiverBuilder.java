package pl.com.devmeet.devmeetcore.domain.messenger_associated.message.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberRepository;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerEntity;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerRepository;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserRepository;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;

import java.time.LocalDateTime;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
class MemberReceiverBuilder {

    private UserRepository userRepository;
    private MemberRepository memberRepository;
    private MessengerRepository messengerRepository;

    private UserEntity userEntity;
    private MemberEntity memberEntity;
    private MessengerEntity messengerEntity;

    public void build() {
        initUser();
        initMember();
        initMessenger();
    }

    private void initUser() {
        this.userEntity = UserEntity.builder()
                .email("test1@test1.pl")
                .password("testPass1")
                .isActive(true)
                .build();
    }

    private void initMember() {
        this.memberEntity = MemberEntity.builder()
                .user(userEntity)
                .nick("testMember2")
                .isActive(true)
                .modificationTime(LocalDateTime.now())
                .creationTime(LocalDateTime.now())
                .build();
    }

    private void initMessenger() {
        this.messengerEntity = MessengerEntity.builder()
                .member(memberEntity)
                .build();
    }
}
