package pl.com.devmeet.devmeetcore.domain.group_associated.meeting.crud;

import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudRepository;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberRepository;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerRepository;
import pl.com.devmeet.devmeetcore.domain.place.crud.PlaceCrudRepository;
import pl.com.devmeet.devmeetcore.domain.place.crud.PlaceCrudService;
import pl.com.devmeet.devmeetcore.domain.place.models.PlaceEntity;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserRepository;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

@DataJpaTest
@RunWith(SpringRunner.class)
public class MeetingCrudServiceTest2 {

    private static GroupEntity firstGroup;
    private static GroupEntity secondGroup;
    private static PlaceEntity firstPlace;
    private static PlaceEntity secondPlace;
    private static MeetingEntity firstMeeting;
    private static MeetingEntity secondMeeting;
    private static MeetingEntity thirdMeeting;
    @Autowired
    private MeetingCrudRepository meetingCrudRepository;
    @Autowired
    private GroupCrudRepository groupCrudRepository;
    @Autowired
    private PlaceCrudRepository placeCrudRepository;
    @Autowired
    private MemberRepository memberRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MessengerRepository messengerRepository;
    private MeetingCrudService meetingCrudService;
    private GroupCrudService groupCrudService;
    private PlaceCrudService placeCrudService;

    @SneakyThrows
    @Before
    public void setUp() {

        meetingCrudService = new MeetingCrudService(meetingCrudRepository, placeCrudRepository, groupCrudRepository, userRepository, memberRepository, messengerRepository);
        groupCrudService = new GroupCrudService(groupCrudRepository, memberRepository, userRepository, messengerRepository);
        placeCrudService = new PlaceCrudService(placeCrudRepository, memberRepository, userRepository, messengerRepository, groupCrudRepository);

        firstGroup = GroupEntity.builder()
                .groupName("Dancing group")
                .description("We dance only salsa")
                .isActive(true)
                .build();

        secondGroup = GroupEntity.builder()
                .groupName("Chińskie garnki")
                .description("We dance only salsa")
                .isActive(true)
                .build();

        firstPlace = PlaceEntity.builder()
                .placeName("Bulwary nad Wisłą")
                .description("Miejsce pod chmurką")
                .website("https://warsawtour.pl/project/bulwary-nad-wisla/")
                .location("Wybrzeże Kościuszkowskie, 00-001 Warszawa")
                .build();

        secondPlace = PlaceEntity.builder()
                .placeName("Indeks")
                .description("Piwo piwko piweńko")
                .website("https://indekspindex.eu")
                .location("Krakowskie Przedmieście 200, 00-328 Warszawa")
                .build();


        firstMeeting = MeetingEntity.builder()
                .meetingName("spotkanie pierwsze")
                .begin(LocalDateTime.now())
                .end(LocalDateTime.now().plusHours(2))
//                .beginDate(LocalDate.now())
//                .beginTime(LocalTime.now())
//                .endDate(LocalDate.now())
//                .endTime(LocalTime.now().plusHours(2))
                .group(firstGroup)
                .isActive(false)
                .place(firstPlace)
                .build();

        secondMeeting = MeetingEntity.builder()
                .meetingName("spotkanie drugie")
                .begin(LocalDateTime.now())
                .begin(LocalDateTime.now().plusHours(2))
//                .beginDate(LocalDate.now())
//                .beginTime(LocalTime.now())
//                .endDate(LocalDate.now())
//                .endTime(LocalTime.now().plusHours(2))
                .group(firstGroup)
                .isActive(false)
                .place(secondPlace)
                .build();

        thirdMeeting = MeetingEntity.builder()
                .meetingName("Sprzedaż garnków naiwnym ludziom")
                .begin(LocalDateTime.now())
                .begin(LocalDateTime.now().plusHours(2))
//                .beginDate(LocalDate.now())
//                .beginTime(LocalTime.now().plusHours(1))
//                .endDate(LocalDate.now())
//                .endTime(LocalTime.now().plusHours(2))
                .group(secondGroup)
                .isActive(false)
                .place(firstPlace)
                .build();

        initTestDB();
    }

    private boolean initTestDB() throws UserNotFoundException, MeetingAlreadyExistsException, MemberNotFoundException, PlaceNotFoundException, GroupNotFoundException {
        boolean result = false;
        try {
            result = createGroups() && createPlaces();

        } catch (GroupAlreadyExistsException | MemberNotFoundException | UserNotFoundException | PlaceAlreadyExistsException e) {
            System.out.println(e.getMessage());
        }

        return result && createTestMeetings();
    }

    private boolean createGroups() throws GroupAlreadyExistsException {
        firstGroup = groupCrudService.add(firstGroup);
        secondGroup = groupCrudService.add(secondGroup);
        return firstGroup.getId() != null
                && secondGroup.getId() != null;
    }

    private boolean createPlaces() throws MemberNotFoundException, UserNotFoundException, PlaceAlreadyExistsException {
        firstPlace = placeCrudService.add(firstPlace);
        secondPlace = placeCrudService.add(secondPlace);
        return firstPlace.getId() != null
                && secondPlace.getId() != null;
    }

    private boolean createTestMeetings() throws UserNotFoundException, MeetingAlreadyExistsException, GroupNotFoundException, PlaceNotFoundException, MemberNotFoundException {
        firstMeeting = meetingCrudService.add(firstMeeting);
        secondMeeting = meetingCrudService.add(secondMeeting);
        thirdMeeting = meetingCrudService.add(thirdMeeting);
        return firstMeeting.getId() != null
                && secondMeeting.getId() != null
                && thirdMeeting.getId() != null;
    }

    @SneakyThrows
    @Test
    public void create_meeting_and_find_meeting() {
//        initTestDB();
        MeetingEntity created = firstMeeting;
        MeetingEntity found = meetingCrudService.findByIdOrFeatures(firstMeeting);

        assertThat(created).isNotNull();
        assertThat(found.getGroup().getGroupName()).isEqualTo(created.getGroup().getGroupName());
        assertThat(found.getPlace().getPlaceName()).isEqualTo(created.getPlace().getPlaceName());

        assertThat(found.getMeetingName()).isEqualTo(created.getMeetingName());
        assertThat(found.getBegin()).isEqualTo(created.getBegin());
        assertThat(found.getEnd()).isEqualTo(created.getEnd());
        assertThat(found.getModificationTime()).isNull();
        assertThat(found.getCreationTime()).isNotNull();
        assertThat(found.isActive()).isTrue();
    }

//    @Test
//    public void find_meeting_by_name() throws MeetingAlreadyExistsException, UserNotFoundException, GroupNotFoundException, PlaceNotFoundException, MemberNotFoundException {
//        meetingCrudService.add(firstMeeting);
//        MeetingDto actualMeetingDto = meetingCrudService.findMeetingByName(firstMeeting.getMeetingName());
//
//        assertSame(firstMeeting.getMeetingName(), actualMeetingDto.getMeetingName());
//    }

    @Test
    public void when_create_existing_meeting_then_throw_an_exception() {
        try {
            meetingCrudService.add(firstMeeting);
            Assert.fail();
        } catch (Exception e) {
            assertThat(e)
                    .isInstanceOf(MeetingAlreadyExistsException.class);
        }
    }

    @SneakyThrows
    @Test
    public void read_list_of_all_existing_meetings() {
        List<MeetingEntity> expectedList = new ArrayList<>();
        expectedList.add(firstMeeting);
        expectedList.add(secondMeeting);
        expectedList.add(thirdMeeting);
        List<MeetingEntity> acutalMeetingEntityList = meetingCrudService.findAll();

        assertEquals(expectedList.toArray().length, acutalMeetingEntityList.toArray().length);
    }

    @SneakyThrows
    @Test
    public void WHEN_try_to_find_Group_Meetings_THEN_return_List_of_Meetings() {
        List<MeetingEntity> foundFirstGroupMeetings = meetingCrudService.findMeetingsByGroup(firstGroup);
        List<MeetingEntity> foundSecondGroupMeetings = meetingCrudService.findMeetingsByGroup(secondGroup);

        assertThat(foundFirstGroupMeetings.size()).isEqualTo(2);
        assertThat(foundFirstGroupMeetings.get(0).getGroup().getId()).isEqualTo(firstGroup.getId());

        assertThat(foundSecondGroupMeetings.size()).isEqualTo(1);
        assertThat(foundSecondGroupMeetings.get(0).getGroup().getId()).isEqualTo(secondGroup.getId());
    }

    @SneakyThrows
    @Test
    public void update_existing_meeting() {
        MeetingEntity toUpdate = changeBasedMeeting(firstMeeting);
        MeetingEntity updated = meetingCrudService.findByIdOrFeatures(meetingCrudService.update(toUpdate));

        assertThat(updated.getPlace().getId()).isEqualTo(firstMeeting.getPlace().getId());
        assertThat(updated.getGroup().getId()).isEqualTo(firstMeeting.getGroup().getId());
        assertThat(updated.getCreationTime()).isEqualTo(firstMeeting.getCreationTime());
        assertThat(updated.isActive()).isTrue();
        assertThat(updated.getBegin()).isNotNull();
        assertThat(updated.getEnd()).isNotNull();
        assertThat(updated.getMeetingName()).isNotNull().isEqualTo(toUpdate.getMeetingName());
    }

    private MeetingEntity changeBasedMeeting(MeetingEntity meetingEntity) {
        return MeetingEntity.builder()
                .id(meetingEntity.getId())
                .group(null)
                .place(null)
                .isActive(false)
                .creationTime(null)
                .modificationTime(null)
                .meetingName("Grupa baletowa")
                .begin(meetingEntity.getBegin().plusDays(2))
                .end(meetingEntity.getBegin().plusDays(2))
                .build();
    }

    @SneakyThrows
    @Test
    public void WHEN_deactivate_existing_Meeting_THEN_return_Meeting() {
        MeetingEntity deleted = meetingCrudService.deactivate(firstMeeting);

        assertThat(deleted.getId()).isNotNull().isEqualTo(firstMeeting.getId());
        assertThat(deleted.isActive()).isFalse();
        assertThat(deleted.getModificationTime()).isNotNull();
    }

    @SneakyThrows
    @Test
    public void WHEN_try_to_deactivate_not_active_Meeting_THEN_return_MeetingFoundButNotActiveException() {
        meetingCrudService.deactivate(firstMeeting);

        try {
            meetingCrudService.deactivate(firstMeeting);
        } catch (MeetingFoundButNotActiveException ex) {
            assertThat(ex)
                    .isInstanceOf(MeetingFoundButNotActiveException.class);
        }
    }

//
//    @Test
//    public void when_try_to_delete_existing_meeting_then_delete_meeting() throws MeetingAlreadyExistsException, MeetingNotFoundException {
//
//        MeetingDto deletedMeeting = createMeeting();
//        MeetingDto meetingDto = meetingCrudService.delete(deletedMeeting);
//        assertThat(meetingDto.isActive()).isFalse();
//        assertThat(meetingDto.getEndTime()).isNull();
//        assertThat(meetingDto.getBeginTime()).isNull();
//
//    }
//
//    @Test
//    public void when_try_to_delete_non_existing_meeting_then_throw_an_exception() {
//
//        try {
//            meetingCrudService.delete(meetingDto);
//            Assert.fail();
//        } catch (MeetingNotFoundException e) {
//            assertThat(e)
//                    .hasMessage("Meeting is not found in our database");
//        }
//    }
//
//    @Test
//    public void findEntity() throws MeetingAlreadyExistsException, MeetingNotFoundException {
//        MeetingDto meetingDto = createMeeting();
//        MeetingEntity foundMeeting = meetingCrudService.findEntity(meetingDto);
//        assertThat(foundMeeting).isNotNull();
//        assertThat(foundMeeting.getMeetingNumber()).isEqualTo(1);
//    }
//
//    @Test
//    public void findEntities() throws MeetingNotFoundException, GroupAlreadyExistsException, MeetingAlreadyExistsException, UserNotFoundException, MemberNotFoundException, GroupNotFoundException, MessengerAlreadyExistsException, MessengerArgumentNotSpecified {
//        GroupDto groupDto = createGroup();
//        MeetingDto createdMeetingDto = createMeeting();
//        MeetingDto createdSecondDto = createSecondMeeting();
//
//        meetingDtosList.add(createdMeetingDto);
//        meetingDtosList.add(createdSecondDto);
//        List<MeetingEntity> meetingEntityList = meetingCrudService.findEntities(groupDto);
//        assertThat(meetingEntityList).isNotNull();
//        assertThat(meetingEntityList.size()).isEqualTo(2);
//        assertThat(meetingEntityList.get(0).getMeetingNumber()).isEqualTo(1);
//        assertThat(meetingEntityList.get(1).getMeetingNumber()).isEqualTo(2);
//    }
//
//    @Test
//    public void WHEN_meeting_exists_THEN_return_true() throws MeetingAlreadyExistsException {
//
//        MeetingDto meetingDto = createMeeting();
//
//        boolean memberExists = meetingCrudService.isExist(meetingDto);
//        assertThat(memberExists).isTrue();
//    }
//
//    @Test
//    public void WHEN_member_does_not_exist_THEN_return_false() {
//        boolean memberDoesNotExist = meetingCrudService.isExist(meetingDto);
//        assertThat(memberDoesNotExist).isFalse();
//    }
}