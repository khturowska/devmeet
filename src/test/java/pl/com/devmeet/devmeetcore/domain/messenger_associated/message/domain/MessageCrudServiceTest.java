package pl.com.devmeet.devmeetcore.domain.messenger_associated.message.domain;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudRepository;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberRepository;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberUserNotActiveException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.message.status_and_exceptions.MessageArgumentNotSpecifiedException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.message.status_and_exceptions.MessageCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.message.status_and_exceptions.MessageFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.message.status_and_exceptions.MessageNotFoundException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerCrudService;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerEntity;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerRepository;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserCrudService;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserRepository;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyActiveException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyExistsEmailDuplicationException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Ignore
@DataJpaTest
@RunWith(SpringRunner.class)
public class MessageCrudServiceTest {

    @Autowired
    MessageRepository messageRepository;
    @Autowired
    MessengerRepository messengerRepository;
    @Autowired
    GroupCrudRepository groupCrudRepository;
    @Autowired
    MemberRepository memberRepository;
    @Autowired
    UserRepository userRepository;

    private UserCrudService userCrudService;
    private MemberCrudService memberCrudService;
    private GroupCrudService groupCrudService;
    private MessengerCrudService messengerCrudService;
    private MessageCrudService messageCrudService;

    private GroupEntity testGroupAndReceiverGroup;
    private MessengerEntity groupsReceiverMessenger;

    private UserEntity firstUser;
    private MemberEntity memberSender;
    private MessengerEntity membersSenderMessenger;

    private UserEntity secondUser;
    private MemberEntity memberReceiver;
    private MessengerEntity membersReceiverMessenger;

    private String standardTestMessageText = "Standard test message text.";
//    int numberOfTestMessages = 3; // <-- better if this parameter is smaller than 10

    @Before
    public void setUp() {

        initSenderMember();
        initReceiverMember();
        initReceiverGroup();

    }

    private void initSenderMember() {
        MemberSenderBuilder senderBuilder = MemberSenderBuilder.builder()
                .userRepository(userRepository)
                .memberRepository(memberRepository)
                .messengerRepository(messengerRepository)
                .build();

        senderBuilder.build();

        this.firstUser = senderBuilder.getUserEntity();
        this.memberSender = senderBuilder.getMemberEntity();
        this.membersSenderMessenger = senderBuilder.getMessengerEntity();
    }

    private void initReceiverMember() {
        MemberReceiverBuilder receiverBuilder = MemberReceiverBuilder.builder()
                .userRepository(userRepository)
                .memberRepository(memberRepository)
                .messengerRepository(messengerRepository)
                .build();

        receiverBuilder.build();

        this.secondUser = receiverBuilder.getUserEntity();
        this.memberReceiver = receiverBuilder.getMemberEntity();
        this.membersReceiverMessenger = receiverBuilder.getMessengerEntity();
    }

    private void initReceiverGroup() {
        TestGroupForMembersAndGroupReceiverBuilder testGroupForMembersAndGroupReceiverBuilder = TestGroupForMembersAndGroupReceiverBuilder.builder()
                .groupRepository(groupCrudRepository)
                .messengerRepository(messengerRepository)
                .build();

        testGroupForMembersAndGroupReceiverBuilder.build();

        this.testGroupAndReceiverGroup = testGroupForMembersAndGroupReceiverBuilder.getGroupEntity();
        this.groupsReceiverMessenger = testGroupForMembersAndGroupReceiverBuilder.getMessengerEntity();
    }

    private UserCrudService initUserCrudFacade() {
        return new UserCrudService(userRepository);
    }

    private MemberCrudService initMemberCrudFacade() {
        return new MemberCrudService(memberRepository, userRepository, messengerRepository, groupCrudRepository);
    }

    private GroupCrudService initGroupCrudFacade() {
        return new GroupCrudService(groupCrudRepository, memberRepository, userRepository, messengerRepository);
    }

    private MessengerCrudService initMessengerCrudFacade() {
        return new MessengerCrudService(messengerRepository, userRepository, memberRepository, groupCrudRepository);
    }

    private MessageCrudService initMessageCrudFacade() {
        return new MessageCrudService(
                messageRepository,
                messengerRepository,
                groupCrudRepository,
                memberRepository,
                userRepository);
    }

    private boolean initTestDB() {
        userCrudService = initUserCrudFacade();
        memberCrudService = initMemberCrudFacade();
        groupCrudService = initGroupCrudFacade();
        messengerCrudService = initMessengerCrudFacade();

        UserEntity userEntityFirst = null;
        UserEntity userEntitySecond = null;
        try {
            userEntityFirst = userCrudService.findByIdOrUserEmail(userCrudService.activate(userCrudService.add(firstUser)));
            userEntitySecond = userCrudService.findByIdOrUserEmail(userCrudService.activate(userCrudService.add(secondUser)));
        } catch (UserNotFoundException | UserAlreadyExistsEmailDuplicationException | UserAlreadyActiveException e) {
            e.printStackTrace();
        }


        MemberEntity memberEntityFirst = null;
        try {
            memberEntityFirst = memberCrudService.find(memberCrudService.add(memberSender));
        } catch (MemberNotFoundException | MemberAlreadyExistsException | UserNotFoundException | MemberUserNotActiveException e) {
            e.printStackTrace();
        }

        MemberEntity memberEntitySecond = null;
        try {
            memberEntitySecond = memberCrudService.find(memberCrudService.add(memberReceiver));
        } catch (MemberNotFoundException | MemberAlreadyExistsException | UserNotFoundException | MemberUserNotActiveException e) {
            e.printStackTrace();
        }

        GroupEntity groupEntity = null;
        try {
            groupEntity = groupCrudService.findByGroup(groupCrudService.add(testGroupAndReceiverGroup));
        } catch (GroupNotFoundException | GroupAlreadyExistsException e) {
            e.printStackTrace();
        }

        return userEntityFirst != null
                && userEntitySecond != null
                && memberEntityFirst != null
                && memberEntitySecond != null
                && groupEntity != null;
    }

    private List<MessageEntity> saveMessagesInToDb(MessengerEntity sender, MessengerEntity receiver, String message, int numberOfTestMessages) throws UserNotFoundException, MessengerNotFoundException, MemberNotFoundException, GroupNotFoundException, MessageArgumentNotSpecifiedException {
        List<MessageEntity> result = new ArrayList<>();
        MessageCrudService messageCrudService = initMessageCrudFacade();

        List<MessageEntity> messagesToSave = new TestMessagesGenerator(message)
                .generateConversation(sender, receiver, numberOfTestMessages);

        for (MessageEntity messageEntity : messagesToSave) {
            result.add(messageCrudService.add(messageEntity));
        }

        return result;
    }

    @Test
    public void INIT_TEST_DB() {
        boolean initDB = initTestDB();
        assertThat(initDB).isTrue();
    }

    @Test
    public void WHEN_create_new_messages_from_MEMBER_to_MEMBER_THEN_return_messages() throws UserNotFoundException, MemberNotFoundException, MessengerNotFoundException, GroupNotFoundException, MessageArgumentNotSpecifiedException {
        initTestDB();
        int numberOfMessagesLocal = 4;
        List<MessageEntity> createdMessages = saveMessagesInToDb(membersSenderMessenger, membersReceiverMessenger, standardTestMessageText, numberOfMessagesLocal);

        assertThat(createdMessages.size()).isEqualTo(numberOfMessagesLocal);
        createdMessages
                .forEach(messageDto ->
                        assertThat(messageDto.getSender().getMember()).isNotNull());
        createdMessages
                .forEach(messageDto ->
                        assertThat(messageDto.getReceiver().getMember()).isNotNull());
        createdMessages
                .forEach(messageDto ->
                        assertThat(messageDto.getMessage()).isNotNull());
        createdMessages
                .forEach(messageDto ->
                        assertThat(messageDto.getCreationTime()).isNotNull());
        createdMessages
                .forEach(messageDto ->
                        assertThat(messageDto.isActive()).isTrue());
    }

    @Test
    public void WHEN_create_new_messages_from_MEMBER_to_GROUP_THEN_return_messages() throws UserNotFoundException, MemberNotFoundException, MessengerNotFoundException, GroupNotFoundException, MessageArgumentNotSpecifiedException {
        initTestDB();
        int numberOfMessagesLocal = 4;
        List<MessageEntity> createdMessages = saveMessagesInToDb(membersSenderMessenger, groupsReceiverMessenger, standardTestMessageText, numberOfMessagesLocal);

        assertThat(createdMessages.size()).isEqualTo(numberOfMessagesLocal);
        createdMessages
                .forEach(messageDto ->
                        assertThat(messageDto.getSender().getMember()).isNotNull());
        createdMessages
                .forEach(messageDto ->
                        assertThat(messageDto.getReceiver().getGroup()).isNotNull());
        createdMessages
                .forEach(messageDto ->
                        assertThat(messageDto.getMessage()).isNotNull());
        createdMessages
                .forEach(messageDto ->
                        assertThat(messageDto.getCreationTime()).isNotNull());
        createdMessages
                .forEach(messageDto ->
                        assertThat(messageDto.isActive()).isTrue());
    }

    @Test
    public void WHEN_try_to_send_empty_message_THEN_throw_MessageArgumentNotSpecifiedException() {
        initTestDB();
        String emptyMessage = "";

        try {
            saveMessagesInToDb(membersSenderMessenger, membersReceiverMessenger, emptyMessage, 1);
            Assert.fail();

        } catch (GroupNotFoundException | UserNotFoundException | MessengerNotFoundException | MemberNotFoundException e) {
            Assert.fail();

        } catch (MessageArgumentNotSpecifiedException e) {
            assertThat(e)
                    .hasMessage(MessageCrudStatusEnum.MESSAGE_IS_EMPTY.toString());
        }
    }

    @Test
    public void WHEN_found_messages_MEMBER_to_MEMBER_THEN_return_messages() throws UserNotFoundException, MemberNotFoundException, MessengerNotFoundException, GroupNotFoundException, MessageNotFoundException, MessageArgumentNotSpecifiedException {
        initTestDB();
        int numberOfMessagesLocal = 4;
        saveMessagesInToDb(membersSenderMessenger, membersReceiverMessenger, standardTestMessageText, numberOfMessagesLocal);

        MessageEntity singleMessage = MessageEntity.builder()
                .sender(membersSenderMessenger)
                .build();

        String senderMemberNick = membersSenderMessenger.getMember().getNick();
        List<MessageEntity> foundMessages = initMessageCrudFacade().findAll(singleMessage);


        assertThat(foundMessages.size()).isEqualTo((numberOfMessagesLocal / 2));
        foundMessages
                .forEach(messageDto ->
                        assertThat(messageDto.getSender().getMember().getNick().equals(senderMemberNick)).isTrue());
        foundMessages
                .forEach(messageDto ->
                        assertThat(messageDto.getReceiver().getMember().getNick().equals(senderMemberNick)).isFalse());
    }

    @Test
    public void WHEN_found_messages_in_GROUP_THEN_return_messages() throws UserNotFoundException, MemberNotFoundException, MessengerNotFoundException, GroupNotFoundException, MessageNotFoundException, MessageArgumentNotSpecifiedException {
        initTestDB();
        int numberOfMessagesLocal = 4;
        saveMessagesInToDb(membersSenderMessenger, groupsReceiverMessenger, standardTestMessageText, numberOfMessagesLocal);

        MessageEntity singleMessage = MessageEntity.builder()
                .receiver(groupsReceiverMessenger)
                .build();

        String groupName = singleMessage.getReceiver().getGroup().getGroupName();
        List<MessageEntity> foundMessages = initMessageCrudFacade().readAllGroupMessages(singleMessage);

        assertThat(foundMessages.size()).isEqualTo(numberOfMessagesLocal);
        foundMessages
                .forEach(messageDto ->
                        assertThat(messageDto.getReceiver().getGroup().getGroupName()).isEqualTo(groupName));
    }

    @Test
    public void WHEN_sender_is_not_specified_THEN_return_MessageArgumentNotSpecified() throws UserNotFoundException, MemberNotFoundException, MessageArgumentNotSpecifiedException, MessengerNotFoundException, GroupNotFoundException {
        initTestDB();
        MessageEntity singleMessage = new MessageEntity();
        saveMessagesInToDb(membersSenderMessenger, membersReceiverMessenger, standardTestMessageText, 1);

        try {
            initMessageCrudFacade().findAll(singleMessage);
            Assert.fail();

        } catch (UserNotFoundException | MessengerNotFoundException | MemberNotFoundException | GroupNotFoundException | MessageNotFoundException e) {
            Assert.fail();

        } catch (MessageArgumentNotSpecifiedException e) {
            assertThat(e)
                    .hasMessage(MessageCrudStatusEnum.NOT_SPECIFIED_SENDER.toString());
        }
    }

    @Test
    public void WHEN_try_to_get_all_chat_group_messages_and_receiver_is_not_specified_THEN_return_MessageArgumentNotSpecified() throws UserNotFoundException, MemberNotFoundException, MessageArgumentNotSpecifiedException, MessengerNotFoundException, GroupNotFoundException {
        initTestDB();
        MessageEntity singleMessage = new MessageEntity();
        saveMessagesInToDb(membersSenderMessenger, groupsReceiverMessenger, standardTestMessageText, 1);

        try {
            initMessageCrudFacade().readAllGroupMessages(singleMessage);
            Assert.fail();

        } catch (UserNotFoundException | MessengerNotFoundException | MemberNotFoundException | GroupNotFoundException | MessageNotFoundException e) {
            Assert.fail();

        } catch (MessageArgumentNotSpecifiedException e) {
            assertThat(e)
                    .hasMessage(MessageCrudStatusEnum.NOT_SPECIFIED_RECEIVER.toString());
        }
    }

    @Test
    public void WHEN_update_message_THEN_return_updated_message() throws UserNotFoundException, MemberNotFoundException, MessengerNotFoundException, GroupNotFoundException, MessageArgumentNotSpecifiedException, MessageNotFoundException {
        initTestDB();
        int numberOfMessagesLocal = 1;
        List<MessageEntity> savedMessages = saveMessagesInToDb(membersSenderMessenger, groupsReceiverMessenger, standardTestMessageText, numberOfMessagesLocal);

        String newMessageText = "UPDATED TEXT";
        MessageEntity oldVersionMessage = savedMessages.get(0);
        MessageEntity newVersionMessage = MessageEntity.builder()
                .message(newMessageText)
                .build();

        MessageEntity updated = initMessageCrudFacade().update(oldVersionMessage, newVersionMessage);

        assertThat(updated.getSender()).isNotNull();
        assertThat(updated.getReceiver()).isNotNull();
        assertThat(updated.getMessage()).isEqualTo(newMessageText);
    }

    @Test
    public void WHEN_try_to_update_empty_message_text_THEN_return_MessageArgumentNotSpecifiedException() throws UserNotFoundException, MemberNotFoundException, MessengerNotFoundException, GroupNotFoundException, MessageArgumentNotSpecifiedException {
        initTestDB();
        int numberOfMessagesLocal = 1;
        List<MessageEntity> savedMessages = saveMessagesInToDb(membersSenderMessenger, groupsReceiverMessenger, standardTestMessageText, numberOfMessagesLocal);

        String newMessageText = "";
        MessageEntity oldVersionMessage = savedMessages.get(0);
        MessageEntity newVersionMessage = MessageEntity.builder()
                .message(newMessageText)
                .build();

        try {
            initMessageCrudFacade().update(oldVersionMessage, newVersionMessage);
            Assert.fail();

        } catch (MessageNotFoundException e) {
            Assert.fail();

        } catch (MessageArgumentNotSpecifiedException e) {
            assertThat(e)
                    .hasMessage(MessageCrudStatusEnum.MESSAGE_IS_EMPTY.toString());
        }
    }

    @Test
    public void WHEN_delete_existing_message_THEN_return_deleted_message() throws UserNotFoundException, MemberNotFoundException, MessengerNotFoundException, GroupNotFoundException, MessageArgumentNotSpecifiedException, MessageNotFoundException, MessageFoundButNotActiveException {
        initTestDB();
        int numberOfMessagesLocal = 3;
        List<MessageEntity> savedMessages = saveMessagesInToDb(membersSenderMessenger, groupsReceiverMessenger, standardTestMessageText, numberOfMessagesLocal);

        MessageEntity messageToDelete = savedMessages.get(0);
        MessageEntity deleted = initMessageCrudFacade().delete(messageToDelete);

        assertThat(deleted.getSender()).isNotNull();
        assertThat(deleted.getReceiver()).isNotNull();
        assertThat(deleted.getCreationTime()).isNotNull();
        assertThat(deleted.isActive()).isFalse();
        assertThat(deleted.getModificationTime()).isNotNull();
    }

    @Test
    public void WHEN_try_to_delete_deleted_message_THEN_return_MessageFoundButNotActiveException() throws UserNotFoundException, MemberNotFoundException, MessengerNotFoundException, GroupNotFoundException, MessageArgumentNotSpecifiedException, MessageNotFoundException, MessageFoundButNotActiveException {
        initTestDB();
        int numberOfMessagesLocal = 3;
        List<MessageEntity> savedMessages = saveMessagesInToDb(membersSenderMessenger, groupsReceiverMessenger, standardTestMessageText, numberOfMessagesLocal);

        MessageEntity messageToDelete = savedMessages.get(0);
        initMessageCrudFacade().delete(messageToDelete);

        try {
            initMessageCrudFacade().delete(messageToDelete);
            Assert.fail();

        } catch (MessageFoundButNotActiveException e) {
            assertThat(e)
                    .hasMessage(MessageCrudStatusEnum.MESSAGE_FOUND_BUT_NOT_ACTIVE.toString());
        }
    }
}