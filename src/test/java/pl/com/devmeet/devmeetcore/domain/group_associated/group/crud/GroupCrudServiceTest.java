package pl.com.devmeet.devmeetcore.domain.group_associated.group.crud;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.*;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberRepository;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerRepository;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerArgumentNotSpecified;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserRepository;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@RunWith(SpringRunner.class)
public class GroupCrudServiceTest {

    @Autowired
    private GroupCrudRepository groupRepository;
    @Autowired
    private MemberRepository memberRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MessengerRepository messengerRepository;

    private GroupEntity testGroup;

    @Before
    public void setUp() {

        testGroup = GroupEntity.builder()
                .groupName("Java test group")
                .website("www.testWebsite.com")
                .description("Welcome to test group")
                .membersLimit(5)
                .creationTime(null)
                .modificationTime(null)
                .isActive(false)
                .build();
    }

    private GroupCrudService initGroupFacade() {
        return new GroupCrudService(groupRepository, memberRepository, userRepository, messengerRepository);
    }

    private GroupEntity createGroup() throws GroupAlreadyExistsException, UserNotFoundException, GroupNotFoundException, MemberNotFoundException, MessengerAlreadyExistsException, MessengerArgumentNotSpecified {
        return initGroupFacade().add(testGroup);
    }

    @Test
    public void WHEN_create_not_exist_group_THEN_return_group() throws GroupAlreadyExistsException, UserNotFoundException, MemberNotFoundException, GroupNotFoundException, MessengerAlreadyExistsException, MessengerArgumentNotSpecified {
        GroupEntity created = createGroup();

        assertThat(created).isNotNull();
        assertThat(created.getCreationTime()).isNotNull();
        assertThat(created.isActive()).isTrue();
    }

    @Test
    public void WHEN_create_exist_group_THEN_return_IllegalArgumentException_group_already_exists() {
        try {
            createGroup();
        } catch (GroupAlreadyExistsException | UserNotFoundException | GroupNotFoundException | MemberNotFoundException | MessengerAlreadyExistsException | MessengerArgumentNotSpecified e) {
            Assert.fail();
        }

        try {
            initGroupFacade().add(testGroup);
            Assert.fail();
        } catch (GroupAlreadyExistsException e) {
            assertThat(e)
                    .isInstanceOf(GroupAlreadyExistsException.class)
                    .hasMessage(GroupCrudStatusEnum.GROUP_ALREADY_EXISTS.toString());
        }
    }

    @Test
    public void WHEN_create_exist_group_but_not_active_THEN_return_group() throws GroupAlreadyExistsException, GroupNotFoundException, GroupFoundButNotActiveException, UserNotFoundException, MessengerArgumentNotSpecified, MemberNotFoundException, MessengerAlreadyExistsException, MessengerNotFoundException {
        GroupEntity createdFirst = createGroup();
        GroupCrudService groupCrudService = initGroupFacade();
        GroupEntity deletedFirst = groupCrudService.delete(testGroup);
        GroupEntity createdSecond = groupCrudService.add(modifiedTestGroup(createdFirst));

        assertThat(createdFirst).isNotNull();
        assertThat(deletedFirst).isNotNull();

        assertThat(createdSecond).isNotNull();
        assertThat(createdSecond.getCreationTime()).isNotNull();
        assertThat(createdSecond.getModificationTime()).isNotNull();
        assertThat(createdSecond.isActive()).isTrue();
    }

    private GroupEntity modifiedTestGroup(GroupEntity dto) {
        dto.setDescription("ModifiedDesc");
        dto.setWebsite("www.modified.com.pl");
        return dto;
    }

    @Test
    public void WHEN_found_existing_group_THEN_return_group() throws GroupAlreadyExistsException, GroupNotFoundException, UserNotFoundException, MessengerArgumentNotSpecified, MemberNotFoundException, MessengerAlreadyExistsException {
        assertThat(createGroup()).isNotNull();
        assertThat(initGroupFacade().findByGroup(testGroup)).isNotNull();
    }

    @Test
    public void WHEN_group_not_found_THEN_return_EntityNotFoundException_group_not_found() {
        try {
            initGroupFacade().findByGroup(testGroup);
            Assert.fail();
        } catch (GroupNotFoundException e) {
            assertThat(e)
                    .isInstanceOf(GroupNotFoundException.class);
        }
    }

    @Test
    public void findAll() throws GroupAlreadyExistsException, UserNotFoundException, MemberNotFoundException, GroupNotFoundException, MessengerAlreadyExistsException, MessengerArgumentNotSpecified {
        createGroup();
        List<GroupEntity> foundGroups = initGroupFacade().findAll();
        int groupsSize = foundGroups.size();

        assertThat(foundGroups.size()).isEqualTo(groupsSize);
    }

    @Test
    public void WHEN_try_to_update_existing_group_THEN_return_group() throws GroupNotFoundException, GroupFoundButNotActiveException, GroupAlreadyExistsException, UserNotFoundException, MessengerArgumentNotSpecified, MemberNotFoundException, MessengerAlreadyExistsException {
        GroupEntity group = createGroup();
        GroupEntity update = modifiedTestGroup(group);

        GroupEntity modifiedGroup = initGroupFacade().update(update);

        assertThat(modifiedGroup.getGroupName()).isEqualTo(update.getGroupName());
        assertThat(modifiedGroup.getWebsite()).isEqualTo(update.getWebsite());
        assertThat(modifiedGroup.getDescription()).isEqualTo(update.getDescription());

        assertThat(modifiedGroup.getMembersLimit()).isEqualTo(group.getMembersLimit());
        assertThat(modifiedGroup.isActive()).isEqualTo(group.isActive());

        assertThat(modifiedGroup.getModificationTime()).isNotNull();
    }

    @Test
    public void WHEN_try_to_update_not_existing_group_THEN_return_EntityNotFoundException_group_not_found() throws GroupException, GroupFoundButNotActiveException {
        testGroup.setId(1l);
        GroupEntity update = modifiedTestGroup(testGroup);
        try {
            initGroupFacade().update(update);
        } catch (GroupNotFoundException e) {
            assertThat(e)
                    .isInstanceOf(GroupNotFoundException.class);
        }
    }

    @Test
    public void WHEN_delete_existing_group_THEN_return_group() throws GroupAlreadyExistsException, GroupNotFoundException, GroupFoundButNotActiveException, UserNotFoundException, MessengerArgumentNotSpecified, MemberNotFoundException, MessengerAlreadyExistsException, MessengerNotFoundException {
        GroupEntity group = createGroup();
        GroupEntity deleted = initGroupFacade().delete(group);

        assertThat(deleted).isNotNull();
        assertThat(deleted.isActive()).isFalse();
        assertThat(deleted.getModificationTime()).isNotNull();
    }

    @Test
    public void WHEN_try_to_delete_not_existing_group_THEN_return_return_EntityNotFoundException_group_not_found() throws GroupFoundButNotActiveException, UserNotFoundException, MessengerNotFoundException, MemberNotFoundException, MessengerAlreadyExistsException {
        try {
            initGroupFacade().delete(testGroup);
        } catch (GroupNotFoundException e) {
            assertThat(e)
                    .isInstanceOf(GroupNotFoundException.class);
        }
    }


    @Test
    public void findEntity() throws GroupAlreadyExistsException, GroupNotFoundException, UserNotFoundException, MessengerArgumentNotSpecified, MemberNotFoundException, MessengerAlreadyExistsException {
        GroupEntity created = createGroup();
        GroupEntity foundEntity = initGroupFacade().findByGroup(created);

        assertThat(foundEntity).isNotNull();
        assertThat(foundEntity.getGroupName()).isEqualTo(created.getGroupName());
        assertThat(foundEntity.getCreationTime()).isEqualTo(created.getCreationTime());
    }
}