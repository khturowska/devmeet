package pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudRepository;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberRepository;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberUserNotActiveException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerArgumentNotSpecified;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerInfoStatusEnum;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserCrudService;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserRepository;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyActiveException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyExistsEmailDuplicationException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@Ignore
@DataJpaTest
@RunWith(SpringRunner.class)
public class MessengerCrudServiceTest {

    @Autowired
    private MessengerRepository messengerRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MemberRepository memberRepository;
    @Autowired
    private GroupCrudRepository groupRepository;

    private MessengerEntity firstTestMemberMessenger;
    private MessengerEntity secondTestMemberMessenger;
    private MessengerEntity firstTestGroupMessenger;
    private MessengerEntity secondTestGroupMessenger;

    private UserEntity firstTestUserEntity;
    private MemberEntity firstTestMemberEntity;
    private GroupEntity firstTestGroupEntity;

    @Before
    public void setUp() {

        this.firstTestUserEntity = UserEntity.builder()
                .email("test1@test1.pl")
                .password("testPass1")
                .build();

        this.firstTestMemberEntity = MemberEntity.builder()
                .user(firstTestUserEntity)
                .nick("testMember2")
                .build();

        this.firstTestGroupEntity = GroupEntity.builder()
                .groupName("Java test group")
                .website("www.testWebsite.com")
                .description("Welcome to test group")
                .membersLimit(5)
                .build();

        this.firstTestMemberMessenger = MessengerEntity.builder()
                .member(firstTestMemberEntity)
                .build();

        this.firstTestGroupMessenger = MessengerEntity.builder()
                .group(firstTestGroupEntity)
                .build();

    }

    private MessengerCrudService initMessengerFacade() {
        return new MessengerCrudService(messengerRepository, userRepository, memberRepository, groupRepository);
    }

    private UserCrudService initUseFacade() {
        return new UserCrudService(userRepository);
    }

    private MemberCrudService initMemberFacade() {
        return new MemberCrudService(memberRepository, userRepository, messengerRepository, groupRepository);
    }

    private GroupCrudService initGroupFacade() {
        return new GroupCrudService(groupRepository, memberRepository, userRepository, messengerRepository);
    }

    private boolean initTestDB() {
        UserCrudService userCrudService = initUseFacade();
        MemberCrudService memberCrudService = initMemberFacade();
        GroupCrudService groupCrudService = initGroupFacade();


        try {
            userCrudService.activate(
                    userCrudService.add(firstTestUserEntity));
        } catch (UserAlreadyActiveException | UserAlreadyExistsEmailDuplicationException | UserNotFoundException e) {
            e.printStackTrace();
        }
        UserEntity userEntityFirst = null;
        try {
            userEntityFirst = userCrudService.findByIdOrUserEmail(firstTestUserEntity);
        } catch (UserNotFoundException e) {
            e.printStackTrace();
        }

        MemberEntity memberEntityFirst = null;
        try {
            memberEntityFirst = memberCrudService.find(memberCrudService.add(firstTestMemberEntity));
        } catch (MemberNotFoundException | MemberAlreadyExistsException | UserNotFoundException | MemberUserNotActiveException e) {
            e.printStackTrace();
        }

        GroupEntity groupEntity = null;
        try {
            groupEntity = groupCrudService.findByGroup(groupCrudService.add(firstTestGroupEntity));
        } catch (GroupNotFoundException | GroupAlreadyExistsException e) {
            e.printStackTrace();
        }

        return userEntityFirst != null
                && memberEntityFirst != null
                && groupEntity != null;
    }

    @Test
    public void INIT_TEST_DB() {
        boolean initDB = initTestDB();
        Assertions.assertThat(initDB).isTrue();
    }

    @Test
    public void WHEN_create_MEMBER_THEN_create_messenger_for_MEMBER_and_return_create_new_messenger() throws UserNotFoundException, GroupNotFoundException, MemberNotFoundException, MessengerNotFoundException {
        initTestDB();
        MessengerEntity created = initMessengerFacade().findByMember(firstTestMemberEntity);

        assertThat(created).isNotNull();

        assertThat(created.getGroup()).isNull();
        assertThat(created.getMember()).isNotNull();

        assertThat(created.getCreationTime()).isNotNull();
        assertThat(created.isActive()).isTrue();
    }

    @Test
    public void WHEN_create_GROUP_THEN_create_messenger_for_GROUP_and_return_create_new_messenger() throws UserNotFoundException, GroupNotFoundException, MemberNotFoundException, MessengerNotFoundException {
        initTestDB();
        MessengerEntity created = initMessengerFacade().findByGroup(firstTestGroupEntity);

        assertThat(created).isNotNull();

        assertThat(created.getGroup()).isNotNull();
        assertThat(created.getMember()).isNull();

        assertThat(created.getCreationTime()).isNotNull();
        assertThat(created.isActive()).isTrue();
    }

    @Test
    public void WHEN_try_to_create_existing_messenger_for_MEMBER_THEN_return_MessengerAlreadyExistsException() {
        initTestDB();
        MessengerCrudService messengerCrudService = initMessengerFacade();

        try {
            messengerCrudService.addToMember(firstTestMemberEntity);
            Assert.fail();

        } catch (MessengerArgumentNotSpecified | MemberNotFoundException | UserNotFoundException | GroupNotFoundException ex) {
            Assert.fail();

        } catch (MessengerAlreadyExistsException e) {
            assertThat(e)
                    .isInstanceOf(MessengerAlreadyExistsException.class)
                    .hasMessage(MessengerInfoStatusEnum.MESSENGER_ALREADY_EXISTS.toString());
        }
    }

    @Test
    public void WHEN_try_to_create_existing_messenger_for_GROUP_THEN_return_MessengerAlreadyExistsException() {
        initTestDB();
        MessengerCrudService messengerCrudService = initMessengerFacade();

        try {
            messengerCrudService.addToGroup(firstTestGroupEntity);
            Assert.fail();

        } catch (MessengerArgumentNotSpecified | MemberNotFoundException | UserNotFoundException | GroupNotFoundException ex) {
            Assert.fail();

        } catch (MessengerAlreadyExistsException e) {
            assertThat(e)
                    .isInstanceOf(MessengerAlreadyExistsException.class)
                    .hasMessage(MessengerInfoStatusEnum.MESSENGER_ALREADY_EXISTS.toString());
        }
    }

    @Test
    public void WHEN_found_messenger_for_MEMBER_THEN_return_messenger() throws UserNotFoundException, GroupNotFoundException, MessengerNotFoundException, MemberNotFoundException {
        initTestDB();
        MessengerEntity messengerEntity = initMessengerFacade().findByMember(firstTestMemberEntity);

        assertThat(messengerEntity).isNotNull();
    }

    @Test
    public void WHEN_found_messenger_for_GROUP_THEN_return_messenger() throws UserNotFoundException, GroupNotFoundException, MessengerNotFoundException, MemberNotFoundException {
        initTestDB();
        MessengerEntity messengerEntity = initMessengerFacade().findByGroup(firstTestGroupEntity);

        assertThat(messengerEntity).isNotNull();
    }

    @Test
    public void WHEN_try_to_find_MEMBER_messenger_which_does_not_exist_THEN_return_MessengerNotFoundException() {
        initTestDB();

        try {
            initMessengerFacade().findByMember(firstTestMemberEntity);

        } catch (GroupNotFoundException | UserNotFoundException | MemberNotFoundException e) {
            e.printStackTrace();

        } catch (
                MessengerNotFoundException e) {
            assertThat(e)
                    .isInstanceOf(MessengerNotFoundException.class)
                    .hasMessage(MessengerInfoStatusEnum.MESSENGER_NOT_FOUND_BY_MEMBER.toString());
        }
    }

    @Test
    public void WHEN_try_to_find_GROUP_messenger_which_does_not_exist_THEN_return_MessengerNotFoundException() {
        initTestDB();

        try {
            initMessengerFacade().findByGroup(firstTestGroupEntity);

        } catch (GroupNotFoundException | UserNotFoundException | MemberNotFoundException e) {
            e.printStackTrace();

        } catch (MessengerNotFoundException e) {
            assertThat(e)
                    .isInstanceOf(MessengerNotFoundException.class)
                    .hasMessage(MessengerInfoStatusEnum.MESSENGER_NOT_FOUND_BY_GROUP.toString());
        }
    }

    @Test
    public void WHEN_delete_existing_MEMBER_messenger_THEN_delete_messenger() throws UserNotFoundException, MessengerNotFoundException, GroupNotFoundException, MemberNotFoundException, MessengerAlreadyExistsException {
        initTestDB();
        MessengerCrudService messengerCrudService = initMessengerFacade();
        MessengerEntity deleted;

        deleted = messengerCrudService.deactivateMembersMessenger(firstTestMemberEntity);

        assertThat(deleted).isNotNull();
        assertThat(deleted.isActive()).isFalse();
    }

    @Test
    public void WHEN_delete_existing_GROUP_messenger_THEN_delete_messenger() throws UserNotFoundException, MessengerNotFoundException, GroupNotFoundException, MemberNotFoundException, MessengerAlreadyExistsException {
        initTestDB();
        MessengerCrudService messengerCrudService = initMessengerFacade();
        MessengerEntity deleted;

        deleted = messengerCrudService.deactivateGroupsMessenger(firstTestGroupEntity);

        assertThat(deleted).isNotNull();
        assertThat(deleted.isActive()).isFalse();
    }

    @Test
    public void WHEN_try_to_delete_non_existing_MEMBER_messenger_then_throw_EntityNotFoundException() {
        initTestDB();
        MessengerCrudService messengerCrudService = initMessengerFacade();

        try {
            messengerCrudService.delete(firstTestMemberMessenger);

        } catch (UserNotFoundException | MemberNotFoundException | GroupNotFoundException | MessengerNotFoundException | MessengerAlreadyExistsException e) {
            Assert.fail();
        }

        try {
            messengerCrudService.delete(firstTestMemberMessenger);
            Assert.fail();

        } catch (MessengerNotFoundException | UserNotFoundException | MemberNotFoundException | GroupNotFoundException e) {
            Assert.fail();

        } catch (MessengerAlreadyExistsException e) {
            assertThat(e)
                    .isInstanceOf(MessengerAlreadyExistsException.class)
                    .hasMessage(MessengerInfoStatusEnum.MESSENGER_FOUND_BUT_NOT_ACTIVE.toString());
        }
    }

    @Test
    public void WHEN_try_to_delete_non_existing_GROUP_messenger_then_throw_EntityNotFoundException() {
        initTestDB();
        MessengerCrudService messengerCrudService = initMessengerFacade();

        try {
            messengerCrudService.delete(firstTestGroupMessenger);
        } catch (UserNotFoundException | MemberNotFoundException | GroupNotFoundException | MessengerNotFoundException | MessengerAlreadyExistsException e) {
            Assert.fail();
        }

        try {
            messengerCrudService.delete(firstTestGroupMessenger);
            Assert.fail();

        } catch (MessengerNotFoundException | UserNotFoundException | MemberNotFoundException | GroupNotFoundException e) {
            Assert.fail();

        } catch (MessengerAlreadyExistsException e) {
            assertThat(e)
                    .isInstanceOf(MessengerAlreadyExistsException.class)
                    .hasMessage(MessengerInfoStatusEnum.MESSENGER_FOUND_BUT_NOT_ACTIVE.toString());
        }
    }
}