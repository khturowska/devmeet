package pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudRepository;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.crud.AvailabilityCrudRepository;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.crud.AvailabilityCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.models.AvailabilityEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions.AvailabilityAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions.AvailabilityNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberRepository;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberUserNotActiveException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerRepository;
import pl.com.devmeet.devmeetcore.domain.poll_associated.availability_vote.domain.status_and_exceptions.*;
import pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.PollCrudRepository;
import pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.PollCrudService;
import pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.PollEntity;
import pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.status_and_exceptions.PollAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.status_and_exceptions.PollNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserCrudService;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserRepository;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyActiveException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyExistsEmailDuplicationException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Ignore
@DataJpaTest
@RunWith(SpringRunner.class)
public class AvailabilityVoteCrudServiceTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private GroupCrudRepository groupRepository;
    @Autowired
    private MemberRepository memberRepository;
    @Autowired
    private AvailabilityCrudRepository availabilityRepository;
    @Autowired
    private PollCrudRepository pollCrudRepository;
    @Autowired
    private AvailabilityVoteCrudRepository availabilityVoteRepository;
    @Autowired
    private MessengerRepository messengerRepository;

    private UserCrudService userCrudService;
    private MemberCrudService memberCrudService;
    private AvailabilityCrudService availabilityCrudService;
    private GroupCrudService groupCrudService;
    private PollCrudService pollCrudService;
    private AvailabilityVoteCrudService voteCrudFacade;

    private UserEntity testUserEntityFirst;
    private UserEntity testUserEntitySecond;
    private MemberEntity testMemberEntityFirst;
    private MemberEntity testMemberEntitySecond;
    private AvailabilityEntity testAvailabilityEntityFirst;
    private AvailabilityEntity testAvailabilityEntitySecond;
    private GroupEntity testGroupEntity;
    private PollEntity testPollEntity;
    private AvailabilityVoteEntity testVoteDto;

    @Before
    public void setUp() throws Exception {

        testUserEntityFirst = UserEntity.builder()
                .email("user1@test.pl")
                .password("testPass")
                .isActive(true)
                .build();

        testUserEntitySecond = UserEntity.builder()
                .email("1resu@test.pl")
                .password("passTest")
                .isActive(true)
                .build();

        testMemberEntityFirst = MemberEntity.builder()
                .user(testUserEntityFirst)
                .nick("Wasacz")
                .build();

        testMemberEntitySecond = MemberEntity.builder()
                .user(testUserEntitySecond)
                .nick("Pieniacz")
                .build();

        testAvailabilityEntityFirst = AvailabilityEntity.builder()
                .member(testMemberEntityFirst)
//                .beginDateTime(new DateTime(2020, 3, 3, 15, 0, 0))
//                .endDateTime(new DateTime(2020, 3, 3, 16, 0, 0))
//                .availabilityVote(null)
                .remoteWork(true)
                .creationTime(null)
                .modificationTime(null)
                .isActive(true)
                .build();

        testAvailabilityEntitySecond = AvailabilityEntity.builder()
                .member(testMemberEntityFirst)
//                .beginDateTime(new DateTime(2020, 2, 2, 17, 0, 0))
//                .endDateTime(new DateTime(2020, 2, 2, 18, 0, 0))
//                .availabilityVote(null)
                .remoteWork(false)
                .creationTime(null)
                .modificationTime(null)
                .isActive(true)
                .build();

        testAvailabilityEntitySecond = AvailabilityEntity.builder()
                .member(testMemberEntitySecond)
//                .beginDateTime(new DateTime(2020, 3, 1, 18, 0, 0))
//                .endDateTime(new DateTime(2020, 3, 1, 20, 0, 0))
//                .availabilityVote(null)
                .remoteWork(true)
                .creationTime(null)
                .modificationTime(null)
                .isActive(true)
                .build();

        testGroupEntity = GroupEntity.builder()
                .groupName("Java test group")
                .website("www.testWebsite.com")
                .description("Welcome to test group")
                .membersLimit(5)
                .creationTime(null)
                .modificationTime(null)
                .isActive(false)
                .build();

        testPollEntity = PollEntity.builder()
                .group(testGroupEntity)
//                .placeVotes(null)
//                .availabilityVotes(null)
                .creationTime(null)
                .active(true)
                .build();

        testVoteDto = AvailabilityVoteEntity.builder()
                .poll(testPollEntity)
                .member(testMemberEntityFirst)
                .availability(testAvailabilityEntityFirst)
                .creationTime(null)
                .isActive(true)
                .build();
    }

    private UserCrudService initUserCrudFacade() {
        return new UserCrudService(userRepository);
    }

    private MemberCrudService initMemberCrudFacade() {
        return new MemberCrudService(memberRepository, userRepository, messengerRepository, groupRepository);
    }

    private AvailabilityCrudService initAvailabilityCrudFacade() {
        return new AvailabilityCrudService(availabilityRepository, memberRepository, userRepository, messengerRepository, groupRepository);
    }

    private GroupCrudService initGroupCrudFacade() {
        return new GroupCrudService(groupRepository, memberRepository, userRepository, messengerRepository);
    }

    private PollCrudService initPollCrudFacade() {
        return new PollCrudService(pollCrudRepository, groupRepository, memberRepository, userRepository, messengerRepository);
    }

    private AvailabilityVoteCrudService initVoteCrudFacade() {
        return new AvailabilityVoteCrudService(
                availabilityVoteRepository,
                pollCrudRepository,
                groupRepository,
                availabilityRepository,
                memberRepository,
                userRepository,
                messengerRepository);
    }

    private boolean initTestDB() {
        userCrudService = initUserCrudFacade();
        memberCrudService = initMemberCrudFacade();
        groupCrudService = initGroupCrudFacade();
        availabilityCrudService = initAvailabilityCrudFacade();
        pollCrudService = initPollCrudFacade();
        voteCrudFacade = initVoteCrudFacade();

        UserEntity userEntityFirst = null;
        try {
            userEntityFirst = userCrudService
                    .findByIdOrUserEmail(
                            userCrudService.activate(
                                    userCrudService.add(testUserEntityFirst)
                            )
                    );
            ;
        } catch (UserNotFoundException | UserAlreadyExistsEmailDuplicationException | UserAlreadyActiveException e) {
            e.printStackTrace();
        }
        UserEntity userEntitySecond = null;
        try {
            userEntitySecond = userCrudService
                    .findByIdOrUserEmail(
                            userCrudService.activate(
                                    userCrudService.add(testUserEntitySecond)
                            )
                    );
            ;
        } catch (UserNotFoundException | UserAlreadyExistsEmailDuplicationException | UserAlreadyActiveException e) {
            e.printStackTrace();
        }

        MemberEntity memberEntityFirst = null;
        try {
            memberEntityFirst = memberCrudService.find(memberCrudService.add(testMemberEntityFirst));
        } catch (MemberNotFoundException | MemberAlreadyExistsException | UserNotFoundException | MemberUserNotActiveException e) {
            e.printStackTrace();
        }

        MemberEntity memberEntitySecond = null;
        try {
            memberEntitySecond = memberCrudService.find(memberCrudService.add(testMemberEntitySecond));
        } catch (MemberNotFoundException | MemberAlreadyExistsException | UserNotFoundException | MemberUserNotActiveException e) {
            e.printStackTrace();
        }

        AvailabilityEntity availabilityEntityFirst = null;
        try {
            availabilityEntityFirst = availabilityCrudService.findByEntity(availabilityCrudService.add(testAvailabilityEntityFirst));
        } catch (MemberNotFoundException | AvailabilityAlreadyExistsException | UserNotFoundException | AvailabilityNotFoundException e) {
            e.printStackTrace();
        }

        AvailabilityEntity availabilityEntitySecond = null;
        try {
            availabilityEntitySecond = availabilityCrudService.findByEntity(availabilityCrudService.add(testAvailabilityEntitySecond));
        } catch (MemberNotFoundException | AvailabilityAlreadyExistsException | UserNotFoundException | AvailabilityNotFoundException e) {
            e.printStackTrace();
        }

        GroupEntity groupEntity = null;
        try {
            groupEntity = groupCrudService.findByGroup(groupCrudService.add(testGroupEntity));
        } catch (GroupNotFoundException | GroupAlreadyExistsException e) {
            e.printStackTrace();
        }

        PollEntity pollEntity = null;
        try {
            pollEntity = pollCrudService.find(pollCrudService.add(testPollEntity));
        } catch (GroupNotFoundException | PollAlreadyExistsException | PollNotFoundException e) {
            e.printStackTrace();
        }

        return userEntityFirst != null
                && userEntitySecond != null
                && memberEntityFirst != null
                && memberEntitySecond != null
                && availabilityEntityFirst != null
                && availabilityEntitySecond != null
                && groupEntity != null
                && pollEntity != null;
    }

    @Test
    public void INIT_TEST_DB() {
        boolean initDB = initTestDB();
        assertThat(initDB).isTrue();
    }

    @Test
    public void WHERE_create_not_existing_vote_THEN_return_vote() throws UserNotFoundException, AvailabilityNotFoundException, GroupNotFoundException, AvailabilityVoteAlreadyExistsException, MemberNotFoundException, PollNotFoundException {
        initTestDB();
        AvailabilityVoteCrudService voteCrudFacade = initVoteCrudFacade();
        AvailabilityVoteEntity voteEntity = voteCrudFacade.add(testVoteDto);

        assertThat(voteEntity).isNotNull();

        assertThat(voteEntity.getPoll()).isNotNull();
        assertThat(voteEntity.getAvailability()).isNotNull();
        assertThat(voteEntity.getMember()).isNotNull();

        assertThat(voteEntity.getCreationTime()).isNotNull();
        assertThat(voteEntity.isActive()).isTrue();
    }

    @Test
    public void WHERE_try_to_create_already_existing_vote_THEN_return_EntityAlreadyExistsException() throws AvailabilityNotFoundException, MemberNotFoundException, GroupNotFoundException, UserNotFoundException, PollNotFoundException {
        initTestDB();
        AvailabilityVoteCrudService voteCrudFacade = initVoteCrudFacade();

        try {
            voteCrudFacade.add(testVoteDto);
        } catch (AvailabilityNotFoundException | GroupNotFoundException | AvailabilityVoteAlreadyExistsException | PollNotFoundException | MemberNotFoundException | UserNotFoundException e) {
            Assert.fail();
        }

        try {
            voteCrudFacade.add(testVoteDto);
            Assert.fail();
        } catch (AvailabilityVoteAlreadyExistsException e) {
            assertThat(e)
                    .isInstanceOf(AvailabilityVoteAlreadyExistsException.class)
                    .hasMessage(AvailabilityVoteCrudStatusEnum.AVAILABILITY_VOTE_ALREADY_EXISTS.toString());
        }
    }

    @Test
    public void WHERE_found_vote_THEN_return_vote() throws UserNotFoundException, AvailabilityNotFoundException, GroupNotFoundException, AvailabilityVoteAlreadyExistsException, MemberNotFoundException, PollNotFoundException, AvailabilityVoteNotFoundException {
        initTestDB();
        AvailabilityVoteCrudService voteCrudFacade = initVoteCrudFacade();
        AvailabilityVoteEntity created = voteCrudFacade.add(testVoteDto);
        AvailabilityVoteEntity found = voteCrudFacade.find(testVoteDto);

        System.out.println("time in created:\t" + created.getCreationTime() + "\ntime in found:\t\t" + found.getCreationTime());

        assertThat(found).isNotNull();

        assertThat(found).isEqualToComparingFieldByFieldRecursively(created);
    }

    @Test
    public void WHEN_cant_find_vote_THEN_return_EntityNotFoundException() throws MemberNotFoundException, UserNotFoundException, GroupNotFoundException, PollNotFoundException {
        initTestDB();
        AvailabilityVoteCrudService voteCrudFacade = initVoteCrudFacade();

        try {
            voteCrudFacade.find(testVoteDto);
        } catch (AvailabilityVoteNotFoundException e) {
            assertThat(e)
                    .isInstanceOf(AvailabilityVoteNotFoundException.class)
                    .hasMessage(AvailabilityVoteCrudStatusEnum.AVAILABILITY_VOTE_NOT_FOUND.toString());
        }
    }

    @Test
    public void WHEN_try_to_read_all_votes_in_the_poll_THEN_return_votes() {
        initTestDB();
        AvailabilityVoteCrudService voteCrudFacade = initVoteCrudFacade();
        AvailabilityVoteEntity createdVote = null;
        List<AvailabilityVoteEntity> foundVotes = null;

        try {
            createdVote = voteCrudFacade.add(testVoteDto);
        } catch (AvailabilityNotFoundException | GroupNotFoundException | AvailabilityVoteAlreadyExistsException | PollNotFoundException | MemberNotFoundException | UserNotFoundException e) {
            Assert.fail();
        }

        try {
            foundVotes = voteCrudFacade.findAll(testVoteDto);
        } catch (GroupNotFoundException | AvailabilityVoteNotFoundException | PollNotFoundException e) {
            Assert.fail();
        }

        assertThat(foundVotes.get(0)).isEqualToComparingFieldByFieldRecursively(createdVote);
    }

    @Test
    public void WHEN_try_to_read_all_votes_in_the_poll_if_there_are_none_THEN_return_AvailabilityVoteNotFoundException() throws GroupNotFoundException, PollNotFoundException {
        initTestDB();
        AvailabilityVoteCrudService voteCrudFacade = initVoteCrudFacade();

        try {
            voteCrudFacade.findAll(testVoteDto);
        } catch (AvailabilityVoteNotFoundException e) {
            assertThat(e)
                    .isInstanceOf(AvailabilityVoteNotFoundException.class)
                    .hasMessage(AvailabilityVoteCrudStatusEnum.AVAILABILITY_VOTES_NOT_FOUND.toString());
        }
    }

    @Test
    public void WHEN_update_exist_vote_THEN_return_vote() throws AvailabilityVoteNotFoundException, GroupNotFoundException, UserNotFoundException, AvailabilityVoteException, MemberNotFoundException, AvailabilityNotFoundException, PollNotFoundException {
        initTestDB();
        AvailabilityVoteCrudService voteCrudFacade = initVoteCrudFacade();
        AvailabilityVoteEntity created = null;
        AvailabilityVoteEntity fakeUpdatedDto = updateVote(testVoteDto);
        AvailabilityVoteEntity updated;

        try {
            created = voteCrudFacade.add(testVoteDto);
        } catch (AvailabilityNotFoundException | GroupNotFoundException | AvailabilityVoteAlreadyExistsException | PollNotFoundException | MemberNotFoundException | UserNotFoundException e) {
            Assert.fail();
        }

        updated = voteCrudFacade.update(testVoteDto, fakeUpdatedDto);

        assertThat(updated.getMember()).isEqualToComparingFieldByFieldRecursively(created.getMember());
        assertThat(updated.getCreationTime()).isEqualTo(created.getCreationTime());

        assertThat(updated.getAvailability().getMember().getUser().getEmail()).isEqualTo(fakeUpdatedDto.getAvailability().getMember().getUser().getEmail());
        assertThat(updated.getAvailability().isActive()).isEqualTo(fakeUpdatedDto.getAvailability().isActive());
        assertThat(updated.getAvailability().getRemoteWork()).isEqualTo(fakeUpdatedDto.getAvailability().getRemoteWork());
    }

    private AvailabilityVoteEntity updateVote(AvailabilityVoteEntity voteDto) {
        voteDto.setAvailability(testAvailabilityEntitySecond);
        voteDto.setCreationTime(new DateTime());
        return voteDto;
    }

    @Test
    public void WHEN_delete_vote_THEN_return_vote() throws UserNotFoundException, AvailabilityNotFoundException, GroupNotFoundException, AvailabilityVoteAlreadyExistsException, MemberNotFoundException, PollNotFoundException, AvailabilityVoteFoundButNotActiveException, AvailabilityVoteNotFoundException {
        initTestDB();
        AvailabilityVoteCrudService voteCrudFacade = initVoteCrudFacade();
        AvailabilityVoteEntity created = voteCrudFacade.add(testVoteDto);
        AvailabilityVoteEntity deleted = voteCrudFacade.delete(testVoteDto);

        assertThat(deleted.isActive()).isNotEqualTo(created.isActive());
    }
}