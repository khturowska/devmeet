package pl.com.devmeet.devmeetcore.domain.member_associated.availability.crud;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudRepository;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.models.AvailabilityEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.availability.status_and_exceptions.*;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberRepository;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberUserNotActiveException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerRepository;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerArgumentNotSpecified;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserCrudService;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserRepository;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyActiveException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyExistsEmailDuplicationException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@RunWith(SpringRunner.class)
public class AvailabilityCrudServiceTest {

    @Autowired
    private AvailabilityCrudRepository repository;
    @Autowired
    private MemberRepository memberRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MessengerRepository messengerRepository;
    @Autowired
    private GroupCrudRepository groupCrudRepository;

    private AvailabilityCrudService availabilityCrudService;
    private MemberCrudService memberCrudService;
    private UserCrudService userCrudService;

    private AvailabilityEntity testAvailabilityEntity;
    private MemberEntity testMemberEntity;
    private UserEntity testUserEntity;
    //   private PlaceDto testPlaceDto;

    private LocalDate entityBeginDate;
    private LocalTime entityBeginTime;
    private LocalDate entityEndDate;
    private LocalTime entityEndTime;

    private String dtoBeginDate;
    private String dtoBeginTime;
    private String dtoEndDate;
    private String dtoEndTime;

    @Before
    public void setUp() {
        this.dtoBeginDate = "2020/04/05";
        this.dtoBeginTime = "17:30";
        this.dtoEndDate = "2020/04/05";
        this.dtoEndTime = "21:00";

        this.entityBeginDate = LocalDate.of(2020, 4, 5);
        this.entityBeginTime = LocalTime.of(17, 30);
        this.entityEndDate = LocalDate.of(2020, 4, 5);
        this.entityEndTime = LocalTime.of(21, 0);

        testUserEntity = UserEntity.builder()
                .email("testavailabilityuser@gmail.com")
                .password("multiPass")
                .isActive(true)
                .build();

        testMemberEntity = MemberEntity.builder()
                .user(testUserEntity)
                .nick("WEK666")
                .build();

        testAvailabilityEntity = AvailabilityEntity.builder()
                .member(testMemberEntity)
                .beginDate(entityBeginDate)
                .beginTime(entityBeginTime)
                .endDate(entityEndDate)
                .endTime(entityEndTime)
//                .beginDateTime(new DateTime(2020, 3, 3, 15, 0, 0))
//                .endDateTime(new DateTime(2020, 3, 3, 16, 0, 0))
                .remoteWork(true)
                .creationTime(null)
                .modificationTime(null)
                .isActive(true)
                .build();
    }

    private UserCrudService initUserCrudFacade() {
        return new UserCrudService(userRepository);
    }

    private MemberCrudService initMemberCrudFacade() {
        return new MemberCrudService(memberRepository, userRepository, messengerRepository, groupCrudRepository); // tworzy obiekt fasady
    }

    private AvailabilityCrudService initAvailabilityCrudFacade() {
        return new AvailabilityCrudService(repository, memberRepository, userRepository, messengerRepository, groupCrudRepository);
    }

    private AvailabilityEntity createAvailability() throws MemberNotFoundException, AvailabilityAlreadyExistsException, UserNotFoundException {
        if (availabilityCrudService == null)
            this.availabilityCrudService = initAvailabilityCrudFacade();

        this.testAvailabilityEntity = availabilityCrudService.add(testAvailabilityEntity);
        return testAvailabilityEntity;
    }

    private boolean initTestDB() {
        userCrudService = initUserCrudFacade();
        memberCrudService = initMemberCrudFacade();

        try {
            testUserEntity = userCrudService
                    .findByIdOrUserEmail(
                            userCrudService.activate(
                                    userCrudService.add(testUserEntity)
                            )
                    );
        } catch (UserNotFoundException | UserAlreadyExistsEmailDuplicationException | UserAlreadyActiveException e) {
            e.printStackTrace();
        }

        try {
            testMemberEntity = memberCrudService
                    .find(memberCrudService.add(testMemberEntity));
        } catch (MemberNotFoundException | MemberAlreadyExistsException | UserNotFoundException | MemberUserNotActiveException e) {
            e.printStackTrace();
        }

        return testUserEntity.getId() != null
                && testMemberEntity.getId() != null;
    }

    @Test
    public void USER_CRUD_FACADE_WR() throws UserAlreadyExistsEmailDuplicationException, UserNotFoundException {
        UserCrudService userCrudService = initUserCrudFacade();
        UserEntity testUser = userCrudService.add(testUserEntity);
        UserEntity userEntity = userCrudService.findByIdOrUserEmail(testUser);
        assertThat(userEntity).isNotNull();
    }

    @Test
    public void MEMBER_CRUD_FACADE_WR() throws UserNotFoundException, MemberAlreadyExistsException, MemberNotFoundException, GroupNotFoundException, MessengerArgumentNotSpecified, MessengerAlreadyExistsException, UserAlreadyExistsEmailDuplicationException, MemberUserNotActiveException, UserAlreadyActiveException {
        MemberCrudService memberCrudService = initMemberCrudFacade();
        UserCrudService userCrudService = initUserCrudFacade();
        userCrudService.add(testUserEntity);
        userCrudService.activate(testUserEntity);
        MemberEntity memberEntity = memberCrudService.find(memberCrudService.add(testMemberEntity));
        assertThat(memberEntity).isNotNull();
    }

    @Test
    public void INIT_TEST_DB() {
        boolean initDB = initTestDB();
        assertThat(initDB).isTrue();
    }

    @Test
    public void WHEN_try_to_create_non_existing_availability_THEN_return_availability() throws MemberNotFoundException, AvailabilityAlreadyExistsException, UserNotFoundException {
        initTestDB();
        availabilityCrudService = initAvailabilityCrudFacade();
        AvailabilityEntity created = availabilityCrudService.add(testAvailabilityEntity);

        assertThat(created.getMember()).isNotNull();
        assertThat(created.getBeginDate()).isNotNull();
        assertThat(created.getBeginTime()).isNotNull();
        assertThat(created.getEndDate()).isNotNull();
        assertThat(created.getEndTime()).isNotNull();
        assertThat(created.getCreationTime()).isNotNull();
        assertThat(created.getModificationTime()).isNull();
        assertThat(created.isActive()).isTrue();
    }

    @Test
    public void WHEN_try_to_create_existing_availability_THEN_EntityAlreadyExistsException() throws MemberNotFoundException, UserNotFoundException {
        initTestDB();
        availabilityCrudService = initAvailabilityCrudFacade();
        try {
            availabilityCrudService.add(testAvailabilityEntity);
        } catch (AvailabilityAlreadyExistsException e) {
            Assert.fail();
        }
        try {
            availabilityCrudService.add(testAvailabilityEntity);
            Assert.fail();
        } catch (AvailabilityAlreadyExistsException e) {
            assertThat(e)
                    .isInstanceOf(AvailabilityAlreadyExistsException.class)
                    .hasMessage(AvailabilityCrudInfoStatusEnum.AVAILABILITY_ALREADY_EXISTS.toString());
        }
    }

    @Test
    public void WHEN_found_availability_THEN_return_availability() throws MemberNotFoundException, AvailabilityAlreadyExistsException, UserNotFoundException, AvailabilityNotFoundException {
        initTestDB();
        AvailabilityEntity created;
        AvailabilityEntity found = null;
        availabilityCrudService = initAvailabilityCrudFacade();

        created = availabilityCrudService.add(testAvailabilityEntity);
        found = availabilityCrudService.findByEntity(testAvailabilityEntity);
        assertThat(found).isNotNull();
    }

    @Test
    public void WHEN_try_to_find_non_existing_availability_THEN_return_EntityNotFoundException() throws MemberNotFoundException, UserNotFoundException {
        initTestDB();
        availabilityCrudService = initAvailabilityCrudFacade();
        try {
            availabilityCrudService.findByEntity(testAvailabilityEntity);
            Assert.fail();
        } catch (AvailabilityNotFoundException e) {
            assertThat(e)
                    .isInstanceOf(AvailabilityNotFoundException.class)
                    .hasMessage(AvailabilityCrudInfoStatusEnum.AVAILABILITY_NOT_FOUND.toString());
        }
    }

    @Test
    public void WHEN_try_to_find_all_availabilities_THEN_return_availabilities() throws MemberNotFoundException, AvailabilityAlreadyExistsException, UserNotFoundException, AvailabilityNotFoundException {
        initTestDB();
        List<AvailabilityEntity> found = null;
        AvailabilityCrudService availabilityCrudService = initAvailabilityCrudFacade();
        availabilityCrudService.add(testAvailabilityEntity);
        found = availabilityCrudService.findAllMemberAvailabilities(testAvailabilityEntity);
        assertThat(found).isNotNull();
    }

    @Test
    public void WHEN_try_to_update_existing_availability_THEN_return_availability() throws MemberNotFoundException, AvailabilityAlreadyExistsException, UserNotFoundException, AvailabilityNotFoundException, AvailabilityMembersAreNotTheSameException {
        initTestDB();
        AvailabilityCrudService availabilityCrudService = initAvailabilityCrudFacade();
        createAvailability();
        final AvailabilityEntity updated = availabilityCrudService.update(updateAvailabilityValues(testAvailabilityEntity));

        assertThat(updated.getRemoteWork()).isFalse();
        assertThat(updated.getMember()).isEqualToComparingFieldByField(testAvailabilityEntity.getMember());
        assertThat(updated.getBeginDate()).isNotNull().isNotEqualTo(entityBeginDate);
        assertThat(updated.getBeginTime()).isNotNull().isNotEqualTo(entityBeginTime);
        assertThat(updated.getEndDate()).isNotNull().isNotEqualTo(entityEndDate);
        assertThat(updated.getEndTime()).isNotNull().isNotEqualTo(entityEndTime);
        assertThat(updated.getCreationTime()).isEqualTo(testAvailabilityEntity.getCreationTime());
        assertThat(updated.isActive()).isEqualTo(testAvailabilityEntity.isActive());
    }

    private AvailabilityEntity updateAvailabilityValues(AvailabilityEntity availabilityEntity) {
        return AvailabilityEntity.builder()
                .id(availabilityEntity.getId())
                .member(availabilityEntity.getMember())
                .beginDate(LocalDate.now())
                .beginTime(LocalTime.now())
                .endDate(LocalDate.now())
                .endTime(LocalTime.now())
                .remoteWork(false)
                .creationTime(LocalDateTime.now())
                .modificationTime(LocalDateTime.now())
                .isActive(false)
                .build();
    }

    @Test
    public void WHEN_try_to_update_non_existing_availability_THEN_return_EntityNotFoundException() throws UserNotFoundException, MemberNotFoundException, AvailabilityMembersAreNotTheSameException {
        initTestDB();
        AvailabilityCrudService availabilityCrudService = initAvailabilityCrudFacade();
        try {
            availabilityCrudService.update(updateAvailabilityValues(testAvailabilityEntity));
        } catch (AvailabilityNotFoundException e) {
            assertThat(e)
                    .isInstanceOf(AvailabilityNotFoundException.class)
                    .hasMessage(AvailabilityCrudInfoStatusEnum.AVAILABILITY_NOT_FOUND.toString());
        }
    }

    @Test
    public void WHEN_delete_existing_availability_THEN_return_availability() throws MemberNotFoundException, AvailabilityAlreadyExistsException, UserNotFoundException, AvailabilityNotFoundException, AvailabilityFoundButNotActiveException {
        initTestDB();
        AvailabilityCrudService availabilityCrudService = initAvailabilityCrudFacade();
        AvailabilityEntity created = availabilityCrudService.add(testAvailabilityEntity);
        AvailabilityEntity deleted = availabilityCrudService.delete(created);

        assertThat(deleted).isNotNull();
        assertThat(deleted.isActive()).isFalse();
        assertThat(deleted.getCreationTime()).isEqualTo(created.getCreationTime());
        assertThat(deleted.getModificationTime()).isNotNull();
    }

    @Test
    public void WHEN_try_to_delete_non_existing_availability_THEN_return_EntityNotFoundException() throws UserNotFoundException, AvailabilityAlreadyExistsException, MemberNotFoundException {
        initTestDB();
        AvailabilityCrudService availabilityCrudService = initAvailabilityCrudFacade();

        try {
            availabilityCrudService.delete(testAvailabilityEntity);
        } catch (AvailabilityNotFoundException | AvailabilityFoundButNotActiveException e) {
            assertThat(e)
                    .isInstanceOf(AvailabilityNotFoundException.class)
                    .hasMessage(AvailabilityCrudInfoStatusEnum.AVAILABILITY_NOT_FOUND.toString());
        }
    }
}