package pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudRepository;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberRepository;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.domain.MessengerRepository;
import pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.status_and_exceptions.PollAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.status_and_exceptions.PollCrudStatusEnum;
import pl.com.devmeet.devmeetcore.domain.poll_associated.poll.domain.status_and_exceptions.PollNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserRepository;

import static org.assertj.core.api.Assertions.assertThat;

@Ignore
@DataJpaTest
@RunWith(SpringRunner.class)
public class PollCrudServiceTest {

    @Autowired
    private PollCrudRepository pollCrudRepository;

    @Autowired
    private GroupCrudRepository groupCrudRepository;
    @Autowired
    private MemberRepository memberRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MessengerRepository messengerRepository;

    private PollCrudService pollCrudService;
    private GroupCrudService groupCrudService;

    private PollEntity testPollEntity;
    private GroupEntity testGroupEntity;

    @Before
    public void setUp() {

        testGroupEntity = new GroupEntity().builder()
                .groupName("Java test group")
                .website("www.testWebsite.com")
                .description("Welcome to test group")
                .membersLimit(5)
                .creationTime(null)
                .modificationTime(null)
                .isActive(false)
                .build();

        testPollEntity = new PollEntity().builder()
                .group(testGroupEntity)
//                .placeVotes(null)
//                .availabilityVotes(null)
                .creationTime(null)
                .active(true)
                .build();
    }

    private GroupCrudService initGroupCrudFacade() {
        return new GroupCrudService(groupCrudRepository, memberRepository, userRepository, messengerRepository);
    }

    private PollCrudService initPollCrudFacade() {
        return new PollCrudService(pollCrudRepository, groupCrudRepository, memberRepository, userRepository, messengerRepository);
    }

    private boolean initTestDB() {
        groupCrudService = initGroupCrudFacade();

        GroupEntity groupEntity = null;
        try {
            groupEntity = groupCrudService
                    .findByGroup(groupCrudService
                            .add(testGroupEntity));
        } catch (GroupNotFoundException | GroupAlreadyExistsException e) {
            e.printStackTrace();
        }

        return groupEntity != null;
    }

    @Test
    public void INIT_TEST_DB() {
        boolean initDB = initTestDB();
        assertThat(initDB).isTrue();
    }

    @Test
    public void WHEN_create_not_existing_poll_THEN_return_poll() throws PollAlreadyExistsException, GroupNotFoundException {
        initTestDB();
        PollEntity pollEntity = initPollCrudFacade().add(testPollEntity);

        assertThat(pollEntity).isNotNull();
        assertThat(pollEntity.getGroup()).isNotNull();
        assertThat(pollEntity.getCreationTime()).isNotNull();
        assertThat(pollEntity.isActive()).isTrue();
    }

    @Test
    public void WHEN_try_to_create_existing_poll_THEN_return_EntityAlreadyExistsException() throws GroupNotFoundException {
        initTestDB();
        PollCrudService pollCrudService = initPollCrudFacade();
        try {
            pollCrudService.add(testPollEntity);
        } catch (PollAlreadyExistsException | GroupNotFoundException e) {
            Assert.fail();
        }

        try {
            pollCrudService.add(testPollEntity);
            Assert.fail();
        } catch (PollAlreadyExistsException e) {
            assertThat(e)
                    .isInstanceOf(PollAlreadyExistsException.class)
                    .hasMessage(PollCrudStatusEnum.POLL_ALREADY_EXISTS.toString());
        }
    }

    @Test
    public void WHEN_found_active_poll_THEN_return_poll() throws PollAlreadyExistsException, GroupNotFoundException, PollNotFoundException {
        initTestDB();
        PollCrudService pollCrudService = initPollCrudFacade();
        PollEntity pollEntity = pollCrudService.add(testPollEntity);
        PollEntity found = pollCrudService.find(pollEntity);

        assertThat(found).isNotNull();
        assertThat(found.isActive()).isTrue();
    }

    @Test
    public void WHEN_try_to_find_not_existing_poll_THEN_return_EntityNotFoundException() throws GroupNotFoundException {
        initTestDB();
        try {
            initPollCrudFacade().find(testPollEntity);
            Assert.fail();
        } catch (PollNotFoundException e) {
            assertThat(e)
                    .isInstanceOf(PollNotFoundException.class)
                    .hasMessage(PollCrudStatusEnum.POLL_NOT_FOUND.toString());
        }
    }

    @Test
    public void WHEN_delete_active_poll_THEN_return_poll() throws PollAlreadyExistsException, GroupNotFoundException, PollNotFoundException {
        initTestDB();
        PollCrudService pollCrudService = initPollCrudFacade();
        pollCrudService.add(testPollEntity);
        PollEntity deleted = pollCrudService.delete(testPollEntity);

        assertThat(deleted).isNotNull();
        assertThat(deleted.isActive()).isFalse();
    }

    @Test
    public void WHEN_delete_not_active_poll_THEN_return_EntityAlreadyExistsException() throws GroupNotFoundException {
        initTestDB();
        PollCrudService pollCrudService = initPollCrudFacade();

        try {
            pollCrudService.add(testPollEntity);
        } catch (PollAlreadyExistsException e) {
            Assert.fail();
        }

        try {
            pollCrudService.delete(testPollEntity);
        } catch (PollNotFoundException | PollAlreadyExistsException e) {
            Assert.fail();
        }

        try {
            initPollCrudFacade().delete(testPollEntity);
        } catch (PollNotFoundException | PollAlreadyExistsException e) {
            assertThat(e)
                    .isInstanceOf(PollAlreadyExistsException.class)
                    .hasMessage(PollCrudStatusEnum.POLL_FOUND_BUT_NOT_ACTIVE.toString());
        }
    }
}