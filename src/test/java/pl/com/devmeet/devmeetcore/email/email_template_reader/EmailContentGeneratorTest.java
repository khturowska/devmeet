package pl.com.devmeet.devmeetcore.email.email_template_reader;

import org.junit.Before;
import org.junit.Test;
import pl.com.devmeet.devmeetcore.email.email_template_reader.exceptions.EmailContentGeneratorException;
import pl.com.devmeet.devmeetcore.email.email_template_reader.exceptions.ResourcesTemplateReaderErrorException;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class EmailContentGeneratorTest {

    private Map<String, String> testParams;
    private String fileName;

    private String email;
    private String password;
    private String link;

    private String expectedContent;

    @Before
    public void setUp() throws Exception {
        fileName = "/email_templates/registrationEmailTemplateWithInitialPassword.txt";

        email = "contentGeneratorString@mail.com";
        password = "superTajneHasło";
        link = "https://fakeLink.com.pl";

        testParams = new HashMap<>();
        testParams.put("email", email);
        testParams.put("password", password);
        testParams.put("link", link);

        expectedContent = "Hello " + email + ",\n" +
                "\n" +
                "Your Devmeet account has been created, please click on the URL below within 24 h to activate it:\n" +
                "\n" +
                link + "\n" +
                "\n" +
                "Your initial password is: " + password + "\n" +
                "\n" +
                "Regards,\n" +
                "Devmeet Team.";
    }

    @Test
    public void generateEmailContentForEmptyConstructorAndAllNeeded() throws EmailContentGeneratorException {
        String inputLocalContent = "Hello I'm Linus,\n" +
                "My email is {email}\n" +
                "My super secret password is {password}\n" +
                "And link to my website is {link}";

        String expectedLocalContent = "Hello I'm Linus,\n" +
                "My email is " + email + "\n" +
                "My super secret password is " + password + "\n" +
                "And link to my website is " + link;

        String mailContent = new EmailContentGenerator().generateEmailContentFromTemplateAndParams(inputLocalContent, testParams);

        assertThat(mailContent).isEqualTo(expectedLocalContent);
    }

    @Test
    public void generateEmailContentFromResourcesTemplates() throws EmailContentGeneratorException, ResourcesTemplateReaderErrorException {
        String readFromFilEmailContent = new EmailContentGenerator(fileName).generateEmailContent(testParams);

        assertThat(readFromFilEmailContent).isEqualTo(expectedContent);
    }
}