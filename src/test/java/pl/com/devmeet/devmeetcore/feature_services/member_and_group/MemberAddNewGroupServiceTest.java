package pl.com.devmeet.devmeetcore.feature_services.member_and_group;

import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.com.devmeet.devmeetcore.feature_services.member_and_group.crud.RelationsMemberGroupCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberUserNotActiveException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerArgumentNotSpecified;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserCrudService;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyActiveException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyExistsEmailDuplicationException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
@RunWith(SpringRunner.class)
public class MemberAddNewGroupServiceTest {

    @Autowired
    private MemberAddNewGroupService memberAddNewGroupService;
    @Autowired
    private RelationsMemberGroupCrudService relationsMemberGroupCrudService;

    @Autowired
    private UserCrudService userCrudService;
    @Autowired
    private MemberCrudService memberCrudService;

    private UserEntity testUserEntity;
    private MemberEntity testMemberEntity;
    private GroupEntity newTestGroup;

    @Before
    public void setUp() throws Exception {
        testUserEntity = UserEntity.builder()
                .email("memberAddNewGroupServiceTestUser@test.pl")
                .password("testPass")
                .isActive(true)
                .build();

        testMemberEntity = MemberEntity.builder()
                .user(testUserEntity)
                .nick("new_group_creator")
                .build();

        newTestGroup = GroupEntity.builder()
                .description("New group created by member")
                .groupName("Member Created Group")
                .website("www.-group,com")
                .build();

        createUserInDB();
        createMemberInDB();
    }

    private void createUserInDB() throws UserAlreadyExistsEmailDuplicationException, UserNotFoundException, UserAlreadyActiveException {
        this.testUserEntity = userCrudService
                .activate(userCrudService
                        .add(testUserEntity));
    }

    private void createMemberInDB() throws MemberUserNotActiveException, GroupNotFoundException, MessengerArgumentNotSpecified, MemberAlreadyExistsException, MessengerAlreadyExistsException, UserNotFoundException, MemberNotFoundException {
        this.testMemberEntity = memberCrudService.add(testMemberEntity);
    }

    @SneakyThrows
    @Test
    public void createNewGroupAndAddMemberToIt() {
        GroupEntity createdGroup = memberAddNewGroupService.memberCreateNewGroup(testMemberEntity.getId(), newTestGroup);

        checkIsGroupNameIsNotNullOrEmpty__cannotBeEmpty(createdGroup);
        checkIfMemberWhoCreatedTheGroupIsGroupMember__shouldBe(createdGroup);
        checkIfMemberWhoCreatedTheGroupIsItsFounder__shouldBe(createdGroup, testMemberEntity);
    }

    private void checkIsGroupNameIsNotNullOrEmpty__cannotBeEmpty(GroupEntity group){
        assertThat(group.getGroupName()).isNotEmpty();
        assertThat(group.getGroupName()).isNotBlank();
    }

    private void checkIfMemberWhoCreatedTheGroupIsGroupMember__shouldBe(GroupEntity group) {
        List<GroupEntity> memberGroups = relationsMemberGroupCrudService.findInWhatGroupsIsAMember(testMemberEntity);

        assertThat(memberGroups.size()).isEqualTo(1);
        assertThat(memberGroups.get(0).getId()).isEqualTo(group.getId());
        assertThat(group.getMembersLimit()).isGreaterThanOrEqualTo(1);
    }

    private void checkIfMemberWhoCreatedTheGroupIsItsFounder__shouldBe(GroupEntity group, MemberEntity founder) {
        List<GroupEntity> foundersGroups = relationsMemberGroupCrudService.findInWhatGroupsMemberIsAFounder(founder);
        assertThat(foundersGroups.get(0).getId()).isEqualTo(group.getId());
        assertThat(foundersGroups.get(0).getFounder().getId()).isEqualTo(founder.getId());
    }
}