package pl.com.devmeet.devmeetcore.feature_services.user_and_member.user_activation_after_registration;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.com.devmeet.devmeetcore.email.email_template_reader.exceptions.EmailContentGeneratorException;
import pl.com.devmeet.devmeetcore.email.email_template_reader.exceptions.ResourcesTemplateReaderErrorException;
import pl.com.devmeet.devmeetcore.feature_services.user_and_member.user_registration.UserRegistrationService;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberUserNotActiveException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerArgumentNotSpecified;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserCrudService;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyActiveException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyExistsEmailDuplicationException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;


// todo zaatakować od strony API, bo aktualnie jest testowany serwis poniżej API
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserActivationServiceTest {

    private static boolean setUpSingleRunFlag = false;
    @Autowired
    private UserActivationService activationService;
    @Autowired
    private UserRegistrationService registrationService;
    @Autowired
    private MemberCrudService memberService;
    @Autowired
    private UserCrudService userService;
    private UserEntity user = UserEntity.builder()
            .email("activationServiceTestUser@devmeet.com.pl")
            .password("thisWillBeChangedToRandomPass")
            .activationKey(UUID.randomUUID())
            .build();

    @Before
    public void setUp() throws Exception {
        if (!setUpSingleRunFlag) {
            setUpSingleRunFlag = true;

            registerTestUser();
            activateTestUser();
        }
    }

    private void registerTestUser() throws UserAlreadyExistsEmailDuplicationException, EmailContentGeneratorException, ResourcesTemplateReaderErrorException {
        registrationService
                .registerNotActiveUserUsingApiWithInitialPasswordAndSendEmailToUser(user);
    }

    private void activateTestUser() throws GroupNotFoundException, MessengerArgumentNotSpecified, MessengerAlreadyExistsException, UserNotFoundException, MemberAlreadyExistsException, MemberNotFoundException, UserAlreadyActiveException, MemberUserNotActiveException {
        activationService.activateUser(user.getEmail(), user.getActivationKey().toString());
    }

    @Test
    public void serviceActivationProcessTest() throws UserNotFoundException {
        UserEntity userAfter = userService.findByIdOrUserEmail(user);

        assertThat(userAfter.isActive()).isTrue();
        assertThat(userAfter.getModificationTime()).isNotNull();
        assertThat(userAfter.getActivationKey()).isNotNull();
    }

    @Test
    public void memberCreationTest() throws UserNotFoundException, MemberNotFoundException {
        UserEntity foundUser = userService.findByIdOrUserEmail(user);
        MemberEntity member = memberService.findByUser(foundUser);

        assertThat(member.getUser().getId()).isEqualTo(foundUser.getId());
    }
}