package pl.com.devmeet.devmeetcore.feature_services.member_and_group;

import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.com.devmeet.devmeetcore.feature_services.member_and_group.crud.RelationsMemberGroupCrudService;
import pl.com.devmeet.devmeetcore.feature_services.member_and_group.exceptions.MemberAddNewGroupServiceException;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberUserNotActiveException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.messenger_associated.messenger.status_and_exceptions.MessengerArgumentNotSpecified;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserCrudService;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyActiveException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyExistsEmailDuplicationException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MemberAddToExistingGroupServiceTest {

    @Autowired
    private MemberAddToExistingGroupService memberAddToExistingGroupService;
    @Autowired
    private MemberAddNewGroupService memberAddNewGroupService;
    @Autowired
    private RelationsMemberGroupCrudService relationsMemberGroupCrudService;

    @Autowired
    private UserCrudService userCrudService;
    @Autowired
    private MemberCrudService memberCrudService;
    @Autowired
    private GroupCrudService groupCrudService;


    private MemberEntity newTestMemberEntity;
    private MemberEntity existingTestMemberEntity;
    private GroupEntity existingTestGroup;


    @Before
    public void setUp() throws Exception {
        UserEntity existingTestUserEntity = UserEntity.builder()
                .email("existingServiceTestUserBla@test.pl")
                .password("testPass")
                .isActive(true)
                .build();

        existingTestMemberEntity = MemberEntity.builder()
                .user(existingTestUserEntity)
                .nick("old_group_member")
                .build();

        existingTestGroup = GroupEntity.builder()
                .description("New group created previously by member")
                .groupName("ExistingGroupForAddingNewMember")
                .website("www.-group,com")
                .membersLimit(1)
                .build();

        existingTestMemberEntity = createMemberInDB(existingTestUserEntity, existingTestMemberEntity);
        existingTestGroup = memberAddNewGroupService.memberCreateNewGroup(existingTestMemberEntity.getId(), existingTestGroup);

        UserEntity newTestUserEntity = UserEntity.builder()
                .email("memberAddToExistingServiceTestUser@test.pl")
                .password("testPass")
                .isActive(true)
                .build();

        newTestMemberEntity = MemberEntity.builder()
                .user(newTestUserEntity)
                .nick("member_for_add_to_existing_group")
                .build();

        newTestMemberEntity = createMemberInDB(newTestUserEntity, newTestMemberEntity);

    }

    private MemberEntity createMemberInDB(UserEntity userEntity, MemberEntity memberEntity) throws MemberUserNotActiveException, GroupNotFoundException, MessengerArgumentNotSpecified, MemberAlreadyExistsException, MessengerAlreadyExistsException, UserNotFoundException, MemberNotFoundException, UserAlreadyExistsEmailDuplicationException, UserAlreadyActiveException {
        userCrudService
                .activate(userCrudService
                        .add(userEntity));
        return memberCrudService.add(memberEntity);
    }

//    private GroupEntity createGroupInDB(GroupEntity groupEntity) throws GroupAlreadyExistsException {
//        return groupCrudService.add(groupEntity);
//    }

    @SneakyThrows
    @Test
    public void WHEN_add_new_Member_to_Group_with_Member_and_memberLimit_equal_2_THEN_return_updated_Group() {

        WHEN_try_to_add_Member_to_Group_with_Member_and_memberLimit_equal_1_THEN_return_MemberLimitInGroupException();
        GroupEntity updatedGroup = WHEN_change_memberLimit_to_2_and_try_to_add_second_Member_to_Group_THEN_return_updated_Group();

        updatedGroup = memberAddToExistingGroupService.addMemberToExistingGroup(updatedGroup.getId(), newTestMemberEntity.getId());
        List<GroupEntity> groupsWithMember = relationsMemberGroupCrudService.findInWhatGroupsIsAMember(newTestMemberEntity);

        assertThat(updatedGroup.getMembersLimit()).isEqualTo(2);

        assertThat(groupsWithMember.size()).isEqualTo(1);
        assertThat(groupsWithMember.get(0).getMembersLimit()).isGreaterThanOrEqualTo(2);
    }

    private void WHEN_try_to_add_Member_to_Group_with_Member_and_memberLimit_equal_1_THEN_return_MemberLimitInGroupException() {
        try {
            existingTestGroup = memberAddToExistingGroupService.addMemberToExistingGroup(existingTestGroup.getId(), newTestMemberEntity.getId());
            Assert.fail();
        } catch (GroupNotFoundException | MemberNotFoundException e) {
            Assert.fail();
        } catch (MemberAddNewGroupServiceException e) {
            assertThat(e.getMessage()).isNotBlank();
        }
    }

    private GroupEntity WHEN_change_memberLimit_to_2_and_try_to_add_second_Member_to_Group_THEN_return_updated_Group() throws GroupNotFoundException, GroupFoundButNotActiveException {
        existingTestGroup.setMembersLimit(2);
        return groupCrudService.update(existingTestGroup);
    }
}