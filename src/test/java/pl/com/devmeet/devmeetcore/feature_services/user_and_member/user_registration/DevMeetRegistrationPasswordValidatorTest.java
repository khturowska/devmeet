package pl.com.devmeet.devmeetcore.feature_services.user_and_member.user_registration;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnitParamsRunner.class)
public class DevMeetRegistrationPasswordValidatorTest {

    private DevMeetRegistrationPasswordValidator pv = new DevMeetRegistrationPasswordValidator();

    @Test
    @Parameters(method = "testSet")
    public void validatePassword(String testPassword, boolean expected, String message) {
        assertThat(pv.validatePassword(testPassword))
                .isEqualTo(expected);
        System.out.println(String.format("Password: \"%s\" is %s", testPassword, message));
    }

    private Object[] testSet() {
        return new Object[]{
                new Object[]{"AlaM4KoTa", true, "good password"},
                new Object[]{"", false, "wrong password, because it's empty"},
                new Object[]{"AlaM4", false, "wrong password, because to short"},
                new Object[]{"alam4kota", false, "wrong password, because it has no uppercase letters"},
                new Object[]{"AlaMaKoTa", false, "wrong password, because it has no numbers"},
                new Object[]{"ala4Makota", true, "good password"},
                new Object[]{"123456789", false, "wrong password, because it has no letters"}
        };
    }
}