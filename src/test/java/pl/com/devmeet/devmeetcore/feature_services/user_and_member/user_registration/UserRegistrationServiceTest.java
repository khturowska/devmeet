package pl.com.devmeet.devmeetcore.feature_services.user_and_member.user_registration;

import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.email.email_template_reader.EmailContentGenerator;
import pl.com.devmeet.devmeetcore.email.email_template_reader.exceptions.EmailContentGeneratorException;
import pl.com.devmeet.devmeetcore.email.email_template_reader.exceptions.ResourcesTemplateReaderErrorException;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRegistrationServiceTest {

    @Autowired
    private UserRegistrationService userRegistrationService;

    private UserEntity newTestUser;
    private String pathToActivationContentTemplate;
    private String expectedEmailContent;
//    @Rule
//    public GreenMailRule fakeMailServer = new GreenMailRule(new ServerSetup(587,""));

    @Before
    public void setUp() throws Exception {
        pathToActivationContentTemplate = "/email_templates/registrationEmailTemplateWithInitialPassword.txt";

        newTestUser = UserEntity.builder()
                .email("registrationServiceTestUser@devmeet.com.pl")
                .password("thisWillBeChangedToPassHash")
                .activationKey(UUID.randomUUID())
                .build();

        expectedEmailContent = generateExpectedContentUsingEmailContentGenerator();
    }

    private String generateExpectedContentUsingEmailContentGenerator() throws EmailContentGeneratorException, ResourcesTemplateReaderErrorException {
        String linkUrl = UriComponentsBuilder.newInstance()
                .scheme("http")
                .host("localhost")
                .port("8080")
                .path(userRegistrationService.getEmailSender().getUriEndpoint() + "/" + newTestUser.getEmail() + "/" + newTestUser.getActivationKey())
                .build()
                .encode()
                .toUriString();

        Map<String, String> params = new HashMap() {{
            put("email", newTestUser.getEmail());
            put("password", newTestUser.getPassword());
            put("link", linkUrl);
        }};

        return new EmailContentGenerator(pathToActivationContentTemplate).generateEmailContent(params);
    }

    @SneakyThrows
    @Test
    public void serviceRegistrationProcessTest() {
        UserEntity user = userRegistrationService.registerNotActiveUserUsingApiWithInitialPasswordAndSendEmailToUser(newTestUser);

        assertThat(user.getId()).isNotNull();
        assertThat(user).isNotNull();
    }

//    @Ignore
//    @Test
//    public void testEmailSending(){
//        // I don't know how to make test for this feature
//    }
}