package pl.com.devmeet.devmeetcore.feature_services.user_and_member.user_activation_after_registration;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import pl.com.devmeet.devmeetcore.email.email_template_reader.UserEmailPostfixTrimmer;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(JUnitParamsRunner.class)
public class UserEmailPostfixTrimmerTest {

    private UserEmailPostfixTrimmer postfixTrimmer;

    @Before
    public void setUp() {
        postfixTrimmer = new UserEmailPostfixTrimmer();
    }

    @Test
    @Parameters(method = "testMails")
    public void trim(String actual, String expected) {
        assertThat(postfixTrimmer.trim(actual)).isEqualTo(expected);
    }

    private Object[] testMails() {
        return new Object[]{
                new Object[]{"test1@gmail.com", "test1"},
                new Object[]{"test2@gmail.com", "test2"},
                new Object[]{"test3@gmail.com", "test3"}
        };
    }
}