package pl.com.devmeet.devmeetcore.feature_services.user_and_member.login;

import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.com.devmeet.devmeetcore.feature_services.user_and_member.login.exceptions.WrongUserPasswordLoginException;
import pl.com.devmeet.devmeetcore.feature_services.user_and_member.user_activation_after_registration.UserActivationService;
import pl.com.devmeet.devmeetcore.feature_services.user_and_member.user_registration.UserRegistrationService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberUserNotActiveException;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserCrudService;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyActiveException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyExistsEmailDuplicationException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserFoundButNotActiveException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserNotFoundException;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserLoginServiceTest {

    private static boolean setUpSingleRunFlag = false;

    @Autowired
    private UserLoginService userLoginService;
    @Autowired
    private UserActivationService activationService;
    @Autowired
    private UserRegistrationService registrationService;
    @Autowired
    private MemberCrudService memberService;
    @Autowired
    private UserCrudService userService;

    private String goodPassword = "goodPassword";
    private UserEntity testUserFromForm = UserEntity.builder()
            .email("loginServiceTestUser@devmeet.com.pl")
            .password(goodPassword)
            .activationKey(UUID.randomUUID())
            .build();


    @Before
    public void setUp() throws Exception {
        if (!setUpSingleRunFlag) {
            setUpSingleRunFlag = true;

            registerTestUser();
        }
    }

    private void registerTestUser() throws UserAlreadyExistsEmailDuplicationException {
        registrationService
                .registerNotActiveUserUsingApiWithInitialPasswordAndSendEmailToUser(testUserFromForm);
    }

    private void activateTestUser() throws UserNotFoundException, MemberAlreadyExistsException, UserAlreadyActiveException, MemberUserNotActiveException {
        activationService.activateUser(testUserFromForm.getEmail(), testUserFromForm.getActivationKey().toString());
    }

    private MemberEntity loginUser(UserEntity user) throws UserNotFoundException, WrongUserPasswordLoginException, MemberNotFoundException, UserFoundButNotActiveException {
        return userLoginService.loginUserAndGetMember(user);
    }

    @SneakyThrows
    @Test
    public void loginUserAndGetMember() {
        first_case_WHEN_User_is_not_active_and_try_to_log_in_THEN_get_throw_UserIsNotActiveException();
        second_case_WHEN_User_is_active_but_try_log_in_with_wrong_password();
        third_case_WHEN_User_log_in_with_correct_values_THEN_create_and_get_new_Users_Member();
    }

    private void first_case_WHEN_User_is_not_active_and_try_to_log_in_THEN_get_throw_UserIsNotActiveException() {
        try {
            loginUser(testUserFromForm);
            Assert.fail();
        } catch (UserFoundButNotActiveException e) {
            assertThat(e.getMessage()).isNotBlank();
            assertThat(testUserFromForm.isActive()).isFalse();
        } catch (WrongUserPasswordLoginException | UserNotFoundException | MemberNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void second_case_WHEN_User_is_active_but_try_log_in_with_wrong_password() {
        try {
            activateTestUser();
            testUserFromForm.setPassword("Wrong password");
            loginUser(testUserFromForm);
            Assert.fail();

        } catch (UserNotFoundException | MemberAlreadyExistsException | UserAlreadyActiveException | MemberUserNotActiveException | MemberNotFoundException | UserFoundButNotActiveException e) {
           e.printStackTrace();
        } catch (WrongUserPasswordLoginException e) {
            assertThat(e.getMessage()).isNotBlank();
        }
    }

    private void third_case_WHEN_User_log_in_with_correct_values_THEN_create_and_get_new_Users_Member() throws UserNotFoundException, MemberNotFoundException, UserFoundButNotActiveException {
        testUserFromForm.setPassword(goodPassword);
        MemberEntity memberEntity = null;
        try {
            memberEntity = loginUser(testUserFromForm);
        } catch (WrongUserPasswordLoginException e) {
            e.printStackTrace();
        }

        assertThat(memberEntity.getId()).isNotNull();
        assertThat(memberEntity.getNick()).isNotNull();
        assertThat(memberEntity.getUser().getId()).isEqualTo(testUserFromForm.getId());
        assertThat(memberEntity.getUser().getEmail()).isEqualTo(testUserFromForm.getEmail());
    }
}