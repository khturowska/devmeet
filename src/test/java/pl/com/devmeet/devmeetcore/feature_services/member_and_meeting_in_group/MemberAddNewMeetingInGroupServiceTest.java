package pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group;

import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupNotFoundException;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.crud.MeetingCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.place.crud.PlaceCrudService;
import pl.com.devmeet.devmeetcore.domain.place.models.PlaceEntity;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceNotFoundException;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserCrudService;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyActiveException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyExistsEmailDuplicationException;
import pl.com.devmeet.devmeetcore.feature_services.member_and_group.MemberAddNewGroupService;
import pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.crud.RelationsMemberGroupMeetingCrudService;
import pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.exceptions.MemberAddNewMeetingInGroupServiceException;
import pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.exceptions.RelationsMemberGroupMeetingServiceException;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MemberAddNewMeetingInGroupServiceTest {

    @Autowired
    private MemberAddNewMeetingInGroupService memberAddNewMeetingInGroupService;
    @Autowired
    private RelationsMemberGroupMeetingCrudService relationsMemberGroupMeetingCrudService;
    @Autowired
    private MemberAddNewGroupService memberAddNewGroupService;

    @Autowired
    private GroupCrudService groupCrudService;
    @Autowired
    private UserCrudService userCrudService;
    @Autowired
    private MemberCrudService memberCrudService;
    @Autowired
    private PlaceCrudService placeCrudService;
    @Autowired
    private MeetingCrudService meetingCrudService;

    private UserEntity testUser;
    private MemberEntity testMember;
    private GroupEntity testGroup;
    private PlaceEntity testPlace;
    private MeetingEntity newMeeting;

    @Before
    public void setUp() throws Exception {
        this.testUser = UserEntity.builder()
                .email("MemberAddNewMeetingInGroupServiceTestUser@test.pl")
                .password("testPass")
                .isActive(true)
                .build();
        this.testUser = createUserInDB(testUser);

        this.testMember = MemberEntity.builder()
                .user(testUser)
                .nick("simple member")
                .build();
        this.testMember = createMemberInDB(testMember);

        this.testGroup = GroupEntity.builder()
                .description("New group created by member")
                .groupName("Simple group from simple member")
                .website("www.-group,com")
                .build();

        this.testPlace = PlaceEntity.builder()
                .placeName("FOCUS")
                .description("Centrum konferencyjne FOCUS - budynek z drzewem na piętrze")
                .website("http://www.budynekfocus.com/pl")
                .location("Aleja Armii Ludowej 26, 00-609 Warszawa")
                .build();
        this.testPlace = createPlaceInDB(testPlace);

        this.newMeeting = MeetingEntity.builder()
                .promoter(testMember)
                .group(testGroup)
                .place(testPlace)
                .meetingName("test meeting")
//                .beginDate(LocalDate.now())
//                .beginTime(LocalTime.now())
//                .endDate(LocalDate.now())
//                .endTime(LocalTime.now().plusHours(2))
                .begin(LocalDateTime.now())
                .end(LocalDateTime.now().plusHours(2))
                .build();
    }

    @SneakyThrows
    private UserEntity createUserInDB(UserEntity user) {
        try {
            return userCrudService
                    .activate(userCrudService
                            .add(user));
        } catch (UserAlreadyActiveException | UserAlreadyExistsEmailDuplicationException e) {
            return userCrudService.findByEmail(user.getEmail());
        }
    }

    @SneakyThrows
    private MemberEntity createMemberInDB(MemberEntity member) {
        try {
            return memberCrudService.add(member);
        } catch (MemberAlreadyExistsException e) {
            return memberCrudService.findByUser(member.getUser());
        }
    }

    @SneakyThrows
    private GroupEntity memberCreateGroupInDB(MemberEntity member, GroupEntity group) {
        try {
            return memberAddNewGroupService.memberCreateNewGroup(member.getId(), group);
        } catch (GroupAlreadyExistsException e) {
            return groupCrudService.findByGroup(group);
        }
    }

    @SneakyThrows
    private PlaceEntity createPlaceInDB(PlaceEntity place) {
        try {
            return placeCrudService.add(place);
        } catch (PlaceAlreadyExistsException e) {
            return placeCrudService.findPlaceByIdOrFeatures(place);
        }
    }

    private MeetingEntity createMeetingInDbWithPromoter(MeetingEntity meeting) throws GroupNotFoundException, PlaceNotFoundException, MemberNotFoundException, MemberAddNewMeetingInGroupServiceException, MeetingNotFoundException {
        try {
            return memberAddNewMeetingInGroupService.addNewMeetingInGroup(meeting);
        } catch (MeetingAlreadyExistsException e) {
            return meetingCrudService.findByIdOrFeatures(meeting);
        }
    }

    @Test
    public void WHEN_Member_adds_Meeting_in_Group_it_becomes_the_promoter_and_first_member_THEN_return_Meeting() {
        first_case_checkIsANonMemberMemberCanAddMeetingInTheGroup__CANT(newMeeting);
        second_case_checkIsMemberOfTheGroupCanAddNewMeetingInTheGroup__CAN(newMeeting);
    }

    private void first_case_checkIsANonMemberMemberCanAddMeetingInTheGroup__CANT(MeetingEntity meeting) {
        try {
            createMeetingInDbWithPromoter(meeting);
            Assert.fail();
        } catch (MeetingNotFoundException | GroupNotFoundException | PlaceNotFoundException | MemberNotFoundException e) {
            Assert.fail();
        } catch (MemberAddNewMeetingInGroupServiceException e) {
            assertThat(e.getMessage())
                    .isNotBlank();
        }
    }

    private void second_case_checkIsMemberOfTheGroupCanAddNewMeetingInTheGroup__CAN(MeetingEntity meeting) {
        try {
            this.testGroup = memberCreateGroupInDB(testMember, testGroup);
            this.newMeeting = createMeetingInDbWithPromoter(meeting);
            GROUP_PERSPECTIVE_WHEN_first_Meeting_has_been_added_to_Group_THEN_somebody_can_find_this_Meeting_in_Group_USING_RelationsMemberGroupMeetingService();
            MEMBER_AS_PROMOTER_PERSPECTIVE__WHEN_Member_add_Meeting_to_Group_THEN_Member_became_promoter_and_can_find_this_Meeting_in_his_promoters_meetings_USING_RelationsMemberGroupMeetingService();
            MEETING_PERSPECTIVE__find_all_members_who_have_signed_up_for_the_meeting_USING_RelationsMemberGroupMeetingService();

        } catch (GroupNotFoundException | PlaceNotFoundException | MemberNotFoundException | MemberAddNewMeetingInGroupServiceException | MeetingNotFoundException e) {
            Assert.fail();
        }
    }

    @SneakyThrows
    private void GROUP_PERSPECTIVE_WHEN_first_Meeting_has_been_added_to_Group_THEN_somebody_can_find_this_Meeting_in_Group_USING_RelationsMemberGroupMeetingService() {
        List<MeetingEntity> allMeetingsInTheGroup = relationsMemberGroupMeetingCrudService.findAllMeetingsInGroup(testGroup);
        assertThat(allMeetingsInTheGroup.size()).isEqualTo(1);
        assertThat(allMeetingsInTheGroup.get(0).getPromoter().getId()).isEqualTo(testMember.getId());
        assertThat(allMeetingsInTheGroup.get(0).getGroup().getId()).isEqualTo(testGroup.getId());
        assertThat(allMeetingsInTheGroup.get(0).getPlace().getId()).isEqualTo(testPlace.getId());
    }

    @SneakyThrows
    private void MEMBER_AS_PROMOTER_PERSPECTIVE__WHEN_Member_add_Meeting_to_Group_THEN_Member_became_promoter_and_can_find_this_Meeting_in_his_promoters_meetings_USING_RelationsMemberGroupMeetingService() {
        List<MeetingEntity> allPromotersMeetings = find_all_meetings_where_member_is_a_promoter_and_it_should_be_1();
        List<MeetingEntity> allMembersMeetings = find_all_member_meetings_and_it_should_be_1();

        assertThat(allPromotersMeetings.size()).isEqualTo(1);
        assertThat(allPromotersMeetings.get(0).getPromoter().getId()).isEqualTo(testMember.getId());
        assertThat(allMembersMeetings.size()).isEqualTo(1);
    }

    private List<MeetingEntity> find_all_meetings_where_member_is_a_promoter_and_it_should_be_1() throws RelationsMemberGroupMeetingServiceException {
        return relationsMemberGroupMeetingCrudService.findAllMeetingsWhereMemberIsAPromoter(testMember);
    }

    private List<MeetingEntity> find_all_member_meetings_and_it_should_be_1() throws RelationsMemberGroupMeetingServiceException {
        return relationsMemberGroupMeetingCrudService.findAllMeetingWhereIsMember(testMember);
    }

    @SneakyThrows
    private void MEETING_PERSPECTIVE__find_all_members_who_have_signed_up_for_the_meeting_USING_RelationsMemberGroupMeetingService() {
        List<MemberEntity> meetingMembers = relationsMemberGroupMeetingCrudService.findAllMeetingMembers(newMeeting);
        assertThat(meetingMembers.size()).isEqualTo(1);
    }
}