package pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group;

import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.crud.GroupCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.models.GroupEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.group.status_and_exceptions.GroupAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.crud.MeetingCrudService;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.models.MeetingEntity;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.group_associated.meeting.status_and_exceptions.MeetingNotFoundException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.crud.MemberCrudService;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.models.MemberEntity;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.member_associated.member.status_and_exceptions.MemberNotFoundException;
import pl.com.devmeet.devmeetcore.domain.place.crud.PlaceCrudService;
import pl.com.devmeet.devmeetcore.domain.place.models.PlaceEntity;
import pl.com.devmeet.devmeetcore.domain.place.status_and_exceptions.PlaceAlreadyExistsException;
import pl.com.devmeet.devmeetcore.domain.user.crud.UserCrudService;
import pl.com.devmeet.devmeetcore.domain.user.models.UserEntity;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyActiveException;
import pl.com.devmeet.devmeetcore.domain.user.status_and_exceptions.UserAlreadyExistsEmailDuplicationException;
import pl.com.devmeet.devmeetcore.feature_services.member_and_group.MemberAddNewGroupService;
import pl.com.devmeet.devmeetcore.feature_services.member_and_group.MemberAddToExistingGroupService;
import pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.crud.RelationsMemberGroupMeetingCrudService;
import pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.exceptions.MemberAddToExistingMeetingInGroupServiceException;
import pl.com.devmeet.devmeetcore.feature_services.member_and_meeting_in_group.exceptions.RelationsMemberGroupMeetingServiceException;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MemberAddToExistingMeetingInGroupServiceTest {

    @Autowired
    private MemberAddNewMeetingInGroupService memberAddNewMeetingInGroupService;
    @Autowired
    private RelationsMemberGroupMeetingCrudService relationsMemberGroupMeetingCrudService;
    @Autowired
    private MemberAddNewGroupService memberAddNewGroupService;
    @Autowired
    private MemberAddToExistingGroupService memberAddToExistingGroupService;
    @Autowired
    private MemberAddToExistingMeetingInGroupService memberAddToExistingMeetingInGroupService;

    @Autowired
    private GroupCrudService groupCrudService;
    @Autowired
    private UserCrudService userCrudService;
    @Autowired
    private MemberCrudService memberCrudService;
    @Autowired
    private PlaceCrudService placeCrudService;
    @Autowired
    private MeetingCrudService meetingCrudService;

    private UserEntity oldUser;
    private MemberEntity existingMember;
    private UserEntity testUser;
    private MemberEntity testMember;
    private GroupEntity testGroup;
    private PlaceEntity testPlace;
    private MeetingEntity testMeeting;

    @Before
    public void setUp() throws Exception {
        this.oldUser = UserEntity.builder()
                .email("oldUserExistingInMeeting@test.pl")
                .password("testPass")
                .isActive(true)
                .build();
        this.testUser = UserEntity.builder()
                .email("AddMemberToExistingMeetingInGroupServiceTestUser@test.pl")
                .password("testPass")
                .isActive(true)
                .build();
        this.oldUser = createUserInDB(oldUser);
        this.testUser = createUserInDB(testUser);

        this.existingMember = MemberEntity.builder()
                .user(oldUser)
                .nick("old member")
                .build();
        this.testMember = MemberEntity.builder()
                .user(testUser)
                .nick("TestMemberToAddToExistingMeeting")
                .build();
        this.existingMember = createMemberInDB(existingMember);
        this.testMember = createMemberInDB(testMember);

        this.testGroup = GroupEntity.builder()
                .description("New group created by member2")
                .groupName("Simple group from simple member2")
                .website("www.-group,com")
                .build();
        this.testGroup = createGroupInDB(testGroup, existingMember);

        this.testPlace = PlaceEntity.builder()
                .placeName("Biblioteka Główna WAT")
                .description("Biblioteka Główna Wojskowej Akademii Technicznej")
                .website("https://www.bg.wat.edu.pl/")
                .location("ul. gen. Sylwestra Kaliskiego 19, 00-908 Warszawa")
                .build();
        this.testPlace = createPlaceInDB();

        this.testMeeting = MeetingEntity.builder()
                .promoter(existingMember)
                .group(testGroup)
                .place(testPlace)
                .meetingName("test meeting")
                .begin(LocalDateTime.now())
                .end(LocalDateTime.now().plusHours(2))
//                .beginDate(LocalDate.now())
//                .beginTime(LocalTime.now())
//                .endDate(LocalDate.now())
//                .endTime(LocalTime.now().plusHours(2))
                .build();
        this.testMeeting = createMeetingInDbWithPromoter(testMeeting);
    }

    @SneakyThrows
    private UserEntity createUserInDB(UserEntity user) {
        try {
            return userCrudService
                    .activate(userCrudService
                            .add(user));
        } catch (UserAlreadyActiveException | UserAlreadyExistsEmailDuplicationException e) {
            return userCrudService.findByEmail(user.getEmail());
        }
    }

    @SneakyThrows
    private MemberEntity createMemberInDB(MemberEntity member) {
        try {
            return memberCrudService.add(member);
        } catch (MemberAlreadyExistsException e) {
            return memberCrudService.findByUser(member.getUser());
        }
    }

    @SneakyThrows
    private GroupEntity createGroupInDB(GroupEntity group, MemberEntity member) {
        try {
            return memberAddNewGroupService.memberCreateNewGroup(member.getId(), group);
        } catch (GroupAlreadyExistsException e) {
            return groupCrudService.findByGroup(group);
        }
    }

    @SneakyThrows
    private void addMemberToExistingGroup(GroupEntity group, MemberEntity member) {
        memberAddToExistingGroupService.addMemberToExistingGroup(group.getId(), member.getId());
    }

    @SneakyThrows
    private PlaceEntity createPlaceInDB() {
        try {
            return placeCrudService.add(testPlace);
        } catch (PlaceAlreadyExistsException e) {
            return placeCrudService.findPlaceByIdOrFeatures(testPlace);
        }
    }

    @SneakyThrows
    private MeetingEntity createMeetingInDbWithPromoter(MeetingEntity meeting) {
        try {
            return memberAddNewMeetingInGroupService.addNewMeetingInGroup(meeting);
        } catch (MeetingAlreadyExistsException e) {
            return meetingCrudService.findByIdOrFeatures(meeting);
        }
    }

    @SneakyThrows
    public void addTestMemberToExistingGroup(GroupEntity group, MemberEntity member) {
        memberAddToExistingGroupService
                .addMemberToExistingGroup(group.getId(), member.getId());
    }


    public MeetingEntity addMemberToExistingMeetingInGroup(MemberEntity member) throws RelationsMemberGroupMeetingServiceException, MeetingNotFoundException, MemberAddToExistingMeetingInGroupServiceException, MemberNotFoundException {
        return memberAddToExistingMeetingInGroupService
                .addMemberToExistingMeetingInGroup(testMeeting.getId(), member.getId());
    }

    @Test
    public void checkIsMemberHasBeenAddedToMeetingMembers() {
        first_case__ifMemberIsNotMemberOfTheGroup();
        second_case__ifMemberIsAlsoMemberOfTheGroup();
    }

    private void first_case__ifMemberIsNotMemberOfTheGroup(){
        try {
            addMemberToExistingMeetingInGroup(testMember);
            Assert.fail();
        } catch (MeetingNotFoundException | RelationsMemberGroupMeetingServiceException | MemberNotFoundException e) {
            Assert.fail();
        } catch (MemberAddToExistingMeetingInGroupServiceException e) {
            assertThat(e.getMessage()).isNotBlank();
        }
    }

    private void second_case__ifMemberIsAlsoMemberOfTheGroup(){
        try {
            addMemberToExistingGroup(testGroup, testMember);
            addMemberToExistingMeetingInGroup(testMember);

            List<MemberEntity> meetingMembers = relationsMemberGroupMeetingCrudService
                    .findAllMeetingMembers(testMeeting);

            MemberEntity addedMember = meetingMembers.stream()
                    .filter(member -> member.getId().equals(testMember.getId()))
                    .findFirst()
                    .orElseGet(null);

            assertThat(addedMember).isNotNull();
            assertThat(addedMember.getModificationTime()).isNotNull();

        } catch (MemberAddToExistingMeetingInGroupServiceException | MeetingNotFoundException | RelationsMemberGroupMeetingServiceException | MemberNotFoundException e) {
            Assert.fail();
        }
    }
}