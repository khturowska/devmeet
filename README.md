# DEVMEET

Devmeet's main purpose is to facilitate the organization of professional meetings during project implementation.

The project is available under the following link:
https://bitbucket.org/khturowska/devmeet/src/develop/

## TECHNICAL INFO:

Devmeet is a single-page application. 
The project was made as REST API with Front-End created using Vaadin.

**Technologies:**
* Java
* Spring/Spring Boot
* Vaadin
* MariaDB
* Access Google API (OAuth 2.0)
* Google API Calendar
* JUnit
* Maven

## SUMMARY

The App can be useful for professional and freelance Dev teams in their work environment, e.g. to organize weekly SCRUM meetings.
Devmeet helps to select the best available time and place of the meeting depending on the time capabilities of group members, as well as their preferences regarding the location of the meeting.
The time and the place of group's meeting are chosen through voting.
After group members are done voting, the App generates meeting in Google Calendar.
In the next step, Devmeet notifies group members about the planned meeting using their email addresses.

## FEATURES: 

Devmeet Users can:
* sign and log into Devmeet using their Google Account;
* set the time when they are available and places they can attend in order to meet (including remote work);
* create a group of their own;
* become a member of groups of their choice;
* message other members of their group, as well as the group as a whole.


## AUTHORS

Devmeet is an ongoing project based on Kamil Ptasiński's original idea. The project is constantly being developed with new functionalities by a small team of programmers. Some of the project's contributors are professional Java Developers. The team members are:

* Kamil Ptasiński: github.com/pt4q (concept, backend, concept and architecture refactoring, configuration)
* Aneta Wróbel: github.com/Aneta90 (backend)
* Grzegorz Chrząszczyk: github.com/chrzasz (backend, frontend, refactoring, configuration)
* Sebastian Lubiecki: github.com/SebastianLubiecki (concept refactoring)
* Klaudia Turowska: github.com/KlaudiaHe (backend, frontend, concept refactoring)


## How to run Devmeet
#### 1. Clone GIT repository from BitBucket
``git clone https://bitbucket.org/khturowska/devmeet.git `` 
#### 2. Compile project using Maven
The project will be compiled in application production mode.

``mvn clean compile package -Pproduction``
#### 3. Run application using Java 8
In the */target* directory you can find file named **devmeet-core-0.0.1-SNAPSHOT.jar**.

``java -jar devmeet-core-0.0.1-SNAPSHOT.jar``

#### 4. Run application using Docker
If you have **devmeet-core-0.0.1-SNAPSHOT.jar** in the */target* directory, you can build Docker image.
We will build Docker image named *devmeet* with version tag *1*.

``docker build -t devmeet:1 {your path to cloned repository}/devmeet/``

Check Docker images created on your computer using 
``docker images`` command.

Example output. As we see, *devmeet* image is on first possition and it has *b231140f0d9b* ID.

  | REPOSITORY         | TAG                  | IMAGE ID           | CREATED            | SIZE      |
  | ------------------ | -------------------- | ------------------ | ------------------ | --------- |
  | devmeet            | 1                    | b231140f0d9b       | 8 minutes ago      | 176MB     |
  | hello-world        | latest               | bf756fb1ae65       | 5 months ago       | 13.3kB    |
  | openjdk            | 8u191-jdk-alpine3.9  | e9ea51023687       | 15 months ago      | 105MB     |

Next we must run image as the Docker container. We use *--detach* option to run container in background as process.  

``docker run --publish 8000:8080 --detach --name devmeet devmeet:1``

You can check running containers using ``docker ps``

Example output of this command.

| CONTAINER ID       | IMAGE              | COMMAND                 | CREATED            | STATUS             | PORTS                             | NAMES   |
| ------------------ | ------------------ | ----------------------- | ------------------ | ------------------ | --------------------------------- | ------- |
| 28bdeda03e25       | devmeet:1          | "/bin/sh -c 'java -j…"  | 54 minutes ago     | Up 54 minutes      | 8000/tcp, 0.0.0.0:8000->8080/tcp  | devmeet |

In this case Devmeet container has *28bdeda03e25* ID.

Now you can use Devmeet. Open your browser and type in the address bar ``localhost:8000``.  You will see Devmeet main page.

After testing you can stop Docker container with the Devmeet.
 
 ``docker kill 28bdeda03e25``
 
 Images usually need lot of disk space, so good practice is to remove unused images. As your remember devmeet image has *b231140f0d9b* ID.

``docker image rm -f b231140f0d9b``